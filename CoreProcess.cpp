#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <fstream>
#include <mysql.h>

using namespace std;

#include "generalfunction.h"
#include "PrepaidServer.h"
#include "Transfer.h"
#include "Request.h"
#include "FileLogger.h"
#include "CoreProcess.h"
#include "MTMessage.h"
#include "BusinessRules.h"
#include "MysqlConnDB.h"
#include "IN_Conn.h"
#include "Post_Conn.h"
#include "date.h"

using namespace ats;

CoreProcess::CoreProcess(string sender, string mo, string oadc, string txnid, PortReg portIN, BusinessRules biz, MTMessage mt,short int smsc_type) {
	phone = sender;
	sms = mo;
	shortcode = oadc;
	tx_id = txnid;
	inPort = portIN;
	mtLib = mt;
	bisnesRulesObject = biz;
	channel = 0;
	smsc_id = smsc_type;
}

short int CoreProcess::QueryProfileIN(Subscriber &userProfile,FileLogger debugLogger) {
	struct IN_UserProfile ProfileResultA;
	IN_Conn profileINA;
	string expiryDt;
	string activeDt;

	ProfileResultA = profileINA.QueryProfile(userProfile.getPhone(),inPort,tx_id);
	if (ProfileResultA.queryStatus==0) {
		debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN success, lang:"+intToString(ProfileResultA.userLang)+",bal:"+floatToString(ProfileResultA.userBalance)+",act:"+ProfileResultA.activationDate+",stat:"+intToString(ProfileResultA.userStatus)+",brand:"+intToString(ProfileResultA.brand)+",exp:"+ProfileResultA.expiryDate+"]");
		userLang = ProfileResultA.userLang;
		userProfile.setLang(userLang);
		userProfile.setBalance(ProfileResultA.userBalance);
		activeDt = ProfileResultA.activationDate.substr(0,4) + ProfileResultA.activationDate.substr(5,2) + ProfileResultA.activationDate.substr(8,2);
		userProfile.setActivationDate(activeDt);
		userProfile.setType(ProfileResultA.userType);
		userProfile.setINType(ProfileResultA.userPlatform);
		userProfile.setINStatus(ProfileResultA.userStatus);
		userProfile.setBrand(ProfileResultA.brand);
		expiryDt = ProfileResultA.expiryDate.substr(0,4) + ProfileResultA.expiryDate.substr(5,2) + ProfileResultA.expiryDate.substr(8,2);
		userProfile.setExpiry(expiryDt);
		//cout << " expiryDt:" << expiryDt;
		//check if user is in grace period
		if (expiryDt < currentDateLimit()) {
			struct tm tmNow, tmExpired;
			string expiryTime = expiryDt.substr(0,4) + expiryDt.substr(4,2) + expiryDt.substr(6,2) + "000000";
			string currentTime = currentDateLimit() + "000000";
			strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
			strptime(stringToChar(expiryTime), "%Y%m%d%H%M%S",&tmExpired);
			double secDiff = difftime(mktime(&tmNow),mktime(&tmExpired));
			int timeDiff=0;
			int gracePeriod = bisnesRulesObject.grc_prd;
			//cout << " grace:" << gracePeriod;
			int gracePeriodInSec = gracePeriod * 24 * 60 * 60;
			//cout << " diff:" << int(secDiff);
			if (secDiff < gracePeriodInSec) {
				userProfile.setGracePeriod(true);
			} else {
				userProfile.setGracePeriod(false);
			}
		} else {
			userProfile.setGracePeriod(false);
		}
		return 0;
	} else if (ProfileResultA.queryStatus==7) {
		//profile not found, query from postpaid
		debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed, response : User  Not Found]");
		struct Post_UserProfile PostProfileResultA;
		Post_Conn postConn;
		PostProfileResultA = postConn.QueryProfile(userProfile.getPhone(),inPort,tx_id);
		if (PostProfileResultA.queryStatus==0) {
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from PG success][usertype:"+intToString(PostProfileResultA.userType)+", accountno:"+PostProfileResultA.act_no+", actdate:"+PostProfileResultA.activationDate+", status:"+intToString(PostProfileResultA.userStatus)+", creditlmit:"+floatToString(PostProfileResultA.credit_limit)+", totaldue:"+floatToString(PostProfileResultA.total_due)+", currentdue:"+floatToString(PostProfileResultA.current_due)+", unbill/current_usage:"+floatToString(PostProfileResultA.unbill)+", lastinvoice:"+PostProfileResultA.last_invoice_date+", LOS:"+intToString(PostProfileResultA.los)+"]");
			/*cout << "--------------------------" << endl;
			cout << "Query Response:" << endl;
			cout << "Act Date:" << PostProfileResultA.activationDate << endl;
			cout << "Acct No:" << PostProfileResultA.act_no << endl;
			cout << "UserType:" << PostProfileResultA.userType << endl;
			cout << "Status:" << PostProfileResultA.userStatus << endl;
			cout << "CreditLimit:" << PostProfileResultA.credit_limit << endl;
			cout << "TotalDue:" << PostProfileResultA.total_due << endl;
			cout << "lastInvoice:" << PostProfileResultA.last_invoice_date << endl;*/
			//activeDt = PostProfileResultA.activationDate.substr(0,4) + PostProfileResultA.activationDate.substr(5,2) + PostProfileResultA.activationDate.substr(8,2);
			//userProfile.setActivationDate(activeDt);
			userProfile.setType(PostProfileResultA.userType);
			userProfile.setAccountNo(PostProfileResultA.act_no);
			userProfile.setINStatus(PostProfileResultA.userStatus);
			userProfile.setCreditLimit(PostProfileResultA.credit_limit/100);
			userProfile.setCurrentUsage(PostProfileResultA.unbill/100);
			userProfile.setCurrentDue(PostProfileResultA.current_due/100);
			//userProfile.setCurrentUsage(299);
			//userProfile.setTotalDue(PostProfileResultA.total_due/100);
			userProfile.setTotalDue(PostProfileResultA.total_due/100);
			userProfile.setLOS(PostProfileResultA.los);

			string stmtDt = PostProfileResultA.last_invoice_date.substr(0,4) + PostProfileResultA.last_invoice_date.substr(5,2) + PostProfileResultA.last_invoice_date.substr(8,2);
			if (userProfile.getStatementDate()!=stmtDt) {
				userProfile.setMonthlyLimit(0);
				userProfile.setStatementDate(stmtDt);
			}
			//userProfile.setLOS(PostProfileResultA.los);
			//userProfile.setLOS(10);
			return 0;
		} else {
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from PG failed!, response:"+intToString(PostProfileResultA.queryStatus)+"]");
			cout << "Query from PG fail! query status:" << PostProfileResultA.queryStatus << endl;
			return PostProfileResultA.queryStatus;
		}
	} else {
		return ProfileResultA.queryStatus;
	}
}

//change each element of the string to upper case
string StripCtrlChar(string strToConvert) {
	string outStr = strToConvert;
	for(unsigned int i=0;i<strToConvert.length();i++)
		   {
			if (iscntrl(strToConvert[i])) {
				outStr.replace(i,1," ");
				//cout << strToConvert[i] << " is ctrn char ";
			}
			//cout << " str:" << outStr;
		   }
	return outStr;//return the converted string
}


MTCollection CoreProcess::ProcessMessage() {
	char ch ;
    string keyword;
    vector<string> smsVector;
    string pwd="";
    string newpwd="";
    int amt;
    string amtString="";
    string requestorName;
    string receiverPhone="";
	short int operation_type=0;
	int statusCode;
	Subscriber profileA;
	IN_Conn profileINA;
	short int userStat;
	short int whiteListStat;
	string resultDB;
	string newPwd;
	string currentPwd;
	string motherName;
	string expiryDt;
	string activeDt;
	int yr,mth,dt,y2,m2,d2;
	long dt1,dt2,dt3;
	short int dbstat;
	short int langDB;
	langDB=1;

	sms = StringToUpper(sms);
	sms = StripCtrlChar(sms);

	//convert new lines with space
	size_t found;
	int startAt;
	bool notFound=false;
	while (notFound==false) {
		found = sms.find("\n");
		if (found!=string::npos) {
			startAt= int(found);
			sms.replace(startAt,1," ");
		} else {
			notFound=true;
		}
	}
	smsVector = Tokenize(sms," ");

	string smsOut1;
	string smsOut2;

	//debug log
	string logFile = mtLib.dir_log+"/debug."+currentDate()+".log";
	//cout << "logfile:" << logFile;
	FileLogger debugLogger (logFile);
	//log for ATS transaction logs
	string logATStx = mtLib.dir_ats_tx+"/"+currentDate()+".tx";
	FileLogger txATSLogger (logATStx);
	//log for ATR transaction logs
	string logATRtx = mtLib.dir_atr_tx+"/"+currentDate()+".tx";
	FileLogger txATRLogger (logATRtx);
	//log for HELP transaction logs
	string logHelptx = mtLib.dir_hlp_tx+"/"+currentDate()+".tx";
	FileLogger txHelpLogger (logHelptx);

    debugLogger.logDebug("[Enter Core Proces] [TX ID:"+tx_id+"] [PHONE:"+phone+"] [SMS:"+sms+"] [SHORTCODE:"+shortcode+"] [SMSC:"+intToString(smsc_id)+"]");

    //do some filtering to reduce use of resources
	if (sms=="") {
		return MTMsg;
	}

    //cout << "---------Check Keyword-------------";
    debugLogger.logDebug("[TX ID:"+tx_id+"] [Check Keyword] [PHONE:"+phone+"]");
    for( int i = 0; i < smsVector.size(); i++ ) {
    	if (i==0) {
    		if (smsVector.at(i).compare("HELP")==0) {
    			//first word is HELP
    			operation_type=3;
    		} else if (smsVector.at(i).compare("CPWD")==0) {
    			//first word is CPWD (change password)
    			operation_type=7;
    		} else if (smsVector.at(i).compare("FPWD")==0) {
    			//first word is FPWD (forgot password)
    			operation_type=8;
    		} else if (smsVector.at(i).compare("CAT")==0) {
    			//first word is CAT
    			operation_type=4;
    		} else if (smsVector.at(i).compare("RM")==0) {
    			//First Word is RM
    			operation_type=1;
    		} else if (smsVector.at(i).compare("RQ")==0) {
    			//First Word is RQ
    			operation_type=2;
    		} else if (smsVector.at(i).compare("YA")==0 || smsVector.at(i).compare("YES")==0 || smsVector.at(i).compare("Y")==0 ) {
    			//ATR approval First Word is YA / YES / Y
    			operation_type=5;
    		} else if (smsVector.at(i).compare("TIDAK")==0 || smsVector.at(i).compare("NO")==0 || smsVector.at(i).compare("N")==0 ) {
    			//ATR approval First Word is TIDAK / NO / N
    			operation_type=6;
    		} else if (smsVector.at(i).compare(0,2,"RM")==0) {
    			//Substring First Word is RM
    			operation_type=1;
    			amtString = smsVector.at(i).substr(2);
    		} else if (smsVector.at(i).compare(0,2,"RQ")==0) {
    			//Substring First Word is RQ
    			operation_type=2;
    			amtString = smsVector.at(i).substr(2);
    		} else if (smsVector.at(i).compare("OK")==0) {
    			//ATS approval
    			operation_type=9;
    		} else if (smsVector.at(i).compare("X")==0) {
    			//ATS approval
    			operation_type=10;
    		} else if (smsVector.at(i).compare("POINT")==0) {
				//check loyalty point
				operation_type=11;
    		} else {
    			//First Word is amount
    			operation_type=1;
    			amtString = smsVector.at(i);
    			//do some filtering to reduce use of resources
    			if (chkAmtFormat(amtString)==false) {
    				debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [amt:"+amtString+"] [Error:Invalid syntax, amount is not valid number]");
			    	txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|20|"+currentDateTime3()+"|"+tx_id);
			    	//Invalid Syntax
			    	MTMsg.smsMT[0].msg = mtLib.getMsg(20,1);
			    	MTMsg.smsMT[0].receiver = phone.substr(1);
			    	MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
			    	return MTMsg;
    			}
    		}
    	} else if (i==1) {
    		if (smsVector.at(i).compare("RM")==0) {
    			//second word is RM, do nothing
    		} else if (smsVector.at(i).compare(0,2,"RM")==0) {
    			//substring second Word is RM
    			amtString = smsVector.at(i).substr(2);
    		} else if (operation_type==4) {
    			//second word is password
    			pwd = smsVector.at(i);
    		} else if (operation_type==5) {
    			//second Word is pwd
    			pwd = smsVector.at(i);
    		} else if (operation_type==7) {
    			//second word is current pwd
    			currentPwd = smsVector.at(i);
    		} else if (operation_type==8) {
    			//second word is mother's name
    			motherName = smsVector.at(i);
			} else if (operation_type==2) {
				if (amtString=="") {
					//second word is amount
					amtString = smsVector.at(i);
				} else {
					//second word is name
					requestorName = smsVector.at(i);
				}
    		} else if (amtString=="") {
    			//second Word is amount
    			amtString = smsVector.at(i);
    		} else {
    			//second Word is pwd
    			pwd = smsVector.at(i);
    		}
    	} else if (i==2) {
    		if (operation_type==4) {
    			//thirs word is mother's name
    			motherName = smsVector.at(i);
    		} else if (operation_type==7) {
    			//third word is new password
    			newPwd = smsVector.at(i);
    		} else if (operation_type==2) {
    			if (amtString=="") {
					//second word is amount
					amtString = smsVector.at(i);
				} else {
					//second word is name
					requestorName = smsVector.at(i);
				}
    		} else if (amtString=="") {
    			//third word is amt
    			amtString = smsVector.at(i);
    		} else if (pwd=="") {
    			//third Word is pwd
    			pwd = smsVector.at(i);
        	}
    	}
    }

    debugLogger.logDebug("[TX ID:"+tx_id+"][CoreProcess-Finish Check Keyword][AMT:"+amtString+"]");
    profileA.setPhone(phone);
    receiverPhone = "6" + shortcode.substr(bisnesRulesObject.shr_len);
    amt = atoi((const char *)amtString.c_str());
    string helpShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.hlp_ptx;

    //do some filtering to reduce use of resources
	if (sms=="") {
		return MTMsg;
	} else if (operation_type==2) {
		amt = stringToInt(amtString);
		if (!chkAmtFormat(amtString) || amt==0) {
			txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|75|"+currentDateTime3()+"|"+tx_id);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [invalid amount to request]");
    		string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = catShortcode;
    		MTMsg.smsMT[0].msg = mtLib.getMsg(75,1);
			return MTMsg;
		}
	}

    debugLogger.logDebug("[TX ID:"+tx_id+"] [CoreProcess-Attempt to chk syntax][Operation Type:"+intToString(operation_type)+"][PHONE:"+phone+"] [AMT:"+amtString+"] [PWD:"+pwd+"] [RECEIVER:"+receiverPhone+"]");

    MysqlConnDB mysqlObject;
    if (mysqlObject.OpenConnDB()) {
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [Core Process] [Connection successfull]");
    	//get profile A
    	dbstat = getProfileDB(profileA,mysqlObject.myData,debugLogger);
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [Core Process] [Get Profile A frm DB][pwd:"+profileA.getPwd()+" type:"+intToString(profileA.getType())+" app_status:"+intToString(profileA.getAppStatus())+" whitelist:"+intToString(profileA.getWhiteListStat())+"]");
    	if (dbstat==1)
    		userLang=1;
    	else if (profileA.getLang()==1 || profileA.getLang()==2)
    		userLang = profileA.getLang();
    	else
    		userLang=1;
		langDB = userLang;
    	profileA.setLang(userLang);
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [get profile A, language for A:"+intToString(userLang)+"]");
    } else {
    	//Cannot connect to database
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [Core Process] [Error:Cannot connect to database]");
    	if (operation_type==1 || operation_type==9 || operation_type==10)
    		txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|8|"+currentDateTime3()+"|"+tx_id);
    	else if (operation_type==2 || operation_type==5 || operation_type==6)
    		txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|8|"+currentDateTime3()+"|"+tx_id);
    	else
    		txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|8|"+currentDateTime3()+"|"+tx_id);
    	mysql_close(mysqlObject.myData);
    	MTMsg.smsMT[0].msg = mtLib.getMsg(8,1);
 		MTMsg.smsMT[0].receiver = phone.substr(1);
 		MTMsg.smsMT[0].oadc = helpShortcode;
 		MTMsg.smsMT[0].charging = 0;
    	return MTMsg;
     }

    if (shortcode.compare(helpShortcode)==0 || operation_type==3) {
    	//HELP menu
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=HELP] [User Language:"+intToString(userLang)+"]");
    	trimString(sms);
    	if (QueryProfileIN(profileA,debugLogger)!=0) {
    		//cannot query from IN, stop process
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
			txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
			MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
			MTMsg.smsMT[0].receiver = phone.substr(1);
			MTMsg.smsMT[0].oadc = helpShortcode;
			MTMsg.smsMT[0].charging = 0;
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else {
	    	if (sms.compare("HELP")==0) {
	    		//help main menu
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(100,userLang, profileA.getBrand());
	    		statusCode=100;
	    	} else if (sms.compare("1")==0) {
	    		//menu 1
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(101,userLang);
	    		statusCode=101;
	    	} else if (sms.compare("2")==0) {
	    		//menu 2
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(102,userLang);
	    		statusCode=102;
	    	} else if (sms.compare("3")==0) {
	    		//menu 3
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(103,userLang);
	    		statusCode=103;
		    } else if (sms.compare("4")==0) {
		    	//menu 4
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(104,userLang);
	    		statusCode=104;
		    } else if (sms.compare("5")==0) {
		    	//menu 5
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(105,userLang);
	    		statusCode=105;
		    } else if (sms.compare("6")==0) {
				//menu 6
				smsOut1 = mtLib.getMsg(107,userLang);
			    smsOut1 = find_replace(smsOut1,"<pnt>",intToString(profileA.getLoyaltyPoint()));
				MTMsg.smsMT[0].msg = smsOut1;
	    		statusCode=107;
		    } else {
		    	//invalid keyword
		    	MTMsg.smsMT[0].msg = mtLib.getMsg(106,userLang);
		    	statusCode=106;
		    }
	    	txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = helpShortcode;
	    	MTMsg.smsMT[0].charging = 15;
	    	mysql_close(mysqlObject.myData);
	    	return MTMsg;
    	}
    } else if (operation_type==11) {
		//check loyalty point
		txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|107|"+currentDateTime3()+"|"+tx_id);
		smsOut1 = mtLib.getMsg(108,userLang);
		smsOut1 = find_replace(smsOut1,"<pnt>",intToString(profileA.getLoyaltyPoint()));
		MTMsg.smsMT[0].msg = smsOut1;
		MTMsg.smsMT[0].receiver = phone.substr(1);
		MTMsg.smsMT[0].oadc = helpShortcode;
		mysql_close(mysqlObject.myData);
    	return MTMsg;
    } else if (operation_type==4) {
    	//CAT registration process
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=REGISTRATION]");
    	string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
    	MTMsg.smsMT[0].receiver = phone.substr(1);
    	MTMsg.smsMT[0].oadc = catShortcode;
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [Register] [User Language:"+intToString(userLang)+"]");
    	string regPwd = pwd;
    	string regMotherName = motherName;
    	if (regPwd=="" || motherName=="") {
    		//invalid format of registration sms
    		statusCode=110;
    		txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
    		MTMsg.smsMT[0].msg = mtLib.getMsg(statusCode,userLang);
    		mysql_close(mysqlObject.myData);
    		return MTMsg;
    	} else {
    		//check password format
    		if (chkPinFormat(regPwd)==false  || chkPinFormatIsNumeric(regPwd)==false) {
    			statusCode=110;
    			txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
    			MTMsg.smsMT[0].msg = mtLib.getMsg(statusCode,userLang);
    			mysql_close(mysqlObject.myData);
    			return MTMsg;
    		} else {
    			//check if user is registered
    			userStat = getUserStatus(phone, mysqlObject.myData, debugLogger);
    			if (userStat==0 || userStat==1 || userStat==9) {
    				if (QueryProfileIN(profileA,debugLogger)!=0) {
	    				//cannot query from IN, stop process
						debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
						txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
						MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
						MTMsg.smsMT[0].receiver = phone.substr(1);
						MTMsg.smsMT[0].oadc = helpShortcode;
						MTMsg.smsMT[0].charging = 0;
						mysql_close(mysqlObject.myData);
						return MTMsg;
    				} else {
	    				//register user in DB
	    				profileA.setPhone(phone);
	    				profileA.setLang(userLang);
	    				profileA.setPwd(regPwd);
	    				profileA.setAppSatus(0);
	    				profileA.setMotherName(regMotherName);
	    				short int regResult;
	    				if (userStat==9) {
	    					//new user
	    					debugLogger.logDebug("[TX ID:"+tx_id+"] [Register New User]");
	    					regResult = registerUserProfile(profileA, tx_id, mysqlObject.myData, debugLogger);
	    				} else {
	    					//pre-registered user
	    					debugLogger.logDebug("[TX ID:"+tx_id+"] [Register Pre-Registered User]");
	    					regResult = updateUserProfile(profileA, tx_id, mysqlObject.myData, debugLogger);
	    				}
	    				if (regResult==0) {
	    					//registration successful
	    					//send 2 msg to user
	    					statusCode=112;
	    					txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
	    					MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[1];
	    					MTMsg.smsMT[0].msg = mtLib.getMsg(112,userLang, profileA.getBrand());
	    					MTMsg.smsMT[0].charging = 15;
	    					MTMsg.smsMT[1].receiver = phone.substr(1);
	    					MTMsg.smsMT[1].oadc = catShortcode;
	    					MTMsg.smsMT[1].msg = mtLib.getMsg(113,userLang);
						//write into ussd cdr
	    					string logUssd = mtLib.dir_ussd_tx+"/reg.cdr";
						FileLogger ussdCdr (logUssd);
						ussdCdr.logData(phone.substr(1)+ "|"+regPwd+"|"+regMotherName+"|"+currentDateTime2()+"|"+tx_id);
			    			mysql_close(mysqlObject.myData);
			    			return MTMsg;
	    				} else {
	    					//registration unsuccessful, error in db/sql
	    					statusCode=5;
	    					txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
	    					MTMsg.smsMT[0].msg = mtLib.getMsg(statusCode,userLang);
			    			mysql_close(mysqlObject.myData);
			    			return MTMsg;
	    				}
    				}
    			} else if (userStat==2) {
    				//user already registered
    				statusCode=111;
    				txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
    				MTMsg.smsMT[0].msg = mtLib.getMsg(statusCode,userLang);
	    			mysql_close(mysqlObject.myData);
	    			return MTMsg;
    			}
    		}
    	}
    } else if (operation_type==7) {
    	//operation is CHANGE PASSWORD
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD]");
    	if (currentPwd=="" || newPwd=="") {
    		//syntax error
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Syntax Error]");
    		MTMsg.smsMT[0].msg = mtLib.getMsg(120,userLang);
    		statusCode=120;
    	} else if (chkPinFormat(newPwd)==false  || chkPinFormatIsNumeric(newPwd)==false) {
    		//invalid format of new password
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Invalid format of new password]");
    		MTMsg.smsMT[0].msg = mtLib.getMsg(122,userLang);
    		statusCode=122;
    	} else {
	    	short int chkResult = chkUserPwd(phone, currentPwd, mysqlObject.myData, debugLogger);
	    	if (chkResult==0) {
			if (QueryProfileIN(profileA,debugLogger)!=0) {
				//cannot query from IN, stop process
				debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
				MTMsg.smsMT[0].msg = mtLib.getMsg(7,userLang);
				statusCode=7;
    			} else {
				if (profileA.getBalance()>=(bisnesRulesObject.chg_pwd_chg/100)) {
					//make sure balance is sufficient to change password
		    			//user found and password correct
		    			short int updateResult = changeUserPwd(phone, newPwd, mysqlObject.myData, debugLogger);
		    			if (updateResult==0) {
			    			//update success
	    					debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Password change success]");
						smsOut1 = mtLib.getMsg(125,langDB);
			    			smsOut1 = find_replace(smsOut1,"<pwd>",newPwd);
						debugLogger.logDebug("[TX ID:"+tx_id+"] [Get Msg from config, lang:"+intToString(langDB)+" msg:"+smsOut1+"]");
			    			MTMsg.smsMT[0].msg = smsOut1;
						//write into ussd cdr
						string logUssd = mtLib.dir_ussd_tx+"/pwd.cdr";
						FileLogger ussdCdr (logUssd);
						ussdCdr.logData(phone.substr(1)+ "|"+newPwd+"|"+currentDateTime2()+"|"+tx_id);
			    			statusCode=125;
	    				} else {
			    			//error in db/sql
	    					debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Error in db & sql]");
			    			MTMsg.smsMT[0].msg = mtLib.getMsg(5,langDB);
			    			statusCode=5;
	    				}
				} else {
					//insufficient balance to change password
					debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Insufficient balance to change password]");
					MTMsg.smsMT[0].msg = mtLib.getMsg(124,langDB);
					statusCode=124;
				}
			}
	    	} else if (chkResult==3) {
	    		//user found but password wrong
	    		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Password wrong]");
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(121,userLang);
	    		statusCode=121;
	    	} else if (chkResult==1) {
	    		//user not found
	    		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:User not found]");
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(123,userLang,profileA.getBrand());
	    		statusCode=123;
	    	} else {
	    		//error in db/sql
	    		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CHANGE PASSWORD] [Error:Error in db & sql]");
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(5,userLang);
	    		statusCode=5;
	    	}
    	}
    	txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
    	mysql_close(mysqlObject.myData);
    	MTMsg.smsMT[0].receiver = phone.substr(1);
    	MTMsg.smsMT[0].oadc = helpShortcode;
    	if (statusCode==125)
    		MTMsg.smsMT[0].charging = bisnesRulesObject.chg_pwd_chg;
    	else
    		MTMsg.smsMT[0].charging = 0;
    	return MTMsg;
    } else if (operation_type==8) {
    	//operation is FORGOT PASSWORD
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=FORGOT PASSWORD]");
    	if (motherName=="") {
    		//syntax error
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [Mother name is blank]");
    		MTMsg.smsMT[0].msg = mtLib.getMsg(126,userLang);
    		statusCode=126;
    	} else {
	    	short int chkResult = getUserMotherName(phone, motherName, mysqlObject.myData, debugLogger);
	    	debugLogger.logDebug("[TX ID:"+tx_id+"] [Check mother name result="+intToString(chkResult)+"]");
	    	if (chkResult==0) {
	    		//user found and mother name is correct
	    		string userPwd;
	    		short int pwdResult = getUserPwd(phone, userPwd, mysqlObject.myData, debugLogger);
	    		if (pwdResult==0) {
	    			//successfully get password from db
	    			smsOut1 = mtLib.getMsg(130,userLang);
	    			smsOut1 = find_replace(smsOut1,"<pwd>",userPwd);
	    			MTMsg.smsMT[0].msg = smsOut1;
	    			statusCode=130;
	    		} else {
	    			//error while get password from db
	    			MTMsg.smsMT[0].msg = mtLib.getMsg(5,userLang);
	    			statusCode=5;
	    		}
	    	} else if (chkResult==3) {
	    		//user found but mother name wrong
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(128,userLang);
	    		statusCode=128;
	    	} else if (chkResult==1) {
	    		//user not found
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(127,userLang,profileA.getBrand());
	    		statusCode=127;
	    	} else if (chkResult==2) {
	    		//error in sql
	    		MTMsg.smsMT[0].msg = mtLib.getMsg(5,userLang);
	    		statusCode=5;
	    	}
    	}
    	txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|"+intToString(statusCode)+"|"+currentDateTime3()+"|"+tx_id);
    	MTMsg.smsMT[0].receiver = phone.substr(1);
    	MTMsg.smsMT[0].oadc = helpShortcode;
    	if (statusCode==130)
    		MTMsg.smsMT[0].charging = 15;
    	else
    		MTMsg.smsMT[0].charging = 0;
	    mysql_close(mysqlObject.myData);
    	return MTMsg;
    } else if (operation_type==2) {
    	//operation is CAR
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CAR] [amt:"+amtString+"]");
    	amt = stringToInt(amtString);
    	if (!chkAmtFormat(amtString) || amt==0) {
    		//invalid amount to request
    		txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|75|"+currentDateTime3()+"|"+tx_id);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [invalid amount to request]");
    		string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = catShortcode;
    		MTMsg.smsMT[0].msg = mtLib.getMsg(75,userLang);
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else if (receiverPhone.empty()) {
    		//receiver phone number is empty
    		txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|76|"+currentDateTime3()+"|"+tx_id);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [receiver phone number is empty]");
			string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = catShortcode;
			MTMsg.smsMT[0].msg = mtLib.getMsg(76,userLang);
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else if (receiverPhone.compare(phone)==0) {
    		//a number = b number
    		txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|77|"+currentDateTime3()+"|"+tx_id);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [a number = b number]");
			string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = catShortcode;
			MTMsg.smsMT[0].msg = mtLib.getMsg(77,userLang);
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else if (chkPhoneFormat(receiverPhone)==false) {
    		//b number is not valid
    		txATRLogger.logData(receiverPhone+ "|"+phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|78|"+currentDateTime3()+"|"+tx_id);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [b number is not valid]");
    		string catShortcode = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ats_ptx;
	    	MTMsg.smsMT[0].receiver = phone.substr(1);
	    	MTMsg.smsMT[0].oadc = catShortcode;
    		MTMsg.smsMT[0].msg = mtLib.getMsg(78,userLang);
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else {
    		//do CAR process
    		if (requestorName.length()>10)
    			requestorName = requestorName.substr(0,10);
    		debugLogger.logDebug("[TX ID:"+tx_id+"] [No syntax error, proceed to process request]");
			if (QueryProfileIN(profileA,debugLogger)!=0) {
				//cannot query from IN, stop process
				debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
				txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
				MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
				MTMsg.smsMT[0].receiver = phone.substr(1);
				MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
				MTMsg.smsMT[0].charging = 0;
				mysql_close(mysqlObject.myData);
				return MTMsg;
    		} else {
				Request reqProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type,requestorName,smsc_id);
				MTCollection mtResponse  = reqProcess.makeRequest();
				debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=REQUEST] [PHONE:"+phone+"] [Request Finish]");
				mysql_close(mysqlObject.myData);
				if (mtResponse.smsMT[0].receiver.length()>0)
					mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
				if (mtResponse.smsMT[1].receiver.length()>0)
					mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
				if (mtResponse.smsMT[2].receiver.length()>0)
					mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
				if (mtResponse.smsMT[3].receiver.length()>0)
					mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
				if (mtResponse.smsMT[4].receiver.length()>0)
					mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
				if (mtResponse.smsMT[5].receiver.length()>0)
					mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
				return mtResponse;
			}
    	}
    } else if (operation_type==5) {
    	//operation is CAR Approve Request
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [CAR Approve]");
		if (QueryProfileIN(profileA,debugLogger)!=0) {
			//cannot query from IN, stop process
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
			txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
			MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
			MTMsg.smsMT[0].receiver = phone.substr(1);
			MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
			MTMsg.smsMT[0].charging = 0;
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else {
			Request reqProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type,"",smsc_id);
			MTCollection mtResponse  = reqProcess.confirmRequest();
			debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CAR APPROVE] [PHONE:"+phone+"] [Request Finish]");
			mysql_close(mysqlObject.myData);
			if (mtResponse.smsMT[0].receiver.length()>0)
				mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
			if (mtResponse.smsMT[1].receiver.length()>0)
				mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
			if (mtResponse.smsMT[2].receiver.length()>0)
				mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
			if (mtResponse.smsMT[3].receiver.length()>0)
				mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
			if (mtResponse.smsMT[4].receiver.length()>0)
				mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
			if (mtResponse.smsMT[5].receiver.length()>0)
				mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
			return mtResponse;
		}
    } else if (operation_type==6) {
    	//operation is CAR Reject Request
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [CAT Reject]");
    	Request reqProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type,"",smsc_id);
		MTCollection mtResponse  = reqProcess.cancelRequest();
		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CAR REJECT] [PHONE:"+phone+"] [Request Finish]");
		mysql_close(mysqlObject.myData);
		if (mtResponse.smsMT[0].receiver.length()>0)
			mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
		if (mtResponse.smsMT[1].receiver.length()>0)
			mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
		if (mtResponse.smsMT[2].receiver.length()>0)
			mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
		if (mtResponse.smsMT[3].receiver.length()>0)
			mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
		if (mtResponse.smsMT[4].receiver.length()>0)
			mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
		if (mtResponse.smsMT[5].receiver.length()>0)
			mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
		return mtResponse;
    } else if (operation_type==9) {
    	//operation is CAT Approve Transfer
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [CAT Approve]");
		if (QueryProfileIN(profileA,debugLogger)!=0) {
			//cannot query from IN, stop process
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
			txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
			MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
			MTMsg.smsMT[0].receiver = phone.substr(1);
			MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
			MTMsg.smsMT[0].charging = 0;
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else {
			Transfer trfProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type,smsc_id);
			MTCollection mtResponse  = trfProcess.approveTransfer();
			debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=APPROVE TRANSFER] [PHONE:"+phone+"] [Transfer Finish]");
			mysql_close(mysqlObject.myData);
			if (mtResponse.smsMT[0].receiver.length()>0)
				mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
			if (mtResponse.smsMT[1].receiver.length()>0)
				mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
			if (mtResponse.smsMT[2].receiver.length()>0)
				mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
			if (mtResponse.smsMT[3].receiver.length()>0)
				mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
			if (mtResponse.smsMT[4].receiver.length()>0)
				mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
			if (mtResponse.smsMT[5].receiver.length()>0)
				mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
			return mtResponse;
		}
    } else if (operation_type==10) {
    	//operation is CAT Cancel Transfer
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [CAT Cancel]");
    	Transfer trfProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type,smsc_id);
		MTCollection mtResponse  = trfProcess.cancelTransfer();
		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=CANCEL TRANSFER] [PHONE:"+phone+"] [Transfer Finish]");
		mysql_close(mysqlObject.myData);
		if (mtResponse.smsMT[0].receiver.length()>0)
			mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
		if (mtResponse.smsMT[1].receiver.length()>0)
			mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
		if (mtResponse.smsMT[2].receiver.length()>0)
			mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
		if (mtResponse.smsMT[3].receiver.length()>0)
			mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
		if (mtResponse.smsMT[4].receiver.length()>0)
			mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
		if (mtResponse.smsMT[5].receiver.length()>0)
			mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
		return mtResponse;
    } else if (!chkAmtFormat(amtString)) {
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [amt:"+amtString+"] [Error:Invalid syntax, amount is not valid number]");
    	txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|20|"+currentDateTime3()+"|"+tx_id);
    	//Invalid Syntax
    	MTMsg.smsMT[0].msg = mtLib.getMsg(20,userLang);
    	MTMsg.smsMT[0].receiver = phone.substr(1);
    	MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
    	mysql_close(mysqlObject.myData);
    	return MTMsg;
    } else if (stringToInt(amtString)==0) {
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [amt:"+amtString+"] [Error:Amount not in range 1-25]");
    	txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|24|"+currentDateTime3()+"|"+tx_id);
    	//Invalid Amount
    	MTMsg.smsMT[0].msg = mtLib.getMsg(24,userLang);
    	MTMsg.smsMT[0].receiver = phone.substr(1);
    	MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
    	mysql_close(mysqlObject.myData);
    	return MTMsg;
    } else if (receiverPhone.empty()) {
    	debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [PHONE:"+phone+"] [Error:B number is empty]");
    	txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|21|"+currentDateTime3()+"|"+tx_id);
    	//B Number is empty
		MTMsg.smsMT[0].msg = mtLib.getMsg(21,userLang);
		MTMsg.smsMT[0].receiver = phone.substr(1);
		MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
		mysql_close(mysqlObject.myData);
    	return MTMsg;
	} else if (receiverPhone.compare(phone)==0) {
		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [PHONE:"+phone+"] [Error:B number and A number is same!]");
		txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|22|"+currentDateTime3()+"|"+tx_id);
		//B number = A number
		MTMsg.smsMT[0].msg = mtLib.getMsg(22,userLang);
		MTMsg.smsMT[0].receiver = phone.substr(1);
		MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
		mysql_close(mysqlObject.myData);
    	return MTMsg;
	} else if (chkPhoneFormat(receiverPhone)==false) {
		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [Receiver:"+receiverPhone+"] [Error:Invald B number format!]");
		txATSLogger.logData(phone+ "|"+receiverPhone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|23|"+currentDateTime3()+"|"+tx_id);
		//Invalid B number format
		MTMsg.smsMT[0].msg = mtLib.getMsg(23,userLang);
		MTMsg.smsMT[0].receiver = phone.substr(1);
		MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
		mysql_close(mysqlObject.myData);
    	return MTMsg;
	} else {
		//operation is CAT
		float prevBalA, prevBalB, newBalA, newBalB;
		amt = stringToInt(amtString);
		debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [PHONE:"+phone+"] [No syntax error to transfer, proceed to transfer]");
		if (QueryProfileIN(profileA,debugLogger)!=0) {
			//cannot query from IN, stop process
			debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!]");
			txHelpLogger.logData(phone+"|"+intToString(profileA.getBrand())+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
			MTMsg.smsMT[0].msg = mtLib.getMsg(7,1);
			MTMsg.smsMT[0].receiver = phone.substr(1);
			MTMsg.smsMT[0].oadc = bisnesRulesObject.ats_pfx + phone.substr(1,3) + bisnesRulesObject.ATSoadc[0];
			MTMsg.smsMT[0].charging = 0;
			mysql_close(mysqlObject.myData);
			return MTMsg;
    	} else {
			Transfer trfProcess(tx_id,profileA,amt,pwd,receiverPhone,bisnesRulesObject,debugLogger,mtLib,inPort,channel,mysqlObject,userLang,operation_type, smsc_id);
			MTCollection mtResponse  = trfProcess.makeTransfer();
			debugLogger.logDebug("[TX ID:"+tx_id+"] [OPERATION=TRANSFER] [PHONE:"+phone+"] [Transfer Finish]");
			mysql_close(mysqlObject.myData);
			if (mtResponse.smsMT[0].receiver.length()>0)
				mtResponse.smsMT[0].receiver = mtResponse.smsMT[0].receiver.substr(1);
			if (mtResponse.smsMT[1].receiver.length()>0)
				mtResponse.smsMT[1].receiver = mtResponse.smsMT[1].receiver.substr(1);
			if (mtResponse.smsMT[2].receiver.length()>0)
				mtResponse.smsMT[2].receiver = mtResponse.smsMT[2].receiver.substr(1);
			if (mtResponse.smsMT[3].receiver.length()>0)
				mtResponse.smsMT[3].receiver = mtResponse.smsMT[3].receiver.substr(1);
			if (mtResponse.smsMT[4].receiver.length()>0)
				mtResponse.smsMT[4].receiver = mtResponse.smsMT[4].receiver.substr(1);
			if (mtResponse.smsMT[5].receiver.length()>0)
				mtResponse.smsMT[5].receiver = mtResponse.smsMT[5].receiver.substr(1);
			return mtResponse;
		}
	}
}

