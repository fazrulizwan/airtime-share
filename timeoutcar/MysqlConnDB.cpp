using namespace std;

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <mysql.h>

#include "MysqlConnDB.h"
#include "generalfunction.h"
#include "FileLogger.h"

namespace Checker {
	
	//constructor
	MysqlConnDB::MysqlConnDB() {
	}
	
	//open connection to mysql database
	bool MysqlConnDB::OpenConnDB() {
		string host;
		string username;
		string password;
		string line;
		string name;
		string value;
		int posEqual;
		string db; 
		
		ifstream fileConfig ("/home/cat/timeoutcar/db.config");
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trimString(name);
				value = line.substr(posEqual+1);
				trimString(value);
				if (name=="host")
					host=value;
				else if (name=="username")
					username=value;
				else if (name=="password")
					password=value;
				else if (name=="db")
					db=value;
			}
		} else {
			host = "localhost";
			username="root";
			password="admin32";
		}
		if ((myData = mysql_init((MYSQL*) 0)) && mysql_real_connect( myData, stringToChar(host), stringToChar(username), stringToChar(password), "", MYSQL_PORT, NULL, 0 ) ) {
			mysql_select_db(myData,stringToChar(db));
			return true;
		} else {
			return false;
		}
	}
	
	//open connection to mysql database
	bool MysqlConnDB::OpenConnDB(string logfile, string host, string user, string pwd, string db) {
		FileLogger debugLogger (logfile);
		debugLogger.logDebug("[Enter DB Conn Class]");
		if ((myData = mysql_init((MYSQL*) 0)) && mysql_real_connect( myData, stringToChar(host), stringToChar(user), stringToChar(pwd), "", MYSQL_PORT, NULL, 0 ) ) {
			debugLogger.logDebug("[Finish Establish Conn]");
			mysql_select_db(myData,stringToChar(db));
			debugLogger.logDebug("[Finish Select DB]");
			//cout << "Connection Successfull";
			return true;
		} else {
			//cout << "Cannot establish connection...\n";
			return false;
		}
	}

}
