#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "ConfigReader.h"

using std::string;

namespace Checker {

	bool ConfigReader::ReadConfig() {
		string line;
		string name;
		string value;
		int posEqual;
		ifstream fileConfig ("/home/cat/timeoutcar/checker.config");
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				value = line.substr(posEqual+1);
				
				if (name=="logdir") {
					logdir = value;
					continue;
				} else if (name=="atr_pending") {
					atr_pending = value;
					continue;
				} else if (name=="ats_pending") {
					ats_pending = value;
					continue;
				} else if (name=="atr_tx") {
					dir_atr_tx = value;
					continue;
				} else if (name=="ats_tx") {
					dir_ats_tx = value;
					continue;
				} else if (name=="ip") {
					ip = value;
					continue;
				} else if (name=="port") {
					port = value;
					continue;
				} else if (name=="atr_timeout") {
					const char *charConst = value.c_str();
					tmt_atr = atoi(charConst);
					continue;
				} else if (name=="ats_timeout") {
					const char *charConst = value.c_str();
					tmt_ats = atoi(charConst);
					continue;
				} else if (name=="atr_fail") {
					dir_atr_fail = value;
					continue;
				} else if (name=="ats_fail") {
					dir_ats_fail = value;
					continue;
				}
			}//end while
		}//end if
	}//end read config
}//end namespace
