#include <iostream>
#include <fstream>
#include <errno.h> 
#include <dirent.h> 

#include "generalfunction.h"
#include <sys/time.h>
#include <time.h>
#include <mysql.h>

#include "MysqlConnDB.h"
#include "FileLogger.h"

using namespace std;

namespace Checker {

	//---------check phone no format---------------
	bool chkPhoneFormat(string phone) 
	{
	     if (phone.length()==11) {
	        return true;
	     } else {
	        return false;
	     }
	}
	
	//---------check password format--------------
	bool chkPinFormat(string pin) 
	{
		char pwd[6];
		int i;
	    if (pin.length()==6) {
	    	return true;
	    } else {
	    	return false;
	    }
	}
	
	//---------check amount format--------------
	bool chkAmtFormat(string amount) 
	{
		int i;
        for (i=0;i<amount.length();i++) {
        	if (!isdigit(amount[i]))
        		return false;
        }
    	return true;
	}
	
	//---------to tokinize string--------------
	vector<string> Tokenize(const string str, const string delimiters = " ")
	{
	    vector<string> returnVector;
	    // Skip delimiters at beginning.
	    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	    // Find first "non-delimiter".
	    string::size_type pos     = str.find_first_of(delimiters, lastPos);
	
	    while (string::npos != pos || string::npos != lastPos)
	    {
	        // Found a token, add it to the vector.
	        returnVector.push_back(str.substr(lastPos, pos - lastPos));
	        // Skip delimiters.  Note the "not_of"
	        lastPos = str.find_first_not_of(delimiters, pos);
	        // Find next "non-delimiter"
	        pos = str.find_first_of(delimiters, lastPos);
	    }
	    return returnVector;
	}
	
	string currentTimestamp() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);
	  
	  	long milliseconds;
	  	char stringMillisecond[100];
	  	struct timeval tv;
	  	gettimeofday (&tv, NULL);
	  	milliseconds = tv.tv_usec / 1000;
	  	strcat(timestamp,".");
	  	sprintf(stringMillisecond,"%d",milliseconds);
	  	strcat(timestamp,stringMillisecond);
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentDateTime() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentUserDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%d/%m/%Y",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentDateLimit() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y%m%d",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentDateTime2() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y%m%d%H%M%S",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentDateTime3() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);
	  
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}
	
	string currentWeekLimit() {
		char timestamp[100];
		string currentWeek;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y%W",mytm);
	  
	  	currentWeek = (string)timestamp;
	  	return currentWeek;
	}
	
	string currentMonthLimit() {
		char timestamp[100];
		string currentMth;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y%m",mytm);
	  
	  	currentMth = (string)timestamp;
	  	return currentMth;
	}
	
	//change each element of the string to upper case
	string StringToUpper(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = toupper(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}
	
	//change each element of the string to lower case
	string StringToLower(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = tolower(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}
	
	//convert string to integer
	int stringToInt(string strValue) {
		const char *charConst = strValue.c_str();
		int intVal = atoi(charConst);
		return intVal;            
	}
	
	//convert string to long integer
	int stringToLong(string strValue) {
		const char *charConst = strValue.c_str();
		long longVal = atol(charConst);
		return longVal;            
	}
		
	//convert string to float
	float stringToFloat(string strValue) {
		const char *charConst = strValue.c_str();
		float floatVal = atof(charConst);
		return floatVal;            
	}

	//convert string to constant char
	char *stringToChar(string strValue) {
		char *charVal;
		charVal = new char[strValue.length()+1];
		strcpy(charVal,strValue.c_str());
		return charVal;            
	}
	
	//convert integer to string
	string intToString(int intVal) {
		char charVal[100];
		sprintf(charVal,"%d",intVal); 
		string strVal(charVal);
		return strVal;
	}
	
	//convert float to string
	string floatToString(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%f",floatVal); 
		string strVal(charVal);
		return strVal;
	}
	
	//convert float to 2 decimal point string
	string floatToString2d(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%.2f",floatVal); 
		string strVal(charVal);
		return strVal;
	}
	
	
	string charToString(char* c) {
		std:string s(c);
		return s;
	}
	//check if file is exist
	short int isFileExist(string fileName) {
		fstream fin;
		const char *cfileName= fileName.c_str();
						
		fin.open(cfileName,ios::in);
		if (fin.is_open()) {
			fin.close();
			return 0;
		}
		fin.close();
		return 1;
	}
	
	//concat string
	void stradd(char *s1, char *s2) {
		strcat(s1,s2);
	}
	
	void trimString(string& str)
	{
	  string::size_type pos = str.find_last_not_of(' ');
	  if(pos != string::npos) {
	    str.erase(pos + 1);
	    pos = str.find_first_not_of(' ');
	    if(pos != string::npos) str.erase(0, pos);
	  }
	  else str.erase(str.begin(), str.end());
	}
	
	//find and replace string
	string find_replace(string oriStr, string needle, string replaceWith) {
		size_t found;
		int startAt;
		bool notFound=false;
		
		while (notFound==false) {
			found = oriStr.find(needle);
		
			//remove 6 prefix from phone number
			if (needle=="<ano>" || needle=="<bno>")
				replaceWith = replaceWith.substr(1);
		
			if (found!=string::npos) {
				startAt= int(found);
				oriStr.replace(startAt,5,replaceWith);
			} else {
				notFound=true;
			}
		}
		
		return oriStr;
	}
	
	int TimeDiffSec(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2);
		return tm1-tm2;
	}
	
	int TimeDiffSecDifDay(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2) + (24*60*60);
		return tm1-tm2;
	}
	
	int tmToInt(string tm) {
		int h = stringToInt(tm.substr(0,4));
		int m = stringToInt(tm.substr(4,2));
		int s = stringToInt(tm.substr(6,2));
		int tmInt = s + (m*60) + (h*60*60);
		return tmInt;
	}

	short int deleteFile(string fileName) {
		char *charFileName = stringToChar(fileName);
		short int resultRemove = remove(charFileName);
		//cout << "Delete file:" << fileName << " result:" << resultRemove;
		return resultRemove;
	}
	
	short int renameFile(string oldFile, string newFile) {
		char *oldname = stringToChar(oldFile);
		char *newname = stringToChar(newFile);
		int resultRename = rename(oldname, newname);
		if (resultRename==0)
			return 0;
		else
			return 1;
	}
	
	short int createFailedLog(string logFailedTxName) {
		FileLogger failedTx (logFailedTxName);
		failedTx.createLog();
		return 0;
	}

	short int getUserLangDB(string phone, MYSQL *myData,  const FileLogger& dlogger) {
		short int lang=0;
		string sqlString;
		string chkUser;
		string chkStat;
		MYSQL_RES *res;
		MYSQL_ROW row;
		
		FileLogger logger = dlogger;
		chkUser = "SELECT language FROM userprofile WHERE phone = '"+phone+"'";
		char *csqlChkUser = stringToChar(chkUser);
		short int a=mysql_real_query(myData, csqlChkUser, strlen(csqlChkUser));
		if (a!=0) {
			lang=0;
			string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot get user language. Error:"+errorMsg+" in sql:"+chkUser+"]");
		} else {
			res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		lang = stringToInt(row[0]);
	    		mysql_free_result( res ) ;
	    		logger.logDebug("[Language for "+phone+" found in DB:"+intToString(lang)+"]");
	    	} else {
	    		lang=0;
	    		logger.logDebug("[Phone number not found in DB:"+phone+"]");
	    	}
		}
		return lang;
	}
	
	//delete pending CAR
	short int deletePendingCAR(string a_phone,string b_phone, MYSQL *myData, const FileLogger& dlogger) {
		short int deleteStat=1;
		string sqlString;
		FileLogger logger = dlogger;
		
		sqlString="DELETE FROM car_pending WHERE a_no = '"+a_phone+"' AND b_no='"+b_phone+"'";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	deleteStat=0;
	    } else {
	    	deleteStat=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot delete car pending. Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return deleteStat;
	}
	
}

