#ifndef CONFIGREADER_H_
#define CONFIGREADER_H_

#include <string>

using namespace std;

namespace Checker {
	class ConfigReader {
    	public:
    		bool ReadConfig();
    		string logdir;
    		string atr_pending;
    		string ats_pending;
    		string dir_atr_tx;
    		string dir_ats_tx;
    		string dir_atr_fail;
    		string dir_ats_fail;
    		string ip;
    		string port;
    		short int tmt_atr;
    		short int tmt_ats;
	};
}

#endif /*CONFIGREADER_H_*/
