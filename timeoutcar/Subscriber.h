#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include <iostream>

using namespace std;

namespace Checker {
	class Subscriber {
    	public:
    		Subscriber();
    		short int getIsRegistered() { return isRegistered; }
    		void setIsRegistered(short int reg) { isRegistered=reg; }
    		string getPhone() {return userPhone;}
    		void setPhone(string phone) {userPhone=phone;}
    		short int getType() {return userType;}
    		void setType(short int utype) {userType=utype;}
    		string getPwd() {return userPwd;}
    		void setPwd(string pwd) {userPwd=pwd;}
    		short int getLang() {return userLang;}
    		void setLang(short int lang) {userLang=lang;}
    		float getBalance() { return userBalance; }
    		void setBalance(float bal) { userBalance=bal; }
    		string getExpiry() {return expiryDate;}
    		void setExpiry(string expiry) {expiryDate=expiry;}
    		short int getINStatus() {return INStatus;}
    		void setINStatus(short int INStat) {INStatus=INStat;}
    		short int getAppStatus() {return AppStatus;}
    		void setAppSatus(short int appStat) {AppStatus=appStat;}
    		short int getSuccessCount() { return successCount; }
    		void setSuccessCount(short int cnt) { successCount = cnt; }
    		string getLastTx() { return lastTx; }
    		void setLastTx(string lstTx) { lastTx = lstTx; }
    		string getActivationDate() { return activationDate; }
    		void setActivationDate(string activationDt) { activationDate = activationDt;  }
    		short int getPwdEnabled() { return pwdEnabled; }
    		void setPwdEnabled(short int pwdEna) { pwdEnabled=pwdEna; }
    		float getDailyLimit() { return dailyLimit; }
    		void setDailyLimit(float dlimit) { dailyLimit=dlimit; }
    		float getWeeklyLimit() { return weeklyLimit; }
    		void setWeeklyLimit(float wlimit) { weeklyLimit=wlimit;}
    		float getMonthlyLimit() { return monthlyLimit; }
    		void setMonthlyLimit(float mlimit) { monthlyLimit=mlimit;}
    		short int getINType() {return INType;}
    		void setINType(short int ntype) {INType=ntype;}
    		float getCreditLimit() { return creditLimit; }
    		void setCreditLimit(float climit) { creditLimit = climit; }
    		float getCurrentUsage() { return currentUsage; }
    		void setCurrentUsage(float usage) { currentUsage = usage; }
    		short int getWhiteListStat() { return whiteListStat; }
    		void setWhitelistStat(short int whiteStat) { whiteListStat=whiteStat;}
    		short int getBrand() { return brand; }
    		void setBrand(short int userBrand) { brand=userBrand; }
    		short int getCounterDay() { return counter; }
    		void setCounterDay(short int cnt) { counter=cnt; }
    		string getMotherName() { return mothersName; }
    		void setMotherName(string mother) { mothersName=mother; }
    		bool getGracePeriod() { return gracePeriod; }
    		void setGracePeriod(bool gp) { gracePeriod=gp;}
    		short int getBlacklistStat() { return blacklistStat; }
    		void setBlacklistStat(short int blackStat) { blacklistStat=blackStat; }
    		short int getPromoStat() { return promoStat; }
    		void setLoyaltyPoint(int lpoint) { loyalty_point=lpoint; }
    		int getLoyaltyPoint() { return loyalty_point; }
		void setPromoStat(short int pStat) { promoStat=pStat; }
		void setDailyReceived(float dreceived) { dailyReceived=dreceived; }
		float getDailyReceived() { return dailyReceived; }
		void setPromoEndDate(string promoEnd) { promoEndDate=promoEnd; }
		string getPromoEndDate() { return promoEndDate; }
    	private:
    		string userPhone;
    		short int userType;
    		string userPwd;
    		short int userLang;
    		string expiryDate;
    		short int INStatus;
    		short int AppStatus;
    		float userBalance;
    		string lastTx;
    		short int successCount;
    		short int isRegistered;
    		string activationDate;
    		short int pwdEnabled;
    		float dailyLimit;
    		float weeklyLimit;
    		float monthlyLimit;
    		short int INType;
    		float creditLimit;
    		float currentUsage;
    		short int whiteListStat;
    		short int brand;
    		short int counter;
    		string mothersName;
    		bool gracePeriod;
    		short int blacklistStat;
    		short int promoStat;
			float dailyReceived;
			string promoEndDate;
			int loyalty_point;
	};
}

#endif

