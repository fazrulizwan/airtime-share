#include <iostream>
#include <fstream>

#include "CDR_MP.h"

using namespace std;

int CDR_MP::insertDebitCDR(string txid, string ts, string msisdn, string amt, string flag){

	ofstream file;
	const char *cdr_file="/home/cat/src/in_tx/DebitCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << flag << "\n";
	file.close();
	return 0;	

}

int CDR_MP::insertCreditCDR(string txid, string ts, string msisdn, string amt, string flag){

	ofstream file;
	const char *cdr_file="/home/cat/src/in_tx/CreditCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << flag <<  "\n";
	file.close();
	return 0;	

}

int CDR_MP::insertRefundCDR(string txid, string ts, string msisdn, string amt, string flag){

        ofstream file;
        const char *cdr_file="/home/cat/src/in_tx/RefundCDR.tx";
        file.open(cdr_file,ios::out | ios::app | ios::binary);
        file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << flag << "\n";
        file.close();
        return 0;

}

int CDR_MP::insertRefundFailCDR(string txid, string ts, string msisdn, string amt, string flag){

        ofstream file;
        const char *cdr_file="/home/cat/src/in_tx/Fail_RefundCDR.tx";
        file.open(cdr_file,ios::out | ios::app | ios::binary);
        file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << flag << "\n";
        file.close();
        return 0;

}




