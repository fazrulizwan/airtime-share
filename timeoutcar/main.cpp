#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string>
#include <sstream>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <mysql.h>

#include "ConfigReader.h"
#include "FileLogger.h"
#include "MysqlConnDB.h"
#include "generalfunction.h"
#include "Subscriber.h"
#include "IN_Conn.h"
#include "MP_Config.h"
#include "Prepaid_Config.h"
#include "Balance_Config.h"

using namespace std;

using namespace Checker;


          short int QueryProfileIN(Subscriber &userProfile,FileLogger debugLogger, string tx_id) {
                        struct IN_UserProfile ProfileResult;
                        IN_Conn profileIN;
                        string expiryDt;
                        string activeDt;

			MPConfig MP_conf;       //*** MP Config object loading mechanism
		        MPConfig_t MP_conf_data;
        		PrepaidConfig prepaid_conf;     //*** Prepaid Config object loading mechanism
        		PrepaidConfig_t prepaid_conf_data;
        		BalanceConfig balance_conf;     //*** balance Config object loading mechanism
	        	BalanceConfig_t balance_conf_data;

			PortReg port_reg;
			MP_conf_data = MP_conf.ReadConfig("/home/cat/src/mp_config.config");
		        port_reg.mp_conn_prof_count=MP_conf_data.mp_conn_prof_count;
		        debugLogger.logDebug("MP Conf: MP Profile Count:"+intToString(port_reg.mp_conn_prof_count));
			//cout << "try get all ip & port" << endl;
		        for(int i=0;i<port_reg.mp_conn_prof_count;i++){
			        port_reg.mp_conn_prof[i].ip_addrs=MP_conf_data.mp_ip_addrs[i];
			        port_reg.mp_conn_prof[i].port_no=MP_conf_data.mp_port[i];
			        //cout << "MP_IP:" << port_reg.mp_conn_prof[i].ip_addrs << endl;
                		//cout << "MP_PORT:" << port_reg.mp_conn_prof[i].port_no << endl;
		        }
		        port_reg.mp_log_path=MP_conf_data.log_path;
		        port_reg.mp_resp_timeout=MP_conf_data.in_resp_timeout;
		        port_reg.mp_retry_delay=MP_conf_data.retry_delay;
		        port_reg.mp_max_retry_count=MP_conf_data.max_retry_count;

			//**** Load balance Config
        		debugLogger.logNotice("Loading balance config");
        		balance_conf_data = balance_conf.ReadConfig("/home/cat/src/balance_config.config");
        		port_reg.balance_conn_prof_count=balance_conf_data.balance_conn_prof_count;
        		debugLogger.logNotice("Balance Conf: Balance Profile Count:"+intToString(port_reg.balance_conn_prof_count));
        		for(int i=0;i<port_reg.balance_conn_prof_count;i++){
            			port_reg.balance_conn_prof[i].ip_addrs=balance_conf_data.balance_ip_addrs[i];
            		port_reg.balance_conn_prof[i].port_no=balance_conf_data.balance_port[i];
       			}
        		port_reg.balance_log_path=balance_conf_data.log_path;
        		port_reg.balance_resp_timeout=balance_conf_data.in_resp_timeout;
        		port_reg.balance_retry_delay=balance_conf_data.retry_delay;
        		port_reg.balance_max_retry_count=balance_conf_data.max_retry_count;

    			//**** Load prepaid Config
        		debugLogger.logNotice("Loading prepaid config");
        		prepaid_conf_data = prepaid_conf.ReadConfig("/home/cat/src/prepaid_config.config");
        		port_reg.prepaid_conn_prof_count=prepaid_conf_data.prepaid_conn_prof_count;
        		debugLogger.logNotice("Prepaid Conf: Prepaid Profile Count:"+intToString(port_reg.prepaid_conn_prof_count));
        		for(int i=0;i<port_reg.prepaid_conn_prof_count;i++){
            			port_reg.prepaid_conn_prof[i].ip_addrs=prepaid_conf_data.prepaid_ip_addrs[i];
            			port_reg.prepaid_conn_prof[i].port_no=prepaid_conf_data.prepaid_port[i];
        		}
        		port_reg.prepaid_log_path=prepaid_conf_data.log_path;
        		port_reg.prepaid_resp_timeout=prepaid_conf_data.in_resp_timeout;
        		//cout<<"aaa:"<<port_reg.prepaid_resp_timeout<<endl;
        		port_reg.prepaid_retry_delay=prepaid_conf_data.retry_delay;
        		port_reg.prepaid_max_retry_count=prepaid_conf_data.max_retry_count;

			//cout << "try get send to MP" << endl;	
			//cout << "log file:" << port_reg.mp_log_path << endl;
                        ProfileResult = profileIN.QueryProfile(userProfile.getPhone(),port_reg,tx_id);
			//cout << "query finish" << endl;
                        if (ProfileResult.queryStatus==0) {
				//cout << "query success" << endl;
                                debugLogger.logDebug("[TX ID:"+tx_id+"][Query from IN success, lang:"+intToString(ProfileResult.userLang)+",bal:"+floatToString(ProfileResult.userBalance)+",act:"+ProfileResult.activationDate+",stat:"+intToString(ProfileResult.userStatus)+",brand:"+intToString(ProfileResult.brand)+",exp:"+ProfileResult.expiryDate+"]");
                                userProfile.setLang(ProfileResult.userLang);
                                userProfile.setBalance(ProfileResult.userBalance);
                                activeDt = ProfileResult.activationDate.substr(0,4) + ProfileResult.activationDate.substr(5,2) + ProfileResult.activationDate.substr(8,2);
                                userProfile.setActivationDate(activeDt);
                                userProfile.setType(ProfileResult.userType);
                                userProfile.setINType(ProfileResult.userPlatform);
                                userProfile.setINStatus(ProfileResult.userStatus);
                                userProfile.setBrand(ProfileResult.brand);
                                expiryDt = ProfileResult.expiryDate.substr(0,4) + ProfileResult.expiryDate.substr(5,2) + ProfileResult.expiryDate.substr(8,2);
                                userProfile.setExpiry(expiryDt);
                                if (expiryDt < currentDateLimit()) {
                                        struct tm tmNow, tmExpired;
                                        string expiryTime = expiryDt.substr(0,4) + expiryDt.substr(4,2) + expiryDt.substr(6,2) + "000000";
                                        string currentTime = currentDateLimit() + "000000";
                                        strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
                                        strptime(stringToChar(expiryTime), "%Y%m%d%H%M%S",&tmExpired);
                                        double secDiff = difftime(mktime(&tmNow),mktime(&tmExpired));
                                        int timeDiff=0;
                                        int gracePeriod = 30;
                                        //cout << " grace:" << gracePeriod;
                                        int gracePeriodInSec = gracePeriod * 24 * 60 * 60;
                                        //cout << " diff:" << int(secDiff);
                                        if (secDiff < gracePeriodInSec) {
                                                userProfile.setGracePeriod(true);
                                        } else {
                                                userProfile.setGracePeriod(false);
                                        }
                                } else {
                                        userProfile.setGracePeriod(false);
                                }
                                return 0;
                        } else {
				cout << "query fail" << endl;
                                debugLogger.logDebug("[TX ID:"+tx_id+"][Error:while Check profile "+userProfile.getPhone()+" from IN:Result="+intToString(ProfileResult.queryStatus)+"]");
                                return ProfileResult.queryStatus;
                        }
              }


int main(int argc, char *argv[])
{

	//cout << "Enter timeout checker";

	ConfigReader config;
	config.ReadConfig();

	string logChecker = config.logdir +"/debug."+currentDate()+".log";
	FileLogger checkerLogger (logChecker);

	//checkerLogger.logDebug("Check ATR Pending");

	//log for ATR transaction logs
    string logATRtx = config.dir_atr_tx+"/"+currentDate()+".tx";
    FileLogger txATRLogger (logATRtx);
	//cout << "Log CDR at:" << logtx;

    //log for ATS transaction logs
    string logATStx = config.dir_ats_tx+"/"+currentDate()+".tx";
    FileLogger txATSLogger (logATStx);
	//cout << "Log CDR at:" << logtx;

	//check pending ShareTopup request from A to B in pending file list
	DIR *pdir;
	struct dirent *pent;
	char strFile[50];
	string fileName;
	string foundFile;
	string fileTimeStamp;
	int errno;
	string tx_id;

	/*
	string directoryATRName = config.atr_pending;
	//checkerLogger.logDebug("Opening atr_pending dir:"+directoryName);
	const char *cdirectoryName= directoryATRName.c_str();
	pdir=opendir(cdirectoryName); //open directory
	if (!pdir){
		checkerLogger.logDebug("Cannot Open Directory!");
	}
	*/

	MysqlConnDB mysqlObject;
	MYSQL_RES *res;
	MYSQL_ROW row;
	MYSQL_RES *res2;
	MYSQL_ROW row2;

	string a_noCAR, b_noCAR, tx_idCAR, dtCAR;
	string amtCAR;
	int langCAR;
	int smsc_id;
	string nickname;

	if (mysqlObject.OpenConnDB()) {
		checkerLogger.logDebug("[Open DB Connection]");
		string sqlString="SELECT a_no, b_no, a_lang, dt, tx_id, amt, smsc_id, nickname FROM car_pending";
    	char *csql = stringToChar(sqlString);

    	short int a=mysql_real_query(mysqlObject.myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(mysqlObject.myData);
	    	if (mysql_affected_rows(mysqlObject.myData)>0) {
	    		while ((row = mysql_fetch_row(res)) != NULL) {
	    			a_noCAR = (row[0] ? row[0] : "");
	    			b_noCAR = (row[1] ? row[1] : "");
	    			langCAR = stringToInt((row[2] ? row[2] : ""));
		    		dtCAR = (row[3] ? row[3] : "");
		    		tx_idCAR = (row[4] ? row[4] : "");
		    		amtCAR = (row[5] ? row[5] : "");
					smsc_id = stringToInt((row[6] ? row[6] : ""));
					nickname = (row[7] ? row[7] : "");

	    			fileTimeStamp = dtCAR;
			    	string currentTime = currentDateTime2();
			    	short int timeout = config.tmt_atr;
			    	int timeoutInSec = timeout * 60;
			    	struct tm tmNow, tmRequest;
			    	strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
			    	strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
			    	double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
			    	int dayDiff=0;
			    	int timeDiff=0;
			    	short int lang=1;
					string updateSQL;
				short int price=0;

			    	if (secDiff>timeoutInSec) {
						//request already expired, delete pending file
						fileName = config.atr_pending+"/"+foundFile;
						//60192435322.60192435320.0.20081001135636.20090809102211.10
						string a_phone = a_noCAR;
						string b_phone = b_noCAR;
						string amt = amtCAR;
						tx_id = tx_idCAR;
						lang = langCAR;

						deletePendingCAR(a_phone, b_phone,mysqlObject.myData,checkerLogger);

						//log transaction into CDR
						checkerLogger.logDebug("[Pending ATR already timeout.Log into CDR][a:"+a_phone+",b:"+b_phone+",amt:"+amt+"]");
						txATRLogger.logData(b_phone+ "|"+a_phone+"|||||"+amt+"|2|80|"+currentDateTime3()+"|"+tx_id);


                                                //send sms to smsc
                                            struct sockaddr_in serverAddress;
                                            int sockID;
                                            string res_packet;
                                            char writeBuff[1028];
                                            int returnStatus;
                                            int n;
                                            string msg3;
                                            string oadc;
                                            string channel;

                                            if (lang==1)
                                                msg3 = "Transaksi tidak berjaya. Penghantar tidak memberi respon dlm tempoh masa yg diberi";
                                        else
                                                msg3 = "Your transaction has been cancelled. Donor failed to respond within allowed response time";

                                        if (msg3!="") {
                                                    sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
                                                if(sockID < 0){
                                                        checkerLogger.logDebug("Error Initialize socket");
                                                } else {
                                                        bzero(&serverAddress,sizeof(serverAddress));
                                                        serverAddress.sin_family = AF_INET;;
                                                        serverAddress.sin_addr.s_addr=inet_addr("127.0.0.1");
                                                                if (smsc_id==1)
                                                                                serverAddress.sin_port = htons(30007);
                                                                else if (smsc_id==2)
                                                                                serverAddress.sin_port = htons(30008);
                                                                else if (smsc_id==3)
                                                                                serverAddress.sin_port = htons(30007);
                                                                else if (smsc_id==4)
                                                                                serverAddress.sin_port = htons(30008);
                                                                else if (smsc_id==5)
                                                                                serverAddress.sin_port = htons(30007);
                                                                else
                                                                        serverAddress.sin_port = htons(30008);


                                                        if (connect(sockID,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) {
                                                                checkerLogger.logDebug("Cannot Connect to SMS Sender");
                                                    } else {
                                                        //send sms to smsc binder
                                                        channel="1";
                                                        oadc = "1"+a_phone.substr(1,3)+"0000000";
                                                                res_packet="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id+"|ADC="+a_phone.substr(1)+"|OADC="+oadc+"|MSG="+msg3+"|CHANNEL="+channel+"|ETX";
                                                        checkerLogger.logDebug("packet to send:"+res_packet);
                                                        strcpy(writeBuff,res_packet.c_str());
                                                        returnStatus=send(sockID,writeBuff,sizeof(writeBuff),0);
                                                        if(returnStatus<0) {
                                                                checkerLogger.logDebug("Error sending msg");
                                                        } else{
                                                                checkerLogger.logDebug("Msg Sent smscid="+intToString(smsc_id));
                                                        }
                                                    }

                                                        close(sockID);
                                                        usleep(20000);
                                                        }//end of else
                                        }//end of msg!=""

						//if nickname not null, increase counter
						if (nickname!="") {
							string sqlString2="SELECT counter, dt FROM abuse_counter_unique WHERE requestor='"+a_phone+"' AND donor='"+b_phone+"'";
							char *csql2 = stringToChar(sqlString2);
							int counter=0;
							string dt="";
							short int a2=mysql_real_query(mysqlObject.myData, csql2, strlen(csql2));
							string currentdt=currentDate();
							if (a2==0) {
								res2=mysql_store_result(mysqlObject.myData);
								if (mysql_affected_rows(mysqlObject.myData)>0) {
									while ((row2 = mysql_fetch_row(res2)) != NULL) {
										counter = stringToInt(row2[0] ? row2[0] : "");
										dt = (row2[1] ? row2[1] : "");
									}
									checkerLogger.logDebug("[Current date:"+currentdt+", date_db:"+dt+"]");
									if (currentdt==dt) {
										updateSQL = "UPDATE abuse_counter_unique SET counter=counter+1 WHERE requestor='"+a_phone+"' AND donor='"+b_phone+"'";
										counter=counter+1;
									} else {
										updateSQL = "UPDATE abuse_counter_unique SET counter=1,dt='"+currentdt+"' WHERE requestor='"+a_phone+"' AND donor='"+b_phone+"'";
										counter=1;
									}
								} else {
									updateSQL="INSERT INTO abuse_counter_unique (requestor, donor, dt, counter) VALUES ('"+a_phone+"', '"+b_phone+"', '"+currentdt+"', 1)";
									counter=1;
								}
								checkerLogger.logDebug("[SQL:"+updateSQL+"]");
								char *csql3 = stringToChar(updateSQL);
								short int a2=mysql_real_query(mysqlObject.myData, csql3, strlen(csql3));

								if (counter>=2) {
									//send warning msg
									struct sockaddr_in serverAddress;
									int sockID;
									string res_packet;
									char writeBuff[1028];
									int returnStatus;
									int n;
									string msg;
									string msg2;
									string oadc;
					    				string channel;
									
									if (counter>1) {
										/*
										if (lang==1)
											msg="T.Kasih kerana menggunakan Celcom Airtime Request. Anda tlh menghantar "+intToString(counter)+" RQ dgn msg peribadi. RQ dgn msg yg gagal/dibatalkan yg seterusnya akan di caj RM0.30";
										else
											msg="Thank you for using Celcom Airtime Request. You have sent "+intToString(counter)+" RQ with personalized message. Your next cancelled or failed RQ with message will be charged RM0.30";
										*/
									} 
									if (counter>2) {
										 //query profile from IN for B
					                                        Subscriber profileA;
                                        					profileA.setPhone(a_phone);//requestor
					                                        checkerLogger.logDebug("[TX ID:"+tx_id+"] [Attempt Query Profile A no from IN]");
					                                        int queryStat = QueryProfileIN(profileA,checkerLogger,tx_id);
                                        					if (queryStat==0) {
					                                           //query success
                                        					   checkerLogger.logDebug("[TX ID:"+tx_id+"][Query from IN success, A balance:"+floatToString(profileA.getBalance())+"]");
					                                           if (profileA.getBalance()>=0.30) {
                                        				                float newBal = profileA.getBalance()-0.30;
											/*
											if (profileA.getLang()==1) 
                                                        				msg2 = "Permintaan anda telah dihantar ke "+b_phone+". Baki akaun anda ialah RM"+floatToString2d(newBal)+". Servis caj RM0.30 telah ditolak dari akaun anda.";
                                				                        else
											msg2="Your RQ with message has been sent to "+b_phone+". Your current balance is RM"+floatToString2d(newBal)+". Svc charge of RM0.30 has been deducted from your acc. TQ";
											*/
											price=30;
                                				                   }
									
					                                        } else if (queryStat==5) {
                                        						   //cannot query from IN, stop process
						                                           checkerLogger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!, error 99,100]");
                                        					}

								    }
					    			if (msg2!="") {
										sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
										if(sockID < 0){
											checkerLogger.logDebug("Error Initialize socket");
										} else {
											bzero(&serverAddress,sizeof(serverAddress));
											serverAddress.sin_family = AF_INET;;
											serverAddress.sin_addr.s_addr=inet_addr("10.122.17.102");
											serverAddress.sin_port = htons(20010);
											if (connect(sockID,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) {
												checkerLogger.logDebug("Cannot Connect to SMS Sender");
											} else {
												//send sms to smsc binder
												channel="1";
												oadc = "1"+a_phone.substr(1,3)+"0000000";
												res_packet="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id+"|ADC="+a_phone.substr(1)+"|OADC="+oadc+"|MSG="+msg2+"|CHANNEL="+channel+"|ETX";
												checkerLogger.logDebug("packet to send:"+res_packet);
												strcpy(writeBuff,res_packet.c_str());
												returnStatus=send(sockID,writeBuff,sizeof(writeBuff),0);
												if(returnStatus<0) {
													checkerLogger.logDebug("Error sending msg");
												} else {
													checkerLogger.logDebug("Msg Sent smscid="+intToString(smsc_id));
												}
											}
										}
										close(sockID);
										usleep(20000);
		  							}//end of msg!==""
									if (msg!="") {
                                                                                sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
                                                                                if(sockID < 0){
                                                                                        checkerLogger.logDebug("Error Initialize socket");
                                                                                } else {
                                                                                        bzero(&serverAddress,sizeof(serverAddress));
                                                                                        serverAddress.sin_family = AF_INET;;
                                                                                        serverAddress.sin_addr.s_addr=inet_addr("127.0.0.1");
                                                                                        serverAddress.sin_port = htons(30007);
                                                                                        if (connect(sockID,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) {
                                                                                                checkerLogger.logDebug("Cannot Connect to SMS Sender");
                                                                                        } else {
                                                                                                //send sms to smsc binder
                                                                                                channel="1";
                                                                                                oadc = "1"+a_phone.substr(1,3)+"0000000";
                                                                                                res_packet="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id+"|ADC="+a_phone.substr(1)+"|OADC="+oadc+"|MSG="+msg+"|CHANNEL="+channel+"|ETX";
												checkerLogger.logDebug("packet to send:"+res_packet);
                                                                                                strcpy(writeBuff,res_packet.c_str());
                                                                                                returnStatus=send(sockID,writeBuff,sizeof(writeBuff),0);
                                                                                                if(returnStatus<0) {
                                                                                                        checkerLogger.logDebug("Error sending msg");
                                                                                                } else {
                                                                                                        checkerLogger.logDebug("Msg Sent smscid="+intToString(smsc_id));
                                                                                                }
                                                                                        }
                                                                                }
                                                                                close(sockID);
                                                                                usleep(20000);
									}//end og msg2
								}//end of counter>2
							}//end if a2==0
						}//end if nickname!=""

						//send sms to smsc

	    			}//end of expired
				}//end of while
	    	} else {
	    		checkerLogger.logDebug("[No Data Found!]");
	    	}
	    } else {
	    	string errorMsg(mysql_error(mysqlObject.myData));
	    	checkerLogger.logError("[SQL Error!][Error "+errorMsg+" in "+sqlString+"]");
	    }
	} else {
		checkerLogger.logDebug("[Cannot connect to DB]");
	}
	mysql_close(mysqlObject.myData);

}


