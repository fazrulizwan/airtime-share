#ifndef BUSINESSRULES_H_
#define BUSINESSRULES_H_

using namespace std;

namespace ats {
	class BusinessRules {
    	public:
    		bool ReadConfig();
    		
			short int shr_len;//prefix shortcode length
			string ats_pfx; //ATS shortcode prefix
			string atr_pfx; //ATR shortcode prefix
			string hlp_ptx; //help postfix
			string hlp_ptx_8; //help postfix for 3+8
			string ats_ptx; //ATS postfix
			int atr_tmt;//ATR respond timeout
			int ats_tmt;//ATS respons timeout for unregistered user
			int atr_min_req;//ATR minimum request amount
			int atr_max_req;//ATR maximum request amount
			int max_rcv_amt;//maximum receiver received amount
			int max_trf_dly_reg;//maximum daily transfer amount for registered user 
			int max_trf_dly_unr;//maximum daily transfer amount for unregistered user
			int max_trf_amt_uni;//maximum daily transfer amount for current user
			int max_suc_txn_reg;//maximu successfull transaction for registered user
			int max_suc_txn_unr;//maximum successfull transaction for unregistered user
			float atr_sms_chg_prd_ano;//ATR charge for donor
			float atr_sms_chg_prd_bno;//ATR charge for receiver
			int min_trf_prd;//minimum transfer amount
			int max_trf_prd;//maximum transfer amount
			float min_bal_aft_trf;//minimum balance amount after transfer
			float ats_sms_chg_prd_ano;//ATS charge for donor
			float ats_sms_chg_prd_bno;//ATS charge for receiver
			int grc_prd;//grace period in days
			int chg_pwd_chg;//change password charge in cent (eg:15 = RM0.15)
			string ATSoadc[10];
			string ATRoadc[10];
			string HLPoadc[1];
			int min_trf_ptd;
			int max_trf_ptd;
			int mth_lim_ptd;
			int los_ptd;
	};
}
	
#endif /*BUSINESSRULES_H_*/

