#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include "PostPaid.h"

using namespace std;

int PostPaid::Send(string msisdn,string txid){

	string cmd;

	//cmd="rm -f /home/ats/postpaid/occi/"+msisdn;
	//system(cmd.c_str());

	cmd="/home/ats/postpaid/occi/qrypostpaid "+msisdn+" "+txid;
	//cout << "cmd: " << cmd << endl;
	system(cmd.c_str());

	return 0;

}

PostPaidDataStruct PostPaid::Recv(string msisdn){

	string filepath="/home/ats/postpaid/occi/"+msisdn;
	PostPaidDataStruct data;
	string line;

	ifstream fileConfig(filepath.c_str());

	if(fileConfig.is_open()){
		while (!fileConfig.eof()) {
			getline (fileConfig,line);
			if(line!=""){
				if(getTagValue(line,"ACCTSTATUS")=="null"){
					data.qry_status=-1;
					fileConfig.close();
					return data;
				}
				data.qry_status=0;
				data.acct_status=getTagValue(line,"ACCTSTATUS");
				data.credit_limit=atof(getTagValue(line,"CREDITLIMIT").c_str());
				data.current_bill=atof(getTagValue(line,"CURRENTBILL").c_str());			
				data.acct_id=getTagValue(line,"ACCTID");
			}
		}
	}
	else{
		data.qry_status=-99;
	}
	fileConfig.close();	
	return data;

}


int PostPaid::WriteCDR(struct CDRDataStruct cdr_data){

	ofstream file;
        const char *cdr_file="/quickshare/postpaid/current_cdr.txt";
        file.open(cdr_file,ios::out | ios::app | ios::binary);
        file << cdr_data.cust_ref << ";" << cdr_data.payer_no << ";" << cdr_data.payee_no << ";" << cdr_data.amount*100<<".0" << ";" << cdr_data.txid << ";" << cdr_data.datetime << ";" << cdr_data.comment  << "\n";
        file.close();
        return 0;

}




string PostPaid::getTagValue(string tags,string key){

        /*************************
        Return tag value based on
        tag key given
        *************************/

        int start_pos=0;
        int end_pos;

        start_pos=tags.find(key,start_pos);
        start_pos=start_pos+key.length()+1;
        end_pos=tags.find(",",start_pos);
        //cout << "packet:" << tags << endl;
        //cout << "start_pos:" << start_pos << endl;
        //cout << "end_pos:" << end_pos << endl;
        return tags.substr(start_pos,end_pos-start_pos);

}

