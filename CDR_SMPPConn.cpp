#include <iostream>
#include <fstream>

#include "CDR_SMPPConn.h"

using namespace std;

int CDR_SMPPConn::insertInATS(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/InATS.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}

int CDR_SMPPConn::insertOutATS(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/OutATS.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}

int CDR_SMPPConn::insertInATR(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/InATR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}

int CDR_SMPPConn::insertOutATR(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/OutATR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}


int CDR_SMPPConn::insertInHelp(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/InHelp.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}

int CDR_SMPPConn::insertOutHelp(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/smpp/OutHelp.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 1;	

}
