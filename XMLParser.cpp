#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "XMLParser.h"

using namespace std;

namespace ats {

	XMLParser::XMLParser(string &xmlStr) {
		xml = xmlStr;
		//cout << " xml:" << endl << xml;
    }

	string XMLParser::FindResponseCode() {
		short int startpos,endpos;
		string startTag = "<ResultCode";
		string endTag = "</ResultCode>";
		startpos=0;
		startpos = xml.find(startTag,startpos) + startTag.length();
		endpos = xml.find(endTag,startpos);
		int strlength = endpos - startpos;
		string result;
		if (startpos>0 && endpos>0) {
			string tempResult = xml.substr(startpos, strlength);
			short int lasttagpos = tempResult.find(">",0);
			result = tempResult.substr(lasttagpos+1);
		}
		return result;
	}

	bool XMLParser::extractResult(string resultTag) {
		short int startpos = xml.find(resultTag,0);
		if (startpos>0) {
			resultXml = xml.substr(startpos);
			return true;
		} else {
			return false;
		}
	}

	string XMLParser::FindResultValue(string tag) {
		short int startpos,endpos;
		string startTag = "<"+tag;
		string endTag = "</"+tag+">";
		startpos=0;
		startpos = xml.find(startTag,startpos) + startTag.length();
		endpos = xml.find(endTag,startpos);
		int strlength = endpos - startpos;
		string result="";
		if (startpos>0 && endpos>0) {
			string tempResult = xml.substr(startpos, strlength);
			short int lasttagpos = tempResult.find(">",0);
			result = tempResult.substr(lasttagpos+1);
		}
		return result;
	}

}
