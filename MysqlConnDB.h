#ifndef MYSQLCONNDB_H_
#define MYSQLCONNDB_H_

#include <iostream>
#include <mysql.h>

using namespace std;

namespace ats {
	class MysqlConnDB {
		public:
			MysqlConnDB();
			char *username;
			MYSQL *myData ;
			bool OpenConnDB();
			bool OpenConnDB(string, string, string, string, string);
    };
}

#endif /*MYSQLCONNDB_H_*/
