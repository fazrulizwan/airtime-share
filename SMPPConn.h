#ifndef SMPPCONN_H_
#define SMPPCONN_H_

#include <iostream>
#include <sstream>

#include "SMPPCoder.h"
#include "CoreProcess.h"

using namespace ats;


const int GENERAL_SYNTAX_ERROR=-1;


typedef struct MO{

	std::string msg_type;
	int src_addrs_ton;
	int src_addrs_npi;
	std::string src_addrs;
	int dest_addrs_ton;
	int dest_addrs_npi;
	std::string dest_addrs;
	int esm_class;
	int protocol_id;
	int prio_flag;
	std::string schedule_delivery_time;
	std::string validity_period;
	int registered_delivery;
	int ripf;
	int data_coding;
	int sdmi;
	int sm_len;
	std::string msg;

};

class SMPPConn {

	public:
		SMPPConn(std::string,std::string);
		SMPPConn(void);
		void Conn(void);
		~SMPPConn(void);
		std::string getTagValue(std::string,std::string);

	private:
		char *mSvrAddr;
		int mSvrPort;
		int m_max_process;
		void trim(std::string&);
		int sendMT(int,struct MTStruct);
		void initMT(struct MTStruct &);
		//void sigchld_handler (int);
		//std::string IntToString(int);
		int readLine(int, char*, int, int);
		std::string buffToStr(char *);
		struct MTStruct translateMT(MT_tau);
		std::string currentTimestamp(void);
		bool isFlooding(std::string,int);
		void regFloodFile(std::string);
		std::string floatToStr(float);
		std::string m_smpp_conf_file;
		std::string m_in_conf_file;

	protected:
		void incSessionCount(void);
		int getSessionCount(void);
		static int msSessionCount;
		int WriteSHMID(std::string);

};

#endif

