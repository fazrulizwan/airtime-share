#ifndef CDR_INCONN_H_
#define CDR_INCONN_H_


#include <iostream>

using namespace std;

class CDR_INConn{

	public:
		//string txid, string ts, string phone_no, string amt, string net_type, string flag
		int insertDebitCDR(string,string,string,string,string,string);	

		//string txid, string ts, string phone_no, string amt, string net_type, string flag
		int insertCreditCDR(string,string,string,string,string,string);	

		
		//string txid, string ts, string phone_no, string amt, string net_type, string flag
		int insertChargeCDR(string,string,string,string,string,string);	


		//string txid, string ts, string phone_no, string amt, string net_type, string flag	Flag: 1-NACK from IN 2-Timeout from IN
		int insertChargeFailCDR(string,string,string,string,string,string);	


		//string txid, string ts, string phone_no, string amt, string net_type, string flag
		int insertRefundCDR(string,string,string,string,string,string);	


		//string txid, string ts, string phone_no, string amt, string net_type, string flag	Flag: 1-NACK from IN 2-Timeout from IN
		int insertRefundFailCDR(string,string,string,string,string,string);	


};

#endif

