#include <iostream>

using namespace std;

typedef struct MT_struct{
	int seq_no;
	string oadc;
	string adc;
	string msg;
}MT_t;

class UCPConn{

	public:
	void Conn(void);
	int sendMT(int,MT_t);

	private:
	string buffToStr(char *buff);
	string IntToStr(int no);
	string trim2zero(string num);

};

