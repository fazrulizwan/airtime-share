/**************************************************************

	UCPConn ver 1.00 (Dev): SMSC (UCP) Gateway Connector
	----------------------------------------------------

	Handles all MO/MT to/from UCP SMSC.
	Spawn a CoreProcess object for business rules process.
	
	Developed by Fazrul Izwan Hassan (fazrulizwan@gmail.com)
	For Idot TV Sdn Bhd
	24 March 2008

	"Maybe I deserve someone better who deserves a better me" 
	- me

***************************************************************/

#include <iostream>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/shm.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <sstream>


#include "FileLogger.h"
#include "UCPCoder.h"

using namespace std;
using namespace ats;

//*** Thread procedures
void* do_qs(void *arg);
void* do_qs_read(void *arg);

int main(int argc, char* argv[]){

	int sockID;
	int connSockID;
	int returnStatus;
	int flag=1;
	char *local_ip_addrs;
	struct sockaddr_in locAddr;
	pthread_t qs_th_id;
	//int listen_port=15555;
	int listen_port=atoi(argv[1]);
	FileLogger log("smscsim.log");
	string str_ip="10.122.17.72";

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                //bail("socket(2)",false);
		exit(1);
        }

	//*** Turn off Nagle's algorithm
        setsockopt(sockID, IPPROTO_TCP, TCP_NODELAY, (char *) &flag,sizeof(int));

 	returnStatus = setsockopt(sockID, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
	if (returnStatus < 0) {
		close(sockID);
		exit(1);
 	}

	//strcpy(local_ip_addrs,"10.122.17.52");
	local_ip_addrs=(char *) str_ip.c_str();

	//***** set destination address struct
        bzero(&locAddr,sizeof(locAddr));
        locAddr.sin_family=AF_INET;
        locAddr.sin_addr.s_addr=inet_addr(local_ip_addrs);
        locAddr.sin_port=htons(listen_port);

	//**** Let's bind to local port
	returnStatus = bind(sockID, (struct sockaddr *) &locAddr, sizeof(locAddr));
    	if (returnStatus < 0) {
		log.logError("Failed to bind");
		close(sockID);
		exit(1);
   	}

	log.logNotice("Listening...");

	//**** Let's listen to incoming query
	returnStatus = listen(sockID, 5);
    	if (returnStatus < 0) {
		log.logError("Failed to listen");	
		close(sockID);
		exit(1);
   	}

	
	//*** Loop for incoming packet
	while(1){
		//*** One connection connected..
		connSockID = accept(sockID, NULL,NULL);

		//*** ...and make it concurent
		returnStatus = pthread_create(&qs_th_id, NULL,do_qs, (void *) connSockID);
		if (returnStatus != 0) {
     			log.logError("Could not create thread.");
			close(sockID);
			exit(1);
    		}
		
     		pthread_detach(qs_th_id);
        	sched_yield();

	}
	

	return 0;


}

string buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string TrailZero(int no){

	if(no<10)
		return "0"+IntToString(no);
	else
		return 	IntToString(no);

}


void* do_qs(void *arg){

	long int sockID;
	char readBuff[1028];
	char writeBuff[1028];
	int returnStatus;
	FileLogger log("smscsim.log");
	UCPCoder ucpcoder;
	string strPDU;
	int tps=10;
	pthread_t qs_th_id;
	string b[101];
	string a[101];
	string resp_packet;
	int seq=1;
	int ind=1;

 b[0]="20163755759";
b[1]="20195811126";
b[2]="20198755751";
b[3]="20138917214";
b[4]="20199334444";
b[5]="20195437075";
b[6]="20137251572";
b[7]="20139704933";
b[8]="20199076910";
b[9]="20195695500";
b[10]="20199656321";
b[11]="20196800082";
b[12]="20193071562";
b[13]="20194948952";
b[14]="20192070535";
b[15]="20195480511";
b[16]="20198679338";
b[17]="20138485166";
b[18]="20193446434";
b[19]="20139638887";
b[20]="20138871201";
b[21]="20199659780";
b[22]="20197045577";
b[23]="20198284612";
b[24]="20198387978";
b[25]="20197648437";
b[26]="20197474474";
b[27]="20192315830";
b[28]="20135606043";
b[29]="20198259022";
b[30]="20192025312";
b[31]="20197936442";
b[32]="20134052345";
b[33]="20194974987";
b[34]="20197429498";
b[35]="20195332952";
b[36]="20135074549";
b[37]="20199243980";
b[38]="20139725650";
b[39]="20199268541";
b[40]="20192579875";
b[41]="20199239646";
b[42]="20193941783";
b[43]="20198422002";
b[44]="20194399118";
b[45]="20199692197";
b[46]="20199463402";
b[47]="20197243625";
b[48]="20196975067";
b[49]="20199484491";
b[50]="20198029510";
b[51]="20199791269";
b[52]="20192994046";
b[53]="20197474368";
b[54]="20193054323";
b[55]="20196959356";
b[56]="20139725773";
b[57]="20199020872";
b[58]="20199435704";
b[59]="20199601354";
b[60]="20192583256";
b[61]="20197879312";
b[62]="20199791913";
b[63]="20192453790";
b[64]="20192574586";
b[65]="20195045998";
b[66]="20199263572";
b[67]="20194649500";
b[68]="20194028912";
b[69]="20137830928";
b[70]="20139714200";
b[71]="20198391688";
b[72]="20197322243";
b[73]="20198521502";
b[74]="20134240550";
b[75]="20192877695";
b[76]="20193492102";
b[77]="20192553255";
b[78]="20199255920";
b[79]="20196799737";
b[80]="20199981620";
b[81]="20139744517";
b[82]="20138742564";
b[83]="20195679278";
b[84]="20192525181";
b[85]="20199957002";
b[86]="20195822815";
b[87]="20199698889";
b[88]="20198369292";
b[89]="20199488294";
b[90]="20195336338";
b[91]="20139745711";
b[92]="20195348126";
b[93]="20193739903";
b[94]="20193459085";
b[95]="20199219075";
b[96]="20199773357";
b[97]="20192588952";
b[98]="20195346114";
b[99]="20199438095";
b[100]="20195336051";
	


a[0]="0193955460";
a[1]="0199635154";
a[2]="0132276401";
a[3]="0195433761";
a[4]="0193689541";
a[5]="0139706619";
a[6]="0138764371";
a[7]="0199778689";
a[8]="0199454900";
a[9]="0199068243";
a[10]="0199650304";
a[11]="0196097475";
a[12]="0199983067";
a[13]="0193745575";
a[14]="0134069888";
a[15]="0198008879";
a[16]="0132909236";
a[17]="0196085139";
a[18]="0196085139";
a[19]="0132050155";
a[20]="0193995602";
a[21]="0132276383";
a[22]="0139252880";
a[23]="0194658090";
a[24]="0193612628";
a[25]="0132276403";
a[26]="0139648081";
a[27]="0198015658";
a[28]="0199330900";
a[29]="0193625394";
a[30]="0199644420";
a[31]="0199765123";
a[32]="0193714042";
a[33]="0198094775";
a[34]="0197422657";
a[35]="0192495335";
a[36]="0193955495";
a[37]="0196353718";
a[38]="0197400215";
a[39]="0193475126";
a[40]="0138545000";
a[41]="0194211156";
a[42]="0195673589";
a[43]="0199901836";
a[44]="0199555082";
a[45]="0195632612";
a[46]="0199417720";
a[47]="0198378888";
a[48]="0199578717";
a[49]="0197662765";
a[50]="0193730282";
a[51]="0195608323";
a[52]="0199473663";
a[53]="0193714606";
a[54]="0197445186";
a[55]="0193730143";
a[56]="0197447174";
a[57]="0198726832";
a[58]="0198506454";
a[59]="0195046130";
a[60]="0199580339";
a[61]="0197490477";
a[62]="0193731615";
a[63]="0199619006";
a[64]="0196794270";
a[65]="0197005068";
a[66]="0192507510";
a[67]="0139700454";
a[68]="0136600607";
a[69]="0193099630";
a[70]="0198119006";
a[71]="0193142713";
a[72]="0197963632";
a[73]="0197437263";
a[74]="0197498875";
a[75]="0198463555";
a[76]="0198095350";
a[77]="0199445869";
a[78]="0199278550";
a[79]="0198098159";
a[80]="0199441569";
a[81]="0139959999";
a[82]="0199765123";
a[83]="0193714042";
a[84]="0198094775";
a[85]="0197422657";
a[86]="0192495335";
a[87]="0193955495";
a[88]="0196353718";
a[89]="0197400215";
a[90]="0193475126";
a[91]="0138545000";
a[92]="0194211156";
a[93]="0195673589";
a[94]="0199901836";
a[95]="0199555082";
a[96]="0195632612";
a[97]="0199417720";
a[98]="0198378888";
a[99]="0199578717";
a[100]="0197662765";
//a[101]="0193730282";

/*
a[0]="0199482615";
a[1]="0199488650";
a[2]="0199493986";
a[3]="0199555082";
a[4]="0199576228";
a[5]="0199578717";
a[6]="0199580339";
a[7]="0199619006";
a[8]="0199634412";
a[9]="0199635154";

b[0]="20199463402";
b[1]="20199482853";
b[2]="20199484491";
b[3]="20199488294";
b[4]="20199494322";
b[5]="20199601354";
b[6]="20199619669";
b[7]="20199656321";
b[8]="20199659780";
b[9]="20199683727";



*/


	sockID = (long int) arg;

		returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
                readBuff[returnStatus]='\0';
                resp_packet=buffToStr(readBuff);
                //***** Error in receiving
                if(returnStatus<0){
                        log.logNotice("cannot receive packet");
                        //cout<<"Error: cannot receive packet."<<endl;
                        close(sockID);
                        exit(1);
                }
		else{
			log.logNotice("Recv:"+resp_packet);
			strPDU="00/00019/R/60/A//6D";
			returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
	                if(returnStatus<0){
        	                log.logNotice("Unable to send MT");
                	        exit(-1);
                	}
                	else{
                        	log.logNotice("60 RA sent:"+strPDU);
                	}

		}


		returnStatus = pthread_create(&qs_th_id, NULL,do_qs_read, (void *) sockID);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        close(sockID);
                        exit(1);
                }

	//exit(1);

	log.logNotice("Flooding in a momment");
	sleep(5);

	//for(int j=1;j<=1;j++){
	int j=1;
	while(1){
	for(int i=1;i<=tps;i++){


			strPDU="";
		seq++;
		if(seq==100)
			seq=1;

		ind++;
		if(ind==50)
			ind=1;

		j++;
		if(j==3)
			j=1;

		strPDU="";
		//sleep(1);	
		if(j==1 && ind%2==0)	
			strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM1 123456");		
		else if(j==1 && ind%2!=0)
			strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM2 123456");
		else if(j==2 && ind%2==0)
			strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM3 123456");
		else if(j==2 && ind%2!=0)
			strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM4 123456");
		else 
                        strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM4 123456");

		usleep(20000);

		returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
        	if(returnStatus<0){
                	log.logNotice("Unable to send MT");
	                exit(-1);
        	}
	        else{
        	        log.logNotice("MO sent:"+strPDU);
        	}

	}
	sleep(1);
	}

//pthread_exit(NULL);
}



void* do_qs_read(void *arg){

	long int sockID;
	int returnStatus;
	char readBuff[1028];	
	FileLogger log("");
	string resp_packet;

	sockID = (long int) arg;

	while(1){
		 returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
                readBuff[returnStatus]='\0';
		resp_packet=buffToStr(readBuff);
                //***** Error in receiving
                if(returnStatus<0){
			log.logNotice("cannot receive packet");
                        //cout<<"Error: cannot receive packet."<<endl;
                        close(sockID);
                        exit(1);
                }

	else{

			
                log.logNotice("Received:: "+resp_packet);
        }
		
	}
}
