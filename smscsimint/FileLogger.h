#ifndef FILE_LOGGER_H
#define FILE_LOGGER_H

#include <iostream>

using namespace std;

namespace ats {
          class FileLogger {
                public:
                		FileLogger();
                       	FileLogger(string);
                       	string filename;
                       	int createLog();
                       	int logError(string);
                       	int logWarn(string);
                       	int logNotice(string);
                       	int logDebug(string);
          };

	  string currentTimestamp(void);
}

#endif

