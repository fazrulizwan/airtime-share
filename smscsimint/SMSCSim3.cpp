/**************************************************************

	UCPConn ver 1.00 (Dev): SMSC (UCP) Gateway Connector
	----------------------------------------------------

	Handles all MO/MT to/from UCP SMSC.
	Spawn a CoreProcess object for business rules process.
	
	Developed by Fazrul Izwan Hassan (fazrulizwan@gmail.com)
	For Idot TV Sdn Bhd
	24 March 2008

	"Maybe I deserve someone better who deserves a better me" 
	- me

***************************************************************/

#include <iostream>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/shm.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <sstream>


#include "FileLogger.h"
#include "UCPCoder.h"

using namespace std;
using namespace ats;

//*** Thread procedures
void* do_qs(void *arg);
void* do_qs_read(void *arg);
void* thread_receive(void *arg);
int runSimulator();
int sendMO();
string TrailZero(int);
int connSockID;
int returnStatus;
string srcAddr;
string destAddr;
string msg;
int seq=1;
int ind=1;
UCPCoder ucpcoder;
string strPDU;
long int sockID;
string IntToString(int);

int main(int argc, char* argv[]){
	int i;
	while (1) {
		cout << "Select action:" << endl;
		cout << "1. Run simulator" << endl;
		cout << "2. Send MO" << endl;
		cin >> i;
		if (i==1) {
			runSimulator();
		} else if (i==2) {
			getline (cin, srcAddr);
			cout << "Source Addrs:";
			getline (cin, srcAddr);
			cout << "Destination Addrs:";
			getline (cin, destAddr);
			cout << "Msg:";
			getline (cin, msg);
			sendMO();
		}
	}
	return 0;
}


int runSimulator() {

	//int sockID;
	int flag=1;
	char *local_ip_addrs;
	struct sockaddr_in locAddr;
	pthread_t qs_th_id;
	//int listen_port=15555;
	//int listen_port=atoi(argv[1]);
	int listen_port=9090;
	FileLogger log("smscsim.log");
	//string str_ip="10.123.7.220";
	string str_ip="10.122.17.72";

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                //bail("socket(2)",false);
		exit(1);
        } else {
		log.logNotice("Listeing sock ID:"+IntToString(sockID));
	}

	//*** Turn off Nagle's algorithm
        setsockopt(sockID, IPPROTO_TCP, TCP_NODELAY, (char *) &flag,sizeof(int));

 	returnStatus = setsockopt(sockID, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
	if (returnStatus < 0) {
		close(sockID);
		exit(1);
 	}

	//strcpy(local_ip_addrs,"10.122.17.52");
	local_ip_addrs=(char *) str_ip.c_str();

	//***** set destination address struct
        bzero(&locAddr,sizeof(locAddr));
        locAddr.sin_family=AF_INET;
        locAddr.sin_addr.s_addr=inet_addr(local_ip_addrs);
        locAddr.sin_port=htons(listen_port);

	//**** Let's bind to local port
	returnStatus = bind(sockID, (struct sockaddr *) &locAddr, sizeof(locAddr));
    	if (returnStatus < 0) {
		log.logError("Failed to bind");
		close(sockID);
		exit(1);
   	}

	log.logNotice("Listening...");

	//**** Let's listen to incoming query
	returnStatus = listen(sockID, 5);
    	if (returnStatus < 0) {
		log.logError("Failed to listen");	
		close(sockID);
		exit(1);
   	}

	returnStatus = pthread_create(&qs_th_id, NULL,thread_receive, (void *) connSockID);
        if (returnStatus != 0) {
	        log.logError("Could not create thread.");
                close(sockID);
                exit(1);
        }
	pthread_detach(qs_th_id);
        sched_yield();

	//*** Loop for incoming packet
	/*while(1){
		//*** One connection connected..
		log.logNotice("Accept 1 Connection..");
		connSockID = accept(sockID, NULL,NULL);

		//*** ...and make it concurent
		
		returnStatus = pthread_create(&qs_th_id, NULL,do_qs, (void *) connSockID);
		if (returnStatus != 0) {
     			log.logError("Could not create thread.");
			close(sockID);
			exit(1);
    		}
		
     		pthread_detach(qs_th_id);
        	sched_yield();
		
	}*/
	

	return 0;


}

void* thread_receive(void *arg){
	FileLogger log("smscsim.log");
	pthread_t qs_th_id;
        //*** Loop for incoming packet
        while(1){
                //*** One connection connected..
                log.logNotice("Accept 1 Connection..");
                connSockID = accept(sockID, NULL,NULL);
		log.logNotice("connsockid:"+IntToString(connSockID));
                //*** ...and make it concurent

                returnStatus = pthread_create(&qs_th_id, NULL,do_qs, (void *) connSockID);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        close(sockID);
                        exit(1);
                }

                pthread_detach(qs_th_id);
                sched_yield();

        }
}

int sendMO() {
	FileLogger log("smscsim.log");
	
         seq++;
         if(seq==100)
               seq=1;

         ind++;
        if(ind==50)
              ind=1;

	log.logNotice("sending MO...");
        strPDU=ucpcoder.getOp51(TrailZero(seq),srcAddr, destAddr, msg);
	log.logNotice("connSockID:"+IntToString(connSockID));
	log.logNotice("PDU:"+strPDU);	

        returnStatus=send(connSockID,strPDU.c_str(),strPDU.length()+1,0);
        if(returnStatus<0){
             log.logNotice("Unable to send MO");
             exit(-1);
        }
        else{
            log.logNotice("MO sent:"+strPDU);
        }

}

string buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string TrailZero(int no){

	if(no<10)
		return "0"+IntToString(no);
	else
		return 	IntToString(no);

}


void* do_qs(void *arg){

	long int rcvsockID;
	char readBuff[1028];
	char writeBuff[1028];
	int returnStatus;
	FileLogger log("smscsim.log");
	UCPCoder ucpcoder;
	string strPDU;
	int tps=1;
	pthread_t qs_th_id;
	//string b[101];
	//string a[101];
	string resp_packet;
	int seq=1;
	int ind=1;

	rcvsockID = (long int) arg;
	log.logNotice("sockID:"+IntToString(rcvsockID));

		returnStatus=recv(rcvsockID,readBuff,sizeof(readBuff)-1,0);
                readBuff[returnStatus]='\0';
                resp_packet=buffToStr(readBuff);
                //***** Error in receiving
                if(returnStatus<0){
                        log.logNotice("cannot receive packet");
                        //cout<<"Error: cannot receive packet."<<endl;
                        close(rcvsockID);
                        exit(1);
                }
		else{
			log.logNotice("Recv:"+resp_packet);
			strPDU="00/00019/R/60/A//6D";
			returnStatus=send(rcvsockID,strPDU.c_str(),strPDU.length()+1,0);
	                if(returnStatus<0){
        	                log.logNotice("Unable to send MT");
                	        exit(-1);
                	}
                	else{
                        	log.logNotice("60 RA sent:"+strPDU);
                	}

		}


		returnStatus = pthread_create(&qs_th_id, NULL,do_qs_read, (void *) rcvsockID);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        close(rcvsockID);
                        exit(1);
                }

	//exit(1);

	log.logNotice("Flooding in a momment");
	sleep(5);

	//for(int j=1;j<=1;j++){
	int j=1;
	//while(1){
/*
	for(int i=1;i<=tps;i++){


			strPDU="";
		seq++;
		if(seq==100)
			seq=1;

		ind++;
		if(ind==50)
			ind=1;

		j++;
		if(j==3)
			j=1;

		strPDU="";
		//sleep(1);	
		//if(j==1 && ind%2==0)	
	//	strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM1 123456");		
		log.logNotice("sending MO...");
		strPDU=ucpcoder.getOp51(TrailZero(seq),"0133638332", "10133638331","RM1 123456");
		//else if(j==1 && ind%2!=0)
		//	strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM2 123456");
		//else if(j==2 && ind%2==0)
		//	strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM3 123456");
		//else if(j==2 && ind%2!=0)
		//	strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],b[ind*2-1],"RM4 123456");
		//else 
                        //strPDU=ucpcoder.getOp51(TrailZero(seq),a[ind*2-2],"2019","help");

		usleep(2000);

		returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
        	if(returnStatus<0){
                	log.logNotice("Unable to send MO");
	                exit(-1);
        	}
	        else{
        	        log.logNotice("MO sent:"+strPDU);
        	}

	}
	sleep(1);
	//}
*/
//pthread_exit(NULL);
}



void* do_qs_read(void *arg){

	long int sockIDrcv;
	int returnStatus;
	char readBuff[1028];	
	FileLogger log("");
	string resp_packet;

	sockIDrcv = (long int) arg;

	while(1){
		 returnStatus=recv(sockIDrcv,readBuff,sizeof(readBuff)-1,0);
                readBuff[returnStatus]='\0';
		resp_packet=buffToStr(readBuff);
                //***** Error in receiving
                if(returnStatus<0){
			log.logNotice("cannot receive packet");
                        //cout<<"Error: cannot receive packet."<<endl;
                        close(sockIDrcv);
                        exit(1);
                }

	else{

			
                log.logNotice("Received:: "+resp_packet);
        }
		
	}
}
