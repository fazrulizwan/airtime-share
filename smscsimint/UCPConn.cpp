/**************************************************************

	UCPConn ver 1.00 (Dev): SMSC (UCP) Gateway Connector
	----------------------------------------------------

	Handles all MO/MT to/from UCP SMSC.
	Spawn a CoreProcess object for business rules process.
	
	Developed by Fazrul Izwan Hassan (fazrulizwan@gmail.com)
	For Idot TV Sdn Bhd
	24 March 2008

	"Maybe I deserve someone better who deserves a better me" 
	- me

***************************************************************/

#include <iostream>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/shm.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <sstream>

#include "UCPCoder.h"
#include "UCPConn.h"

using namespace std;

//**** Proto fn
void* thread_keepalive(void *arg); //***thread, handling keep alive separately n concurrently 

//**** Modular global vars
static int childCount; //**** to keep track of child process count
bool sh_ok_reset_alarm;	//*** flag, shared between threads, if true then reset alarm

// ***** Child process signal handler
void sigchld_handler(int signo)
{
 	while (waitpid(-1, NULL, WNOHANG) > 0);
	childCount--;

	/* Re-instate handler */
        signal(SIGCHLD,sigchld_handler);
}


//*** Executor
void UCPConn::Conn(){

	int returnStatus;
	struct sockaddr_in svrAddr;
	char writeBuff[128];
	char readBuff[128];
	int sockID;
	string smsc_ip;
	int smsc_port;
	int flag=1;
	UCPCoder ucpcoder;
	string strPDU;
	int byteLen;
	char *mSvrAddr;
	int mSvrPort;
	string resp_packet;
	string out_packet;
	int thr_result;
	pthread_t thread_id;
	MT_t mt;
	MO_t mo;

	//*** Hard coded
	smsc_ip="10.122.29.12";
	//smsc_ip="10.122.15.21";
	//smsc_ip="10.122.17.52";
	//mSvrPort=19999;
	mSvrPort=4212;

	mSvrAddr=(char *) smsc_ip.c_str();

	//***** get a socket FD
	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
		cout << "Error getting a socket FD" << endl;
		exit(1);
        }

	//***** set destination address struct
	bzero(&svrAddr,sizeof(svrAddr));
        svrAddr.sin_family=AF_INET;
        svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
        svrAddr.sin_port=htons(mSvrPort);	

	//***** Let's connect to SMSC
	returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));
        // ** Fail to connect to SMSC
        if(returnStatus<0){
		cout << "Error: cannot connect to SMSC"<<endl;
		close(sockID);
		exit(1);
        }

	cout << "Connected to SMSC" << endl;

	//*** Turn off Nagle's algorithm
       	setsockopt(sockID, IPPROTO_TCP, TCP_NODELAY, (char *) &flag,sizeof(int));
		
	//***** Establishing UCP binding
	strPDU=ucpcoder.getOp60("2014","celcat2014");
	returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
	cout<<"Sent Op60 packet:"<<strPDU<<":"<<endl;
	//*** Cannot send TxRx PDU
        if(returnStatus<0){
                cout<<"Error: cannot establish bind handshake."<<endl;
		close(sockID);
		exit(1);
        }

	//*** Wait for TxRx response from SMSC
	returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
	readBuff[returnStatus]='\0';
	resp_packet=buffToStr(readBuff);
	//*** Error in reading PDU
	if(returnStatus<0){
		cout<<"error: cannot receive bind handshake response"<<endl;
		close(sockID);
		exit(1);
	}
	else{
		cout << "Received:: " << returnStatus << " bytes"<<endl;
		cout << "UCP packet received::"<<resp_packet<<"::"<<endl;
		
		//*** Binding OK
		if( ucpcoder.decodeResp(resp_packet)==OP60_RESP_OK){
			cout << "SMSC Binding OK" << endl;
		}
		else{ //*** Binding fail
			cout << "SMSC Binding fail!" << endl;
			exit(1);
		}
			
	}

	//**** Creating a thread to handle keep alive
	thr_result = pthread_create(&thread_id, NULL, thread_keepalive, (void *) sockID);
    	if (thr_result != 0) {
      		cout<<"Could not create thread for enquire link."<<endl;
		close(sockID);
		exit(1);
    	}
	pthread_detach(thread_id);
        sched_yield();

	//***** Signal handler for child process
	signal(SIGCHLD, sigchld_handler);

	//***** Looping for MO/MT
	while(1){
		strcpy(readBuff,"");

		//*** Waiting for incoming packet
		cout<<"Waiting for incoming packet.."<<endl;
		returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
		readBuff[returnStatus]='\0';
		//***** Error in receiving
		if(returnStatus<0){
			cout<<"Error: cannot receive packet."<<endl;
			close(sockID);
			exit(1);
		}
		else{ //*** We've received a packet
			resp_packet=buffToStr(readBuff);
			mo=ucpcoder.getMODetail(resp_packet);
				int test=(int)resp_packet.at(0);
			cout<<"test:"<<test<<endl;

			//*** Incoming MO from SMSC
			if(ucpcoder.decodeResp(resp_packet)==OP52_OP_PACKET){
				cout<<"Received MO:" << resp_packet <<":"<<returnStatus<<" bytes"<<endl;
				//*** Get the ACK resp packet	
				out_packet=ucpcoder.getOp52Resp(resp_packet,"A");
				cout << "Sending OP 52 RESP ACK..." << endl;
      				returnStatus=send(sockID,out_packet.c_str(),out_packet.length()+1,0);
				if(returnStatus<0){
					cout << "Error: cannot sent DELIVER_SM_RESP ACK" << endl;
					close(sockID);
					exit(1);
				}
				else{
					cout<<"Sent Op52 Resp:"<<out_packet<<": "<<returnStatus<<" bytes"<<endl;	
				}
	
								cout<<"MO details: seq["<<mo.seq_no<<"] oadc["<<mo.oadc<<"] adc["<<mo.adc<<"] msg["<<mo.msg<<"]"<<endl;	

				if( mo.oadc=="0145400017"){
					mt.seq_no=mo.seq_no;
	                                //mt.oadc=mo.adc;
					mt.oadc="20140000000";
                                	mt.adc=mo.oadc;
                        	        mt.msg="yahoo!";
                	                if(sendMT(sockID,mt))
        	                                cout<<"sendMT OK"<<endl;
	                                else
                                 	       cout<<"sendMT fail"<<endl;
				}

			}
			else if(ucpcoder.decodeResp(resp_packet)==OP51_RESP_OK){
                                cout<<"Received Op51 ACK: "<<resp_packet<<endl;
                        }
			//***Op 57
			else if(ucpcoder.decodeResp(resp_packet)==OP57_OP_PACKET){
                                cout<<"Received Op 57"<<endl;
                                out_packet=ucpcoder.getOp57Resp();
                                cout << "Sending OP 57 RESP ACK..." << endl;
                                returnStatus=send(sockID,out_packet.c_str(),out_packet.length()+1,0);
                                if(returnStatus<0){
                                        cout << "Error: cannot sent DELIVER_SM_RESP ACK" << endl;
                                        close(sockID);
                                        exit(1);
                                }
                                else{
                                        cout<<"Sent Op57 Resp:"<<out_packet<<": "<<returnStatus<<" bytes"<<endl;
                                }

                        }
			else{
				cout<<"Received other packet:"<<resp_packet<<":"<<endl;
			}	
		}

	} //** while(1)

}

int UCPConn::sendMT(int sockID, MT_t mt){

	/***********************************
	sendMT
	sends submit_sm PDU to smsc
	receives: sockID (the existing FD from main)
		  MT structure
	returns : non-negative value if success
	***********************************/

	string strPDU;
	UCPCoder ucpcoder;
	int returnStatus;
	
	strPDU=ucpcoder.getOp51(trim2zero(IntToStr(mt.seq_no)),mt.oadc,mt.adc,mt.msg);	
	returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
	if(returnStatus<0){
		cout<<"Unable to send MT"<<endl;
		return -1;
	}
	else{
		cout<<"MT sent ("<<returnStatus<<" bytes)"<<strPDU<<endl;
		return 1;	
	}
}

string UCPConn::trim2zero(string num){

        if(num.length()==1)
                return "0"+num;
        else
                return num;

}


string UCPConn::IntToStr( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string UCPConn::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


void* thread_keepalive(void *arg){
	/********************
	thread_keepalive
	Receives sock fd from parent thread
	To handle keep-alive.
	********************/

	long t_sockID;
	int t_returnStatus;
	int t_keep_alive_period;
	char t_writeBuff[128];
	int t_byteLen;
	string t_strPDU;
	time_t start,finish;
	UCPCoder ucpcoder;

	//**** Copying a sock FD from parent thread
	t_sockID=dup((long) arg);

	start=time(NULL);

	//**** Loop indefinately
	while(1){
		//**** Check for flag,whether its time to reset alarm
		if(sh_ok_reset_alarm){
			//*** Resetting alarm
			sh_ok_reset_alarm=false;
			start=time(NULL);
			cout<<"Alarm has been reset"<<endl;
		}

		//**** Check flag,whether its time to send keep alive
		finish=time(NULL);
		if(finish-start>=150){
			t_strPDU=ucpcoder.getKeepAlive();
			t_returnStatus=send(t_sockID,t_strPDU.c_str(),t_strPDU.length()+1,0);
			if(t_returnStatus<0){
				cout<<"Error sending enquire link"<<endl;
			}
			else{
				cout<<"KEEP ALIVE sent ("<<t_returnStatus<<" bytes):"<<t_strPDU<<endl;
			}
			start=time(NULL);
		}

		sleep(1);
	}

}


