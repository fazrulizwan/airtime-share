#include <iostream>
#include <sstream>
#include <math.h>

#include "NginCoder.h"
#include "XMLParser.h"

using namespace std;
using namespace ats;

string NginCoder::getQueryPacket(string msisdn, string txid){
	string reqPacket;
	string bodyPacket;
	string serial_number = txid;

	bodyPacket += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:bus=\"http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg\" xmlns:com=\"http://www.huawei.com/bme/cbsinterface/common\" xmlns:bus1=\"http://www.huawei.com/bme/cbsinterface/cbs/businessmgr\">";
	bodyPacket += "<soapenv:Header/>";
	bodyPacket += "<soapenv:Body>";
		bodyPacket += "<bus:QueryProfileRequestMsg>";
		bodyPacket += "<RequestHeader>";
			bodyPacket += "<com:CommandId>QueryProfile</com:CommandId>";
			bodyPacket += "<com:Version>1</com:Version>";
			bodyPacket += "<com:TransactionId></com:TransactionId>";
			bodyPacket += "<com:SequenceId>1</com:SequenceId>";
			bodyPacket += "<com:RequestType>Event</com:RequestType>";
			bodyPacket += "<com:SerialNo>"+serial_number+"</com:SerialNo>";
		bodyPacket += "</RequestHeader>";
		bodyPacket += "<QueryProfileRequest>";
			bodyPacket += "<bus1:SubscriberNo>"+msisdn.substr(1)+"</bus1:SubscriberNo>";
		bodyPacket += "</QueryProfileRequest>";
		bodyPacket += "</bus:QueryProfileRequestMsg>";
	bodyPacket += "</soapenv:Body>";
	bodyPacket += "</soapenv:Envelope>";

	int bodyLen = bodyPacket.length();

	reqPacket="POST / HTTP/1.1";
	reqPacket += "\nContent-Type: text/xml; charset=utf-8";
	reqPacket += "\nContent-Length: "+IntToString(bodyLen);
	reqPacket += "\nConnection: close";
	reqPacket += "\nSOAPAction: \"\"";
	reqPacket += "\n\n";
	reqPacket += bodyPacket;
	return reqPacket;

}

int NginCoder::getReturnStatus(string packet){

	XMLParser xml(packet);
	string errorcode = xml.FindResponseCode();
	if (errorcode=="405000000")
		return ACK;
	else if (errorcode!="")
		return EXT_FAIL;
	else
		return NACK;

}

string NginCoder::getDebitPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string tx_type){
	string reqPacket;
	string bodyPacket;
	string serial_number = txid+"01";
	float amountNgin = amount * 100;

	bodyPacket = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:acc=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg\" xmlns:com=\"http://www.huawei.com/bme/cbsinterface/common\" xmlns:acc1=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgr\">";
	bodyPacket += "<soap:Header/>";
	bodyPacket += "<soap:Body>";
		bodyPacket += "<acc:ModifyBalanceRequestMsg>";
			bodyPacket += "<RequestHeader>";
				bodyPacket += "<com:CommandId>ModifyBalanceRequest</com:CommandId>";
				bodyPacket += "<com:Version>1</com:Version>";
				bodyPacket += "<com:TransactionId></com:TransactionId>";
				bodyPacket += "<com:SequenceId>1</com:SequenceId>";
				bodyPacket += "<com:RequestType>Event</com:RequestType>";
				bodyPacket += "<com:SerialNo>"+serial_number+"</com:SerialNo>";
			bodyPacket += "</RequestHeader>";
			bodyPacket += "<ModifyBalanceRequest>";
				bodyPacket += "<acc1:TransactionID>"+txid+"</acc1:TransactionID>";
				bodyPacket += "<acc1:SubscriberNo>"+msisdn.substr(1)+"</acc1:SubscriberNo>";
				bodyPacket += "<acc1:CurrAcctChgAmt>"+floatToString0d(amountNgin)+"</acc1:CurrAcctChgAmt>";
				bodyPacket += "<acc1:OppositePartyNumber>"+b_no.substr(1)+"</acc1:OppositePartyNumber>";
				bodyPacket += "<acc1:OperateType>2</acc1:OperateType>";
				bodyPacket += "<acc1:Validity>"+IntToString(validity)+"</acc1:Validity>";
				bodyPacket += "<acc1:AdditionalInfo>CAT-TFR</acc1:AdditionalInfo>";
			bodyPacket += "</ModifyBalanceRequest>";
		bodyPacket += "</acc:ModifyBalanceRequestMsg>";
	bodyPacket += "</soap:Body>";
	bodyPacket += "</soap:Envelope>";
	int bodyLen = bodyPacket.length();

	reqPacket="POST / HTTP/1.0";
	reqPacket += "\nContent-Type: text/xml; charset=\"utf-8\"";
	reqPacket += "\nContent-Length: "+IntToString(bodyLen);
	reqPacket += "\nConnection: close";
	reqPacket += "\nSOAPAction: \"\"";
	reqPacket += "\n\n";
	reqPacket += bodyPacket;
	return reqPacket;
}

string NginCoder::getCreditPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string tx_type){
	string reqPacket;
	string bodyPacket;
	string serial_number = txid+"02";
	float amountNgin = amount * 100;

	bodyPacket = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:acc=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg\" xmlns:com=\"http://www.huawei.com/bme/cbsinterface/common\" xmlns:acc1=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgr\">";
	bodyPacket += "<soap:Header/>";
	bodyPacket += "<soap:Body>";
		bodyPacket += "<acc:ModifyBalanceRequestMsg>";
			bodyPacket += "<RequestHeader>";
				bodyPacket += "<com:CommandId>ModifyBalanceRequest</com:CommandId>";
				bodyPacket += "<com:Version>1</com:Version>";
				bodyPacket += "<com:TransactionId></com:TransactionId>";
				bodyPacket += "<com:SequenceId>1</com:SequenceId>";
				bodyPacket += "<com:RequestType>Event</com:RequestType>";
				bodyPacket += "<com:SerialNo>"+serial_number+"</com:SerialNo>";
			bodyPacket += "</RequestHeader>";
			bodyPacket += "<ModifyBalanceRequest>";
				bodyPacket += "<acc1:TransactionID>"+txid+"</acc1:TransactionID>";
				bodyPacket += "<acc1:SubscriberNo>"+msisdn.substr(1)+"</acc1:SubscriberNo>";
				bodyPacket += "<acc1:CurrAcctChgAmt>"+floatToString0d(amountNgin)+"</acc1:CurrAcctChgAmt>";
				bodyPacket += "<acc1:OppositePartyNumber>"+b_no.substr(1)+"</acc1:OppositePartyNumber>";
				bodyPacket += "<acc1:OperateType>1</acc1:OperateType>";
				bodyPacket += "<acc1:Validity>"+IntToString(validity)+"</acc1:Validity>";
				bodyPacket += "<acc1:AdditionalInfo>CAT-RCV</acc1:AdditionalInfo>";
			bodyPacket += "</ModifyBalanceRequest>";
		bodyPacket += "</acc:ModifyBalanceRequestMsg>";
	bodyPacket += "</soap:Body>";
	bodyPacket += "</soap:Envelope>";
	int bodyLen = bodyPacket.length();

	reqPacket="POST / HTTP/1.0";
	reqPacket += "\nContent-Type: text/xml; charset=\"utf-8\"";
	reqPacket += "\nContent-Length: "+IntToString(bodyLen);
	reqPacket += "\nConnection: close";
	reqPacket += "\nSOAPAction: \"\"";
	reqPacket += "\n\n";
	reqPacket += bodyPacket;
	return reqPacket;
}

string NginCoder::getVerifyPacket(string phone_no,string txid,int variation, string b_no){
	string reqPacket;
	string bodyPacket;
	int op_type;
	string serial_number = txid+"03";
	if (variation==1) {
		//debit
		op_type=2;
	} else {
		//credit
		op_type=1;
	}
	bodyPacket = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:acc=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg\" xmlns:com=\"http://www.huawei.com/bme/cbsinterface/common\" xmlns:acc1=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgr\">";
	bodyPacket += "<soapenv:Header/>";
	bodyPacket += "<soapenv:Body>";
		bodyPacket += "<acc:ModifyVerifyRequestMsg>";
			bodyPacket += "<RequestHeader>";
				bodyPacket += "<com:CommandId>ModifyVerify</com:CommandId>";
				bodyPacket += "<com:Version>1</com:Version>";
				bodyPacket += "<com:TransactionId></com:TransactionId>";
				bodyPacket += "<com:SequenceId>1</com:SequenceId>";
				bodyPacket += "<com:RequestType>Event</com:RequestType>";
				bodyPacket += "<com:SerialNo>"+serial_number+"</com:SerialNo>";
			bodyPacket += "</RequestHeader>";
			bodyPacket += "<ModifyVerifyRequest>";
				bodyPacket += "<acc1:TransactionID>"+txid+"</acc1:TransactionID>";
				bodyPacket += "<acc1:SubscriberNo>"+phone_no.substr(1)+"</acc1:SubscriberNo>";
				bodyPacket += "<acc1:OppositePartyNumber>"+b_no.substr(1)+"</acc1:OppositePartyNumber>";
				bodyPacket += "<acc1:OperateType>"+IntToString(op_type)+"</acc1:OperateType>";
		bodyPacket += "</ModifyVerifyRequest>";
		bodyPacket += "</acc:ModifyVerifyRequestMsg>";
	bodyPacket += "</soapenv:Body></soapenv:Envelope>";

	int bodyLen = bodyPacket.length();
	cout << "body len:" << bodyLen << endl;

	reqPacket="POST / HTTP/1.1";
	reqPacket += "\nContent-Type: text/xml; charset=utf-8";
	reqPacket += "\nContent-Length: "+IntToString(bodyLen);
	reqPacket += "\nConnection: close";
	reqPacket += "\nSOAPAction: \"\"";
	reqPacket += "\n\n";
	reqPacket += bodyPacket;
	return reqPacket;

}

int NginCoder::getVerifyReturnStatus(string packet){
	XMLParser xml(packet);
	string errorcode = xml.FindResponseCode();
	if (errorcode=="405000000")
		return 0;
	else
		return -1;
}

int NginCoder::getRefundReturnStatus(string packet){
	XMLParser xml(packet);
	string errorcode = xml.FindResponseCode();
	if (errorcode=="405000000")
	    return 0;
    else
        return -1;
}

string NginCoder::getRefundPacket(string a_no,string txid,float amount,string tx_type,string b_no){
	string reqPacket;
	string bodyPacket;
	string serial_number = txid+"04";
	float amountNgin = amount * 100;

	bodyPacket = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:acc=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg\" xmlns:com=\"http://www.huawei.com/bme/cbsinterface/common\" xmlns:acc1=\"http://www.huawei.com/bme/cbsinterface/cbs/accountmgr\">";
	bodyPacket += "<soap:Header/>";
	bodyPacket += "<soap:Body>";
		bodyPacket += "<acc:ModifyBalanceRequestMsg>";
			bodyPacket += "<RequestHeader>";
				bodyPacket += "<com:CommandId>ModifyBalanceRequest</com:CommandId>";
				bodyPacket += "<com:Version>1</com:Version>";
				bodyPacket += "<com:TransactionId></com:TransactionId>";
				bodyPacket += "<com:SequenceId>1</com:SequenceId>";
				bodyPacket += "<com:RequestType>Event</com:RequestType>";
				bodyPacket += "<com:SerialNo>"+serial_number+"</com:SerialNo>";
			bodyPacket += "</RequestHeader>";
			bodyPacket += "<ModifyBalanceRequest>";
				bodyPacket += "<acc1:TransactionID>"+txid+"</acc1:TransactionID>";
				bodyPacket += "<acc1:SubscriberNo>"+a_no.substr(1)+"</acc1:SubscriberNo>";
				bodyPacket += "<acc1:CurrAcctChgAmt>"+floatToString0d(amountNgin)+"</acc1:CurrAcctChgAmt>";
				bodyPacket += "<acc1:OppositePartyNumber>"+b_no.substr(1)+"</acc1:OppositePartyNumber>";
				bodyPacket += "<acc1:OperateType>3</acc1:OperateType>";
				bodyPacket += "<acc1:AdditionalInfo>CAT-RFND</acc1:AdditionalInfo>";
			bodyPacket += "</ModifyBalanceRequest>";
		bodyPacket += "</acc:ModifyBalanceRequestMsg>";
	bodyPacket += "</soap:Body>";
	bodyPacket += "</soap:Envelope>";
	int bodyLen = bodyPacket.length();

	reqPacket="POST / HTTP/1.0";
	reqPacket += "\nContent-Type: text/xml; charset=\"utf-8\"";
	reqPacket += "\nContent-Length: "+IntToString(bodyLen);
	reqPacket += "\nConnection: close";
	reqPacket += "\nSOAPAction: \"\"";
	reqPacket += "\n\n";
	reqPacket += bodyPacket;
	return reqPacket;

}

MP_Transfer_Status NginCoder::LoadTransferStatus(string packet){

	/***********************************************
	Receives raw transfer reply packet and loads
	details into structure
	***********************************************/

	MP_Transfer_Status mp_tran_status;
	string new_balance;
	string new_expiry_date;

	XMLParser xml(packet);

	//-------balance----------
	string balance = xml.FindResultValue("CurrAcctBal");
	if (balance=="")
		mp_tran_status.transfer_status=-1;
	else {
		float balanceCAT = StrToFloat(balance) / 100;
		mp_tran_status.new_balance=Round(balanceCAT,2);
	}

	//-------expiry----------
	string expirydate = xml.FindResultValue("NewExpireddate");
	if (expirydate=="")
		mp_tran_status.transfer_status=-1;
	else
		mp_tran_status.new_expiry_date=expirydate.substr(0,4)+"-"+expirydate.substr(4,2)+"-"+expirydate.substr(6,2);


	//transfer status
	string errorcode = xml.FindResponseCode();
	if (errorcode=="405000000")
		mp_tran_status.transfer_status=1;
	else
		mp_tran_status.transfer_status=-1;

    return mp_tran_status;

}


string NginCoder::trim(string& s)
{
	string drop=" ";
 	string r=s.erase(s.find_last_not_of(drop)+1);
 	return r.erase(0,r.find_first_not_of(drop));
}


string NginCoder::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string NginCoder::floatToStr(float val){

        char buff[20];

        sprintf(buff,"%f", val);
        return buffToStr(buff);
}

string NginCoder::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}

MP_Usr_Profile NginCoder::loadUserProfile(string packet){
	MP_Usr_Profile prof;
	XMLParser xml(packet);

	//********* AIRTIME ****************************************
	string balance = xml.FindResultValue("PPSBalance");
	if (balance=="")
		prof.balance=0;
	else {
		float balanceCAT = StrToFloat(balance) / 100;
		prof.balance=Round(balanceCAT,2);
	}

	//********* EXPIRY DATE ************************************
    string expirydate = xml.FindResultValue("ValidityDate");
	if (expirydate=="")
		prof.expiry_date="";
	else
		prof.expiry_date=expirydate.substr(0,4)+"-"+expirydate.substr(4,2)+"-"+expirydate.substr(6,2);


	//********* STATUS ****************************************
    string user_status = xml.FindResultValue("State");
    if (user_status=="")
    	prof.status=0;
    else {
    	if (user_status=="1")
    		prof.status=2;
    	else
    		prof.status=StrToInt(user_status);
	}

	//********* LANGUAGE  ****************************************
    string language = xml.FindResultValue("SMSLang");
	if (language=="")
		prof.status=0;
	else
		prof.language=StrToInt(language);

	//********* BRAND ****************************************
	string brand = xml.FindResultValue("MainProductID");
	/*----BRAND MAPPING BY NGIN
	816695	XOX
	816684	MTRADE
	816688	Data Only
	816672	CEP (Celcom Staff)
	816677	3G CEP (Celcom Staff)
	816667	3G MAX
	56		3GSchoolsallday
	58		3GSchoolsstandard
	59		CYallday
	60		CYpersecond
	61		CYstandard
	66		test
	30024	Blue
	90001	Xpax(Max)
	90007	Xpax (Mid)
	90008	Xpax(Lite)
	816481	XPAX
	816484	MAX

	BRAND MAPPING BY CELCOM
	 1                         Max
	 2                         Xceed
	 3                         Xplore
	 4                         InTM
	 5                         TA
	 7                         Mid
	 8                         Lite
	 9                         3G Max
	10                        3G Mid
	11                        3G Lite
	12                        CEP
	13                        3G CEP
	14                        MTrade
	15                        Youth
	16                        3G Youth
	22                        SOX
	23                        SOX3G
	24                        Blue
	25                        UOX
	26                        UOX3G
	30                        (not define)
	31                        (not define)
	37                        Sukses International
	38                        Sukses International
	*/
	if (brand=="")
		prof.brand=0;
	else if (brand=="816684")
		prof.brand=14;
	else if (brand=="30024")
			prof.brand=24;
	else if (brand=="816672")
			prof.brand=12;
	else if (brand=="816677")
			prof.brand=13;
	else if (brand=="816484")
			prof.brand=1;
	else if (brand=="816695")
			prof.brand=30;
	else if (brand=="816688")
			prof.brand=17;
	else if (brand=="816667")
			prof.brand=9;
	else
		prof.brand=StrToInt(brand);

	//********* ACTIVATION DATE ****************************************
    string act_date = xml.FindResultValue("ActivationDate");
	if (act_date=="")
		prof.activation_date="";
	else
		prof.activation_date=act_date.substr(0,4)+"-"+act_date.substr(4,2)+"-"+act_date.substr(6,2);

	//********* INP NUMBER ****************************************
	string pay_type = xml.FindResultValue("PayType");
	if (pay_type=="")
		prof.inp=0;
	else if (pay_type=="2")
		prof.inp=-1;
	else
		prof.inp=StrToInt(pay_type);

	return prof;

}


int NginCoder::StrToInt(string inval){

        istringstream buffer(inval);
        int retval;

        buffer>>retval;

        return retval;
}

float NginCoder::StrToFloat(string inval){

	std::stringstream ss;
	float retval;

	ss<<inval;
	ss>>retval;

	return retval;
}


//convert float to 2 decimal point string
string NginCoder::floatToString2d(float floatVal) {
	char charVal[100];
	sprintf(charVal,"%.2f",floatVal);
	string strVal(charVal);
	return strVal;
}

//convert float to 2 decimal point string
string NginCoder::floatToString0d(float floatVal) {
	char charVal[100];
	sprintf(charVal,"%.0f",floatVal);
	string strVal(charVal);
	return strVal;
}

float NginCoder::Round(float x,int places)
{
	/*********************************
	Rounding a float into decimal places
	*********************************/

        float const shift = powf( 10.0f, places );

        x *= shift;
        x = floorf( x + 0.5f );
        x /= shift;

        return x;
}
