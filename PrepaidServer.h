#include <iostream>

using namespace std;

typedef struct UserProfileIN {
	 short int queryStatus;
	 short int userLang;
	 short int userPlatform;
	 short int userType;
	 short int userStatus;
	 string expiryDate;
	 string activationDate;
	 float userBalance;
}; 

typedef struct INResult {
	short int transferStatus;
	string aExpiryDate;
	string bExpiryDate;
	float aBalance;
	float bBalance;
};

namespace ats {
          
    class PrepaidServer {
          public :
        	  	 string phone;
        	  	 string a_no;
        	  	 string b_no;
        	  	 float amount;
        	  	 struct UserProfileIN ProfileResult;
                 struct INResult TransferResult;
                 
                 struct UserProfileIN queryProfile();
                 struct INResult Transfer();
                 PrepaidServer(string,string,float);
                 PrepaidServer(string);
    };

}
