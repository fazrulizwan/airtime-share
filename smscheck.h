#ifndef SMSCHECK_H_
#define SMSCHECK_H_

#include <string>

using namespace std;

namespace ats {
	bool checkSyntax(string);
}

#endif /*SMSCHECK_H_*/
