#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string>
#include <sstream>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <mysql.h>

#include "ConfigReader.h"
#include "FileLogger.h"
#include "MysqlConnDB.h"
#include "generalfunction.h"

using namespace std;

using namespace Checker;

int main(int argc, char *argv[])
{
	
	//cout << "Enter timeout checker";
	
	ConfigReader config;
	config.ReadConfig();

	string logChecker = config.logdir +"/debug."+currentDate()+".log";
	FileLogger checkerLogger (logChecker);
	
	//checkerLogger.logDebug("Check ATR Pending");

	//log for ATR transaction logs
    string logATRtx = config.dir_atr_tx+"/"+currentDate()+".tx";
    FileLogger txATRLogger (logATRtx);
	//cout << "Log CDR at:" << logtx;
    
    //log for ATS transaction logs
    string logATStx = config.dir_ats_tx+"/"+currentDate()+".tx";
    FileLogger txATSLogger (logATStx);
	//cout << "Log CDR at:" << logtx;

	//check pending ShareTopup request from A to B in pending file list
	DIR *pdir;
	struct dirent *pent;
	char strFile[50];
	string fileName;
	string foundFile;
	string fileTimeStamp;
	int errno;
	string tx_id;
	
	MysqlConnDB mysqlObject;
	MYSQL_RES *res;
	MYSQL_ROW row;
	string a_noCAT, b_noCAT, tx_idCAT, dtCAT;
	string amtCAT;
	int langCAT;
	int smsc_id;
	
	if (mysqlObject.OpenConnDB()) {
		checkerLogger.logDebug("[Open DB Connection]");
		string sqlString="SELECT a_no, b_no, a_lang, dt, tx_id, amt, smsc_id FROM cat_pending";
    	char *csql = stringToChar(sqlString);
    	
    	short int a=mysql_real_query(mysqlObject.myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(mysqlObject.myData);
	    	if (mysql_affected_rows(mysqlObject.myData)>0) {
	    		while ((row = mysql_fetch_row(res)) != NULL) {
	    			a_noCAT = (row[0] ? row[0] : "");
	    			b_noCAT = (row[1] ? row[1] : "");
	    			langCAT = stringToInt((row[2] ? row[2] : ""));
		    		dtCAT = (row[3] ? row[3] : "");
		    		tx_idCAT = (row[4] ? row[4] : "");
		    		amtCAT = (row[5] ? row[5] : "");
	    			smsc_id = stringToInt((row[6] ? row[6] : ""));		    		
	    			fileTimeStamp = dtCAT;
			    	string currentTime = currentDateTime2();
			    	short int timeout = config.tmt_ats;
			    	int timeoutInSec = timeout * 60;
			    	struct tm tmNow, tmRequest;
			    	strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
			    	strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
			    	double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
			    	int dayDiff=0;
			    	int timeDiff=0;
			    	short int lang=1;
			    	
			    	if (secDiff>timeoutInSec) {
						//request already expired, delete pending file
						//fileName = config.atr_pending+"/"+foundFile;
						//60192435322.60192435320.0.20081001135636.20090809102211.10
						string a_phone = a_noCAT;
						string b_phone = b_noCAT;
						string amt = amtCAT;
						tx_id = tx_idCAT;
						lang = langCAT;
						
						checkerLogger.logDebug("[Pending ATS near to timeout.send reminder][a:"+a_phone+",b:"+b_phone+",amt:"+amt+"]");
					    struct sockaddr_in serverAddress;
					    int sockID;
					    string res_packet;
					    char writeBuff[1028];
					    int returnStatus;
					    int n;
					    string msg;
					    string oadc;
					    string channel;
					   /* 
					    if (lang==1)
		  	        		msg = "Transaksi tidak berjaya. Tempoh "+intToString(timeout)+" minit yang diberi untuk anda menghantar respon telah tamat. Utk bantuan, taip HELP dan htr ke 1019.";
		  	        	else
		  	        		msg = "Transaction Failed. You have reached the "+intToString(timeout)+" minutes period given to response. For info type HELP and send to 1019.";
		  		        */
                                            if (lang==1)
                                                msg = "Tempoh 5 minit yang diberi untuk anda menghantar respon telah hampir tamat. Sila taip OK atau X";
                                        else
                                                msg = "You have almost reached the 5 minutes period given to response. For info type HELP and send to 1019.";

		  		    	if (msg!="") {
		  				    sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		  			    	if(sockID < 0){
		  			    		checkerLogger.logDebug("Error Initialize socket");
		  			    	} else {
		  			    		bzero(&serverAddress,sizeof(serverAddress));
		  				    	serverAddress.sin_family = AF_INET;;
		  				    	serverAddress.sin_addr.s_addr=inet_addr("127.0.0.1");
							if (smsc_id==1)
		  				    		serverAddress.sin_port = htons(30007);
							else if (smsc_id==2)
								serverAddress.sin_port = htons(30008);
							else if (smsc_id==3)
								serverAddress.sin_port = htons(30007);
							else if (smsc_id==4)
								serverAddress.sin_port = htons(30008);
							else if (smsc_id==5)
								serverAddress.sin_port = htons(30007);
							else
								serverAddress.sin_port = htons(30008);
		  				    	if (connect(sockID,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) {
		  				    		checkerLogger.logDebug("Cannot Connect to SMS Sender");
		  			    	    } else {
		  			    	    	//send sms to smsc binder
		  			    	    	channel="1";
		  			    	    	oadc = "1"+a_phone.substr(1,3)+"0000000";
		  				    		res_packet="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id+"|ADC="+a_phone.substr(1)+"|OADC="+oadc+"|MSG="+msg+"|CHANNEL="+channel+"|ETX";
		  			    	    	strcpy(writeBuff,res_packet.c_str());
		  			                returnStatus=send(sockID,writeBuff,sizeof(writeBuff),0);
		  			                if(returnStatus<0) {
		  			                	checkerLogger.logDebug("Error sending msg");
		  			                } else{
		  			                	checkerLogger.logDebug("Msg Sent smscid="+intToString(smsc_id));
		  			                }
		  			    	    }
		  					}
		  				    close(sockID);
		  				}
			    	}
	    		}
	    	} else {
	    		checkerLogger.logDebug("[No Data Found!]");
	    	}
	    } else {
	    	string errorMsg(mysql_error(mysqlObject.myData));
	    	checkerLogger.logError("[SQL Error!][Error "+errorMsg+" in "+sqlString+"]");
	    }
	} else {
		checkerLogger.logDebug("[Cannot connect to DB]");
	}
	mysql_close(mysqlObject.myData);
		

}

