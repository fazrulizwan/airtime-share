#include <iostream>

#include "TCPData.h"

using namespace std;

TCPData::TCPData(){

}

void TCPData::setRetryCount(int val){
	TCPData::m_retry_count=val;
}

int TCPData::getRetryCount(void){
	return TCPData::m_retry_count;
}

void TCPData::setOADC(string val){
        TCPData::m_oadc=val;
}

string TCPData::getOADC(void){
        return TCPData::m_oadc;
}

void TCPData::setADC(string val){
        TCPData::m_adc=val;
}

string TCPData::getADC(void){
        return TCPData::m_adc;
}

void TCPData::setMSG(string val){
        TCPData::m_msg=val;
}

string TCPData::getMSG(void){
        return TCPData::m_msg;
}

void TCPData::setTXID(string val){
        TCPData::m_txid=val;
}

string TCPData::getTXID(void){
        return TCPData::m_txid;
}

void TCPData::setMTIndex(int val){
        TCPData::m_mt_index=val;
}

int TCPData::getMTIndex(void){
        return TCPData::m_mt_index;
}



