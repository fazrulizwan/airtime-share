/*************************************************************************
	IN Connector ver 1.0 (Dev)
	Connector to Celcom MP API.

	By Fazrul Izwan Hassan
	17 Mar 2008

	"Hot heads and cold hearts never solved anything" - Billy Graham

**************************************************************************/


#include <iostream>
#include <sstream>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

#include "IN_Conn.h"
#include "MPCoder.h"
#include "FileLogger.h"
#include "myfunction.h"
#include "sharedmem.h"
#include "CDR_MP.h"

using namespace std;
using namespace ats;

// ***** Error reporting mechanism

static void bail(const char *on_what, bool resume, string log_file) {

        /***********************************
        bail
        to report detailed reason of error
        on_what: the operation
        resume: set to false to halt program
        ************************************/

	string errMsg;
        FileLogger log(log_file);
        errMsg=buffToStr(strerror(errno))+": "+buffToStr((char*) on_what);
        log.logError(errMsg);

        if(!resume)
                exit(1);
}



string IN_Conn::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string IN_Conn::floatToStr(float val){

	char buff[20];

	sprintf(buff,"%f", val);
	return buffToStr(buff);

}

int __nsleep(const struct  timespec *req, struct timespec *rem) {
            struct timespec temp_rem;
            if(nanosleep(req,rem)==-1)
                __nsleep(rem,&temp_rem);
            else
                return 1;
}

int msleep(unsigned long milisec) {
            struct timespec req={0},rem={0};
            time_t sec=(int)(milisec/1000);
            milisec=milisec-(sec*1000);
            req.tv_sec=sec;
            req.tv_nsec=milisec*1000000L;
            __nsleep(&req,&rem);
            return 1;
}

struct IN_UserProfile IN_Conn::QueryProfile(string phone_no,PortReg port_reg,string txid){

	int returnStatus;
	struct sockaddr_in svrAddr;
        char writeBuff[1028];
        char readBuff[1028];
        int sockID;
	char *mSvrAddr;
	int mSvrPort;
	FileLogger log(port_reg.balance_log_path);
	int retry_count=0;
	bool isMaxRetry=false;
	string reqPacket;
	string resPacket;
	struct IN_UserProfile usrprof;
	float balance;
	string strBalance;
	int rndval;
	//int portlist[4];
	bool isConnected=false;
	char convBuff[10];
	int shm_id,ret;
        struct shmid_ds shmds;
        void *localAddrs;
	RRInfo *rr_shared;
	struct sembuf semaphore;
	int sem_id;
	//int next_port;
	int next_prof;
	int byteLen;
	//USRProfile profile;
	MP_Usr_Profile profile;
	struct IN_UserProfile usr_prof;
	int port_count;
	int port_list[10];
	string in_ip_addrs;
	//struct sigaction act; //*** Timeout signal struct
	bool isTimeout=false;
	MPCoder mpcoder;
	struct timeval tv;

	log.logNotice("Querying Profile");
	log.logNotice("--------------------------------");
	//log.logNotice("txid:"+txid+" phone_no:"+phone_no+" sleep 100 ms");
	//msleep(100);
	//log.logNotice("txid:"+txid+" wakeup");

	//**** Normalizing phone no, eliminating 6; country code
	phone_no=phone_no.substr(1,phone_no.length()-1);
	srand((unsigned)time(0)+(unsigned)HexStrToInt(txid.substr(8,8).c_str()));



	next_prof=(rand()%port_reg.balance_conn_prof_count)+1;
	//*** Adjust for array subscript
	next_prof--;
	log.logDebug("prof:"+IntToString(next_prof));

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",true,port_reg.balance_log_path);
		usr_prof.queryStatus=1;
                return usr_prof;
        }

	//**** Get server port based on port index
	mSvrPort=port_reg.balance_conn_prof[next_prof].port_no;
	//**** Assign IP IN addrs
	mSvrAddr=(char *) port_reg.balance_conn_prof[next_prof].ip_addrs.c_str();
	log.logDebug("MP IP:"+buffToStr(mSvrAddr));

	//**** Loop until connected or reaching max retry count
	while(!isConnected && !isMaxRetry){

		//***** set destination address struct
	        bzero(&svrAddr,sizeof(svrAddr));
       		svrAddr.sin_family=AF_INET;
       		svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
       		svrAddr.sin_port=htons(mSvrPort);

		//***** Let's connect to IN
		log.logNotice("Connecting to "+ port_reg.balance_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");
       		returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

      		// ** Fail to connect to IN
        	if(returnStatus<0){
                	log.logError("Error: cannot connect to MP [txid:"+txid+"]");
               		bail("connect(2)",true,port_reg.balance_log_path);
			log.logWarn("Cannot connect to "+ port_reg.balance_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

			//**** Make some delay if need be
			usleep(port_reg.balance_retry_delay);

			//**** Inc retry count
			retry_count++;

			//**** Fail Over: get another conn profile to retry
			if(next_prof==port_reg.balance_conn_prof_count){
				next_prof=0;
				mSvrPort=port_reg.balance_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.balance_conn_prof[next_prof].ip_addrs.c_str();
			}
			else{
				next_prof++;
				mSvrPort=port_reg.balance_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.balance_conn_prof[next_prof].ip_addrs.c_str();
			}

			//**** Check if reach max retry count
			if(retry_count>=port_reg.balance_max_retry_count)
				isMaxRetry=true;	//**then raise the flag

			if(!isMaxRetry)
				log.logNotice("Retrying to "+port_reg.balance_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

       		 }
		else{
			//**** Raise the flag once we get connected
			isConnected=true;
		}

	} //*** while

	//**** Max Retry when connecting has reached
	if(isMaxRetry){
		log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"]");
		close(sockID);
		usrprof.queryStatus=1;	//**** return with error status;
		return usrprof;			//**** return with error status
	}

	log.logNotice("Connected to MP [txid:"+txid+"]");

	//************ QUERYING PACKET **************************
       	reqPacket=mpcoder.getQueryPacket(phone_no);
	strcpy(writeBuff,reqPacket.c_str());

	returnStatus=send(sockID,writeBuff,reqPacket.length()+1,0);
	if(returnStatus<0){
		log.logError("Failed to send query packet [txid:"+txid+"]");
		close(sockID);
		usr_prof.queryStatus=1;
                return usr_prof;
	}

	log.logNotice("[txid:"+txid+"] Sent packet to MP:"+reqPacket);

	//*** Setting up timeout
	tv.tv_usec=0;
        tv.tv_sec = port_reg.balance_resp_timeout;
        setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

	log.logDebug("timeout value:"+IntToString(port_reg.balance_resp_timeout));

	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
	readBuff[returnStatus]='\0';
	if(returnStatus<0){
		//**** If timeout occurs
		if (returnStatus==-1){
			log.logWarn("Timeout, while waiting for query reply! [txid:"+txid+"] ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");
                        isTimeout=true;
                        close(sockID);
                        usr_prof.queryStatus=QRY_STATUS_FAIL;
                        return usr_prof;
		}
		else{
			log.logError("Error receiving query resp from MP [txid:"+txid+"] ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");
			close(sockID);
			usr_prof.queryStatus=1;
                	return usr_prof;
		}
	}
	else{
		//**** Disabling timeout
		//alarm(0);
		log.logNotice("[txid:"+txid+"] Received packet from MP:"+buffToStr(readBuff)+" ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");

		resPacket=buffToStr(readBuff);

		//*** ACK
                if(mpcoder.getReturnStatus(resPacket)==0){
                	log.logNotice("Querying OK [txid:"+txid+"] ");
                        profile = mpcoder.LoadUsrProf(resPacket);
                        log.logNotice("Profile details> msisdn:["+profile.msisdn+"] balance:["+floatToStr(profile.balance)+"] usr_status:["+IntToString(profile.status)+"] activation_date:["+profile.activation_date+"] expiry_date:["+profile.expiry_date+"] Platform:["+IntToString(profile.platform)+"] Language:["+IntToString(profile.language)+"] Brand:["+IntToString(profile.brand)+"] INP:["+IntToString(profile.inp)+"] [txid:"+txid+"]");

                        //** load ret struct and return it
                        usr_prof.queryStatus=QRY_STATUS_OK;
                        usr_prof.userLang=profile.language;
                        usr_prof.userPlatform=profile.platform;
			if(profile.inp==-1)
                        	usr_prof.userType=USR_TYPE_POSTPAID;
			else
				usr_prof.userType=USR_TYPE_PREPAID;
                        usr_prof.userStatus=profile.status;
                        usr_prof.activationDate=profile.activation_date;
                        usr_prof.expiryDate=profile.expiry_date;
                        usr_prof.userBalance=profile.balance;
			usr_prof.brand=profile.brand;

                        close(sockID);
                        return usr_prof;
                }
		else if(mpcoder.getReturnStatus(resPacket)==EXT_FAIL){
			log.logWarn("External fail reply while querying [txid:"+txid+"] ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");
			 usr_prof.queryStatus=QRY_STATUS_EXTERNAL_FAIL;
			close(sockID);
                        return usr_prof;

		}
		else if(mpcoder.getReturnStatus(resPacket)==NOT_FOUND){
			log.logWarn("Response from MP : not found [txid:"+txid+"] ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");
			usr_prof.queryStatus=QRY_STATUS_NOT_FOUND;
			close(sockID);
			return usr_prof;
		}
		//*** else
                else{
                 	log.logWarn("Errorneous querying reply status [txid:"+txid+"] ip:["+port_reg.balance_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.balance_conn_prof[next_prof].port_no)+"]");
                        close(sockID);
                        usr_prof.queryStatus=QRY_STATUS_FAIL;
                        return usr_prof;
                }
	}

	//***** This portion is a defensive move in case flow slipped above condition test
	//** load ret struct and return it
        usr_prof.queryStatus=QRY_STATUS_OK;
        usr_prof.userLang=profile.language;
        usr_prof.userPlatform=profile.platform;
	if(profile.inp==-1)
        	usr_prof.userType=USR_TYPE_POSTPAID;
	else
		usr_prof.userType=USR_TYPE_PREPAID;
        usr_prof.userStatus=profile.status;
        usr_prof.activationDate=profile.activation_date;
        usr_prof.expiryDate=profile.expiry_date;
        usr_prof.userBalance=profile.balance;
        close(sockID);
        return usr_prof;

}


string IN_Conn::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}



struct IN_TransferResult IN_Conn::Transfer(string a_no,string b_no,float amt,int validity,float a_charging,float b_charging,string txid,int a_pform,int b_pform,PortReg port_reg,string tx_type){

	int returnStatus;
	struct sockaddr_in svrAddr;
        char writeBuff[1028];
        char readBuff[1028];
        int sockID;
	char *mSvrAddr;
	int mSvrPort;
	string mp_ip_addrs;
	MPCoder mpcoder;
	struct timeval tv;
	string req_packet;
        string res_packet;
	int shm_id,ret;
        struct shmid_ds shmds;
        void *localAddrs;
        RRInfo *rr_shared;
        struct sembuf semaphore;
        int sem_id;
	struct IN_TransferResult tran_result;
	MP_Transfer_Status tran_status;
	FileLogger log(port_reg.mp_log_path);
	int next_prof;
	int retry_count=0;
        bool isMaxRetry=false;
	bool isConnected=false;
	CDR_MP cdr_writer;
	//struct IN_UserProfile usr_prof;

	log.logNotice("Transfering");
        log.logNotice("--------------------------------");
        log.logNotice("txid:"+txid+" a_no:"+a_no+" b_no:"+b_no+" a_charge:"+floatToString2d(a_charging)+" b_charge:"+floatToString2d(b_charging));

	//**** Normalizing phone no, eliminating 6; country code
        a_no=a_no.substr(1,a_no.length()-1);
	b_no=b_no.substr(1,b_no.length()-1);
	srand((unsigned)time(0)+(unsigned)HexStrToInt(txid.substr(8,8).c_str()));



	next_prof=(rand()%port_reg.mp_conn_prof_count)+1;
	//*** Adjust for array subscript
	next_prof--;

	//************** CONNECTION FOR DEBIT OPERATION *************************************
	//log.logNotice("txid:"+txid+" sleep 100 ms");
	//msleep(100);
	//log.logNotice("txid:"+txid+" wakeup");

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",true,port_reg.mp_log_path);
		tran_result.transferStatus=QRY_STATUS_FAIL;
                return tran_result;
        }

	//**** Get server port based on port index
	mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
	//**** Assign IP IN addrs
	mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
	log.logDebug("MP IP:"+buffToStr(mSvrAddr));

	//**** Loop until connected or reaching max retry count
	while(!isConnected && !isMaxRetry){

		//***** set destination address struct
	        bzero(&svrAddr,sizeof(svrAddr));
       		svrAddr.sin_family=AF_INET;
       		svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
       		svrAddr.sin_port=htons(mSvrPort);

		//***** Let's connect to IN
		log.logNotice("Connecting to "+ port_reg.mp_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");
       		returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

      		// ** Fail to connect to IN
        	if(returnStatus<0){
                	log.logError("Error: cannot connect to MP [txid:"+txid+"] ");
               		bail("connect(2)",true,port_reg.mp_log_path);
			log.logWarn("Cannot connect to "+ port_reg.mp_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

			//**** Make some delay if need be
			usleep(port_reg.mp_retry_delay);

			//**** Inc retry count
			retry_count++;

			//**** Fail Over: get another conn profile to retry
			if(next_prof==port_reg.mp_conn_prof_count){
				next_prof=0;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}
			else{
				next_prof++;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}

			//**** Check if reach max retry count
			if(retry_count>=port_reg.mp_max_retry_count)
				isMaxRetry=true;	//**then raise the flag

			if(!isMaxRetry)
				log.logNotice("Retrying to "+port_reg.mp_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

       		 }
		else{
			//**** Raise the flag once we get connected
			isConnected=true;
		}

	} //*** while

	//**** Max Retry when connecting has reached
	if(isMaxRetry){
		log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"] ");
		close(sockID);
		tran_result.transferStatus=1;	//**** return with error status;
		return tran_result;			//**** return with error status
	}

	log.logNotice("Connected to MP [txid:"+txid+"] ");

	//*** Proto: string getDebitPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no);
        req_packet=mpcoder.getDebitPacket(a_no,amt+a_charging,validity,txid,a_pform,b_no,tx_type);
        strcpy(writeBuff,req_packet.c_str());

        returnStatus=send(sockID,writeBuff,req_packet.length()+1,0);
        if(returnStatus<0){
                log.logError("Cannot send [txid:"+txid+"]");
                //exit(1);
		 tran_result.transferStatus=QRY_STATUS_FAIL;
		return tran_result;
        }
	else{
		log.logNotice("[txid:"+txid+"] Packet sent:"+req_packet);

		//**** Setup timeout
	        tv.tv_usec=0;
       		tv.tv_sec =port_reg.mp_resp_timeout;
        	setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

        	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
        	readBuff[returnStatus]='\0';
        	if(returnStatus<0){
                	//**** If timeout occurs
                	if (returnStatus==-1){
                        	log.logWarn("Timeout, while waiting for query reply! [txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
				returnStatus=Refund(a_no,b_no,txid,amt+a_charging,port_reg,tx_type,1,1);
                               if(returnStatus==3){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				else if(returnStatus==0){
					log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous credit operation for b_no was actually success at IN.");
					tran_result.transferStatus=0;
				}
				else if(returnStatus==1){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous debit operation for a_no was actually not done at IN.");
                                        tran_result.transferStatus=1;
                                }

                                else{
                                        tran_result.transferStatus=returnStatus;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }

                        	close(sockID);
				return tran_result;
                        	//exit(1);
                	}
                	else{
                        	log.logError("Error receiving query resp from MP [txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
				returnStatus=Refund(a_no,b_no,txid,amt+a_charging,port_reg,tx_type,1,1);
				if(returnStatus==3){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else if(returnStatus==0){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous credit operation for b_no was actually success at IN.");
                                        tran_result.transferStatus=0;
                                }
                                else if(returnStatus==1){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous debit operation for a_no was actually not done at IN.");
                                        tran_result.transferStatus=1;
                                }

                                else{
                                        tran_result.transferStatus=returnStatus;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				/*
                               if(returnStatus==0){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else{
                                        tran_result.transferStatus=QRY_STATUS_REFUND_FAIL;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				*/

                        	close(sockID);
				return tran_result;
                        	//exit(1);
                	}
        	}
		else{
			res_packet=buffToStr(readBuff);
	                log.logNotice("[txid:"+txid+"] Received packet:"+res_packet+" ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");

			//*** Loads debit status structure
			tran_status=mpcoder.LoadTransferStatus(res_packet);
			//*** If debit is success
			if(tran_status.transfer_status==1){
				tran_result.aBalance=tran_status.new_balance;
				tran_result.aExpiryDate=tran_status.new_expiry_date;
				//tran_result.aBalance=20;
				//tran_result.aExpiryDate="2008-11-12";
				//TODO: insertDebitCDR
				returnStatus=cdr_writer.insertDebitCDR(txid, currentTimestamp(), a_no,floatToStr(amt+a_charging), "0");
				if(returnStatus!=0){
					log.logWarn("Unable writing Debit CDR");
				}

				log.logNotice("<"+txid+"> Debit success. New_balance["+floatToString2d(tran_status.new_balance)+"] New_exp_date["+tran_status.new_expiry_date+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
			}
			else if(mpcoder.getReturnStatus(res_packet)==EXT_FAIL){
				log.logWarn("External fail reply while querying [txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
			 	tran_result.transferStatus=QRY_STATUS_EXTERNAL_FAIL;
				close(sockID);
                        	return tran_result;

			}
			else{ //*** Debit failure
				log.logWarn("<"+txid+"> failed to debit ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
				close(sockID);
                                tran_result.transferStatus=QRY_STATUS_FAIL;
                                return tran_result;
			}

		}

	}

	close(sockID);

	//************** RE-CONNECTION FOR CREDIT OPERATION *************************************
        //log.logNotice("txid:"+txid+" sleep 100 ms");
        //msleep(100);
        //log.logNotice("txid:"+txid+" wakeup");

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",true,port_reg.mp_log_path);
		tran_result.transferStatus=QRY_STATUS_FAIL;
                return tran_result;
        }

	//**** Get random profile
        next_prof=(rand()%port_reg.mp_conn_prof_count)+1;
        //*** Adjust for array subscript
        next_prof--;

	//**** Get server port based on port index
	mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
	//**** Assign IP IN addrs
	mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
	log.logDebug("MP IP:"+buffToStr(mSvrAddr));

	//**** Stain removal from above debit operation
	isConnected=false;
	isMaxRetry=false;
	retry_count=0;

	//**** Loop until connected or reaching max retry count
	while(!isConnected && !isMaxRetry){

		//***** set destination address struct
	        bzero(&svrAddr,sizeof(svrAddr));
       		svrAddr.sin_family=AF_INET;
       		svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
       		svrAddr.sin_port=htons(mSvrPort);

		//***** Let's connect to IN
		log.logNotice("Connecting to "+ port_reg.mp_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");
       		returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

      		// ** Fail to connect to IN
        	if(returnStatus<0){
                	log.logError("Error: cannot connect to MP [txid:"+txid+"]");
               		bail("connect(2)",true,port_reg.mp_log_path);
			log.logWarn("Cannot connect to "+ port_reg.mp_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");

			//**** Make some delay if need be
			usleep(port_reg.mp_retry_delay);

			//**** Inc retry count
			retry_count++;

			//**** Fail Over: get another conn profile to retry
			if(next_prof==port_reg.mp_conn_prof_count){
				next_prof=0;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}
			else{
				next_prof++;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}

			//**** Check if reach max retry count
			if(retry_count>=port_reg.mp_max_retry_count)
				isMaxRetry=true;	//**then raise the flag

			if(!isMaxRetry)
				log.logNotice("Retrying to "+port_reg.mp_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");

       		 }
		else{
			//**** Raise the flag once we get connected
			isConnected=true;
		}

	} //*** while

	//**** Max Retry when connecting has reached
	if(isMaxRetry){
		log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"] ");
		close(sockID);
		tran_result.transferStatus=1;	//**** return with error status;
		return tran_result;			//**** return with error status
	}

	log.logNotice("Connected to MP [txid:"+txid+"] ");

	//*** Proto: string getCreditPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no);
        req_packet=mpcoder.getCreditPacket(b_no,amt-b_charging,validity,txid,a_pform,a_no,tx_type);
        strcpy(writeBuff,req_packet.c_str());

        returnStatus=send(sockID,writeBuff,req_packet.length()+1,0);
        if(returnStatus<0){
                log.logError("Cannot send [txid:"+txid+"] ");
                //exit(1);
		tran_result.transferStatus=QRY_STATUS_FAIL;
                return tran_result;
        }
	else{
		log.logNotice("[txid:"+txid+"] Packet sent:"+req_packet);

		//**** Setup timeout
	        tv.tv_usec=0;
       		tv.tv_sec =port_reg.mp_resp_timeout;
        	setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

        	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
        	readBuff[returnStatus]='\0';
        	if(returnStatus<0){
                	//**** If timeout occurs
                	if (returnStatus==-1){
                        	log.logWarn("Timeout, while waiting for query reply! [txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
				returnStatus=Refund(a_no,b_no,txid,amt+a_charging,port_reg,tx_type,2,1);
				if(returnStatus==3){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else if(returnStatus==0){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous credit operation for b_no was actually success at IN.");
                                        tran_result.transferStatus=0;
                                }
                                else if(returnStatus==1){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous debit operation for a_no was actually not done at IN.");
                                        tran_result.transferStatus=1;
                                }

                                else{
                                        tran_result.transferStatus=returnStatus;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				/*
                               if(returnStatus==0){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else{
                                        tran_result.transferStatus=QRY_STATUS_REFUND_FAIL;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				*/

                        	close(sockID);
				return tran_result;
                        	//exit(1);
                	}
                	else{
                        	log.logError("Error receiving query resp from MP [txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
                        	close(sockID);
				return tran_result;
                        	//exit(1);
                	}
        	}
		else{ //*** packet successfully received
			res_packet=buffToStr(readBuff);
	                log.logNotice("[txid:"+txid+"] Received packet:"+res_packet+" ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");

			//*** Loads credit status structure
			tran_status=mpcoder.LoadTransferStatus(res_packet);
			//*** If credit is success, ACK
			if(tran_status.transfer_status==1){
				tran_result.bBalance=tran_status.new_balance;
				tran_result.bExpiryDate=tran_status.new_expiry_date;
				//tran_result.bBalance=10;
				//tran_result.bExpiryDate="2008-12-30";
				//TODO: insertCreditCDR
				returnStatus=cdr_writer.insertCreditCDR(txid, currentTimestamp(), b_no,floatToStr(amt-b_charging), "0");
				if(returnStatus!=0){
					log.logWarn("Unable writing Credit CDR");
				}
				log.logNotice("<"+txid+"> Credit success. New_balance["+floatToString2d(tran_status.new_balance)+"] New_exp_date["+tran_status.new_expiry_date+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
			}
			else if(mpcoder.getReturnStatus(res_packet)==EXT_FAIL){
				log.logWarn("External fail reply while querying,attempting refund...[txid:"+txid+"] ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"] ");

				returnStatus=Refund(a_no,b_no,txid,amt+a_charging,port_reg,tx_type,1,1);
                                close(sockID);
				if(returnStatus==3){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else if(returnStatus==0){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous credit operation for b_no was actually success at IN.");
                                        tran_result.transferStatus=0;
                                }
                                else if(returnStatus==1){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous debit operation for a_no was actually not done at IN.");
                                        tran_result.transferStatus=1;
                                }

                                else{
                                        tran_result.transferStatus=returnStatus;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"1"); //*** AUG 17 2009, flag 1 means, refund fail
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				/*
                                if(returnStatus==0){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else{
                                        tran_result.transferStatus=QRY_STATUS_REFUND_FAIL;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				*/

				//tran_result.transferStatus=QRY_STATUS_EXTERNAL_FAIL; -> commented by taufik on 27/02/2011
                                return tran_result;


			}
			else{ //*** Debit failure, NACK
				log.logWarn("<"+txid+"> NACK during credit operation,attempting refund... ip:["+port_reg.mp_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"]");
				//TODO: refund
				returnStatus=Refund(a_no,b_no,txid,amt+a_charging,port_reg,tx_type,1,1);
				close(sockID);
				if(returnStatus==3){
                                        tran_result.transferStatus=QRY_STATUS_REFUND_OK;
                                        log.logNotice("[txid:"+txid+"] Refund OK");
                                        returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
                                else if(returnStatus==0){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous credit operation for b_no was actually success at IN.");
                                        tran_result.transferStatus=0;
                                }
                                else if(returnStatus==1){
                                        log.logNotice("Refund NOT DONE [txid:"+txid+"] Reason: Ambigous debit operation for a_no was actually not done at IN.");
                                        tran_result.transferStatus=1;
                                }

                                else{
                                        tran_result.transferStatus=returnStatus;
                                        log.logWarn("[txid:"+txid+"] Refund fail");
                                        returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
                                        if(returnStatus!=0){
                                                log.logWarn("Unable writing Refund Fail CDR");
                                        }
                                }
				/*
				if(returnStatus==0){
					tran_result.transferStatus=QRY_STATUS_REFUND_OK;
					log.logNotice("[txid:"+txid+"] Refund OK");
	                                returnStatus=cdr_writer.insertRefundCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
        	                        if(returnStatus!=0){
                	                        log.logWarn("Unable writing Refund Fail CDR");
                        	        }
				}
				else{
					tran_result.transferStatus=QRY_STATUS_REFUND_FAIL;
					log.logWarn("[txid:"+txid+"] Refund fail");
	                                returnStatus=cdr_writer.insertRefundFailCDR(txid,currentTimestamp(),a_no,floatToStr(amt),"0");
        	                        if(returnStatus!=0){
                	                        log.logWarn("Unable writing Refund Fail CDR");
                        	        }
				}
				*/
				return tran_result;
			}

		}

	}


}


int IN_Conn::Refund(string a_no,string b_no,string txid,float amt,PortReg port_reg,string tx_type,int variation,int option){

	/**********************************************************
	option: 1-after verification NACK for variation 2 case,
		flow WILL NOT proceed to refund credit

		2-after verification NACK for variation 2 case,
                flow WILL proceed to refund credit

	***********************************************************/

	int returnStatus;
        struct sockaddr_in svrAddr;
        char writeBuff[1028];
        char readBuff[1028];
        int sockID;
        char *mSvrAddr;
        int mSvrPort;
        string mp_ip_addrs;
        MPCoder mpcoder;
        struct timeval tv;
        string req_packet;
        string res_packet;
        int shm_id,ret;
        struct shmid_ds shmds;
        void *localAddrs;
        RRInfo *rr_shared;
        struct sembuf semaphore;
        int sem_id;
        struct IN_TransferResult tran_result;
        MP_Transfer_Status tran_status;
        FileLogger log(port_reg.prepaid_log_path);
        int next_prof;
        int retry_count=0;
        bool isMaxRetry=false;
        bool isConnected=false;
	string reqPacket;
	string resPacket;

	log.logNotice("Refunding (verifying) txid:"+txid+" a_no:"+a_no+" b_no:"+b_no+" amt:"+floatToStr(amt)+" txtype:"+tx_type+" variation:"+IntToString(variation)+" option:"+IntToString(option));

	srand((unsigned)time(0)+(unsigned)HexStrToInt(txid.substr(8,8).c_str()));


	next_prof=(rand()%port_reg.prepaid_conn_prof_count)+1;
	//*** Adjust for array subscript
	next_prof--;
	log.logDebug("prof:"+IntToString(next_prof));

	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",true,port_reg.prepaid_log_path);
                //return -1;
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
        }

	//**** Get server port based on port index
	mSvrPort=port_reg.prepaid_conn_prof[next_prof].port_no;
	//**** Assign IP IN addrs
	mSvrAddr=(char *) port_reg.prepaid_conn_prof[next_prof].ip_addrs.c_str();
	log.logDebug("Prepaid IP:"+buffToStr(mSvrAddr));

	//**** Loop until connected or reaching max retry count
	while(!isConnected && !isMaxRetry){

		//***** set destination address struct
	        bzero(&svrAddr,sizeof(svrAddr));
       		svrAddr.sin_family=AF_INET;
       		svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
       		svrAddr.sin_port=htons(mSvrPort);

		//***** Let's connect to IN
		log.logNotice("Connecting to "+ port_reg.prepaid_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");
       		returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

      		// ** Fail to connect to IN
        	if(returnStatus<0){
                	log.logError("Error: cannot connect to Prepaid [txid:"+txid+"]");
               		bail("connect(2)",true,port_reg.prepaid_log_path);
			log.logWarn("Cannot connect to "+ port_reg.prepaid_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

			//**** Make some delay if need be
			usleep(port_reg.prepaid_retry_delay);

			//**** Inc retry count
			retry_count++;

			//**** Fail Over: get another conn profile to retry
			if(next_prof==port_reg.prepaid_conn_prof_count){
				next_prof=0;
				port_reg.prepaid_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.prepaid_conn_prof[next_prof].ip_addrs.c_str();
			}
			else{
				next_prof++;
				port_reg.prepaid_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.prepaid_conn_prof[next_prof].ip_addrs.c_str();
			}

			//**** Check if reach max retry count
			if(retry_count>=port_reg.prepaid_max_retry_count)
				isMaxRetry=true;	//**then raise the flag

			if(!isMaxRetry)
				log.logNotice("Retrying to "+port_reg.prepaid_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

       		 }
		else{
			//**** Raise the flag once we get connected
			isConnected=true;
		}

	} //*** while

	//**** Max Retry when connecting has reached
	if(isMaxRetry){
		log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"]");
		close(sockID);
		//return -1;			//**** return with error status
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
	}

	log.logNotice("Connected to Prepaid [txid:"+txid+"]");

	//************ VERIFYING PACKET **************************
	if(variation==1)
       		reqPacket=mpcoder.getVerifyPacket(a_no,txid,variation);
	else if(variation==2)
		reqPacket=mpcoder.getVerifyPacket(b_no,txid,variation);
	strcpy(writeBuff,reqPacket.c_str());

	returnStatus=send(sockID,writeBuff,reqPacket.length()+1,0);
	if(returnStatus<0){
		log.logError("Failed to send verify query packet [txid:"+txid+"]");
		close(sockID);
                //return -1;
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
	}

	log.logNotice("[txid:"+txid+"] Sent verify packet to Prepaid:"+reqPacket);

	//*** Setting up timeout
	tv.tv_usec=0;
        tv.tv_sec = port_reg.prepaid_resp_timeout;
        setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
	readBuff[returnStatus]='\0';
	if(returnStatus<0){
		//**** If timeout occurs
		if (returnStatus==-1){
			log.logWarn("Timeout, while waiting for query reply! [txid:"+txid+"] ip:["+port_reg.prepaid_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+"]");
                        close(sockID);
                        //return -1;
			if(variation==1)
			        return 4;
			else if(variation==2)
			        return 6;
		}
		else{
			log.logError("Error receiving query resp from Prepaid [txid:"+txid+"] ip:["+port_reg.prepaid_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+"]");
			close(sockID);
                	//return -1;
			if(variation==1)
			        return 4;
			else if(variation==2)
			        return 6;
		}
	}
	else{
		//**** Disabling timeout
		//alarm(0);
		log.logNotice("[txid:"+txid+"] Received packet from Prepaid:"+buffToStr(readBuff)+" ip:["+port_reg.prepaid_conn_prof[next_prof].ip_addrs+"] port:["+IntToString(port_reg.prepaid_conn_prof[next_prof].port_no)+"]");

		resPacket=buffToStr(readBuff);

		if(variation==1){
			//*** ACK
        	        if(mpcoder.getVerifyReturnStatus(resPacket)==0){
                		log.logNotice("Verification (debit) OK [txid:"+txid+"] proceed to refund (credit a_no)");

                	}
			//*** else
        	        else{
                	 	log.logNotice("Verification (debit) fail [txid:"+txid+"] refund not proceed at this time");
                        	close(sockID);
	                        return 4;
        	        }
		}
		else if(variation==2){

			//*** ACK
                        if(mpcoder.getVerifyReturnStatus(resPacket)==0){
                                log.logNotice("Verification (credit) OK [txid:"+txid+"] this tx is a success. Refund need not to be done");
				close(sockID);
                                return 0;

                        }
                        //*** else
                        else{
				if(option==1){
                                	log.logWarn("Verification (credit) fail [txid:"+txid+"] Option 1: refund not proceed at this time");
					close(sockID);
                                	return 6;
				}
				else if(option==2){
					log.logWarn("Verification (credit) fail [txid:"+txid+"] Option 2: proceed to refund (credit a_no)");
				}
                        }


		}
	}

	close(sockID);

	//************** RE-CONNECTION FOR REFUND OPERATION *************************************
	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",true,port_reg.mp_log_path);
                //return -1;
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
        }

	srand((unsigned)time(0)+(unsigned)HexStrToInt(txid.substr(8,8).c_str()));



	next_prof=(rand()%port_reg.mp_conn_prof_count)+1;
        //*** Adjust for array subscript
        next_prof--;

	//**** Get server port based on port index
	mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
	//**** Assign IP IN addrs
	mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
	log.logDebug("MP IP:"+buffToStr(mSvrAddr));

	//**** Stain removal from above debit operation
	isConnected=false;
	isMaxRetry=false;
	retry_count=0;

	//**** Loop until connected or reaching max retry count
	while(!isConnected && !isMaxRetry){

		//***** set destination address struct
	        bzero(&svrAddr,sizeof(svrAddr));
       		svrAddr.sin_family=AF_INET;
       		svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
       		svrAddr.sin_port=htons(mSvrPort);

		//***** Let's connect to IN
		log.logNotice("Connecting to "+ port_reg.mp_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");
       		returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

      		// ** Fail to connect to IN
        	if(returnStatus<0){
                	log.logError("Error: cannot connect to MP [txid:"+txid+"]");
               		bail("connect(2)",true,port_reg.mp_log_path);
			log.logWarn("Cannot connect to "+ port_reg.mp_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");

			//**** Make some delay if need be
			usleep(port_reg.mp_retry_delay);

			//**** Inc retry count
			retry_count++;

			//**** Fail Over: get another conn profile to retry
			if(next_prof==port_reg.mp_conn_prof_count){
				next_prof=0;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}
			else{
				next_prof++;
				mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
				mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
			}

			//**** Check if reach max retry count
			if(retry_count>=port_reg.mp_max_retry_count)
				isMaxRetry=true;	//**then raise the flag

			if(!isMaxRetry)
				log.logNotice("Retrying to "+port_reg.mp_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");

       		 }
		else{
			//**** Raise the flag once we get connected
			isConnected=true;
		}

	} //*** while

	//**** Max Retry when connecting has reached
	if(isMaxRetry){
		log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"] ");
		close(sockID);
		//return -1;			//**** return with error status
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
	}

	log.logNotice("Connected to MP [txid:"+txid+"] ");

	//*** Proto: string getCreditPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no);
	//**string a_no,string txid,float amt,string tx_type,string b_no
        req_packet=mpcoder.getRefundPacket(a_no,txid,amt,tx_type,b_no);
        strcpy(writeBuff,req_packet.c_str());

        returnStatus=send(sockID,writeBuff,req_packet.length()+1,0);
        if(returnStatus<0){
                log.logError("Cannot send [txid:"+txid+"] ");
                //exit(1);
		//return -1;
		if(variation==1)
		        return 4;
		else if(variation==2)
		        return 6;
        }
	else{
		log.logNotice("[txid:"+txid+"] Packet sent:"+req_packet);

		//**** Setup timeout
	        tv.tv_usec=0;
       		tv.tv_sec =port_reg.mp_resp_timeout;
        	setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

        	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
        	readBuff[returnStatus]='\0';
        	if(returnStatus<0){
                	//**** If timeout occurs
                	if (returnStatus==-1){
                        	log.logWarn("Timeout, while waiting for refund reply! [txid:"+txid+"] ");
                        	close(sockID);
                        	//exit(1);
				//return -1;
				if(variation==1)
				        return 4;
				else if(variation==2)
				        return 6;
                	}
                	else{
                        	log.logError("Error receiving query resp from MP [txid:"+txid+"] ");
                        	close(sockID);
                        	//return -1;
				if(variation==1)
				        return 4;
				else if(variation==2)
				        return 6;
                	}
        	}
		else{ //*** packet successfully received
			res_packet=buffToStr(readBuff);
	                log.logNotice("[txid:"+txid+"] Received packet:"+res_packet);

			if(mpcoder.getRefundReturnStatus(res_packet)==0){

				close(sockID);
				return 3;
			}
			else{
				close(sockID);
				if(variation==1)
					return 4;
				else if(variation==2)
					return 6;
			}

		}

	}


}


//convert float to 2 decimal point string
string IN_Conn::floatToString2d(float floatVal) {
	char charVal[100];
	sprintf(charVal,"%.2f",floatVal);
	string strVal(charVal);
	return strVal;
}

string IN_Conn::currentTimestamp(void){

	char timestamp[25];
                string currentdatetime;
                time_t mytime;
                struct tm *mytm;
                mytime=time(NULL);
                mytm=localtime(&mytime);

                strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

                currentdatetime = (string)timestamp;
                return currentdatetime;


}

long int IN_Conn::HexStrToInt(string hexStr){

        string tempHexStr="";
        int i;

        for(i=0;i<hexStr.length();i++){
                if(hexStr.substr(i,1)!=" ")
                        tempHexStr=tempHexStr+hexStr.substr(i,1);
        }

        return strtol(tempHexStr.c_str(),NULL,16);

}
