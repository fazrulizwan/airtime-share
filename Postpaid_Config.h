#ifndef POSTPAID_CONFIG_H_
#define POSTPAID_CONFIG_H_


using namespace std;

typedef struct PostpaidConfigStruct{

	int postpaid_conn_prof_count;
	string postpaid_ip_addrs[200];
	int postpaid_port[200];
	int max_retry_count;
	long retry_delay;
	int load_status;
	int in_resp_timeout;
	string log_path;

}PostpaidConfig_t;

	class PostpaidConfig {
    	public:
    		PostpaidConfig_t ReadConfig(string);

	private:
		void trim(string&);


    	};

#endif /*BUSINESSRULES_H_*/
