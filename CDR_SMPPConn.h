#ifndef CDR_SMPPCONN_H_
#define CDR_SMPPCONN_H_


#include <iostream>

using namespace std;

class CDR_SMPPConn{

	public:
		//string txid, string gw_ts, string oadc, string adc, string msg, string flag
		int insertInATS(string,string,string,string,string,string);	
		int insertOutATS(string,string,string,string,string,string);
		int insertInATR(string,string,string,string,string,string);
		int insertOutATR(string,string,string,string,string,string);
		int insertInHelp(string,string,string,string,string,string);
		int insertOutHelp(string,string,string,string,string,string);

};

#endif

