#ifndef IN_CONFIG_H_
#define IN_CONFIG_H_

using namespace std;

typedef struct INConfigStruct{

	int huawei_port_count;
	int huawei_port_list[10];
	int ericson_port_count;
	int ericson_port_list[10];
	int max_retry_count;
	long retry_delay;
	string huawei_in_ip_addrs;
	string huawei_in_usr_name;
	string huawei_in_pwd;
	string ericson_in_ip_addrs;
	string ericson_in_usr_name;
	string ericson_in_pwd;
	int load_status;
	int in_resp_timeout;
	string log_path;

}INConfig_t;

	class INConfig {
    	public:
    		INConfig_t ReadConfig(string);

	private:	
		void trim(string&);
	
		
    	};
	
#endif /*BUSINESSRULES_H_*/
