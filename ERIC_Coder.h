#ifndef ERIC_CODER_H_
#define ERIC_CODER_H_


#include <iostream>

const int ERIC_RESP_OK=0;
const int ERIC_RESP_UNKNOWN=99;

typedef struct ERICUSRProfileStruct{

        std::string msisdn;
        float balance;
        int status;
        std::string activation_date;
        std::string expiry_date;

}ERICUSRProfile;

class ERICCoder{

	public:
	//**** Username, password
	std::string getEricLogin(std::string,std::string);
	std::string getEricLogout(std::string);
	std::string getEricQuery(std::string);
	std::string getEricDeduct(std::string,float);
	std::string getEricRefill(std::string,std::string,float);
	int decodeEricResp(std::string);
	ERICUSRProfile getQueryProfile(std::string);
	int  getBytes (char *, std::string string );

	private:
	float fround(float,unsigned);
	std::string floatToStr(float);
	std::string buffToStr(char *);
	 long int HexStrToInt(std::string);


};

#endif
