#ifndef NGINCODER_H_
#define NGINCODER_H_

#include <iostream>

//**** Constants
const short int ACK=0;
const short int NACK=-1;
const short int EXT_FAIL=-2;

typedef struct MP_Usr_Profile_Struct{
	std::string msisdn;
	float balance;
	std::string expiry_date; //*** ddmmyyyy
	int status; //*** 1-Pre-registered 2-active 4-terminated
	int platform; //*** 0:NEC 1:Logica -9:postpaid
	int language; //*** 1:BM 2:English 3:Mandarin
	int brand; //*** 1 � Max 2 � Xceed 3 � Xplore 4 � InTm 5 � TA 6 � Max 7 � Mid 8 - Lite
	std::string activation_date;
	int inp; //*** -9 � Postpaid, 0 � NEC, 1 � 019 1st Quad, 2 � TA 1st Quad, 3 � TA 2nd Quad ( N Class ), 4 � 019 2nd Quad ( K Class).
}MP_Usr_Profile;

typedef struct MP_Transfer_Status_Struct{
	int transfer_status;	//*** 1:success -1:failure (reason unknown)
	float new_balance;
	std::string new_expiry_date;
}MP_Transfer_Status;

class NginCoder{

	public:
	//*** txid,msisdn,nettype
	std::string getQueryPacket(std::string,std::string);
	std::string getDebitPacket(std::string, float, int, std::string, int, std::string, std::string);
	std::string getRefundPacket(std::string, std::string, float, std::string, std::string);
	std::string getCreditPacket(std::string, float, int, std::string, int, std::string, std::string);
	std::string getVerifyPacket(std::string, std::string, int, std::string);
	int getReturnStatus(std::string);
	int getVerifyReturnStatus(std::string);
	int getRefundReturnStatus(std::string);
	MP_Usr_Profile loadUserProfile(std::string);
	MP_Transfer_Status LoadTransferStatus(std::string);

	private:
	std::string Trail3Zero(int);
	std::string IntToString(int);
	std::string floatToStr(float);
	std::string buffToStr(char *);
	int StrToInt(std::string);
	float StrToFloat(std::string);
	float Round(float,int);
	std::string trim(std::string&);
	std::string floatToString2d(float);
	std::string floatToString0d(float);
};

#endif
