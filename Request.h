#include <iostream>

#include "FileLogger.h"
#include "BusinessRules.h"
#include "CoreProcess.h"
#include "MTMessage.h"
#include "MysqlConnDB.h"

using namespace std;

namespace ats {
          class Request {
                public:
                       Request(string, Subscriber, int, string, string,  const BusinessRules&, const FileLogger&, const MTMessage&, PortReg, short int, MysqlConnDB&, short int, short int, string, short int);
                       MTCollection makeRequest();
                       MTCollection confirmRequest();
                       MTCollection cancelRequest();
                       MTCollection blockRequest();
                private:
                       string a_phone;
                       string b_phone;
                       Subscriber profileA;
                       int a_type;
                       int b_type;
                       int amt;
                       string pin;
                       string tx_id;
                       FileLogger logger;
                       BusinessRules bizrules;
                       MTCollection MTMsgOut; 
                       MTMessage mtLibrary;
                       PortReg inPort;
                       short int channel;
                       short int langA;
                       short int langB;
                       short int operation_type;
                       string requestor_name;
                       MysqlConnDB mySQLConn;
                       short int QueryProfileIN(Subscriber&,FileLogger);
		       short int smsc_id;
          };
}

