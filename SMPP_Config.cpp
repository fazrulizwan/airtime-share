#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "SMPP_Config.h"

using namespace std;

	SMPPConfig_t SMPPConfig::ReadConfig(string conf_file) {
		string line;
		string name;
		string value;
		int posEqual;
		SMPPConfig_t inconf;
		ifstream fileConfig (conf_file.c_str());

		//**** Init of conf struct
		inconf.port=-1;
		inconf.max_concurrency=-1;
		inconf.smsc_ip_addrs="";
		inconf.keep_alive=-1;
		inconf.load_status=-1;

		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;

				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trim(name);
				value = line.substr(posEqual+1);
				trim(value);
				//cout << "[Name:" << name << "=" << value << "]";

				if (name=="port") {
					inconf.port=atoi(value.c_str());
					continue;
				} else if (name=="smsc_ip_addrs") {
					inconf.smsc_ip_addrs = value;
					continue;
				}
				else if (name=="max_concurrency") {
					inconf.max_concurrency = atoi(value.c_str());
					continue;
				}
				else if (name=="keep_alive") {
					inconf.keep_alive = atoi(value.c_str());
					continue;
				}
				else if (name=="flood_prune") {
					inconf.flood_prune = atoi(value.c_str());
					continue;
				}
				else if (name=="debug") {
					inconf.debug = atoi(value.c_str());
					continue;
				}
				else if (name=="smsc_sys_id") {
					inconf.smsc_sys_id = value;
					continue;
				}
				else if (name=="smsc_pwd") {
					inconf.smsc_pwd = value;
					continue;
				}
				else if (name=="db_ip_addrs") {
					inconf.db_ip_addrs = value;
					continue;
				}
				else if (name=="db_port") {
					inconf.db_port = atoi(value.c_str());
					continue;
				}
				else if (name=="db_name") {
					inconf.db_name = value;
					continue;
				}
				else if (name=="db_pwd") {
					inconf.db_pwd = value;
					continue;
				}
				else if (name=="db_usr_name") {
					inconf.db_usr_name = value;
					continue;
				}
				else if (name=="log_path") {
					inconf.log_path = value;
					continue;
				}
				else if (name=="smsc_id") {
					inconf.smsc_id = atoi(value.c_str());
					continue;
				}

			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			inconf.load_status=1;
			return inconf;
		} else {
			//cout << "Cannot open config file!";
			return inconf;
		}
	}


void SMPPConfig::trim(string& str)
{
        string::size_type pos1 = str.find_first_not_of(' ');
        string::size_type pos2 = str.find_last_not_of(' ');
        str = str.substr(pos1 == string::npos ? 0 : pos1,
        pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

