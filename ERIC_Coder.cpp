#include <iostream>
#include <math.h>

#include "ERIC_Coder.h"

using namespace std;

string ERICCoder::getEricLogin(string usr_name, string pwd){

	//return "LOGIN:"+usr_name+":"+pwd+"";
	return "LOGIN:"+usr_name+":"+pwd;
}

string ERICCoder::getEricLogout(string usr_name){

	return "LOGOUT:"+usr_name+";";

}

int ERICCoder::decodeEricResp(string resp_packet){

	int resp_status;

	resp_status=atoi(resp_packet.substr(5,1).c_str());

	if(resp_status==0)
		return ERIC_RESP_OK;
	else
		return ERIC_RESP_UNKNOWN;

}



float ERICCoder::fround(float n, unsigned d)
{
        return floor(n * pow(10., d) + .5) / pow(10., d);
}

string ERICCoder::getEricQuery(string msisdn){

	return "GET:ACCOUNTINFORMATION:SubscriberNumber,"+msisdn+";";

}


string ERICCoder::getEricDeduct(string msisdn, float amt){

	string retval;

	retval="SET:ACCOUNTADJUSTMENT:SubscriberNumber,"+msisdn+":Currency,1:Action,SUBTRACT:AdjustmentAmount,"+floatToStr(amt)+";";

	return retval;

}

string ERICCoder::getEricRefill(string tx_id,string msisdn,float amt){

	return "SET:REFILL:SubscriberNumber,"+msisdn+":TransactionAmount,"+floatToStr(amt)+":Currency,10:PaymentProfile,5:ExternalData1,"+tx_id+":ExternalData2,quickshare_refill;";	

}


ERICUSRProfile ERICCoder::getQueryProfile(string packet){

	ERICUSRProfile prof;
	int start_pos;
	int end_pos;
	string str_balance;

	//**** MSISDN	
	start_pos=packet.find(":MasterSubscriberNumber,",0);
	end_pos=packet.find(":",start_pos+1);
	start_pos=start_pos+24; //*** pos at value, after field name
	prof.msisdn=packet.substr(start_pos,end_pos-start_pos);

	//**** BALANCE
	start_pos=packet.find(":AccountBalance,",0);
        end_pos=packet.find(":",start_pos+1);
        start_pos=start_pos+16; //*** pos at value, after field name
        str_balance=packet.substr(start_pos,end_pos-start_pos);
	prof.balance=atof(str_balance.c_str());

	//**** STATUS
	start_pos=packet.find(":Status,",0);
        end_pos=packet.find(":",start_pos+1);
        start_pos=start_pos+8; //*** pos at value, after field name
	//cout << "debug: "<< packet.substr(start_pos,end_pos-start_pos) << endl;
	//cout << "start:" << packet.substr(end_pos,1) << endl;
        if(packet.substr(start_pos,end_pos-start_pos)=="Active")
        	prof.status=2;
	else
		prof.status=-1;

	//**** ACTIVATION DATE
	start_pos=packet.find(":ActivationDate,",0);
        end_pos=packet.find(":",start_pos+1);
        start_pos=start_pos+16; //*** pos at value, after field name
        prof.activation_date=packet.substr(start_pos,end_pos-start_pos);
	
	//**** EXPIRY DATE
	start_pos=packet.find(":SupervisionPeriodExpiryDate,",0);
        end_pos=packet.find(":",start_pos+1);
        start_pos=start_pos+29; //*** pos at value, after field name
        prof.expiry_date=packet.substr(start_pos,end_pos-start_pos);

	return prof;
}


string ERICCoder::floatToStr(float val){

        char buff[20];

        sprintf(buff,"%.2f", val);
        return buffToStr(buff);

}


string ERICCoder::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


int  ERICCoder::getBytes (char *byteArray, string plainText){

       int i;
        string token;
        int dataLen=0;

        //*byteArray='\2';
        //byteArray++;

        for(i=0;i<plainText.length();i+=2){
                dataLen++;
                token=plainText.substr(i,2);
                *byteArray=(unsigned char)HexStrToInt(token);
                byteArray++;
                //cout << "Token:" << HexStrToInt(token) << endl;
        }
        //*byteArray=(unsigned char)13;
        //byteArray++;
        //*byteArray=(unsigned char)3;
        *byteArray='\0';
        return dataLen;


        //cout << "Token:" << byteArray << endl; 
}


long int ERICCoder::HexStrToInt(string hexStr){

        string tempHexStr="";
        int i;

        for(i=0;i<hexStr.length();i++){
                if(hexStr.substr(i,1)!=" ")
                        tempHexStr=tempHexStr+hexStr.substr(i,1);
        }

        return strtol(tempHexStr.c_str(),NULL,16);

}

