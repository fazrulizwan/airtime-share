#ifndef SMSC_CONFIG_H_
#define SMSC_CONFIG_H_

using namespace std;

typedef struct SMSCConfigStruct{

	int smsc_port;			//**** SMSC port no
	string smsc_ip_addrs;		//**** SMSC IP addrs
	string smsc_shortcode;		//**** SMSC sys id
	string smsc_pwd;		//**** SMSC pwd
	int max_concurrency;		//**** No. of child process allowed 0-unlimited
						//(not recommended)
	int keep_alive;			//**** Delay in secs to send enquire link op, 0-
						//never send enquire link op
	int load_status;		//**** Conf loadins status -ve means failure
	int flood_prune;		//**** anti flooding aging mechanism
	int debug;			//**** Debugging mode
	string db_ip_addrs;		//**** db IP
	int db_port;			//**** Port
	string db_name;			//**** db name
	string db_usr_name;		//**** db user name
	string db_pwd;			//**** db password
	string log_path;		//*** log path
	string instance_id;
	int smsc_id;

}SMSCConfig_t;

	class SMSCConfig {
    	public:
    		SMSCConfig_t ReadConfig(string);

	private:
		void trim(string&);


    	};

#endif /*BUSINESSRULES_H_*/
