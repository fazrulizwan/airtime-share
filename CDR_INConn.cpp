#include <iostream>
#include <fstream>

#include "CDR_INConn.h"

using namespace std;

int CDR_INConn::insertDebitCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/DebitCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}


int CDR_INConn::insertCreditCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/CreditCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}


int CDR_INConn::insertChargeCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/ChargeCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}

int CDR_INConn::insertChargeFailCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/Fail_ChargeCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}



int CDR_INConn::insertRefundCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/RefundCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}

int CDR_INConn::insertRefundFailCDR(string txid, string ts, string msisdn, string amt, string net_type, string flag){

	ofstream file;
	const char *cdr_file="/quickshare/in_cdr/Fail_RefundCDR.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << ts << "|" << msisdn << "|" << amt << "|" << net_type << "|" << flag << "\n";
	file.close();
	return 0;	

}

