#ifndef CDR_MP_H_
#define CDR_MP_H_


#include <iostream>

using namespace std;

class CDR_MP{

	public:
		//string txid, string gw_ts, string oadc, string adc, string msg, string flag
		int insertDebitCDR(string,string,string,string,string);	
		int insertCreditCDR(string,string,string,string,string);
		int insertRefundCDR(string,string,string,string,string);
		int insertRefundFailCDR(string,string,string,string,string);
		
};

#endif


