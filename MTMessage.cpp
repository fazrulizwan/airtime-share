#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "MTMessage.h"
#include "generalfunction.h"

using std::string;

namespace ats {

	bool MTMessage::ReadMessage() {
		string line;
		string name;
		string value;
		int posEqual;
		int msg_index;
		
		//read config for malay language
		ifstream fileConfig ("mt_m.config");
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				if (line.substr(0,3)=="dir") {
					posEqual=line.find('=');
					name = line.substr(4,posEqual-4);
					trimString(name);
					value = line.substr(posEqual+1);
					trimString(value);
					
					if (name=="log") {
						dir_log=value;
						cout << "value for dir.log:" << value;
					} else if (name=="error") {
						dir_error=value;
					} else if (name=="ats_tx") {
						dir_ats_tx=value;
					} else if (name=="atr_tx") {
						dir_atr_tx=value;
					} else if (name=="ats_success") {
						dir_ats_success=value;
					} else if (name=="atr_success") {
						dir_atr_success=value;
					} else if (name=="atr_pending") {
						dir_atr_pending=value;
					} else if (name=="ats_pending") {
						dir_ats_pending=value;
					} else if (name=="ussd_tx") {
						dir_ussd_tx=value;
					} else if (name=="help_tx") {
						dir_hlp_tx=value;
					}
				} else {
					posEqual=line.find('=');
					name = line.substr(0,posEqual);
					trimString(name);
					msg_index = stringToInt(name);
					value = line.substr(posEqual+1);
					trimString(value);
					msg_m[msg_index] = value;
				}
			}
			fileConfig.close();
		} 
		
		//read config for english language
		ifstream fileConfig2 ("mt_e.config");
		if (fileConfig2.is_open()) {
			while (!fileConfig2.eof()) {
				getline (fileConfig2,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trimString(name);
				msg_index = stringToInt(name);
				value = line.substr(posEqual+1);
				trimString(value);
				msg_e[msg_index] = value;
			
			}
			//Finish reading config file
			fileConfig2.close();
			return true;
		} else {
			//Cannot open config file
			return false;
		}
	}
	
	string MTMessage::getMsg(int msgIndex, short int lang) {
		if (lang==1) 
			return msg_m[msgIndex];
		else
			return msg_e[msgIndex];
	}
	
	string MTMessage::getMsg(int msgIndex, short int lang, short int brand) {
		string msg;
		if (lang==1) {
			msg = msg_m[msgIndex];
		} else {
			msg = msg_e[msgIndex];
		}
		//14 - MTRADE
		//24 - BLUE
		if (brand==14)
			msg = find_replace_celcom(msg,"Celcom","MTRADE");
		else if (brand==24 || brand==28)
			msg = find_replace_celcom(msg,"Celcom","BLUE");
		
		return msg;
	}
}

