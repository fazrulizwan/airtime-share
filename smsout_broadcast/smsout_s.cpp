/**************************************************************************
        SMSOUT ver 1.0
        23 September 2008

        Receives requests via socket to send MT to SMSC

        Developed by Fazrul Izwan Hassan
        Copyrights (C) Idottv Sdn Bhd 2008

        "..when you breathe, I want to be the air for you..." - a song

**************************************************************************/

#include <map>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <errno.h>
#include <sstream>
#include <arpa/inet.h>
#include <time.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <cstdlib>
#include <ctime>

#include "FileLogger.h"
#include "Data.h"
#include "UCPCoder.h"

using namespace std;
using namespace ats;

//*** Key seq no
long int key_seq; //** Range 1-999999

//*** Mapping objs
map<long int, Data> qry_map;
map<long int, Data> res_map;

//*** Thread procedures
void* do_qs(void *arg);
void* do_inq(void *arg);
void* do_inq_child(void *arg);
void* thread_proc(void *arg);
void* thread_rcvr(void *arg);

//*** Query Mutex
pthread_mutex_t qry_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t res_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t seq_mutex = PTHREAD_MUTEX_INITIALIZER;

//*** Queue Control
int MAX_QUEUE;

//**** flag, if true then its time to send SMPP enquire link, shared resource
static bool sh_is_time_keep_alive;

//*** flag, shared between threads, if true then reset alarm
bool sh_ok_reset_alarm;

void catch_alarm(int signo){

        //**** alarm signal handler

        //**** Ok time is high, we set the flag now its time to keep alive
        sh_is_time_keep_alive=true;
        //** Re-enable signal handler
        signal (signo,catch_alarm);
}

string IntToString ( long int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


string buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string trim2zero(string num){
       
        if(num.length()==1)
                return "0"+num;
        else
                return num;
}




static void bail(const char *on_what, bool resume) {

        /***********************************
        bail
        to report detailed reason of error
        on_what: the operation
        resume: set to false to halt program
        ************************************/

        string errMsg;
        FileLogger log("smsout.log");

        errMsg=buffToStr(strerror(errno))+": "+buffToStr((char*) on_what);
        log.logError(errMsg);

        if(!resume){
                log.logWarn("SMSOUT terminated normally, with error");
                exit(1);
        }
}


void initMT(MT_t& mt){

        mt.oadc="";
        mt.adc="";
        srand((unsigned)time(0));
        mt.seq_no=(rand()%99)+1;
        mt.msg="";

}



int main(){

        struct sockaddr_in locAddr;
        int returnStatus;
        int sockID;
        int connSockID;
        pthread_t qs_th_id, inq_th_id;
        int listen_port=10008;
        int flag=1;
        int inq_count=10;
        char *local_ip_addrs="10.122.17.102";
	FileLogger log("smsout.log");
	
	MAX_QUEUE=500;

	//*** initial key value
        key_seq=1;	

	//**** Init keep alive val
        sh_is_time_keep_alive=false;
        sh_ok_reset_alarm=false;


	//***** get a socket FD
        sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",false);
        }

        returnStatus = setsockopt(sockID, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
        if (returnStatus < 0) {
                exit(1);
        }

	//***** set destination address struct
        bzero(&locAddr,sizeof(locAddr));
        locAddr.sin_family=AF_INET;
        locAddr.sin_addr.s_addr=inet_addr(local_ip_addrs);
        locAddr.sin_port=htons(listen_port);

        //**** Let's bind to local port
        returnStatus = bind(sockID, (struct sockaddr *) &locAddr, sizeof(locAddr));
        if (returnStatus < 0) {
                log.logError("Failed to bind");
                bail("bind (1)",false);
        }

        log.logNotice("Listening...");

	//**** Let's listen to incoming query
        returnStatus = listen(sockID, 5);
        if (returnStatus < 0) {
                log.logError("Failed to listen");
                bail("listen(2)",false);
        }

        //*** Thread(s) spawner for INQ
        for(int i=1;i<=inq_count;i++){
                //*** Create thread for INQ
                returnStatus=pthread_create(&inq_th_id,NULL,do_inq,(void *) i);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        exit(1);
                }

                pthread_detach(inq_th_id);
                sched_yield();

        }

	
	//*** Loop for incoming packet
        while(1){
                //*** One connection connected..
                connSockID = accept(sockID, NULL,NULL);

                //*** ...and make it concurent
                returnStatus = pthread_create(&qs_th_id, NULL,do_qs, (void *) connSockID);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        exit(1);
                }

                pthread_detach(qs_th_id);
                sched_yield();

        }

        return 0;

}

string extractTags(string packet){

        /****************************
        Extract Tags out of Packet
        ****************************/

        int start_pos=0;
        int end_pos;

        for(int i=0;i<2;i++){
                start_pos=packet.find("|",start_pos+1);
        }

        end_pos=packet.find("|ETX",start_pos+1);

        cout << "end_pos:" << end_pos << endl;
        return packet.substr(start_pos+1,end_pos-start_pos-1)+"|";
}


string getTagValue(string tags,string key){

        /*************************
        Return tag value based on
        tag key given
        *************************/

        int start_pos=0;
        int end_pos;

        start_pos=tags.find(key,start_pos);
	if (start_pos==string::npos)
		return "";

        start_pos=start_pos+key.length()+1;
        end_pos=tags.find("|",start_pos);
	        //cout << "packet:" << tags << endl;
        //cout << "start_pos:" << start_pos << endl;
        //cout << "end_pos:" << end_pos << endl;
        return tags.substr(start_pos,end_pos-start_pos);

}


//**** Query Server: Serves Query
void* do_qs(void *arg){

        long int sockID;
        char readBuff[1028];
        char writeBuff[1028];
        int returnStatus;
        FileLogger log("smsout.log");
        string qry_packet;
        string res_packet;
        string tags;
        string opcode;
        string txid;
	string adc;
	string oadc;
        long int key;
        Data data;
        bool have_response=false;
        string packet_len;
        time_t start,finish;
        int map_item_count=0;
        int l_qs_timeout;


	sockID = (long int) arg;

	log.logDebug("New thread");

        returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
        readBuff[returnStatus]='\0';
        if(returnStatus<0){
                log.logError("[QS] Failed to Receive. Exiting");
		exit(1);
        }
	else{
                qry_packet=buffToStr(readBuff);
                log.logNotice("[QS] Received query packet:"+qry_packet);
                tags=extractTags(qry_packet);
                opcode=getTagValue(tags,"OPCODE");
                txid=getTagValue(tags,"TXID");
		adc=getTagValue(tags,"ADC");
		oadc=getTagValue(tags,"OADC");

                //***** Map Queue checking mechanism
		/*
                //*** Lock qry_map
                pthread_mutex_lock(&qry_mutex);
                //*** Browse qry map,and count no of item in the map
                for (map<long int, Data>::iterator it = qry_map.begin();
                	it != qry_map.end(); ++it) {
                        map_item_count++;
                }
                //*** Unlock qry_map
                pthread_mutex_unlock(&qry_mutex);
		*/

		//log.logDebug("debug:"+IntToString(qry_map.size()));
		map_item_count=qry_map.size();
                log.logDebug("Current map item:"+IntToString(map_item_count));

		//*** Current map item count exceeds what is allowable
                if(map_item_count>MAX_QUEUE){
                	log.logWarn("Map queue full,rejecting query...");
                        tags="QRYRES=RES|OPCODE="+opcode+"|TXID="+txid+"|ADC="+adc+"|OADC="+oadc+"|RESP=-99";
                        //*** get the packet len
                        packet_len=IntToString(tags.length()+12);
                        //*** Construct resp packet
                        res_packet="STX|"+packet_len+"|"+tags+"|ETX";
                        strcpy(writeBuff,res_packet.c_str());
                        returnStatus=send(sockID,writeBuff,res_packet.length()+1,0);
                        if(returnStatus<0){
                        	log.logError("Error sending response MAX");
                                exit(1);
                        }
                        else{
                        	log.logNotice("<"+txid+"><"+IntToString(key)+">Resp sent:"+res_packet);
                        }
                        close(sockID);
                        //*** Exit thread
                        pthread_exit(NULL);
                }

		//*** Lock key_seq
                pthread_mutex_lock(&seq_mutex);
                key=key_seq;                 //*** Max reset mechanism
                if(key_seq==999999)
                        key_seq=1;
                else
                        key_seq++;
                //*** Unlock key_seq
                pthread_mutex_unlock(&seq_mutex);

		log.logDebug("<"+txid+">[QS] Key for this MT:"+IntToString(key));

                //*** Load up tags into data
                data.setQRYTags(tags);

		//*** Lock qry_map
                pthread_mutex_lock(&qry_mutex);
                qry_map[key] = data;        //*** Insert data into map
                //*** Unlock qry_map
                pthread_mutex_unlock(&qry_mutex);

		//*** Start timer for QS-level timeout
                start=time(NULL);

                //*** OK lets see if INQ done its job
                while(1){
                        //*** Heartbeat
                        //usleep(200000);
			usleep(2000);

                        //*** QS-level timeout
                        finish=time(NULL);
                        //** Test whether we have reached timeout
                        if(finish-start>=50){
                                //**** Look for unfetched item in qry map
                                //*** Lock qry_map
                                pthread_mutex_lock(&qry_mutex);
                                //*** We browse the response map for the key
                                map<long int, Data>::iterator it = qry_map.find(key);
                                //*** Yes, there's item
                                if (it != qry_map.end()) {
                                        qry_map.erase(key); //*** Delete the map item
                                        log.logDebug("Item in QRY Map erased due to timeout");
                                }
                                //*** Unlock qry_map
                                pthread_mutex_unlock(&qry_mutex);

                                log.logWarn("<"+txid+"><"+IntToString(key)+">[QS] QS-level timeout reached. This thread is aborted.");
                                //*** Get out of loop. We finish.
				break;
			}

			//*** Lock qry_map
                                pthread_mutex_lock(&res_mutex);
                                //*** We browse the response map for the key
                                map<long int, Data>::iterator it = res_map.find(key);
                                //*** Yes, there's item
                                if (it != res_map.end()) {
                                        //*** get the resp tags
                                        tags=it->second.getRESTags();
                                        log.logNotice("<"+txid+"><"+IntToString(key)+">[QS] Map Response received. Tags:"+tags);
                                        res_map.erase(key); //*** Delete the map item
                                        have_response=true;
                                }
                                //*** Unlock qry_map
                                pthread_mutex_unlock(&res_mutex);
			
				//*** There's response
                        if(have_response){
                                //*** get the packet len
                                packet_len=IntToString(tags.length()+12);
                                //*** Construct resp packet
                                res_packet="STX|"+packet_len+"|"+tags+"|ETX";
				log.logDebug("respackt:"+res_packet);
                                strcpy(writeBuff,res_packet.c_str());
                                returnStatus=send(sockID,writeBuff,res_packet.length()+1,0);
                                if(returnStatus<0){
                                        log.logError("Error sending response");
                                        exit(1);
                                }
                                else{
                                        log.logNotice("<"+txid+"><"+IntToString(key)+">Resp sent:"+res_packet);
                                }
                                have_response=false;

                                //*** Get out of loop, since we finish
                                break;
				//close(sockID);
				//pthread_exit(NULL);
                        }


		}
	}
	close(sockID);
}



//**** SMSC INQ
void* do_inq(void *arg){

        string tags;
        FileLogger log("smsout.log");
        string txid;
        long int key;
        Data data;
        bool have_operation=false;
        string opcode;
        int returnStatus;
        struct sockaddr_in svrAddr;
        char writeBuff[1028];
        char readBuff[1028];
        int sockID;
        //char *mSvrAddr="192.168.118.129";
        //char *mSvrAddr="10.122.15.21";
        //int mSvrPort=3302;
	
	//---smsc S
	char *mSvrAddr="10.123.14.2";
	int mSvrPort=18248;
	
	//--smsc E
	//char *mSvrAddr="10.123.15.3";
        //int mSvrPort=19350;

        string ip_addrs;
        string reqPacket;
        string resPacket;
        unsigned long int sid=(unsigned long int) arg;
        int byteLen;
	bool isTimeout=false;
        bool stillRecv=true;
        bool willRestart=false;
        struct timeval tv;
        int l_in_resp_timeout;
        long int l_heartbeat_interval;
        int l_session_restart_interval;
        string in_user_name;
        string in_password;
	int flag=1;
	string strPDU;
	UCPCoder smppc;
	pthread_t inq_child_th_id,keep_th_id,rcvr_thread_id;

	//***** get a socket FD
        sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                log.logError("Error getting a socket FD");
                bail("socket(2)",false);
        }

        //***** set destination address struct
        bzero(&svrAddr,sizeof(svrAddr));
        svrAddr.sin_family=AF_INET;
        svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
        svrAddr.sin_port=htons(mSvrPort);

	//***** Let's connect to SMSC
        returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));
        // ** Fail to connect to SMSC
        if(returnStatus<0){
                log.logError("Error: cannot connect to SMSC");
                bail("connect(2)",false);
        }

        log.logNotice("Connected to SMSC");

	//*** Create thread for INQ
                returnStatus=pthread_create(&keep_th_id,NULL,thread_proc,(void *) sockID);
                if (returnStatus != 0) {
                        log.logError("Could not create thread.");
                        exit(1);
                }

		pthread_detach(keep_th_id);
                sched_yield();	

        //*** Turn off Nagle's algorithm
        setsockopt(sockID, IPPROTO_TCP, TCP_NODELAY, (char *) &flag,sizeof(int));

        //***** Establishing SMPP transceiver binding
        //strPDU=smppc.getOp60("1012","celcat1012");<-for nextgen
	//<-for smsc S
	strPDU=smppc.getOp60("40001","MYSMS40001");
	//strPDU=smppc.getOp60("1014","celcat1014");<- for smsc E
        returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
        //*** Cannot send TxRx PDU
        if(returnStatus<0){
                log.logError("Error: cannot establish bind handshake.");
                bail("send(2)",false);
        }

	//*** Wait for TxRx response from SMSC
        returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
        readBuff[returnStatus]='\0';
        resPacket=buffToStr(readBuff);
        //*** Error in reading PDU
        if(returnStatus<0){
                log.logError("error: cannot receive bind handshake response");
                bail("recv(2)",false);
        }
        else{
                log.logNotice("Received:: "+IntToString(returnStatus)+" bytes");
                log.logNotice("SMPP PDU received::"+ resPacket +"::");
                
                //*** ACK TxRx Received
                if(smppc.decodeResp(resPacket)==OP60_RESP_OK){
                        log.logNotice("Bind success.");
                }
                else{   //*** NACK TxRx Received
                        log.logWarn("Bind Fail");
                        exit(1);
                }
        }

	//***Thread for receiver
	returnStatus=pthread_create(&rcvr_thread_id,NULL,thread_rcvr,(void *) sockID);
        if (returnStatus != 0) {
              log.logError("Could not create thread.");
              exit(1);
        }
        pthread_detach(keep_th_id);
        sched_yield();

	
	while(1){
		//usleep(200000);
		usleep(2000);
		//*** Lock qry_map
                pthread_mutex_lock(&qry_mutex);
                //*** Browse qry map,see if we have operation
                for (map<long int, Data>::iterator it = qry_map.begin();
                        it != qry_map.end(); ++it) {
                        tags=it->second.getQRYTags();
                        log.logDebug("[INQ] Tags:"+tags);
                        key=it->first;
                        log.logDebug("[INQ] Key:"+IntToString(key));
                        qry_map.erase(key);
                        have_operation=true;
                        break;
                }
                //*** Unlock qry_map                 
		pthread_mutex_unlock(&qry_mutex);

		if(have_operation){
			opcode=getTagValue(tags,"OPCODE");
			log.logDebug("Opcode:"+opcode);
			if(opcode=="01"){
				//TODO: init op item in tcp map
				tags=tags+"|SOCKID="+IntToString(sockID)+"|KEY="+IntToString(key)+"|";
				log.logNotice("Received send MT request");
				returnStatus=pthread_create(&inq_child_th_id,NULL,do_inq_child,(void *) tags.c_str());
		                if (returnStatus != 0) {
                		        log.logError("Could not create thread.");
                      		        exit(1);
               			}

                		pthread_detach(inq_child_th_id);
                		sched_yield();
			}

			have_operation=false;

		} //** if have operation
	

	}

	close(sockID);
}


//**** SMSC CHILD INQ
void* do_inq_child(void *arg){

	string tags;
	int sockID;
	long int key;
	string strPDU;
	string adc;
	string oadc;
	string msg;
	string txid;
	string alphamask;
	int byteLen;
	int returnStatus;
	UCPCoder smppc;
	FileLogger log("smsout.log");
	MT_t mt;
	Data data;
	char writeBuff[1028];

	initMT(mt);

	tags=(char *) arg;
	sockID=dup(atoi(getTagValue(tags,"SOCKID").c_str()));
	key=atoi(getTagValue(tags,"KEY").c_str());
	adc=getTagValue(tags,"ADC");
	oadc=getTagValue(tags,"OADC");
	msg=getTagValue(tags,"MSG");
	txid=getTagValue(tags,"TXID");
	alphamask=getTagValue(tags,"ALPHAMASK");
	log.logDebug("Alphamask::"+alphamask+"::tags::"+tags);
	mt.adc=adc;
	mt.oadc=oadc;
	mt.msg=msg;

	if(alphamask=="" || alphamask=="0")		
		strPDU=smppc.getOp51(trim2zero(IntToString(mt.seq_no)),mt.oadc,mt.adc,mt.msg);
	else
		strPDU=smppc.getOp51Mask(trim2zero(IntToString(mt.seq_no)),mt.oadc,mt.adc,mt.msg);
        
        returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
        if(returnStatus<0){
                log.logError("Error while sending notification MT  txid["+txid+"](status:"+IntToString(returnStatus)+")");
                bail("send(2)",true);
		
		tags="QRYRES=RES|OPCODE=01|TXID="+txid+"|ADC="+adc+"|OADC="+oadc+"|"+"RESP=-2";
                //return -1;
        }
	else{
        	log.logNotice("SUBMIT_SM sent txid["+txid+"] ("+IntToString(returnStatus)+" bytes):"+strPDU);

		tags="QRYRES=RES|OPCODE=01|TXID="+txid+"|ADC="+adc+"|OADC="+oadc+"|"+"RESP=0";
		data.setRESTags(tags);

	}

	//TODO: update retry

	//TODO: to eliminate this writing to res_map
	//*** Lock qry_map
        pthread_mutex_lock(&res_mutex);
        res_map[key] = data;        //*** Insert data into map
        //*** Unlock qry_map
        pthread_mutex_unlock(&res_mutex);
         
	close(sockID);

}

void* thread_rcvr(void *arg){

	long int t_sockID;
        int t_returnStatus;
        //KeepAlive t_keep_alive;
        char t_readBuff[1028];
        int t_byteLen;
        string t_strPDU;
        UCPCoder ucpcoder;
        FileLogger t_log("smsout.log");

	t_sockID=dup((long int) arg);
	
	while(1){

	t_returnStatus=recv(t_sockID,t_readBuff,sizeof(t_readBuff)-1,0);
	t_readBuff[t_returnStatus]='\0';
	if(t_returnStatus<0){
		t_log.logError("Error: cannot receive packet.");
		close(t_sockID);
		exit(1);
	}
	else{
		t_strPDU=buffToStr(t_readBuff);
		if(ucpcoder.decodeResp(t_strPDU)==OP51_RESP_OK){
			t_log.logNotice("Received Op51 ACK: "+t_strPDU);
		}
		else{
			t_log.logNotice("Received other packet:"+t_strPDU);
		}	
	}

	}

}


void* thread_proc(void *arg){
        /********************
        thread_proc
        Receives sock fd from parent thread
        To handle keep-alive.
        ********************/

        long int t_sockID;
        int t_returnStatus;
        //KeepAlive t_keep_alive;
        int t_keep_alive_period;
        char t_writeBuff[128];
        int t_byteLen;
        string t_strPDU;
        UCPCoder t_smppc;
        FileLogger t_log("smsout.log");
	time_t start,finish;

        //**** Copying a sock FD from parent thread
        t_sockID=dup((long int) arg);

        //**** Registering signal handler
        //signal(SIGALRM,catch_alarm);

        //**** Registering alarm to wake up after keep alive period
        //alarm(20);

	start=time(NULL);
//**** Loop indefinately
        while(1){
		finish=time(NULL);
                //**** Check for flag,whether its time to reset alarm
                //if(sh_ok_reset_alarm){
                        //*** Resetting alarm
                //        alarm(20);
                //        sh_ok_reset_alarm=false;
                //        t_log.logDebug("Alarm has been reset");
               // }

                //**** Check flag,whether its time to send enquire link
                if(finish-start>=20){
                        t_log.logDebug("Sending enquire link");
                        t_strPDU=t_smppc.getKeepAlive();
                        t_returnStatus=send(t_sockID,t_strPDU.c_str(),t_strPDU.length()+1,0);
                        sh_is_time_keep_alive=false;
                        if(t_returnStatus<0){
                                t_log.logError("Error sending enquire link");
                                bail("send(2)",true);
                        }
                        else{
                                t_log.logNotice("Keep Alive sent ("+IntToString(t_returnStatus)+" bytes):"+t_strPDU);
                        }
			start=time(NULL);
			//sh_ok_reset_alarm=true;
                }

                sleep(1);

}

}


