#include <iostream>
#include <fstream>
#include <errno.h>
#include <dirent.h>
#include <vector>

#include "generalfunction.h"
#include <sys/time.h>
#include <time.h>
#include <mysql.h>

#include "MysqlConnDB.h"
#include "FileLogger.h"

using namespace std;

namespace Exporter {

	//---------to tokinize string--------------
	vector<string> Tokenize(const string str, const string delimiters = " ")
	{
	    vector<string> returnVector;
	    // Skip delimiters at beginning.
	    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	    // Find first "non-delimiter".
	    string::size_type pos     = str.find_first_of(delimiters, lastPos);

	    while (string::npos != pos || string::npos != lastPos)
	    {
	        // Found a token, add it to the vector.
	        returnVector.push_back(str.substr(lastPos, pos - lastPos));
	        // Skip delimiters.  Note the "not_of"
	        lastPos = str.find_first_not_of(delimiters, pos);
	        // Find next "non-delimiter"
	        pos = str.find_first_of(delimiters, lastPos);
	    }
	    return returnVector;
	}

	string currentTimestamp() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	long milliseconds;
	  	char stringMillisecond[100];
	  	struct timeval tv;
	  	gettimeofday (&tv, NULL);
	  	milliseconds = tv.tv_usec / 1000;
	  	strcat(timestamp,".");
	  	sprintf(stringMillisecond,"%d",milliseconds);
	  	strcat(timestamp,stringMillisecond);
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentTimestamp2() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m%d%H%M%S",mytm);

	  	long milliseconds;
	  	char stringMillisecond[100];
	  	struct timeval tv;
	  	gettimeofday (&tv, NULL);
	  	milliseconds = tv.tv_usec / 1000;
	  	sprintf(stringMillisecond,"%d",milliseconds);
	  	strcat(timestamp,stringMillisecond);
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}


	string currentDateTime() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateLimit() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m%d",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateTime2() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m%d%H%M%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateTime3() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string yesterdayDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t rawtime;
	  	struct tm *timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		timeinfo->tm_mday = timeinfo->tm_mday-1;
		mktime ( timeinfo );
		strftime(timestamp,sizeof timestamp,"%Y-%m-%d",timeinfo);
		currentdatetime = (string)timestamp;
		return currentdatetime;
	}

	string yesterdayDate2() {
		char timestamp[100];
		string currentdatetime;
	  	time_t rawtime;
	  	struct tm *timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		timeinfo->tm_mday = timeinfo->tm_mday-1;
		mktime ( timeinfo );
		strftime(timestamp,sizeof timestamp,"%Y%m%d",timeinfo);
		currentdatetime = (string)timestamp;
		return currentdatetime;
	}

	string tomorrowDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t rawtime;
	  	struct tm *timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		timeinfo->tm_mday = timeinfo->tm_mday+1;
		mktime ( timeinfo );
		strftime(timestamp,sizeof timestamp,"%Y%m%d",timeinfo);
		currentdatetime = (string)timestamp;
		return currentdatetime;
	}


	//change each element of the string to upper case
	string StringToUpper(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = toupper(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}

	//change each element of the string to lower case
	string StringToLower(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = tolower(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}

	//convert string to integer
	int stringToInt(string strValue) {
		const char *charConst = strValue.c_str();
		int intVal = atoi(charConst);
		return intVal;
	}

	//convert string to float
	float stringToFloat(string strValue) {
		const char *charConst = strValue.c_str();
		float floatVal = atof(charConst);
		return floatVal;
	}

	//convert string to constant char
	char *stringToChar(string strValue) {
		char *charVal;
		charVal = new char[strValue.length()+1];
		strcpy(charVal,strValue.c_str());
		return charVal;
	}

	//convert integer to string
	string intToString(int intVal) {
		char charVal[100];
		sprintf(charVal,"%d",intVal);
		string strVal(charVal);
		return strVal;
	}

	//convert float to string
	string floatToString(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%f",floatVal);
		string strVal(charVal);
		return strVal;
	}

	//convert float to 2 decimal point string
	string floatToString2d(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%.2f",floatVal);
		string strVal(charVal);
		return strVal;
	}


	string charToString(char* c) {
		std:string s(c);
		return s;
	}


	//concat string
	void stradd(char *s1, char *s2) {
		strcat(s1,s2);
	}

	void trimString(string& str)
	{
	  string::size_type pos = str.find_last_not_of(' ');
	  if(pos != string::npos) {
	    str.erase(pos + 1);
	    pos = str.find_first_not_of(' ');
	    if(pos != string::npos) str.erase(0, pos);
	  }
	  else str.erase(str.begin(), str.end());
	}

	string find_replace(string oriStr, string needle, string replaceWith) {
		size_t found;
		found = oriStr.find(needle);
		int startAt;
		if (found!=string::npos) {
			startAt= int(found);
			oriStr.replace(startAt,5,replaceWith);
		}
		return oriStr;
	}

	string find_and_replace_char(string oriStr, string needle, string replaceWith) {
		size_t found;
		found = oriStr.find(needle);
		int startAt;
		if (found!=string::npos) {
			startAt= int(found);
			oriStr.replace(startAt,1,replaceWith);
		}
		return oriStr;
	}

	int TimeDiffSec(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2);
		return tm1-tm2;
	}

	int TimeDiffSecDifDay(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2) + (24*60*60);
		return tm1-tm2;
	}

	int tmToInt(string tm) {
		int h = stringToInt(tm.substr(0,4));
		int m = stringToInt(tm.substr(4,2));
		int s = stringToInt(tm.substr(6,2));
		int tmInt = s + (m*60) + (h*60*60);
		return tmInt;
	}

	short int deleteFile(string fileName) {
		char *charFileName = stringToChar(fileName);
		short int resultRemove = remove(charFileName);
		//cout << "Delete file:" << fileName << " result:" << resultRemove;
		return resultRemove;
	}

	//---------check txid format--------------
	bool chkTxIDFormat(string txid)
	{
		int i;
        for (i=0;i<txid.length();i++) {
        	if (!isdigit(txid[i]))
        		return false;
        }
    	return true;
	}

	short int ChkTblExist(string tblName, MYSQL *myData, string logdir) {
		short int isExist=0;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;
		MYSQL_RES *res;
		MYSQL_ROW row;
		short int tblStat=0;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);


                sqlString="SELECT 1 FROM Information_schema.tables WHERE table_name = '"+tblName+"' AND table_schema = 'ats'";
                char *csql = stringToChar(sqlString);
                short int b=mysql_real_query(myData,csql,strlen(csql));

                if (b==0) {
                        res=mysql_store_result(myData);
                        if (mysql_affected_rows(myData)>0) {
                                row = mysql_fetch_row( res );
                                tblStat = stringToInt((row[0] ? row[0] : ""));
                        }
                } else {
                        string errorMsg(mysql_error(myData));
                        debugLogger.logError("Error in mysql"+errorMsg+" in sql:"+sqlString);
                }
                if (tblStat==1) {
                        isExist=1;
                        debugLogger.logDebug("[Table "+tblName+" already exist]");
                } else {
                        isExist=0;
                        debugLogger.logDebug("[Table "+tblName+" not exist yet]");
                }
		/*
		sqlString="SELECT 1 FROM "+tblName;
		char *csql = stringToChar(sqlString);
		short int b=mysql_real_query(myData,csql,strlen(csql));

	    if (b==0) {
	    	isExist=1;
	    	res=mysql_store_result(myData);
	    	mysql_free_result( res ) ;
	    	debugLogger.logDebug("[Table "+tblName+" already exist]");
	    } else {
	    	isExist=0;
	    	debugLogger.logDebug("[Table "+tblName+" not exist yet]");
	    }
*/
		delete[] csql;
		return isExist;
	}

	short int CreateTxTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  a_no varchar(20), ";
		sqlString+="  b_no varchar(20), ";
		sqlString+="  a_type tinyint(1), ";
		sqlString+="  b_type tinyint(1), ";
		sqlString+="  a_brand tinyint(1), ";
		sqlString+="  b_brand tinyint(1), ";
		sqlString+="  amt int(11), ";
		sqlString+="  tx_type tinyint(1), ";
		sqlString+="  status int(1), ";
		sqlString+="  req_date datetime, ";
		sqlString+="  transaction_id varchar(20), ";
		sqlString+="  refund_status tinyint(1) default 0,";
		sqlString+="  KEY req_date (req_date),";
		sqlString+="  KEY status (status),";
		sqlString+="  KEY tx_id (transaction_id),";
		sqlString+="  KEY ano_date (a_no,req_date),";
		sqlString+="  KEY status_amt_reqdate (status,amt,req_date),";
		sqlString+="  KEY date_n_status (req_date,status))  TYPE = MyISAM";
	    char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}


	short int CreateHelpTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  a_no varchar(20), ";
		sqlString+="  a_brand tinyint(1), ";
		sqlString+="  tx_type tinyint(1), ";
		sqlString+="  status int(1), ";
		sqlString+="  req_date datetime, ";
		sqlString+="  transaction_id varchar(20), ";
		sqlString+="  KEY req_date (req_date),";
		sqlString+="  KEY status (status),";
		sqlString+="  KEY tx_id (transaction_id),";
		sqlString+="  KEY ano_reqdate (a_no,req_date),";
		sqlString+="  KEY date_n_status (req_date,status))  TYPE = MyISAM ";
	    char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}

	short int CreateINTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  tx_id varchar(15), ";
		sqlString+="  phone varchar(20), ";
		sqlString+="  dt datetime,";
		sqlString+="  charge float(10,2),";
		sqlString+="  in_type tinyint(1),";
		sqlString+="  flag tinyint(1),";
		sqlString+="  stat tinyint(1),";
		sqlString+="  opp_msisdn varchar(20),";
		sqlString+="  serial_no varchar(20),";
		sqlString+="  adj_code varchar(20),";
		sqlString+="  result_code varchar(20),";
		sqlString+="  result_desc varchar(200),";
		sqlString+="  KEY tx_id (tx_id),";
		sqlString+="  KEY dt_type_stat (dt,in_type,stat),";
		sqlString+="  KEY dt_intype (dt,in_type))  TYPE = MyISAM";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}

	short int CreateINChargeTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  tx_id varchar(15), ";
		sqlString+="  phone varchar(20), ";
		sqlString+="  dt datetime,";
		sqlString+="  charge float(10,2),";
		sqlString+="  in_type tinyint(1),";
		sqlString+="  flag tinyint(1),";
		sqlString+="  stat tinyint(1),";
		sqlString+="  recharge_count tinyint(4),";
		sqlString+="  recharge_datetime datetime,";
		sqlString+="  recharge_status tinyint(4) DEFAULT '0',";
		sqlString+="  KEY tx_id (tx_id),";
		sqlString+="  KEY dt_type_stat (dt,in_type,stat),";
		sqlString+="  KEY dt_intype (dt,in_type))  TYPE = MyISAM";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}


	short int CreateIncomingTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  transaction_id varchar(20), ";
		sqlString+="  short_code varchar(20), ";
		sqlString+="  phone varchar(20), ";
		sqlString+="  content varchar(254), ";
		sqlString+="  connector_timestamp datetime, ";
		sqlString+="  tx_type tinyint(1), ";
		sqlString+="  KEY phone (phone),";
		sqlString+="  KEY in_date (connector_timestamp),";
		sqlString+="  KEY intype_timestamp (tx_type,connector_timestamp),";
		sqlString+="  KEY date_n_phone (phone,connector_timestamp))   TYPE = MyISAM";
	    char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}

	short int CreateOutgoingTable(string tblName, MYSQL *myData, string logdir) {
		short int createStatus=1;
		Exporter::MysqlConnDB mysqlObject;
		string sqlString;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		sqlString="create table "+tblName;
		sqlString+="( id integer primary key auto_increment, ";
		sqlString+="  transaction_id varchar(20), ";
		sqlString+="  phone varchar(20), ";
		sqlString+="  oadc varchar(20), ";
		sqlString+="  content varchar(254), ";
		sqlString+="  connector_timestamp datetime, ";
		sqlString+="  tx_type tinyint(1), ";
		sqlString+="  charge int, ";
		sqlString+="  KEY phone (phone),";
		sqlString+="  KEY in_date (connector_timestamp),";
		sqlString+="  KEY date_n_phone (phone,connector_timestamp))   TYPE = MyISAM";
	    char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	createStatus=0;
	    } else {
	    	createStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot create table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return createStatus;
	}

	short int InsertTransaction(string tblName, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string a_no, b_no, a_type, b_type;
		string a_brand, b_brand;
		string amt, status;
		string req_date, tx_id;
		string channel;
		string tx_type;
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string str;
		string::size_type pos;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}
		/*
		for( int i = 0; i < smsVector.size(); i++ ) {
			cout << " var[" << i << "] " << smsVector.at(i);
        }
		*/

		if (smsVector.size()<9) {
			debugLogger.logError("[Data not complete!]["+txData+"]");
			return 1;
		}
		a_no = smsVector.at(0);
		b_no = smsVector.at(1);
		a_type = smsVector.at(2);
		b_type = smsVector.at(3);
		a_brand = smsVector.at(4);
		b_brand = smsVector.at(5);
		amt = smsVector.at(6);
		tx_type = smsVector.at(7);
		status = smsVector.at(8);
		req_date = smsVector.at(9);
		tx_id = smsVector.at(10);
		tblName = tblName+req_date.substr(0,4)+req_date.substr(5,2);

		sqlString="INSERT INTO "+tblName;
		sqlString+=" (a_no, b_no, a_type, b_type, a_brand, b_brand, amt, tx_type, status, req_date, transaction_id) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+a_no+"','"+b_no+"','"+a_type+"', '"+b_type+"', '"+a_brand+"', '"+b_brand+"', '"+amt+"', '"+tx_type+"', '"+status+"', '"+req_date+"', '"+tx_id+"')";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert transaction into table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}

	short int InsertTransactionHelp(string tblName, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string a_no, b_no, a_type, b_type, a_brand;
		string amt, status;
		string req_date, tx_id, tx_type;
		string channel;
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string str;
		string::size_type pos;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}
		/*
		for( int i = 0; i < smsVector.size(); i++ ) {
			cout << " var[" << i << "] " << smsVector.at(i);
        }
		*/

		if (smsVector.size()<6) {
			debugLogger.logError("[Data not complete!]["+txData+"]");
			return 1;
		}
		a_no = smsVector.at(0);
		a_brand = smsVector.at(1);
		tx_type = smsVector.at(2);
		status = smsVector.at(3);
		req_date = smsVector.at(4);
		tx_id = smsVector.at(5);
		tblName = tblName+req_date.substr(0,4)+req_date.substr(5,2);

		sqlString="INSERT INTO "+tblName;
		sqlString+=" (a_no, a_brand, tx_type, status, req_date, transaction_id) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+a_no+"', '"+a_brand+"', '"+tx_type+"','"+status+"', '"+req_date+"', '"+tx_id+"')";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert help transaction into table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}


	short int InsertIncomingQueue(string tblName, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string phone;
		string shortcode, content;
		string tx_date, tx_id;
		string channel;
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string tx_type="0";
		string str;
		string::size_type pos;
		pos=0;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}
		/*
		for( int i = 0; i < smsVector.size(); i++ ) {
			cout << " var[" << i << "] " << smsVector.at(i);
        }
		*/

		if (smsVector.size()<6) {
			if (smsVector.at(0).length()==14 && chkTxIDFormat(smsVector.at(0))==true) {
				debugLogger.logError("[Data not complete and is header !]["+txData+"]");
				return 3;
			} else {
				debugLogger.logError("[Data not complete and not header !]["+txData+"]");
				return 2;
			}
		}
		tx_id = smsVector.at(0);
		shortcode = smsVector.at(1);
		phone = smsVector.at(2);
		content = smsVector.at(3);

		char *cContent = stringToChar(content);
		char *newContent = new char[strlen(cContent)*4+1];
		mysql_real_escape_string(myData, newContent, cContent, strlen(cContent));
		content = charToString(newContent);

		tx_date = smsVector.at(4);

		tblName = tblName+tx_date.substr(0,4)+tx_date.substr(5,2);

		sqlString="INSERT INTO "+tblName;
		sqlString+=" (transaction_id, short_code, phone, content, connector_timestamp) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+tx_id+"','"+shortcode+"','"+phone+"', '"+content+"', '"+tx_date+"')";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert help incoming into table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}

	short int InsertIncomingQueueSMPP(string tblName, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string phone;
		string shortcode, content;
		string tx_date, tx_id;
		string channel;
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string tx_type="0";
		string str;
		string::size_type pos;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}
		/*
		for( int i = 0; i < smsVector.size(); i++ ) {
			cout << " var[" << i << "] " << smsVector.at(i);
        }
		*/

		if (smsVector.size()<6) {
			if (smsVector.at(0).length()==14 && chkTxIDFormat(smsVector.at(0))==true) {
				debugLogger.logError("[Data not complete and is header !]["+txData+"]");
				return 3;
			} else {
				debugLogger.logError("[Data not complete and not header !]["+txData+"]");
				return 2;
			}
		}
		tx_id = smsVector.at(0);
		tx_date = smsVector.at(1);
		phone = smsVector.at(2);
		shortcode = smsVector.at(3);
		content = smsVector.at(4);

		char *cContent = stringToChar(content);
		char *newContent = new char[strlen(cContent)*4+1];
		mysql_real_escape_string(myData, newContent, cContent, strlen(cContent));
		content = charToString(newContent);

		tblName = tblName+tx_date.substr(0,4)+tx_date.substr(5,2);

		sqlString="INSERT INTO "+tblName;
		sqlString+=" (transaction_id, short_code, phone, content, connector_timestamp) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+tx_id+"','"+shortcode+"','"+phone+"', '"+content+"', '"+tx_date+"')";
		char *csql = stringToChar(sqlString);
		//cout << " sql:" << csql << "\n";
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert help smpp incoming into table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}

	short int InsertOutgoingQueue(string tblName, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string phone;
		string oadc, content;
		string tx_date, tx_id;
		string channel, charge;
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string tx_type;
		string str;
		string::size_type pos;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}

		if (smsVector.size()<6) {
			debugLogger.logError("[Data not complete!]["+txData+"]");
			return 1;
		}
		tx_id = smsVector.at(0);
		tx_date = smsVector.at(1);
		oadc = smsVector.at(2);
		phone = smsVector.at(3);
		content = smsVector.at(4);
		channel = smsVector.at(5);
		charge = smsVector.at(6);

		char *cContent = stringToChar(content);
		char *newContent = new char[strlen(cContent)*4+1];
		mysql_real_escape_string(myData, newContent, cContent, strlen(cContent));
		content = charToString(newContent);

		tblName = tblName+tx_date.substr(0,4)+tx_date.substr(5,2);
		sqlString="INSERT INTO "+tblName;
		sqlString+=" (transaction_id, phone, oadc, content, connector_timestamp, charge) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+tx_id+"','"+phone+"', '"+oadc+"','"+content+"', '"+tx_date+"', '"+charge+"')";
		char *csql = stringToChar(sqlString);
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert outgoing into table Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}

        short int InsertRefundCDR(string tblName, int stat, string txData, MYSQL *myData, string logdir) {
                short int insertStatus=1;
                string sqlString;
                string phone, flag;
                string tx_date, tx_id;
                string charge;
                string in_type;
                string opp_msisdn;
                string serial_no;
                string adj_code;
                string result_code;
                string result_desc;

                vector<string> smsVector;
                Exporter::MysqlConnDB mysqlObject;

                string logDebug = logdir + "/debug."+currentDate()+".log";
                FileLogger debugLogger (logDebug);

                int i=0;
                int strLength=0;
                string tx_type;
                string str;
                string::size_type pos;
                str = txData;
                strLength = str.length();
                while (strLength>0 && string::npos != pos) {
                        strLength = str.length();

                        pos = str.find_first_of("|");
                        //cout << "[length:" << strLength << "] [pos:" << pos << "]";
                        if (pos==0) {
                                smsVector.push_back("");
                                //cout << i << "=nothing\n";
                                str = str.substr(1);
                        } else {
                                smsVector.push_back(str.substr(0,pos));
                                //cout << i << "=" << str.substr(0,pos) << "\n";
                                str = str.substr(pos+1);
                        }

                        //cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
                        i++;
                }

                if (smsVector.size()<5) {
                        debugLogger.logError("[Data not complete!]["+txData+"]");
                        return 1;
                }
                tx_id = smsVector.at(0);
                tx_date = smsVector.at(1);
                phone = smsVector.at(2);
                charge = smsVector.at(3);
                flag = smsVector.at(4);

                tblName = tblName+tx_date.substr(0,4)+tx_date.substr(5,2);
                sqlString="INSERT INTO "+tblName;
                sqlString+=" (tx_id, phone, dt, charge, flag, stat) ";
                sqlString+= "VALUES ";
                sqlString+= "('"+tx_id+"','"+phone+"', '"+tx_date+"','"+charge+"', '"+flag+"','"+intToString(stat)+"')";
                char *csql = stringToChar(sqlString);
                short int b=mysql_real_query(myData,csql,strlen(csql));
            //printf(" b= %d ",b);
            if (b==0) {
                insertStatus=0;
            } else {
                insertStatus=1;
                string errorMsg(mysql_error(myData));
                debugLogger.logError("[Cannot insert into "+tblName+". Error:"+errorMsg+" in sql:"+sqlString+"]");
            }

                delete[] csql;
                return insertStatus;
        }



	short int InsertINCDR(string tblName, int stat, string txData, MYSQL *myData, string logdir) {
		short int insertStatus=1;
		string sqlString;
		string phone, flag;
		string tx_date, tx_id;
		string charge;
		string in_type;
		string opp_msisdn;
		string serial_no;
		string adj_code;
		string result_code;
		string result_desc;
		
		vector<string> smsVector;
		Exporter::MysqlConnDB mysqlObject;

		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);

		int i=0;
		int strLength=0;
		string tx_type;
		string str;
		string::size_type pos;
		str = txData;
		strLength = str.length();
		while (strLength>0 && string::npos != pos) {
			strLength = str.length();

			pos = str.find_first_of("|");
			//cout << "[length:" << strLength << "] [pos:" << pos << "]";
			if (pos==0) {
				smsVector.push_back("");
				//cout << i << "=nothing\n";
				str = str.substr(1);
			} else {
				smsVector.push_back(str.substr(0,pos));
				//cout << i << "=" << str.substr(0,pos) << "\n";
				str = str.substr(pos+1);
			}

			//cout << " pos:" << pos << " str:" << str << " length:" << strLength << "\n";
			i++;
		}

		if (smsVector.size()<5) {
			debugLogger.logError("[Data not complete!]["+txData+"]");
			return 1;
		}
		tx_id = smsVector.at(0);
		tx_date = smsVector.at(1);
		phone = smsVector.at(2);
		charge = smsVector.at(3);
		flag = smsVector.at(4);
		opp_msisdn = smsVector.at(5);
		serial_no = smsVector.at(6);		
		adj_code = smsVector.at(7);
		result_code = smsVector.at(8);		
		result_desc = smsVector.at(9);

		tblName = tblName+tx_date.substr(0,4)+tx_date.substr(5,2);
		sqlString="INSERT INTO "+tblName;
		sqlString+=" (tx_id, phone, dt, charge, flag, stat, opp_msisdn, serial_no, adj_code, result_code, result_desc) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+tx_id+"','"+phone+"', '"+tx_date+"','"+charge+"', '"+flag+"','"+intToString(stat)+"', '"+opp_msisdn+"', '"+serial_no+"', '"+adj_code+"', '"+result_code+"', '"+result_desc+"')";
		char *csql = stringToChar(sqlString);
		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	insertStatus=0;
	    } else {
	    	insertStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	debugLogger.logError("[Cannot insert into "+tblName+". Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return insertStatus;
	}
}

