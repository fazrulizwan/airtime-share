using namespace std;

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <mysql.h>

#include "MysqlConnDB.h"
#include "generalfunction.h"
#include "FileLogger.h"

namespace Exporter {

	//constructor
	MysqlConnDB::MysqlConnDB() {
	}

	//open connection to mysql database
	bool MysqlConnDB::OpenConnDB(string logdir, string host, string user, string pwd) {
		string logDebug = logdir + "/debug."+currentDate()+".log";
		FileLogger debugLogger (logDebug);
		debugLogger.logDebug("[Enter DB Conn Class]");
		if ((myData = mysql_init((MYSQL*) 0)) && mysql_real_connect( myData, stringToChar(host), stringToChar(user), stringToChar(pwd), "", MYSQL_PORT, NULL, 0 ) ) {
			debugLogger.logDebug("[Finish Establish Conn]");
			mysql_select_db(myData,"ats");
			debugLogger.logDebug("[Connection to DB successfull]");
			//cout << "Connection Successfull";
			return true;
		} else {
			//cout << "Cannot establish connection...\n";
			debugLogger.logError("[Cannot connect to Database]");
			return false;
		}
	}


	/*
	int main() {
		MYSQL *myData ;
		MYSQL_RES *res;
		MYSQL_ROW row;
		char msql[2048];
		int j,k;

		strcpy(msql,"select * from userprofile;");

		int a=mysql_real_query(myData,msql,strlen(msql));
		res=mysql_store_result(myData);
		row = mysql_fetch_row( res );

		j = mysql_num_fields( res ) ;
		for ( k = 0 ; k < j ; k++ ) {
			printf(" %s ",row[k]);
		}

		mysql_free_result( res ) ;

		mysql_close(myData);

		return 0;
	}
	*/
}
