#include <iostream>
#include <vector>
#include <mysql.h>

using namespace std;

namespace Exporter {

	
	//---------to tokinize string--------------
	vector<string> Tokenize(const string, const string );
	
	//--------to stamp current timestamp (Y-m-d H:i:s.ms)-------------------
	string currentTimestamp();
	
	//--------to stamp current timestamp (YmdHisms)-------------------
	string currentTimestamp2();
	
	//--------to stamp current date (Y-m-d)-------------------
	string currentDate();
	
	//to stamp current date to check limit (Ymd)
	string currentDateLimit();
	
	//to stamp current date time (YmdHis)
	string currentDateTime2();
	
	//to stamp current date time (Y-m-d H:i:s)
	string currentDateTime3();
	
	//to stamp current date (Y-m-d)
	string yesterdayDate();
	
	//to stamp current date (Ymd)
	string yesterdayDate2();
	
	//to stamp tomorrow date
	string tomorrowDate();
	
	//convert string to upper case
	string StringToUpper(string);
	
	//convert string to lower case
	string StringToLower(string);
	
	//convert string to int
	int stringToInt(string);
	
	//convert string to const char
	char const stringToConstChar(string);
	
	//convert int to string
	string intToString(int);
	
	//convert float to string
	string floatToString(float);
	
	//convert float to string in 2 floating point
	string floatToString2d(float);
	
	//convert char to string
	string charToString(char*);
	
	//convert string to float
	float stringToFloat(string);
	
	//convert string to char
	char *stringToChar(string);
	
	//trim string
	void trimString(string&);
	
	//find and replace string
	string find_replace(string,string,string);
	
	//find and replace 1 character
	string find_and_replace_char(string,string,string);
	
	//calculate time different in second within same day
	int TimeDiffSec(string,string);
	
	//calculate time different in second different day
	int TimeDiffSecDifDay(string,string);
	
	//convert time to int second
	int tmToInt(string);
	
	//delete pending file
	short int deleteFile(string);
	
	//check if table exist
	short int ChkTblExist(string, MYSQL *, string);
	
	//insert transaction log into database
	short int InsertTransaction(string, string, MYSQL *, string);
	
	//insert help transaction log into database
	short int InsertTransactionHelp(string, string, MYSQL *, string);
	
	//insert into incoming queue
	short int InsertIncomingQueue(string, string, MYSQL *, string);
	
	//insert into smpp incoming
	short int InsertIncomingQueueSMPP(string, string, MYSQL *, string);
	
	//insert into outgoing
	short int InsertOutgoingQueue(string, string, MYSQL *, string);
	
	//create transaction table for tomorrow
	short int CreateTxTable(string, MYSQL *, string);
	
	//create help transaction table for tomorrow
	short int CreateHelpTable(string, MYSQL *, string);
		
	//create incoming table for tomorrow
	short int CreateIncomingTable(string, MYSQL *, string);
	
	//create table outgoing for tomorrow
	short int CreateOutgoingTable(string, MYSQL *, string);
	
	//create table in cdr
	short int CreateINTable(string, MYSQL *, string);
	
	//create table in charge cdr
	short int CreateINChargeTable(string, MYSQL *, string);
	
	//insert into in cdr
	short int InsertINCDR(string, int, string, MYSQL *, string);
}
