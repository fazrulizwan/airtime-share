#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <dirent.h>

using namespace std;

#include "MysqlConnDB.h"
#include "FileLogger.h"
#include "generalfunction.h"
#include "ConfigReader.h"

using namespace Exporter;

int main(int argc, char *argv[])
{
	ConfigReader config;
	config.ReadConfig();

	string logExporter = config.logdir + "/debug."+currentDate()+".log";
	FileLogger exporterLogger (logExporter);
	exporterLogger.logDebug("Enter Exporter Program");

	//start reading file
	string line;
	short int insertStat;
	short int createStat1;
	short int createStat2;
	short int createStat3;
	short int createStat4;
	short int createStat5;
	short int createStat6;
	short int createStat7;
	short int createStat8;
	short int createStat9;
	short int chkTblStat1;
	short int chkTblStat2;
	short int chkTblStat3;
	short int chkTblStat4;
	short int chkTblStat5;
	short int chkTblStat6;
	short int chkTblStat7;
	short int chkTblStat8;
	short int chkTblStat9;
	string tx_month;
	DIR *pdir;
	DIR *pdir2;
	DIR *pdir3;
	DIR *pdir4;
	DIR *pdir5;
	DIR *pdir6;
	struct dirent *pent;
	string files;
	string searchName;
	string searchName1;
	string searchName2;
	string searchName3;
	string searchName4;
	string searchName5;
	string searchName6;
	MysqlConnDB mysqlObject;
	if (mysqlObject.OpenConnDB(config.logdir, config.db_host, config.db_username, config.db_password)) {

		//check if current transaction's month table exist
		tx_month = currentDateLimit().substr(0,6);
		chkTblStat6 = ChkTblExist("in_debit_"+tx_month, mysqlObject.myData, config.logdir);
		chkTblStat7 = ChkTblExist("in_credit_"+tx_month, mysqlObject.myData, config.logdir);
		chkTblStat8 = ChkTblExist("in_charge_"+tx_month, mysqlObject.myData, config.logdir);
		chkTblStat9 = ChkTblExist("in_refund_"+tx_month, mysqlObject.myData, config.logdir);
		exporterLogger.logDebug("Check table result: [result1:"+intToString(chkTblStat1)+"] [result2:"+intToString(chkTblStat2)+"] [result3:"+intToString(chkTblStat3)+"] [result4:"+intToString(chkTblStat4)+"] [result5:"+intToString(chkTblStat5)+"]");

		//create table if not exist

		if (chkTblStat6==0) {
			createStat6 = CreateINTable("in_debit_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table in debit result:"+intToString(createStat6));
		}
		if (chkTblStat7==0) {
			createStat7 = CreateINTable("in_credit_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table in credit result:"+intToString(createStat7));
		}
		if (chkTblStat8==0) {
			createStat8 = CreateINChargeTable("in_charge_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table in charge result:"+intToString(createStat8));
		}
		if (chkTblStat9==0) {
			createStat9 = CreateINTable("in_refund_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table in refund result:"+intToString(createStat9));
		}

		string tblName,sqlString,newFile;
		short int insertStatus;


		//export in CDR
		exporterLogger.logDebug("Try to open IN CDR directory..");
		string dirName6 = config.in_txdir;
		exporterLogger.logDebug("Finish find IN CDR file : "+dirName6);
		const char *cdirectoryName6= dirName6.c_str();
		string newFile6;
		string newCDRName;
		pdir6=opendir(cdirectoryName6); //open directory
		if (!pdir6){
			exporterLogger.logDebug("Cannot Open IN CDR Directory!");
		} else {
			while ((pent=readdir(pdir6))){
				files = charToString(pent->d_name);
				newCDRName="";
				if (files.length()>2) {
					exporterLogger.logDebug("Transaction file found:"+files);
					searchName1 = files.substr(0,6);
					searchName2 = files.substr(0,5);
					string filepath6 = dirName6 + "/" + files;
					const char *filepathname6= filepath6.c_str();
					ifstream fileTx6 (filepathname6);
					if (fileTx6.is_open()) {
						exporterLogger.logDebug("Finish Open directory.Reading file from in cdr directory..");
						while (!fileTx6.eof()) {
							getline (fileTx6,line);
							if (line!="") {
								if (searchName2.compare("Debit")==0) {
									exporterLogger.logDebug("Inserting into success debit cdr log database..");
									insertStat = InsertINCDR("in_debit_", 0, line, mysqlObject.myData, config.logdir);
									newCDRName = "Debit";
								} else if (searchName1.compare("Credit")==0) {
									exporterLogger.logDebug("Inserting into success credit cdr log database..");
									insertStat = InsertINCDR("in_credit_", 0, line, mysqlObject.myData, config.logdir);
									newCDRName = "Credit";
								} else if (searchName1.compare("Charge")==0) {
									exporterLogger.logDebug("Inserting into success charge cdr log database..");
									insertStat = InsertINCDR("in_charge_", 0, line, mysqlObject.myData, config.logdir);
									newCDRName = "Charge";
								} else if (searchName1.compare("Refund")==0) {
									exporterLogger.logDebug("Inserting into success refund cdr log database..");
									insertStat = InsertRefundCDR("in_refund_", 0, line, mysqlObject.myData, config.logdir);
									newCDRName = "Refund";
								} else {
									if (files.length()>11) {
										if (files.substr(0,11).compare("Fail_Charge")==0) {
											exporterLogger.logDebug("Inserting into fail charge refund cdr log database..");
											insertStat = InsertINCDR("in_charge_", 1, line, mysqlObject.myData, config.logdir);
											newCDRName = "Fail_Charge";
										} else if (files.substr(0,11).compare("Fail_Refund")==0) {
											exporterLogger.logDebug("Inserting into fail refund cdr log database..");
											insertStat = InsertRefundCDR("in_refund_", 1, line, mysqlObject.myData, config.logdir);
											newCDRName = "Fail_Refund";
										} else if (files.substr(0,14).compare("Ambigous_Debit")==0) {
											exporterLogger.logDebug("Inserting into ambigious debit cdr log database..");
											insertStat = InsertINCDR("in_debit_", 2, line, mysqlObject.myData, config.logdir);
											newCDRName = "Ambigious_Debit";
										} else if (files.substr(0,15).compare("Ambigous_Credit")==0) {
											exporterLogger.logDebug("Inserting into ambigious credit cdr log database..");
											insertStat = InsertINCDR("in_credit_", 2, line, mysqlObject.myData, config.logdir);
											newCDRName = "Ambigious_Credit";
										}
									}
								}
								exporterLogger.logDebug("Insert result:"+intToString(insertStat));
							}
						}


						newFile6 = config.in_cdr+"/"+currentTimestamp2()+"."+newCDRName+".CDR";
						exporterLogger.logDebug("Rename file to:"+newFile6);

						char *newname6 = stringToChar(newFile6);
						int resultRename6 = rename(filepathname6, newname6);
						if (resultRename6==0)
							exporterLogger.logDebug("Successfully rename file to:"+newFile6);
						else
				    	    exporterLogger.logDebug("Failed rename file:"+filepath6);
					} else {
						exporterLogger.logDebug("No IN cdr file to process");
					}
					fileTx6.close();
				}
			}
		}


		//finish exporting data
		mysql_close(mysqlObject.myData);
	} else {
		exporterLogger.logDebug("Cannot connect to database");
	}

    return EXIT_SUCCESS;
}


