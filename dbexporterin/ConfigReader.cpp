#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "ConfigReader.h"

using std::string;

namespace Exporter {
	bool ConfigReader::ReadConfig() {
		string line;
		string name;
		string value;
		int posEqual;
		ifstream fileConfig ("/home/cat/dbexporterin/dbexporter.config");
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;

				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				value = line.substr(posEqual+1);

				if (name=="logdir") {
					logdir = value;
					continue;
				} else if (name=="ats_txdir") {
					ats_txdir = value;
					continue;
				} else if (name=="atr_txdir") {
					atr_txdir = value;
					continue;
				} else if (name=="help_txdir") {
					help_txdir = value;
					continue;
				} else if (name=="db_host") {
					db_host = value;
					continue;
				} else if (name=="db_user") {
					db_username = value;
					continue;
				} else if (name=="db_pwd") {
					db_password = value;
					continue;
				} else if (name=="ats_cdr") {
					ats_cdr = value;
					continue;
				} else if (name=="atr_cdr") {
					atr_cdr = value;
					continue;
				} else if (name=="help_cdr") {
					help_cdr = value;
					continue;
				} else if (name=="incoming_txdir") {
					incoming_txdir = value;
					continue;
				} else if (name=="incoming_cdr") {
					incoming_cdr = value;
					continue;
				} else if (name=="smpp_txdir") {
					smpp_txdir = value;
					continue;
				} else if (name=="smpp_cdr") {
					smpp_cdr = value;
					continue;
				} else if (name=="in_txdir") {
					in_txdir = value;
					continue;
				} else if (name=="in_cdr") {
					in_cdr = value;
					continue;
				}
			}//end while
		}//end if
	}//end read config
}//end namespace
