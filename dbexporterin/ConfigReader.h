#ifndef CONFIGREADER_H_
#define CONFIGREADER_H_

using namespace std;

namespace Exporter {
	class ConfigReader {
    	public:
    		bool ReadConfig();
    		string logdir;
    		string ats_txdir;
    		string atr_txdir;
    		string help_txdir;
    		string ats_cdr;
    		string atr_cdr;
    		string help_cdr;
    		string smpp_txdir;
    		string smpp_cdr;
    		string db_host;
    		string db_username;
    		string db_password;
    		string incoming_txdir;
    		string incoming_cdr;
    		string in_txdir;
    		string in_cdr;
	};
}

#endif /*CONFIGREADER_H_*/
