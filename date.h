#ifndef _DATE_H_
#define _DATE_H_

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class date {
	protected:
		int year_;
		int month_;
		int day_;
	public:
		date();
		date(const long& d);
		date(const int& d, const int& m, const int& y);
		bool valid(void) const;
		int day() const;
		int month() const;
		int year() const;
		void set_day (const int& day );
		void set_month (const int& month );
		void set_year (const int& year );

		date operator ++(); // prex
		date operator ++(int); // postx
		date operator --(); // prex
		date operator --(int); // postx
	};

	date operator +=(date&, const int&); // increment and decrement
	date operator -=(date&, const int&); // (with number of days)

	bool operator == (const date&, const date&); // comparison operators
	bool operator != (const date&, const date&);
	bool operator < (const date&, const date&);
	bool operator > (const date&, const date&);
	bool operator <= (const date&, const date&);
	bool operator >= (const date&, const date&);

	ostream& operator << ( ostream& os, const date& d); // output operator
	long long_date(const date&); // calculations
	date next_date(const date&);
	date previous_date(const date&);
	date operator +(const date&, const int&); // add number of days
	date operator -(const date&, const int&);
	date next_month(const date& d); // specic incrementaiton
	date add_months(const date& d, const int& no_months);

	///////////////////////////// inline denitions //////////
	inline date::date(){ year_ = 0; month_ = 0; day_ = 0;};
	inline int date::day() const { return day_ ; };
	inline int date::month() const { return month_ ; };
	inline int date::year() const { return year_ ; };

	inline void date::set_day (const int& day) { date::day_ = day; };
	inline void date::set_month(const int& month) { date::month_ = month; };
	inline void date::set_year (const int& year) { date::year_ = year; };

#endif
