
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string>
#include <sstream>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <mysql.h>
#include <sys/time.h>
#include <time.h>

#include "ConfigReader.h"
#include "FileLogger.h"
#include "MysqlConnDB.h"
#include "generalfunction.h"

using namespace std;

using namespace Checker;

int main(int argc, char *argv[])
{

//cout << "Enter timeout checker";

ConfigReader config;
config.ReadConfig();

string reportLog = config.logdir +"/debug."+currentDate()+".log";
FileLogger reportLogger (reportLog);

MYSQL_RES *res;
MYSQL_ROW row;
MYSQL_RES *res2;
MYSQL_ROW row2;
MYSQL_ROW row3;
MYSQL_RES *res3;
MYSQL_ROW row4;
MYSQL_RES *res4;


//send sms to smsc
struct sockaddr_in serverAddress2;
int sockID2;
string res_packet2;
char writeBuff2[1028];
int returnStatus2;
int n2;
string msg1, msg2, msg3, msg4, msgDt ,msg ,msg5;
string msg6; //*** For POSTPAID ATS
string oadc2;
string channel2;
string yrMth = currentYrMth();
int totalTxATS, totalSuccessATS, totalUserFailATS, ATSerror_4, ATSerror_5, ATSerror_6, ATSerror_7, ATSerror_8, ATSerror_9, ATSerror_11, ATSerror_13, ATSerror_15, ATSerror_16, ATSerror_19;
int totalTxATR, totalSuccessATR, totalUserFailATR, ATRerror_4, ATRerror_5, ATRerror_6, ATRerror_7, ATRerror_8, ATRerror_9, ATRerror_11, ATRerror_13, ATRerror_15, ATRerror_16, ATRerror_19;
int totalTx, totalSuccess, totalUserFail, totalSystemFail;
int totalSysFailINCAT, totalSysFailINCAR, totalSysFailIdotCAT, totalSysFailIdotCAR,totalSysFailIN, totalSysFailIdot;
int codeStatus;
int bilATS, bilATR;
int seq=10;
string line;
char timestamp[100];
char timestamp2[100];
string yesterdayMth;
string yesterdayDt;
int bilATSpost=0;
int totalTxATSpost=0;
int totalSuccessATSpost=0;
int totalUserFailATSpost=0;
int bilATRpost=0;
int totalTxATRpost=0;
int totalSuccessATRpost=0;
int totalUserFailATRpost=0;
int totalSysFailINATSpost=0;
int totalSysFailIDOTATSpost=0;
int totalSysFailINATRpost=0;
int totalSysFailIDOTATRpost=0;

//query data
MysqlConnDB mysqlObject;
if (mysqlObject.OpenConnDB()) {
reportLogger.logDebug("[Open DB Connection]");

time_t myTime = time(NULL);
tm *myUsableTime = localtime(&myTime);
myUsableTime->tm_mday = myUsableTime->tm_mday-1;
mktime ( myUsableTime );
strftime(timestamp,sizeof timestamp,"%Y-%m-%d",myUsableTime);
yesterdayDt = (string)timestamp;
strftime(timestamp2,sizeof timestamp2,"%Y%m",myUsableTime);
yesterdayMth = (string)timestamp2;



//get total ATS
string getTx = "SELECT COUNT(*) as bil,status FROM transaction_log_ats_"+yesterdayMth+" WHERE req_date >= '"+yesterdayDt+" 00:00:00' AND req_date < '"+currentDate()+" 00:00:00' AND a_type=0 GROUP BY status";
char *cgetTx = stringToChar(getTx);
short int a=mysql_real_query(mysqlObject.myData, cgetTx, strlen(cgetTx));
if (a!=0) {
string errorMsg(mysql_error(mysqlObject.myData));
reportLogger.logError("[Cannot query transaction_log_ats_"+yrMth+". Error:"+errorMsg+" in sql:"+getTx+"]");
} else {
bilATS=0;
totalSuccessATS=0;
totalUserFailATS=0;
ATSerror_4=0;
ATSerror_5=0;
ATSerror_6=0;
ATSerror_7=0;
ATSerror_8=0;
ATSerror_9=0;
ATSerror_11=0;
ATSerror_13=0;
ATSerror_15=0;
ATSerror_16=0;
ATSerror_19=0;
totalTxATS=0;
res=mysql_store_result(mysqlObject.myData);
if (mysql_affected_rows(mysqlObject.myData)>0) {
while ((row = mysql_fetch_row(res)) != NULL) {
bilATS = atoi(row[0]);
codeStatus = atoi(row[1]);
if (codeStatus==0) {
totalSuccessATS = totalSuccessATS + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus>20) {
totalUserFailATS = totalUserFailATS + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==4) {
ATSerror_4 = ATSerror_4 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==5) {
ATSerror_5 = ATSerror_5 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==6) {
ATSerror_6 = ATSerror_6 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==7) {
ATSerror_7 = ATSerror_7 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==8) {
ATSerror_8 = ATSerror_8 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==9) {
ATSerror_9 = ATSerror_9 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==11) {
ATSerror_11 = ATSerror_11 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==13) {
ATSerror_13 = ATSerror_13 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==15) {
ATSerror_15 = ATSerror_15 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==16) {
ATSerror_16 = ATSerror_16 + bilATS;
totalTxATS = totalTxATS + bilATS;
} else if (codeStatus==19) {
ATSerror_19 = ATSerror_19 + bilATS;
totalTxATS = totalTxATS + bilATS;
}


//totalTxATS = totalTxATS + bilATS;
}
mysql_free_result( res ) ;
} else {
reportLogger.logDebug("[No result from incoming_queue]");
}
    }


    //get total ATR
string getTx2 = "SELECT COUNT(*) as bil,status FROM transaction_log_atr_"+yesterdayMth+" WHERE req_date >= '"+yesterdayDt+" 00:00:00' AND req_date < '"+currentDate()+" 00:00:00' AND a_type=0 GROUP BY status";
char *cgetTx2 = stringToChar(getTx2);
short int a2=mysql_real_query(mysqlObject.myData, cgetTx2, strlen(cgetTx2));
if (a2!=0) {
string errorMsg(mysql_error(mysqlObject.myData));
reportLogger.logError("[Cannot query transaction_log_atr_"+yrMth+". Error:"+errorMsg+" in sql:"+getTx2+"]");
} else {
bilATR=0;
totalSuccessATR=0;
totalUserFailATR=0;
ATRerror_4=0;
ATRerror_5=0;
ATRerror_6=0;
ATRerror_7=0;
ATRerror_8=0;
ATRerror_9=0;
ATRerror_11=0;
ATRerror_13=0;
ATRerror_15=0;
ATRerror_16=0;
ATRerror_19=0;
totalTxATR=0;
res2=mysql_store_result(mysqlObject.myData);
if (mysql_affected_rows(mysqlObject.myData)>0) {
while ((row2 = mysql_fetch_row(res2)) != NULL) {
bilATR = atoi(row2[0]);
codeStatus = atoi(row2[1]);
if (codeStatus==0) {
totalSuccessATR = totalSuccessATR + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus>20) {
totalUserFailATR = totalUserFailATR + bilATR;
totalTxATR = totalTxATR + bilATR;
}else if (codeStatus==4) {
ATRerror_4 = ATRerror_4 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==5) {
ATRerror_5 = ATRerror_5 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==6) {
ATRerror_6 = ATRerror_6 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==7) {
ATRerror_7 = ATRerror_7 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==8) {
ATRerror_8 = ATRerror_8 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==9) {
ATRerror_9 = ATRerror_9 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==11) {
ATRerror_11 = ATRerror_11 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==13) {
ATRerror_13 = ATRerror_13 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==15) {
ATRerror_15 = ATRerror_15 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==16) {
ATRerror_16 = ATRerror_16 + bilATR;
totalTxATR = totalTxATR + bilATR;
} else if (codeStatus==19) {
ATRerror_19 = ATRerror_19 + bilATR;
totalTxATR = totalTxATR + bilATR;
}



//totalTxATR = totalTxATR + bilATR;
}
mysql_free_result( res2 ) ;
} else {
reportLogger.logDebug("[No result from incoming_queue]");
}
    }


//***************** For POstpaid reportings ******************************************************
//get total ATS
string getTx3 = "SELECT COUNT(*) as bil,status FROM transaction_log_ats_"+yesterdayMth+" WHERE req_date >= '"+yesterdayDt+" 00:00:00' AND req_date < '"+currentDate()+" 00:00:00' AND a_type=1 GROUP BY status";
char *cgetTx3 = stringToChar(getTx3);
short int a3=mysql_real_query(mysqlObject.myData, cgetTx3, strlen(cgetTx3));
if (a3!=0) {
string errorMsg(mysql_error(mysqlObject.myData));
reportLogger.logError("[Postpaid ATS: Cannot query transaction_log_ats_"+yrMth+". Error:"+errorMsg+" in sql:"+getTx3+"]");
} else {
//*** var init
bilATSpost=0;
codeStatus=0;
totalTxATSpost=0;
bilATSpost=0;
totalSuccessATSpost=0;
totalUserFailATSpost=0;

res3=mysql_store_result(mysqlObject.myData);
if (mysql_affected_rows(mysqlObject.myData)>0) {
while ((row3 = mysql_fetch_row(res3)) != NULL) {
bilATSpost = atoi(row3[0]);
codeStatus = atoi(row3[1]);
if (codeStatus==0) {
totalSuccessATSpost = totalSuccessATSpost + bilATSpost;
totalTxATSpost = totalTxATSpost + bilATSpost;
} else if (codeStatus>20) {
totalUserFailATSpost = totalUserFailATSpost + bilATSpost;
totalTxATSpost = totalTxATSpost + bilATSpost;
}else if (codeStatus==7 || codeStatus==9 || codeStatus==11 || codeStatus==13 || codeStatus==15 || codeStatus==16 || codeStatus==19) {
totalTxATSpost = totalTxATSpost + bilATSpost;
totalSysFailINATSpost = totalSysFailINATSpost + bilATSpost;
}else if (codeStatus==4 || codeStatus==5 || codeStatus==6 || codeStatus==8) {
totalTxATSpost = totalTxATSpost + bilATSpost;
totalSysFailIDOTATSpost = totalSysFailIDOTATSpost + bilATSpost;
}



//totalTxATR = totalTxATR + bilATR;
}
mysql_free_result( res3 ) ;
} else {
reportLogger.logDebug("[No result from incoming_queue]");
}
    }

//get total ATR postpaid
string getTx4 = "SELECT COUNT(*) as bil,status FROM transaction_log_atr_"+yesterdayMth+" WHERE req_date >= '"+yesterdayDt+" 00:00:00' AND req_date < '"+currentDate()+" 00:00:00' AND a_type=1 GROUP BY status";
char *cgetTx4 = stringToChar(getTx4);
short int a4=mysql_real_query(mysqlObject.myData, cgetTx4, strlen(cgetTx4));
if (a4!=0) {
string errorMsg(mysql_error(mysqlObject.myData));
reportLogger.logError("[Postpaid ATR: Cannot query transaction_log_atr_"+yrMth+". Error:"+errorMsg+" in sql:"+getTx4+"]");
} else {
//*** var init
bilATRpost=0;
codeStatus=0;
totalTxATRpost=0;
bilATRpost=0;
totalSuccessATRpost=0;
totalUserFailATRpost=0;

res4=mysql_store_result(mysqlObject.myData);
if (mysql_affected_rows(mysqlObject.myData)>0) {
while ((row4 = mysql_fetch_row(res4)) != NULL) {
bilATRpost = atoi(row4[0]);
codeStatus = atoi(row4[1]);
if (codeStatus==0) {
totalSuccessATRpost = totalSuccessATRpost + bilATRpost;
totalTxATRpost = totalTxATRpost + bilATRpost;
} else if (codeStatus>20) {
totalUserFailATRpost = totalUserFailATRpost + bilATRpost;
totalTxATRpost = totalTxATRpost + bilATRpost;
}else if (codeStatus==7 || codeStatus==9 || codeStatus==11 || codeStatus==13 || codeStatus==15 || codeStatus==16 || codeStatus==19) {
totalTxATRpost = totalTxATRpost + bilATRpost;
totalSysFailINATRpost = totalSysFailINATRpost + bilATRpost;
}else if (codeStatus==4 || codeStatus==5 || codeStatus==6 || codeStatus==8) {
totalTxATRpost = totalTxATRpost + bilATRpost;
totalSysFailIDOTATRpost = totalSysFailIDOTATRpost + bilATRpost;
}



//totalTxATR = totalTxATR + bilATR;
}
mysql_free_result( res4 ) ;
} else {
reportLogger.logDebug("[No result from incoming_queue]");
}
    }


}
mysql_close(mysqlObject.myData);

totalTx = totalTxATS + totalTxATR + totalTxATSpost + totalTxATRpost;
totalSuccess = totalSuccessATS + totalSuccessATR + totalSuccessATSpost + totalSuccessATRpost;
totalUserFail = totalUserFailATS + totalUserFailATR + totalUserFailATSpost;
totalSysFailINCAT = (ATSerror_7 + ATSerror_9 + ATSerror_11 + ATSerror_13 + ATSerror_15 + ATSerror_16 + ATSerror_19);
totalSysFailINCAR = (ATRerror_7 + ATRerror_9 + ATRerror_11 + ATRerror_13 + ATRerror_15 + ATRerror_16 + ATRerror_19);
totalSysFailIdotCAT = (ATSerror_4 + ATSerror_5 + ATSerror_6 + ATSerror_8);
totalSysFailIdotCAR = (ATRerror_4 + ATRerror_5 + ATRerror_6 + ATRerror_8);
totalSysFailIN = totalSysFailINCAT + totalSysFailINCAR + totalSysFailINATSpost + totalSysFailINATRpost;
totalSysFailIdot = totalSysFailIdotCAT + totalSysFailIdotCAR + totalSysFailIDOTATSpost + totalSysFailIDOTATRpost;
seq=seq+1;






//send message to smsout
msgDt = "Date:"+yesterdayDt;
msg1 = "TxCAT:"+intToString(totalTxATS)+" ScCAT:"+intToString(totalSuccessATS)+" UFCAT:"+intToString(totalUserFailATS)+" SFCATIN:"+intToString(totalSysFailINCAT)+" SFCATIDOT:"+intToString(totalSysFailIdotCAT);
msg2 = "TxCAR:"+intToString(totalTxATR)+" ScCAR:"+intToString(totalSuccessATR)+" UFCAR:"+intToString(totalUserFailATR)+" SFCARIN:"+intToString(totalSysFailINCAR)+" SFCARIDOT:"+intToString(totalSysFailIdotCAR);
msg3 = "TOTTx:"+intToString(totalTx)+" TotSuc:"+intToString(totalSuccess)+" TxPre:"+intToString(totalTxATS+totalTxATR)+" SucPre:"+intToString(totalSuccessATS + totalSuccessATR)+" TxPos:"+intToString(totalTxATSpost+totalTxATRpost)+" SucPos:"+intToString(totalSuccessATSpost+totalSuccessATRpost)+"  UserF:"+intToString(totalUserFail)+" SFIN:"+intToString(totalSysFailIN)+" SFIDOT:"+intToString(totalSysFailIdot);
msg4 = "SYSTEM ERROR IDOT  : Error 4:"+intToString(ATSerror_4+ATRerror_4)+" Error 5 :"+intToString(ATSerror_5+ATRerror_5)+" Error 6 :"+intToString(ATSerror_6+ATRerror_6)+" Error 8 :"+intToString(ATSerror_8+ATRerror_8);
msg5 = "SYSTEM ERROR IN : Error 7 :"+intToString(ATSerror_7+ATRerror_7)+" Error 9 :"+intToString(ATSerror_9+ATRerror_9)+" Error 11 :"+intToString(ATSerror_11+ATRerror_11)+" Error 13 :"+intToString(ATSerror_13+ATRerror_13)+" Error 15 :"+intToString(ATSerror_15+ATRerror_15)+" Error 16 :"+intToString(ATSerror_16+ATRerror_16)+" Error 19 :"+intToString(ATSerror_19+ATRerror_19);
string sms1 = msgDt+"  "+msg3;
string sms2 = "";
string sms3 = "";
string sms4 = "";
string sms5 = "";

ifstream fileConfig ("/home/cat/smsreport/postpaid/phone.txt");
if (fileConfig.is_open()) {
while (!fileConfig.eof()) {
getline (fileConfig,line);

string a_phone2=line;
string tx_id2=currentDateTime2()+intToString(seq);

if (a_phone2!="" && sms1!="") {
sockID2=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
if(sockID2 < 0){
reportLogger.logDebug("Error Initialize socket");
} else {
bzero(&serverAddress2,sizeof(serverAddress2));
serverAddress2.sin_family = AF_INET;;
//serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.52");
serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.102");
//serverAddress2.sin_port = htons(10005);
serverAddress2.sin_port = htons(9991);

if (connect(sockID2,(struct sockaddr *) &serverAddress2,sizeof(serverAddress2)) < 0) {
reportLogger.logDebug("Cannot Connect to SMS Sender");
} else {
//send first sms
channel2="1";
oadc2 = "1"+a_phone2.substr(1,3)+"0000000";
res_packet2="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id2+"|ADC="+a_phone2.substr(1)+"|OADC="+oadc2+"|MSG="+sms1+"|CHANNEL="+channel2+"|ETX";
strcpy(writeBuff2,res_packet2.c_str());
returnStatus2=send(sockID2,writeBuff2,sizeof(writeBuff2),0);
if(returnStatus2<0) {
reportLogger.logDebug("Error sending msg");
} else{
reportLogger.logDebug("Msg Sent:"+res_packet2);
}

}
}
close(sockID2);
}

}




ifstream fileConfig ("/home/cat/smsreport/postpaid/phone_INT.txt");
if (fileConfig.is_open()) {
while (!fileConfig.eof()) {
getline (fileConfig,line);

string a_phone3=line;
string tx_id3=currentDateTime2()+intToString(seq);

if (a_phone3!="" && sms2!="") {
sockID2=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
if(sockID2 < 0){
reportLogger.logDebug("Error Initialize socket");
} else {
bzero(&serverAddress2,sizeof(serverAddress2));
serverAddress2.sin_family = AF_INET;;
serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.102");
serverAddress2.sin_port = htons(9991);

if (connect(sockID2,(struct sockaddr *) &serverAddress2,sizeof(serverAddress2)) < 0) {
reportLogger.logDebug("Cannot Connect to SMS Sender");
} else {
//send second sms
channel2="1";
oadc2 = "1"+a_phone3.substr(1,3)+"0000000";
sleep(2);
//send second sms
res_packet2="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id3+"|ADC="+a_phone3.substr(1)+"|OADC="+oadc2+"|MSG="+sms2+"|CHANNEL="+channel2+"|ETX";
strcpy(writeBuff2,res_packet2.c_str());
returnStatus2=send(sockID2,writeBuff2,sizeof(writeBuff2),0);
if(returnStatus2<0) {
reportLogger.logDebug("Error sending msg");
} else{
reportLogger.logDebug("Msg Sent:"+res_packet2);
}

}
}
close(sockID2);
}

//send 3rd sms
if (a_phone3!="" && sms3!="") {
sockID2=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
if(sockID2 < 0){
reportLogger.logDebug("Error Initialize socket");
} else {
bzero(&serverAddress2,sizeof(serverAddress2));
serverAddress2.sin_family = AF_INET;;
//serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.52");
serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.102");
//serverAddress2.sin_port = htons(10005);
serverAddress2.sin_port = htons(9991);

if (connect(sockID2,(struct sockaddr *) &serverAddress2,sizeof(serverAddress2)) < 0) {
reportLogger.logDebug("Cannot Connect to SMS Sender");
} else {
//send 3rd sms
channel2="1";
oadc2 = "1"+a_phone3.substr(1,3)+"0000000";
res_packet2="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id3+"|ADC="+a_phone3.substr(1)+"|OADC="+oadc2+"|MSG="+sms3+"|CHANNEL="+channel2+"|ETX";
strcpy(writeBuff2,res_packet2.c_str());
returnStatus2=send(sockID2,writeBuff2,sizeof(writeBuff2),0);
if(returnStatus2<0) {
reportLogger.logDebug("Error sending msg");
} else{
reportLogger.logDebug("Msg Sent:"+res_packet2);
}

}
}
close(sockID2);
}

//send 4th sms
if (a_phone3!="" && sms4!="") {
sockID2=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
if(sockID2 < 0){
reportLogger.logDebug("Error Initialize socket");
} else {
bzero(&serverAddress2,sizeof(serverAddress2));
serverAddress2.sin_family = AF_INET;;
//serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.52");
serverAddress2.sin_addr.s_addr=inet_addr("10.122.17.102");
//serverAddress2.sin_port = htons(10005);
serverAddress2.sin_port = htons(9991);

if (connect(sockID2,(struct sockaddr *) &serverAddress2,sizeof(serverAddress2)) < 0) {
reportLogger.logDebug("Cannot Connect to SMS Sender");
} else {
//send 4th sms
channel2="1";
oadc2 = "1"+a_phone3.substr(1,3)+"0000000";
res_packet2="STX|023|QRYRES=QRY|OPCODE=01|TXID="+tx_id3+"|ADC="+a_phone3.substr(1)+"|OADC="+oadc2+"|MSG="+sms4+"|CHANNEL="+channel2+"|ETX";
strcpy(writeBuff2,res_packet2.c_str());
returnStatus2=send(sockID2,writeBuff2,sizeof(writeBuff2),0);
if(returnStatus2<0) {
reportLogger.logDebug("Error sending msg");
} else{
reportLogger.logDebug("Msg Sent:"+res_packet2);
}

}
}
close(sockID2);
}

}

}
}
}
