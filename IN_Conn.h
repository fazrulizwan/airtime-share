#ifndef IN_CONN_H_
#define IN_CONN_H_

#include <iostream>
#include "sharedmem.h"

using namespace std;

//**** Query Status const
const short int QRY_STATUS_OK=0;
const short int QRY_STATUS_FAIL=1;
const short int QRY_STATUS_REFUND_OK=3;
const short int QRY_STATUS_REFUND_FAIL=4; //*** debit
const short int QRY_STATUS_EXTERNAL_FAIL=5;
const short int QRY_STATUS_REFUND_FAIL_CREDIT=6;
const short int QRY_STATUS_NOT_FOUND=7;

//**** Language Const
const short int LANG_BM=1;
const short int LANG_ENGLISH=2;
const short int LANG_MANDARIN=3;

//*** Platform Const
const short int PFORM_NEC=0;
const short int PFORM_LOGICA=1;
const short int PFORM_POSTPAID=-9;

//*** User status const
const short int USR_STATUS_PREREG=1;
const short int USR_STATUS_ACTIVE=2;
const short int USR_STATUS_TERMINATED=4;

//**** User type const
const short int USR_TYPE_PREPAID=0;
const short int USR_TYPE_POSTPAID=1;

//*********** Return structure
//*** Query Profile return structure
typedef struct IN_UserProfile
{
   short int queryStatus; //*** use query status const
   short int userLang;	//*** use language const
   short int userPlatform; // *** use platform const
   short int userStatus; //*** use user status const
   std::string expiryDate; //*** YYYY-MM-DD
   float userBalance;
   short int userType; //Use user type const
   std::string activationDate; //*** YYYY-MM-DD
   int brand;
   float creditLimit;
   std::string stmtDate;
   float totalDue;
   float unBilled;
}USRProfile;

typedef struct IN_TransferResult{
	short int transferStatus; //*** use query status const
	std::string aExpiryDate; //*** YYYY-MM-DD
   	std::string bExpiryDate; //*** YYYY-MM-DD
   	float aBalance;
   	float bBalance;
};

class IN_Conn{

	public:
	struct IN_UserProfile QueryProfile(string phone_no,PortReg port_reg,string txid);
	struct IN_TransferResult Transfer(string a_no,string b_no,float amt,int validity,float a_charging,float b_charging,string txid,int a_pform,int b_pform,PortReg port_reg,string);
	int Refund(string a_no,string b_no,string txid,float amt,PortReg port_reg,string tx_type,int variation,int option);

	private:
	string buffToStr(char *);
	string IntToString ( int number );
	string floatToStr(float val);
	string floatToString2d(float floatVal);
	string currentTimestamp(void);
	long int HexStrToInt(string hexStr);

};

#endif
