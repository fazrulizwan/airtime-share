#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <dirent.h>

using namespace std;

#include "MysqlConnDB.h"
#include "FileLogger.h"
#include "generalfunction.h"
#include "ConfigReader.h"

using namespace Exporter;

int main(int argc, char *argv[])
{
	ConfigReader config;
	config.ReadConfig();

	string logExporter = config.logdir + "/debug."+currentDate()+".log";
	FileLogger exporterLogger (logExporter);
	exporterLogger.logDebug("Enter Exporter Program");

	//start reading file
	string line;
	short int insertStat;
	short int createStat1;
	short int createStat2;
	short int createStat3;
	short int createStat4;
	short int createStat5;
	short int createStat6;
	short int createStat7;
	short int createStat8;
	short int createStat9;
	short int chkTblStat1;
	short int chkTblStat2;
	short int chkTblStat3;
	short int chkTblStat4;
	short int chkTblStat5;
	short int chkTblStat6;
	short int chkTblStat7;
	short int chkTblStat8;
	short int chkTblStat9;
	string tx_month;
	DIR *pdir;
	DIR *pdir2;
	DIR *pdir3;
	DIR *pdir4;
	DIR *pdir5;
	DIR *pdir6;
	struct dirent *pent;
	string files;
	string searchName;
	string searchName1;
	string searchName2;
	string searchName3;
	string searchName4;
	string searchName5;
	string searchName6;
	MysqlConnDB mysqlObject;
	string sqlString;
	string tblName;
	short int insertStatus;

	if (mysqlObject.OpenConnDB(config.logdir, config.db_host, config.db_username, config.db_password)) {

		//check if current transaction's month table exist
		tx_month = currentDateLimit().substr(0,6);
		chkTblStat3 = ChkTblExist("incoming_queue_"+tx_month, mysqlObject.myData, config.logdir);
		chkTblStat4 = ChkTblExist("outgoing_sms_"+tx_month, mysqlObject.myData, config.logdir);
		exporterLogger.logDebug("Check table result: [result1:"+intToString(chkTblStat1)+"] [result2:"+intToString(chkTblStat2)+"] [result3:"+intToString(chkTblStat3)+"] [result4:"+intToString(chkTblStat4)+"] [result5:"+intToString(chkTblStat5)+"]");

		//create table if not exist

		if (chkTblStat3==0) {
			createStat3 = CreateIncomingTable("incoming_queue_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table incoming result:"+intToString(createStat3));
		}
		if (chkTblStat4==0) {
			createStat4 = CreateOutgoingTable("outgoing_sms_"+tx_month, mysqlObject.myData, config.logdir);
			exporterLogger.logDebug("Create table outgoing result:"+intToString(createStat4));
		}

		//export smpp incoming queue
		exporterLogger.logDebug("Try to open SMPP directory..");
		string dirName5 = config.smpp_txdir;
		exporterLogger.logDebug("Finish find yesterday's file : "+dirName5);
		const char *cdirectoryName5= dirName5.c_str();
		string newFile5;
		pdir5=opendir(cdirectoryName5); //open directory
		if (!pdir5){
			exporterLogger.logDebug("Cannot Open SMPP TX Directory!");
		} else {
			while ((pent=readdir(pdir5))){
				files = charToString(pent->d_name);
				if (files.length()>2) {
					searchName1 = files.substr(0,2);
					searchName2 = files.substr(0,3);
					if (searchName1.compare("In")==0 || searchName2.compare("Out")==0) {

						exporterLogger.logDebug("Transaction file found:"+files);
						string filepath5 = dirName5 + "/" + files;

						if (searchName1.compare("In")==0) {
							tblName = "incoming_queue_"+tx_month;
							sqlString=" LOAD DATA LOCAL INFILE '"+filepath5+"'";
							sqlString+=" INTO TABLE "+tblName;
							sqlString+=" FIELDS TERMINATED BY '|'";
							sqlString+=" LINES TERMINATED BY '\n'";
							sqlString+=" (transaction_id, connector_timestamp, phone, short_code, content ) ";
						} else {
							tblName  = "outgoing_sms_"+tx_month;
							sqlString=" LOAD DATA LOCAL INFILE '"+filepath5+"'";
							sqlString+=" INTO TABLE "+tblName;
							sqlString+=" FIELDS TERMINATED BY '|'";
							sqlString+=" LINES TERMINATED BY '\n'";
							sqlString+=" (transaction_id, connector_timestamp, oadc, phone, content, channel, charge) ";
						}
						exporterLogger.logDebug("sql:"+sqlString);
						char *csql = stringToChar(sqlString);
						//cout << " sql:" << csql << "\n";
						short int b=mysql_real_query(mysqlObject.myData,csql,strlen(csql));
						//printf(" b= %d ",b);
						if (b==0) {
							insertStatus=0;
							exporterLogger.logDebug("Insert Successfull into "+tblName);
							if (searchName1.compare("In")==0) {
                                                        	newFile5 = config.smpp_cdr+"/"+currentTimestamp2()+".INCOMING.CDR";
	                                                        exporterLogger.logDebug("Rename file to:"+newFile5);
        	                                        } else if (searchName2.compare("Out")==0) {
                	                                        newFile5 = config.smpp_cdr+"/"+currentTimestamp2()+".OUTGOING.CDR";
                        	                                exporterLogger.logDebug("Rename file to:"+newFile5);
                                	                }

                                        	        const char *filepathname5= filepath5.c_str();
                                                	ifstream fileTx5 (filepathname5);
	                                                char *newname5 = stringToChar(newFile5);
        	                                        int resultRename5 = rename(filepathname5, newname5);
                	                                if (resultRename5==0)
                        	                                exporterLogger.logDebug("Successfully rename file to:"+newFile5);
                                	                else
                                        			exporterLogger.logDebug("Failed rename file:"+filepath5);

						} else {
							insertStatus=1;
							string errorMsg(mysql_error(mysqlObject.myData));
							exporterLogger.logError("[Cannot insert help incoming into table Error:"+errorMsg+" in sql:"+sqlString+"] [File will not move]");
						}

						delete[] csql;


					}
				}
			}
		}



		//finish exporting data
		mysql_close(mysqlObject.myData);
	} else {
		exporterLogger.logDebug("Cannot connect to database");
	}

    return EXIT_SUCCESS;
}


