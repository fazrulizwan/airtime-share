#ifndef FILE_LOGGER_H
#define FILE_LOGGER_H

#include <iostream>

using namespace std;

namespace Exporter {
          class FileLogger {
                public:
                		FileLogger();
                       	FileLogger(string);
                       	string filename;
                       	int createLog();
                       	int logError(string);
                       	int logWarn(string);
                       	int logNotice(string);
                       	int logDebug(string);
                       	int logData(string);
          };
}

#endif
