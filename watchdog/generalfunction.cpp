#include <iostream>
#include <fstream>
#include <errno.h>
#include <dirent.h>

#include "generalfunction.h"
#include <sys/time.h>
#include <time.h>
#include <mysql.h>

#include "MysqlConnDB.h"
#include "Subscriber.h"
#include "FileLogger.h"

using namespace std;

namespace ats {

	//---------check phone no format---------------
	bool chkPhoneFormat(string phone)
	{
	     if (phone.length()==11) {
	        return true;
	     } else {
	        return false;
	     }
	}

	//---------check password format--------------
	bool chkPinFormat(string pin)
	{
		char pwd[6];
		int i;
	    if (pin.length()==6) {
	    	return true;
	    } else {
	    	return false;
	    }
	}

	//---------check amount format--------------
	bool chkAmtFormat(string amount)
	{
		int i;
        for (i=0;i<amount.length();i++) {
        	if (!isdigit(amount[i]))
        		return false;
        }
    	return true;
	}

	//---------to tokinize string--------------
	vector<string> Tokenize(const string str, const string delimiters = " ")
	{
	    vector<string> returnVector;
	    // Skip delimiters at beginning.
	    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	    // Find first "non-delimiter".
	    string::size_type pos     = str.find_first_of(delimiters, lastPos);

	    while (string::npos != pos || string::npos != lastPos)
	    {
	        // Found a token, add it to the vector.
	        returnVector.push_back(str.substr(lastPos, pos - lastPos));
	        // Skip delimiters.  Note the "not_of"
	        lastPos = str.find_first_not_of(delimiters, pos);
	        // Find next "non-delimiter"
	        pos = str.find_first_of(delimiters, lastPos);
	    }
	    return returnVector;
	}

	string currentTimestamp() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	long milliseconds;
	  	char stringMillisecond[100];
	  	struct timeval tv;
	  	gettimeofday (&tv, NULL);
	  	milliseconds = tv.tv_usec / 1000;
	  	strcat(timestamp,".");
	  	sprintf(stringMillisecond,"%d",milliseconds);
	  	strcat(timestamp,stringMillisecond);
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateTime() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentUserDate() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%d/%m/%Y",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateLimit() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m%d",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateTime2() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m%d%H%M%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentDateTime3() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}

	string currentWeekLimit() {
		char timestamp[100];
		string currentWeek;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%W",mytm);

	  	currentWeek = (string)timestamp;
	  	return currentWeek;
	}

	string currentMonthLimit() {
		char timestamp[100];
		string currentMth;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);

	  	strftime(timestamp,sizeof timestamp,"%Y%m",mytm);

	  	currentMth = (string)timestamp;
	  	return currentMth;
	}

	//change each element of the string to upper case
	string StringToUpper(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = toupper(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}

	//change each element of the string to lower case
	string StringToLower(string strToConvert) {
	   for(unsigned int i=0;i<strToConvert.length();i++)
	   {
	      strToConvert[i] = tolower(strToConvert[i]);
	   }
	   return strToConvert;//return the converted string
	}

	//convert string to integer
	int stringToInt(string strValue) {
		const char *charConst = strValue.c_str();
		int intVal = atoi(charConst);
		return intVal;
	}

	//convert string to long integer
	int stringToLong(string strValue) {
		const char *charConst = strValue.c_str();
		long longVal = atol(charConst);
		return longVal;
	}

	//convert string to float
	float stringToFloat(string strValue) {
		const char *charConst = strValue.c_str();
		float floatVal = atof(charConst);
		return floatVal;
	}

	//convert string to constant char
	char *stringToChar(string strValue) {
		char *charVal;
		charVal = new char[strValue.length()+1];
		strcpy(charVal,strValue.c_str());
		return charVal;
	}

	//convert integer to string
	string intToString(int intVal) {
		char charVal[100];
		sprintf(charVal,"%d",intVal);
		string strVal(charVal);
		return strVal;
	}

	//convert float to string
	string floatToString(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%f",floatVal);
		string strVal(charVal);
		return strVal;
	}

	//convert float to 2 decimal point string
	string floatToString2d(float floatVal) {
		char charVal[100];
		sprintf(charVal,"%.2f",floatVal);
		string strVal(charVal);
		return strVal;
	}

	//search file in directory
	short int searchFile(string directoryName,string fileName) {
		DIR *pdir;
		struct dirent *pent;

		const char *cdirectoryName= directoryName.c_str();
		pdir=opendir(cdirectoryName); //open directory
		if (!pdir){
			//cout << "Cannot Open Directory!";
			return 2;
		}
		errno=0;

		const char *cfileName= fileName.c_str();
		string fname;
		while ((pent=readdir(pdir))){
			if (strncmp(pent->d_name,cfileName,30)==0) {
				//cout << "Found file!";
				return 0;
			}
		}
		if (errno){
			//cout << "Error Opening Directory!";
			return 3;
		}
		closedir(pdir);
		return 1;
	}


	//search pending file in from ats_pending directory
	short int searchPendingFile(string directoryName,string fileName, string& foundFile) {
		DIR *pdir;
		struct dirent *pent;
		char strFile[50];

		const char *cdirectoryName= directoryName.c_str();
		pdir=opendir(cdirectoryName); //open directory
		if (!pdir){
			//cout << "Cannot Open Directory!";
			return 2;
		}
		errno=0;
		const char *cfileName= fileName.c_str();
		string fname;
		//cout << "Find File:" << fileName << " in dir:" << directoryName;
		while ((pent=readdir(pdir))){
			if (strncmp(pent->d_name,cfileName,23)==0) {
				//cout << "Found file!";
				foundFile = charToString(pent->d_name);
				return 0;
			}
		}
		if (errno){
			//cout << "Error Opening Directory!";
			return 3;
		}
		closedir(pdir);
		return 1;
	}

	//sum amount received for today
	int sumAmountReceived(string directoryName, string phone) {
		DIR *pdir;
		struct dirent *pent;
		char strFile[50];
		int amount;

		const char *cdirectoryName= directoryName.c_str();
		pdir=opendir(cdirectoryName); //open directory
		if (!pdir){
			//cout << "Cannot Open Directory!";
			return 0;
		}
		amount=0;
		string fileName = phone;
		const char *cfileName= fileName.c_str();
		const char *cfiles;
		string files;
		while ((pent=readdir(pdir))){
			files = charToString(pent->d_name);
			if (files.length()>29) {
				cfiles = files.substr(27).c_str();
				//cout << " cfiles: " << cfiles;
				if (strncmp(cfiles,cfileName,11)==0) {
					amount = amount + stringToInt(files.substr(39));
				}
			}
		}

		closedir(pdir);
		return amount;
	}

	//sum amount transfered for current receiver for today
	int sumAmountTransfered(string directoryName, string donor, string receiver) {
		DIR *pdir;
		struct dirent *pent;
		char strFile[50];
		int amount;

		const char *cdirectoryName= directoryName.c_str();
		pdir=opendir(cdirectoryName); //open directory
		if (!pdir){
			//cout << "Cannot Open Directory!";
			return 0;
		}
		amount=0;
		string fileName = donor + "." + receiver;
		const char *cfileName= fileName.c_str();
		const char *cfiles;
		string files;
		while ((pent=readdir(pdir))){
			files = charToString(pent->d_name);
			if (files.length()>29) {
				cfiles = files.substr(15).c_str();
				//cout << " cfiles: " << cfiles;
				if (strncmp(cfiles,cfileName,23)==0) {
					amount = amount + stringToInt(files.substr(39));
				}
			}
		}

		closedir(pdir);
		return amount;
	}

	short int checkFailedATRTx(string directoryName, string a_no, string b_no, int& totalAll, int& totalDonor) {
		DIR *pdir;
		struct dirent *pent;
		char strFile[50];
		int amount;
		totalAll=0;
		totalDonor=0;

		const char *cdirectoryName= directoryName.c_str();
		pdir=opendir(cdirectoryName); //open directory
		if (!pdir){
			//cout << "Cannot Open Directory!";
			return 0;
		}
		string fileName = a_no;
		const char *cfileName= fileName.c_str();
		//const char *cfiles;
		string fileName2 = a_no+"."+b_no;
		const char *cfileName2= fileName2.c_str();
		const char *cfiles;
		string files;
		while ((pent=readdir(pdir))){
			files = charToString(pent->d_name);
			if (files.length()>29) {
				cfiles = files.substr(15).c_str();
				//cout << "\nFiles:" << cfiles;
				if (strncmp(cfiles,cfileName,13)==0) {
					totalAll = totalAll + 1;
				}
				if (strncmp(cfiles,cfileName2,27)==0) {
					totalDonor = totalDonor + 1;
				}
			}
		}
		closedir(pdir);
		return 0;
	}

	string charToString(char* c) {
		std:string s(c);
		return s;
	}
	//check if file is exist
	short int isFileExist(string fileName) {
		fstream fin;
		const char *cfileName= fileName.c_str();

		fin.open(cfileName,ios::in);
		if (fin.is_open()) {
			fin.close();
			return 0;
		}
		fin.close();
		return 1;
	}

	//concat string
	void stradd(char *s1, char *s2) {
		strcat(s1,s2);
	}

	void trimString(string& str)
	{
	  string::size_type pos = str.find_last_not_of(' ');
	  if(pos != string::npos) {
	    str.erase(pos + 1);
	    pos = str.find_first_not_of(' ');
	    if(pos != string::npos) str.erase(0, pos);
	  }
	  else str.erase(str.begin(), str.end());
	}

	//---------check if user profile exist in database--------------
	short int chkProfile(string phone, MYSQL *myData, const FileLogger& dlogger) {
	    MYSQL_RES *res;
	    MYSQL_ROW row;
	    string sqlString;
	    short int j,k;
	    short int isExist = 0;
	    FileLogger logger = dlogger;

	    logger.logDebug("[Check Profile]");
    	sqlString="SELECT COUNT(phone) AS bil FROM userprofile WHERE phone = '"+phone+"'";
    	char *csql = stringToChar(sqlString);

    	short int a=mysql_real_query(myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		if (atoi(row[0])>0)
	    			isExist=1;
	    		else
	    			isExist=0;
	    		mysql_free_result( res ) ;
	    	} else {
	    		isExist=0;
	    	}
    	} else {
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Error while checking userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    	isExist=2;
    	}
    	delete[] csql;
	    return isExist;
	}

	//---------check if user profile exist in database--------------
	short int getUserStatus(string phone, MYSQL *myData, const FileLogger& dlogger) {
	    MYSQL_RES *res;
	    MYSQL_ROW row;
	    string sqlString;
	    short int j,k;
	    short int userStatus = 0;
	    FileLogger logger = dlogger;

	    logger.logDebug("[Get User Status]");
    	sqlString="SELECT app_status FROM userprofile WHERE phone = '"+phone+"'";
    	char *csql = stringToChar(sqlString);

    	short int a=mysql_real_query(myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		userStatus=atoi(row[0]);
	    		mysql_free_result( res ) ;
	    	} else {
	    		userStatus=9;
	    	}
    	} else {
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Error while get user status Error:"+errorMsg+" in sql:"+sqlString+"]");
	    	userStatus=10;
    	}
    	delete[] csql;
	    return userStatus;
	}

	//pre-register new user in userprofile
	short int preRegisterUserProfile(Subscriber user, string tx_id, MYSQL *myData, const FileLogger& dlogger) {
		short int regStatus=1;
		string sqlString;
		string dayDt;
		string weekDt;
		string mthDt;
		string activeDate;
		FileLogger logger = dlogger;
		short int pwdEnabled=1;
		dayDt=currentDateLimit();

		logger.logDebug("[preRegisterUserProfile]");
		sqlString="INSERT INTO userprofile ";
		sqlString+="(phone, app_status, in_status, in_date, user_type, tx_id, daily_trf_amt, day_dt, language, update_date, brand) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+user.getPhone()+"',1,"+intToString(user.getINStatus())+",'"+currentDateTime()+"',"+intToString(user.getType())+",'"+tx_id+"',0,'"+dayDt+"', "+intToString(user.getLang())+", '"+currentDateTime()+"', "+intToString(user.getBrand())+");";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	regStatus=0;
	    } else {
	    	regStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot insert into pre-userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return regStatus;
	}

	//register new user in userprofile
	short int registerUserProfile(Subscriber user, string tx_id, MYSQL *myData, const FileLogger& dlogger) {
		short int regStatus=1;
		string sqlString;
		string dayDt;
		string weekDt;
		string mthDt;
		string activeDate;
		FileLogger logger = dlogger;
		short int pwdEnabled=1;
		dayDt=currentDateLimit();

		logger.logDebug("[registerUserProfile]");
		sqlString="INSERT INTO userprofile ";
		sqlString+="(phone, pwd, app_status, in_status, reg_date, in_date, user_type, tx_id, daily_trf_amt, day_dt, language, update_date, mother, brand) ";
		sqlString+= "VALUES ";
		sqlString+= "('"+user.getPhone()+"','"+user.getPwd()+"',2,"+intToString(user.getINStatus())+",'"+currentDateTime()+"','"+currentDateTime()+"',"+intToString(user.getType())+",'"+tx_id+"',0,'"+dayDt+"', "+intToString(user.getLang())+", '"+currentDateTime()+"', '"+user.getMotherName()+"', "+intToString(user.getBrand())+");";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	regStatus=0;
	    } else {
	    	regStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot insert into userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return regStatus;
	}

	//update userprofile
	short int updateUserProfile(Subscriber user, string tx_id, MYSQL *myData, const FileLogger& dlogger) {
		short int regStatus=1;
		string sqlString;
		string dayDt;
		string weekDt;
		string mthDt;
		string activeDate;
		FileLogger logger = dlogger;
		short int pwdEnabled=1;
		dayDt=currentDateLimit();

		logger.logDebug("[updateUserProfile]");
		sqlString="UPDATE userprofile ";
		sqlString+="SET pwd = '"+user.getPwd()+"', ";
		sqlString+="app_status = 2, ";
		sqlString+="in_status = "+intToString(user.getINStatus())+", ";
		sqlString+="reg_date = '"+currentDateTime()+"', ";
		sqlString+="user_type = "+intToString(user.getType())+", ";
		sqlString+="tx_id = '"+tx_id+"', ";
		sqlString+="language = "+intToString(user.getLang())+", ";
		sqlString+="update_date = '"+currentDateTime()+"', ";
		sqlString+="mother = '"+user.getMotherName()+"', ";
		sqlString+="brand = '"+intToString(user.getBrand())+"' ";
		sqlString+= "WHERE phone = '"+user.getPhone()+"'";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	regStatus=0;
	    } else {
	    	regStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot update userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return regStatus;
	}

	//get user profile data from DB
	short int getProfileDB(Subscriber& userProfile,MYSQL *myData, const FileLogger& dlogger) {
		MYSQL_RES *res;
		MYSQL_ROW row;
	    string sqlString;
		short int j,k;
		string activationDate;
		string lastTx;
		string pwd;
		FileLogger logger = dlogger;
		logger.logDebug("[getProfileDB]");
		sqlString = "SELECT pwd, user_type, app_status, in_status, reg_date, daily_sc_trf, last_sc_trf,";
		sqlString += " daily_trf_amt, day_dt, brand, whitelist_stat, language, day_counter, counter_dt, blacklist_stat, promo_stat ";
		sqlString +=" FROM userprofile WHERE phone = '"+userProfile.getPhone()+"'";
		char *csql = stringToChar(sqlString);
	    short int a=mysql_real_query(myData, csql, strlen(csql));
    	res=mysql_store_result(myData);
    	delete[] csql;
    	//printf(" a= %d ",a);
    	string errorMsg(mysql_error(myData));

    	if (a!=0) {
    		logger.logError("[Cannot query profile from userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
    		return 2;
    	}
    	if (mysql_affected_rows(myData)>0) {
    		//cout << "Profile found from DB";
    		row = mysql_fetch_row( res );
    		pwd = (row[0] ? row[0] : "");
    		int user_type = stringToInt(row[1] ? row[1] : "");
    		int app_stat = stringToInt(row[2] ? row[2] : "");
    		int successCount = stringToInt(row[5] ? row[5] : "");
    		lastTx = (row[6] ? row[6] : "");
    		float dayLimit = stringToFloat((row[7] ? row[7] : "0"));
    		string dayLimitDt = (row[8] ? row[8] : "");
    		//short int brand = stringToInt((row[9] ? row[9] : ""));
    		short int whiteList = stringToInt((row[10] ? row[10] : ""));
    		short int lang = stringToInt((row[11] ? row[11] : ""));
    		short int dayCounter = stringToInt((row[12] ? row[12] : ""));
    		string counterDt = (row[13] ? row[13] : "");
    		short int blacklistStat = stringToInt((row[14] ? row[14] : "0"));
			short int promoStat = stringToInt((row[15] ? row[15] : "0"));

    		userProfile.setIsRegistered(1);//is user registered
    		userProfile.setAppSatus(app_stat);//application status (active or blacklist)
    		userProfile.setBlacklistStat(blacklistStat);
    		if (pwd != "")
    			userProfile.setPwd(pwd);//password
    		userProfile.setType(user_type);//postpaid or prepaid
    		userProfile.setSuccessCount(successCount);//count of today successfull transfer
    		if (lastTx != "")
    			userProfile.setLastTx(lastTx);//timestamp of last successfull transaction
    		string currentDt = currentDateLimit();
    		if (dayLimitDt.compare(currentDt)==0) {
    			userProfile.setDailyLimit(dayLimit); //daily limit
    			userProfile.setSuccessCount(successCount); //successfull transfer for today
    		} else {
    			userProfile.setDailyLimit(0);
    			userProfile.setSuccessCount(0);
    		}
    		if (counterDt.compare(currentDt)==0) {
    			userProfile.setCounterDay(dayCounter); //day counter (counter for 0 balance and in grace period)
    		} else {
    			userProfile.setCounterDay(0);
    		}
    		userProfile.setWhitelistStat(whiteList); //whitelist status
    		userProfile.setPromoStat(promoStat); //promo status
    		//userProfile.setLang(lang); //language
    		//userProfile.setBrand(brand);//brand
    		mysql_free_result( res ) ;
    	} else {
    		userProfile.setIsRegistered(0);
    		userProfile.setDailyLimit(0);
    		userProfile.setSuccessCount(0);
    		userProfile.setAppSatus(0);
    		userProfile.setBlacklistStat(0);
    		userProfile.setPromoStat(0);
    		return 1;
    	}

		return 0;
	}

	//update user daily, weekly, monthly limit
	short int updateUserLimit(string phone, float dailyLimit, short int successCount, MYSQL *myData, const FileLogger& dlogger) {
		short int updateStatus=1;
		string sqlString;
		string dayDt;
		string weekDt;
		string mthDt;
		FileLogger logger = dlogger;

		dayDt=currentDateLimit();
		weekDt=currentWeekLimit();
		mthDt=currentMonthLimit();

		logger.logDebug("updateUserLimit");
		sqlString="UPDATE userprofile SET ";
		sqlString+=" daily_trf_amt="+floatToString(dailyLimit)+",";
		sqlString+=" day_dt="+dayDt+",";
		sqlString+=" daily_sc_trf="+intToString(successCount)+",";
		sqlString+=" last_sc_trf='"+currentDateTime()+"'";
		sqlString+=" WHERE phone = '"+phone+"'";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	updateStatus=0;
	    } else {
	    	updateStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot update limit in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return updateStatus;
	}

	//find and replace string
	string find_replace(string oriStr, string needle, string replaceWith) {
		size_t found;
		int startAt;
		bool notFound=false;

		while (notFound==false) {
			found = oriStr.find(needle);

			//remove 6 prefix from phone number
			if (needle=="<ano>" || needle=="<bno>")
				replaceWith = replaceWith.substr(1);

			if (found!=string::npos) {
				startAt= int(found);
				oriStr.replace(startAt,5,replaceWith);
			} else {
				notFound=true;
			}
		}

		return oriStr;
	}

	//find and replace 'Celcom' string
	string find_replace_celcom(string oriStr, string needle, string replaceWith) {
		size_t found;
		int startAt;
		bool notFound=false;
		int strLen = needle.length();

		while (notFound==false) {
			found = oriStr.find(needle);

			if (found!=string::npos) {
				startAt= int(found);
				oriStr.replace(startAt,strLen,replaceWith);
			} else {
				notFound=true;
			}
		}

		return oriStr;
	}

	int TimeDiffSec(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2);
		return tm1-tm2;
	}

	int TimeDiffSecDifDay(string time1, string time2) {
		int tm1 = tmToInt(time1);
		int tm2 = tmToInt(time2) + (24*60*60);
		return tm1-tm2;
	}

	int tmToInt(string tm) {
		int h = stringToInt(tm.substr(0,4));
		int m = stringToInt(tm.substr(4,2));
		int s = stringToInt(tm.substr(6,2));
		int tmInt = s + (m*60) + (h*60*60);
		return tmInt;
	}

	short int chkUserPwd(string phone, string pwd, MYSQL *myData, const FileLogger& dlogger) {
		MYSQL_RES *res;
	    MYSQL_ROW row;
	    string sqlString;
	    short int j,k;
	    short int chkStatus = 0;
	    string currentPwdDB;
	    FileLogger logger = dlogger;

	    logger.logDebug("[chkUserPwd]");
	    sqlString="SELECT pwd FROM userprofile WHERE phone = '"+phone+"' AND app_status=2";
    	char *csql = stringToChar(sqlString);
    	short int a=mysql_real_query(myData, csql, strlen(csql));

    	if (a!=0) {
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot get pwd in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    	chkStatus=2;
    	} else {
    		res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		string newData = (row[0] ? row[0] : "");
	    		if (newData.compare(pwd)==0)
	    			chkStatus=0;
	    		else
	    			chkStatus=3;
	    		mysql_free_result( res ) ;
	    	} else {
	    		chkStatus=1;
	    	}
    	}
    	delete[] csql;
	    return chkStatus;
	}

	short int getUserPwd(string phone, string& pwd, MYSQL *myData,const FileLogger& dlogger) {
		MYSQL_RES *res;
	    MYSQL_ROW row;
	    string sqlString;
	    short int j,k;
	    short int pwdStat;
	    FileLogger logger = dlogger;

	    logger.logDebug("[getUserPwd]");
    	sqlString="SELECT pwd FROM userprofile WHERE phone = '"+phone+"' AND app_status=2";
    	char *csql = stringToChar(sqlString);
    	short int a=mysql_real_query(myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		pwd = (row[0] ? row[0] : "");
	    		pwdStat=0;
	    		mysql_free_result( res ) ;
	    	} else {
	    		pwdStat=1;
	    	}
    	} else {
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot get pwd in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    	pwdStat=2;
    	}
    	delete[] csql;
	    return pwdStat;
	}

	short int getUserMotherName(string phone, string motherName, MYSQL *myData,const FileLogger& dlogger) {
		MYSQL_RES *res;
	    MYSQL_ROW row;
	    string sqlString;
	    short int j,k;
	    FileLogger logger = dlogger;
	    short int chkStatus = 0;

	    sqlString="SELECT mother FROM userprofile WHERE phone = '"+phone+"' AND app_status=2";
	    logger.logDebug("[getUserPwd sql:"+sqlString+"]");
	    char *csql = stringToChar(sqlString);
    	short int a=mysql_real_query(myData, csql, strlen(csql));
    	if (a==0) {
	    	res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		string newData = (row[0] ? row[0] : "");
	    		newData = StringToUpper(newData);
	    		logger.logDebug("[compare:"+motherName+" with db:"+newData+"]");
	    		if (motherName.compare(newData)==0) {
	    			chkStatus=0;
	    		} else {
	    			chkStatus=3;
	    		}
	    		mysql_free_result( res ) ;
	    	} else {
	    		chkStatus=1;
	    	}
    	} else {
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot get mother name in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    	chkStatus=2;
    	}
    	delete[] csql;
	    return chkStatus;
	}

	short int changeUserPwd(string phone, string newPwd, MYSQL *myData, const FileLogger& dlogger) {
		short int updateStatus=1;
		string sqlString;
		FileLogger logger = dlogger;

		logger.logDebug("[changeUserPwd]");
		sqlString="UPDATE userprofile SET ";
		sqlString+=" pwd='"+newPwd+"'";
		sqlString+=" WHERE phone = '"+phone+"'";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	updateStatus=0;
	    } else {
	    	updateStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot update pwd in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return updateStatus;
	}

	short int deleteFile(string fileName) {
		char *charFileName = stringToChar(fileName);
		short int resultRemove = remove(charFileName);
		//cout << "Delete file:" << fileName << " result:" << resultRemove;
		return resultRemove;
	}

	short int renameFile(string oldFile, string newFile) {
		char *oldname = stringToChar(oldFile);
		char *newname = stringToChar(newFile);
		int resultRename = rename(oldname, newname);
		if (resultRename==0)
			return 0;
		else
			return 1;
	}

	short int createFailedLog(string logFailedTxName) {
		FileLogger failedTx (logFailedTxName);
		failedTx.createLog();
		return 0;
	}

	string getUserLangDB(string phone, MYSQL *myData,  const FileLogger& dlogger) {
		short int lang=0;
		short int whiteStat=2;
		string sqlString;
		string chkUser;
		string chkStat;
		MYSQL_RES *res;
		MYSQL_ROW row;

		FileLogger logger = dlogger;
		chkUser = "SELECT language,whitelist_stat FROM userprofile WHERE phone = '"+phone+"'";
		char *csqlChkUser = stringToChar(chkUser);
		short int a=mysql_real_query(myData, csqlChkUser, strlen(csqlChkUser));
    	if (a!=0) {
    		lang=0;
    		whiteStat=2;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot get user language. Error:"+errorMsg+" in sql:"+chkUser+"]");
    	} else {
			res=mysql_store_result(myData);
	    	if (mysql_affected_rows(myData)>0) {
	    		row = mysql_fetch_row( res );
	    		lang = stringToInt(row[0]);
	    		whiteStat = stringToInt(row[1]);
	    		mysql_free_result( res ) ;
	    		logger.logDebug("[Language for "+phone+" found in DB:"+intToString(lang)+", whitelist status:"+intToString(whiteStat)+"]");
	    	} else {
	    		lang=0;
	    		whiteStat=2;
	    		logger.logDebug("[Phone number not found in DB:"+phone+"]");
	    	}
    	}
    	chkStat = intToString(lang) + "," + intToString(whiteStat);
    	return chkStat;
	}

	short int updateDayCounter(string phone, short int counter, MYSQL *myData, const FileLogger& dlogger) {
		short int updateStatus=1;
		string sqlString;
		FileLogger logger = dlogger;

		logger.logDebug("[changeUserPwd]");
		sqlString="UPDATE userprofile SET ";
		sqlString+=" day_counter = "+intToString(counter)+",";
		sqlString+=" counter_dt = '" +currentDateLimit()+ "'";
		sqlString+=" WHERE phone = '"+phone+"'";
		char *csql = stringToChar(sqlString);

		short int b=mysql_real_query(myData,csql,strlen(csql));
	    //printf(" b= %d ",b);
	    if (b==0) {
	    	updateStatus=0;
	    } else {
	    	updateStatus=1;
	    	string errorMsg(mysql_error(myData));
	    	logger.logError("[Cannot update counter in userprofile Error:"+errorMsg+" in sql:"+sqlString+"]");
	    }

		delete[] csql;
		return updateStatus;
	}
}

