#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

//#include "generalfunction.h"
#include "FileLogger.h"

using namespace std;

namespace ats {
    
	FileLogger::FileLogger() {
	}
	
	FileLogger::FileLogger(string nameFile) {
		filename = nameFile; 
    }
    
	int FileLogger::createLog() {
		ofstream myfile;
      	const char *filelog = filename.c_str();
      	myfile.open(filelog,ios::out | ios::binary);
      	myfile.close();
      	return 0;
	}
	
	int FileLogger::logDebug (string inputString) {
      	ofstream myfile;
      	string currentDateTime;
      	const char *filelog = filename.c_str();
      	myfile.open(filelog,ios::out | ios::app | ios::binary);
      	currentDateTime = currentTimestamp();
      	myfile << "DEBUG[" << currentDateTime << "]" << inputString << "\n";
      	myfile.close();
      	return 0;
    }
    
    int FileLogger::logError (string inputString) {
      	ofstream myfile;
      	string currentDateTime;
      	const char *filelog = filename.c_str();
      	myfile.open(filelog,ios::out | ios::app | ios::binary);
      	currentDateTime = currentTimestamp();
      	myfile << "ERROR[" << currentDateTime << "]" << inputString << "\n";
      	myfile.close();
      	return 0;
    }
    
    int FileLogger::logNotice (string inputString) {
        ofstream myfile;
        string currentDateTime;
        const char *filelog = filename.c_str();
        myfile.open(filelog,ios::out | ios::app | ios::binary);
        currentDateTime = currentTimestamp();
        myfile << "NOTICE[" << currentDateTime << "]" << inputString << "\n";
        myfile.close();
        return 0;
    }
    
    int FileLogger::logWarn (string inputString) {
      	ofstream myfile;
      	string currentDateTime;
      	const char *filelog = filename.c_str();
      	myfile.open(filelog,ios::out | ios::app | ios::binary);
      	currentDateTime = currentTimestamp();
      	myfile << "WARNING[" << currentDateTime << "]" << inputString << "\n";
      	myfile.close();
      	return 0;
    }
    
    int FileLogger::logData (string inputString) {
      	ofstream myfile;
      	const char *filelog = filename.c_str();
      	myfile.open(filelog,ios::out | ios::app | ios::binary);
      	if (myfile)
      	{
      		myfile << inputString << "\n";
      		myfile.close();
      		return 0;
      	} else {
      		return 1;
      	}
      	
    }
    

string FileLogger::currentTimestamp() {
		char timestamp[100];
		string currentdatetime;
	  	time_t mytime;
	  	struct tm *mytm;
	  	mytime=time(NULL);
	  	mytm=localtime(&mytime);
	
	  	strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);
	  
	  	long milliseconds;
	  	char stringMillisecond[100];
	  	struct timeval tv;
	  	gettimeofday (&tv, NULL);
	  	milliseconds = tv.tv_usec / 1000;
	  	strcat(timestamp,".");
	  	sprintf(stringMillisecond,"%d",milliseconds);
	  	strcat(timestamp,stringMillisecond);
	  	currentdatetime = (string)timestamp;
	  	return currentdatetime;
	}


}
