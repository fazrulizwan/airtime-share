#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "FileLogger.h"

using namespace std;
using namespace ats;

int CountLine(string keyword){

	string fname=keyword+".watch";
	int counter=0;
	string line;

	ifstream fileConfig (fname.c_str());

	if (fileConfig.is_open()) {
		while (!fileConfig.eof()) {
			getline (fileConfig,line);
			counter++;	
		}
	}

	fileConfig.close();

	return counter;

}

int Check(string keyword){

	string cmd;

	cmd="ps -ef | grep "+keyword+" | grep -v watchdog | grep -v grep > "+keyword+".watch";	

	system(cmd.c_str());	

	sleep(1);

	if(CountLine(keyword)>1)
		return 0;
	else
		return -1;

}


string buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}

int main(int argc,char* argv[]){

	string keyword;
	string runscript;

	if(argc!=3){
		cout<<"keyword runscript"<<endl;
		return 1;
	}

	keyword=buffToStr(argv[1]);
	runscript=buffToStr(argv[2]);

	FileLogger log("/home/cat/src/logs/watchdog.log");

	if (Check(keyword)<0){
		log.logWarn(keyword+" down.Attempting restart..");
		sleep(1);
		system(runscript.c_str());
		sleep(1);
		if(Check(keyword)<0){
			log.logWarn(keyword+" still down.");
		}
		else{
			log.logNotice(keyword+" up and running");
		}
	}
	else{
		log.logNotice(keyword+" up and running");

	}
}
