-- MySQL dump 10.11
--
-- Host: 10.122.17.71    Database: ats
-- ------------------------------------------------------
-- Server version	5.1.34-percona-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `userprofile`
--

DROP TABLE IF EXISTS `userprofile`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(40) DEFAULT NULL,
  `pwd` varchar(10) DEFAULT 'NULL' COMMENT 'user password',
  `mother` varchar(100) DEFAULT NULL,
  `app_status` tinyint(1) DEFAULT '0' COMMENT 'application status (0-active 1-blacklist)',
  `in_date` datetime DEFAULT NULL,
  `in_status` tinyint(1) DEFAULT '0' COMMENT 'status from IN (telco server) (0-active 1-expired 2-grace period 3-blacklist)',
  `reg_date` datetime DEFAULT NULL COMMENT 'user registration date to ShareTopup system',
  `user_type` tinyint(1) DEFAULT '0' COMMENT '0-prepaid 1-postpaid',
  `brand` tinyint(1) DEFAULT NULL,
  `tx_id` varchar(20) DEFAULT NULL,
  `daily_trf_amt` int(11) DEFAULT '0' COMMENT 'accumulative daily limit',
  `day_dt` varchar(8) DEFAULT 'NULL' COMMENT 'daily limit date (YYYYMMDD-20071102)',
  `daily_sc_trf` int(11) DEFAULT '0' COMMENT 'total count daily successfull transfer',
  `last_sc_trf` datetime DEFAULT NULL COMMENT 'timestamp of last successfull tranfer',
  `package` varchar(20) DEFAULT NULL,
  `language` tinyint(1) NOT NULL DEFAULT '1',
  `update_date` datetime DEFAULT NULL,
  `whitelist_stat` tinyint(1) NOT NULL DEFAULT '0',
  `day_counter` tinyint(1) NOT NULL DEFAULT '0',
  `counter_dt` varchar(8) DEFAULT NULL,
  `blacklist_stat` tinyint(4) DEFAULT '0',
  `reason_blacklist` varchar(500) DEFAULT NULL,
  `promo_stat` tinyint(1) DEFAULT NULL,
  `daily_received` int(11) DEFAULT NULL,
  `received_dt` varchar(8) DEFAULT NULL,
  `promo_end` date DEFAULT NULL,
  `promo_start` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`),
  KEY `phone_n_status` (`phone`,`app_status`),
  KEY `promo_end` (`promo_end`)
) ENGINE=InnoDB AUTO_INCREMENT=23964405 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-05-31  1:06:36
