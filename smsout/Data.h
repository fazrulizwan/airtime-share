#include <iostream>

using namespace std;

class Data{
        public:
        Data();
        string getQRYTags(void);
        void setQRYTags(string);
        string getRESTags(void);
        void setRESTags(string);

        private:
        string m_qry_tags;
        string m_res_tags;
};


