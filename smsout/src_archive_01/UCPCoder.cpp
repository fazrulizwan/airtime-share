/************************************************************
	UCP Coder v1.00 (Dev) - Low-level Logica/EMI UCP
				coder

	Developed by Fazrul Izwan Hassan
	24 Mar 2008
	Idot tv Sdn Bhd 

*************************************************************/

#include <iostream>
#include <sstream>
#include <limits>
#include <bitset>

#include "UCPCoder.h"

using namespace std;

string UCPCoder::getOp60(string oadc,string pwd){

	/******************************************
	EMI Operation 60 -Session Login/Management-
	In: SMSC username (oadc) and password
	Out: Op60 string packet	
	******************************************/

	string UCP_ret; //**holds return value
	string checksum;

	//*** the ^^^^^^ is pre-set because we don't know yet the len of whole packet, it will be replaced shortly before the actual packet is returned
	UCP_ret="00/^^^^^/O/60/"+oadc+"/"+OTON+"/"+ONPI+"/"+STYPE+"/"+AsciiToHexDump(pwd,false)+"//"+VERSION+"//////";		

	return STX+setPacketLen(UCP_ret)+ETX;

}

/*
struct MO UCPCoder::getMODetail(string packet){

	//****************************************
	Given a Op52 packet (deliver sm),extract
	the MO details
	*****************************************

	struct MO retval;

	int start_pos=0;
	int end_pos=0;
	int slashcount=0;

	retval.seq_no=StrToInt(packet.substr(1,2));

	for(int i=0;i<packet.length();i++){
            if(packet.at(i)=='/'){
                slashcount++;
                if(slashcount==4)
                    start_pos=i+1;
                if(slashcount==5){
                    end_pos=i-1;
                    break;
                }
            }

        }
        retval.adc=packet.substr(start_pos,end_pos-start_pos+1);

	slashcount=0; //**reset for oadc
	for(int i=0;i<packet.length();i++){
            if(packet.at(i)=='/'){
                slashcount++;
                if(slashcount==5)
                    start_pos=i+1;
                if(slashcount==6){
                    end_pos=i-1;
                    break;
                }
            }

        }
        retval.oadc=packet.substr(start_pos,end_pos-start_pos+1);

	slashcount=0; //** reset for msg
	for(int i=0;i<packet.length();i++){
            if(packet.at(i)=='/'){
                slashcount++;
                if(slashcount==24)
                    start_pos=i+1;
                if(slashcount==25){
                    end_pos=i-1;
                    break;
                }
            }

        }
        retval.msg=HexDumpToAscii(packet.substr(start_pos,end_pos-start_pos+1));

	return retval;

}
*/

string UCPCoder::getOp51(string seq_no,string oadc,string adc,string msg){

	/********************************************************* 
	   EMI Operation 51 -Submit msg-  
	   In: packet sequence no, oadc, adc , msg
	   Out: UCP packet in String (Operation 51 -submit msg)	
	*********************************************************/

	string UCP_ret; //**holds return value
        string checksum;

        //*** the ^^^^^^ is pre-set because we don't know yet the len of whole packet, it will be replaced shortly before the actual packet is returned
	UCP_ret=seq_no+"/^^^^^/O/51/"+adc+"/"+oadc+"/////////////////3//"+AsciiToHexDump(msg,false)+"/////////////";		
	return STX+setPacketLen(UCP_ret)+ETX;

}

string UCPCoder::getOp57Resp(){

        /********************************************
        EMI Response for Operation 57
        *********************************************/

        string UCP_ret;

        UCP_ret="00/00020/R/57/A///9A";

        return STX+UCP_ret+ETX;

}


string UCPCoder::getKeepAlive(){

	/*********************************************
	Return keep alive packet
	*********************************************/

	string UCP_ret;

	//** Dummy packet, meaningless, just to wake the SMSC
	UCP_ret="00/00076/O/52/0123456789/0123456789/////////////////3//414243/////////////00";	

	return STX+UCP_ret+ETX;
}

string UCPCoder::getOp52Resp(string resp_packet,string resp_type){

	/**********************************************
	EMI Operation 52 Resp -Responding to Op52 frm SMSC
	In: MO packet, resp_type A-ACK N-NACK
	Out: Raw response packet
	***********************************************/

	string UCP_ret;
	string seq_no=resp_packet.substr(1,2);

	UCP_ret=seq_no+"/^^^^^/R/52/"+resp_type+"///";

	return STX+setPacketLen(UCP_ret)+ETX;	
}

string UCPCoder::AsciiToHexDump (string asciiStr, bool c_string){

	/*************************************
	AsciiToHexDump
	given ascii readable string, converts into hex string.
	if c_string is true, '00' (null) is appended to end of string
	*************************************/

	int i;
	string retval;

	
	for(i=0;i<asciiStr.length();i++){
		if(asciiStr.at(i)=='|') //*** If pipe is found,it is a CR
			retval=retval+intToHexStr(13); //*** CR
		else
			retval=retval+intToHexStr((int)asciiStr.at(i));
	}

	if(c_string)
		retval=retval+"00";	

	return retval;

}

string UCPCoder::HexDumpToAscii(string hexDump){

	/***********************************
	Converts HexDump to readable text
	************************************/

	int i;
	string retVal="";

	for(i=0;i<hexDump.length();i+=2){
		retVal=retVal+(char)HexStrToInt(hexDump.substr(i,2));	
	}
	
	return retVal;		

} 

string UCPCoder::setPacketLen(string ucp_packet){
	
	/*****************************************
	Task: to count the length of the whole packet (excluding STX,ETX) and replace the pre-set ^^^^^ mask with
	fix length 5-byte LEN item in header, and to complete the checksum (not in this method)
	In: UCP packet (excluding 2-byte checksum, including msg in IA5, LEN (header) must be 5-byte char ^^^^^)
	out:UCP packet with LEN is set, WITH checksum. 	
	****************************************/

	int len=ucp_packet.length()+2; //** Assuming 2-byte checksum
	string lenstr;
	string leftside;
	string rightside;
	string packet;

	leftside=ucp_packet.substr(0,3); //** Get the left cut before the first ^
	rightside=ucp_packet.substr(8,ucp_packet.length()); //** Get rightcut after the last ^

	//*** Put trailing zeroes
	if(len<10)
		lenstr="0000"+IntToString(len);
	else if(len<100)
		lenstr="000"+IntToString(len);
	else if(len<1000)
		lenstr="00"+IntToString(len);
	else if(len<10000)
		lenstr="0"+IntToString(len);
	else if(len<100000)
		lenstr=IntToString(len);

	packet=leftside+lenstr+rightside; //*** Combine all parts
	packet=packet+getCheckSum(packet); //** Now concat with checksum

	return packet;
	
}

string UCPCoder::getCheckSum(string ucp_packet){

	/***********************************************
	Task: summing all bytes of packet (excluding STX,ETX) and return the checksum
	In: UCP packet (without checksum)
	Out: the checksum 
	***********************************************/
	
	int sum=0;
	string bin_string;
	int dec;
	string hex_string; 
	
	for(int i=0;i<ucp_packet.length();i++)	
		sum=sum+(int) ucp_packet.at(i);

	bin_string=IntToBinaryString(sum); //** Converts int to 8-bit binary string,ONLY the 8 LSB
	dec=BinaryStringToInt(bin_string); //** Converts the 8 LSB to int
	hex_string=intToHexStr(dec); //** Converts the dec into hexstr

	return hex_string;	

}

string UCPCoder::intToHexStr(int n){

	char buff[10];
	sprintf(buff,"%X",n);
	string retval;

	if(strlen(buff)==1){
		buff[9]='\0';
                buff[8]=buff[1];
                buff[7]=buff[0];
                buff[6]='0';
                buff[5]='0';
                buff[4]='0';
                buff[3]='0';
                buff[2]='0';
                buff[1]='0';
                buff[0]='0';
        }

	//cout << "Buffer:::"<<buff<<endl;
	retval=buffToStr(buff);
	//log.logDebug("input:"+IntToString(n)+" retval:"+retval);
	//log.logDebug("Debughere:"+retval.substr(retval.length()-2,2));
	return retval.substr(retval.length()-2,2);

}

string UCPCoder::IntToBinaryString(int no){

	/**************************************************
	Converts an int no to binary string. Bit len determines
	length of the bit
	e.g. 255 to 11111111
	**************************************************/

	std::ostringstream ret;
	static const unsigned BIT_LENGTH = 8;
	std::bitset<BIT_LENGTH> b = no;
	ret << b;

	return ret.str();
}


int UCPCoder::BinaryStringToInt(string in)
{

	std::ostringstream ret;
    	int retval;

      	ret << bitset<numeric_limits<unsigned long>::digits>(in).to_ulong();
	istringstream strval(ret.str());
	strval>>retval;

      	return retval;
}



string UCPCoder::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


string UCPCoder::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


int  UCPCoder::getBytes (char *byteArray, string plainText){

	/*****************************************************
	Converts textual packet into bytes representation.
	In: referenced byte array for output, plaintext packet
		for input
	Return: length of the bytes
	*****************************************************/

	int i;
	string token;
	int dataLen=0;

	for(i=0;i<plainText.length();i+=2){
		dataLen++;
		token=plainText.substr(i,2);
		*byteArray=(unsigned char)HexStrToInt(token);
		byteArray++;
	}
	*byteArray='\0';
	return dataLen;

}

long int UCPCoder::HexStrToInt(string hexStr){

	string tempHexStr="";
	int i;

	for(i=0;i<hexStr.length();i++){
		if(hexStr.substr(i,1)!=" ")
			tempHexStr=tempHexStr+hexStr.substr(i,1);
	}

	return strtol(tempHexStr.c_str(),NULL,16);

}


int UCPCoder::decodeResp (string resp_packet){

	/*************************************
	Receive UCP response packet and decode
	the packet into packet type
	In: raw resp UCP packet
	Out: packet type
	*************************************/
	//*** Op51
        if(resp_packet.substr(12,2)=="51"){
                if(resp_packet.substr(15,1)=="A"){
                        return OP51_RESP_OK;
                }
		else if(resp_packet.substr(15,1)=="N"){
                        return OP51_RESP_FAIL;
                }
	}

	//*** Op60
	if(resp_packet.substr(12,2)=="60"){
		if(resp_packet.substr(15,1)=="A"){
			return OP60_RESP_OK;
		}
		else if(resp_packet.substr(15,1)=="N"){
                        return OP60_RESP_FAIL;
                }
		else{
			return INVALID_RESP_FORMAT;
		}
	}

	//*** Op52
	if(resp_packet.substr(12,2)=="52"){
		if(resp_packet.substr(10,1)=="O")
			return OP52_OP_PACKET;
	}

	//*** Op57
        if(resp_packet.substr(12,2)=="57"){
                if(resp_packet.substr(10,1)=="O")
                        return OP57_OP_PACKET;
        }


}

int UCPCoder::StrToInt(string inval){

	istringstream buffer(inval);
	int retval;

	buffer>>retval;

	return retval;		

} 
