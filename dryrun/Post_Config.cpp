#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "Post_Config.h"

using namespace std;

	PostConfig_t PostConfig::ReadConfig(string conf_file) {
		string line;
		string name;
		string value;
		int posEqual;
		PostConfig_t inconf;
		ifstream fileConfig (conf_file.c_str());

		//**** Init of conf struct
		inconf.pg_endpoint="";
		inconf.pg_port=-1;
		inconf.SSL_cert="";
		inconf.pg_login="";
		inconf.pg_pwd="";
		inconf.ema_ip="";
		inconf.ema_port=-1;
		inconf.postconn_log_path="";
		inconf.ema_login="";
		inconf.ema_pwd="";
		
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trim(name);
				value = line.substr(posEqual+1);
				trim(value);
				//cout << "[Name:" << name << "=" << value << "]";
				
				if (name=="pg_endpoint") {
					inconf.pg_endpoint=value.c_str();
					continue;
				}
				else if (name=="pg_port") {
					inconf.pg_port=atoi(value.c_str());
					continue;
				} else if (name=="SSL_cert") {
					inconf.SSL_cert= value.c_str();
					continue;
				}
				else if (name=="pg_login") {
					inconf.pg_login = value.c_str();
					continue;
				}
				else if (name=="pg_pwd") {
					inconf.pg_pwd = value.c_str();
					continue;
				}	
				else if (name=="ema_ip") {
					inconf.ema_ip = value.c_str();
					continue;
				}
				else if (name=="ema_port") {
					inconf.ema_port = atoi(value.c_str());
					continue;
				}
				else if (name=="log_path") {
					inconf.postconn_log_path = value.c_str();
					continue;
				}
				else if (name=="ema_login") {
					inconf.ema_login = value.c_str();
					continue;
				}
				else if (name=="ema_pwd") {
					inconf.ema_pwd = value.c_str();
					continue;
				}
				
				
			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			inconf.load_status=1;
			return inconf;
		} else {
			//cout << "Cannot open config file!";
			return inconf;
		}
	}


void PostConfig::trim(string& str)
{
        string::size_type pos1 = str.find_first_not_of(' ');
        string::size_type pos2 = str.find_last_not_of(' ');
        str = str.substr(pos1 == string::npos ? 0 : pos1,
        pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

