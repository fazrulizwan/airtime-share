#ifndef COREPROCESS_H_
#define COREPROCESS_H_

#include <string>

#include "sharedmem.h"
#include "MTMessage.h"
#include "BusinessRules.h"
#include "FileLogger.h"
#include "Subscriber.h"

using namespace std;

namespace ats {

typedef struct MT {
	string receiver;
	string oadc;
	string msg;
	int charging;
}MT_tau;

typedef struct MTCollectionStruct {
        struct MT smsMT[6];
}MTCollection;
;

class CoreProcess {
	public:
		CoreProcess(string,string,string,string,PortReg,BusinessRules,MTMessage,short int);
		CoreProcess(string,string,string,string,PortReg,BusinessRules,MTMessage,short int,short int);
		MTCollection ProcessMessage(void);
		string phone;
		string sms;
		string shortcode;
		string tx_id;
		MTCollection MTMsg;
		PortReg inPort;
		short int channel;
		short int userLang;
		short int smsc_id; //1-nextgen 2-trinode
	private:
		MTMessage mtLib;
		BusinessRules bisnesRulesObject;
		short int QueryProfileIN(Subscriber&,FileLogger);
  };



}

#endif /*COREPROCESS_H_*/
