#ifndef POST_CONN_H_
#define POST_CONN_H_

#include <iostream>

#include "sharedmem.h"

using namespace std;

const short int TRANSFER_STATUS_CREDIT_FAIL=7;
const short int TRANSFER_STATUS_DEBIT_FAIL=8;

//**** Query Status const
/*
const short int QRY_STATUS_OK=0;
const short int QRY_STATUS_FAIL=1;
const short int QRY_STATUS_REFUND_OK=3;
const short int QRY_STATUS_REFUND_FAIL=4; //*** debit
const short int QRY_STATUS_EXTERNAL_FAIL=5;
const short int QRY_STATUS_REFUND_FAIL_CREDIT=6;

//**** Language Const
const short int LANG_BM=1;
const short int LANG_ENGLISH=2;
const short int LANG_MANDARIN=3;

//*** Platform Const
const short int PFORM_NEC=0;
const short int PFORM_LOGICA=1;
const short int PFORM_POSTPAID=-9;

//*** User status const
const short int USR_STATUS_PREREG=1;
const short int USR_STATUS_ACTIVE=2;
const short int USR_STATUS_TERMINATED=4;

//**** User type const
const short int USR_TYPE_PREPAID=0;
const short int USR_TYPE_POSTPAID=1;
*/

//*********** Return structure
//*** Query Profile return structure
typedef struct Post_UserProfile
{
   short int queryStatus; //*** use query status const
   short int userLang;	//*** use language const 
   short int userPlatform; // *** use platform const 
   short int userStatus; //*** use user status const
   std::string expiryDate; //*** YYYY-MM-DD
   float userBalance;
   short int userType; //Use user type const 
   std::string activationDate; //*** YYYY-MM-DD
   int brand;
   int roaming;
   float credit_limit;
   float total_due;//overdue
   float current_due;	
   float unbill;
   int vip_code;
   bool has_1p5;
   bool has_act_tcl;	
   bool has_LA;
   string last_invoice_date;
   string act_date; //activation date
   int los;
   string act_no;							
}Post_USRProfile;

typedef struct Post_TransferResult{
	short int transferStatus; //*** use query status const
	std::string aExpiryDate; //*** YYYY-MM-DD
   	std::string bExpiryDate; //*** YYYY-MM-DD
   	float aBalance;
   	float bBalance;
};

typedef struct Credit_Op{
	int op_status;
	float new_bal;
	string new_expdate;
}CreditOp;

class Post_Conn{

	public:
	struct Post_UserProfile QueryProfile(string phone_no,PortReg port_reg,string txid);
	struct Post_TransferResult Transfer(string a_no,string b_no,float amt,int validity,float a_charging,float b_charging,string txid,int a_pform,int b_pform,PortReg port_reg,string tx_type,string act_no);
	string getAccNum(string,string,PortReg);
	struct Credit_Op DoCredit(string a_no,string b_no,string txid,float amt,int validity,int b_pform,PortReg port_reg,string tx_type,float b_charging);	
	string getStatementDate(string phone_no,PortReg port_reg,string txid,string accnum);	
	string boolToString(bool);
	bool strToBool(string);

	private:
	string buffToStr(char *buff);
	string floatToStr(float val);
	string currentTimestamp(void);
	string IntToStr(int);
	string IntToString(int);
	long int HexStrToInt(string);
	

};

#endif

