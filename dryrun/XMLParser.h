#ifndef XML_PARSER_H
#define XML_PARSER_H

#include <iostream>

using namespace std;

namespace ats {
          class XMLParser {
                public:
                		string xml;
                		string resultXml;
                		XMLParser(string&);
                		bool extractResult(string);
                       	string FindResponseCode();
			string FindResponseDesc();
                       	string FindResultValue(string);
          };
}

#endif

