#include <iostream>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>

#include "generalfunction.h"
#include "Request.h"
#include "Subscriber.h"
#include "FileLogger.h"
#include "MysqlConnDB.h"
#include "BusinessRules.h"
#include "date.h"
#include "IN_Conn.h"
#include "Post_Conn.h"

using namespace std;

namespace ats {
          Request::Request(string txid, Subscriber senderProfile, int transferAmt, string userPin, string receiverPhone,  const BusinessRules& rules, const FileLogger& fileLogger, const MTMessage& mtMsg, PortReg portIN, short int channelType, MysqlConnDB& myData, short int lang, short int op_type, string requestor, short int smsc_type) {
        	 profileA = senderProfile;
        	 a_phone = profileA.getPhone();
             amt = transferAmt;
             pin = userPin;
             b_phone = receiverPhone;
             tx_id = txid;
             bizrules = rules;
             mtLibrary = mtMsg;
             logger = fileLogger;
             inPort = portIN;
             channel = channelType;
             langA = profileA.getLang();
             mySQLConn = myData;
             operation_type = op_type;
             requestor_name = requestor;
	     	 smsc_id = smsc_type;
             logger.logDebug("[TX ID:"+tx_id+"] [Constructor Request] [A PHONE:"+a_phone+"] [PWD:"+pin+"] [B PHONE:"+b_phone+"] [AMT:"+intToString(amt)+"]");
          }

          //transfer process
          MTCollection Request::makeRequest() {
             //check syntax
        	 float prevBalanceA=0;
        	 float prevBalanceB=0;
        	 float newBalanceA=0;
        	 float newBalanceB=0;
        	 float max_trf_amt;
        	 string smsOut1;
        	 string smsOut2;
        	 string foundFile;
        	 string fileTimeStamp;
        	 string dtCAR, txidCAR;
        	 int langCAR, amtCAR;
		     string nickname;

        	 //log for transaction logs
        	 string logtx = mtLibrary.dir_atr_tx+"/"+currentDate()+".tx";
        	 FileLogger txLogger (logtx);

        	 //log for debug
        	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest - START] ");

             MTMsgOut.smsMT[0].receiver = a_phone;
             MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

             //check pending request from DB

             //check pending ShareTopup request from A to B in pending file list
             string fileToSearch = a_phone+"."+b_phone;
             short int isExist = searchPendingCAR(a_phone, b_phone,mySQLConn.myData,logger,langCAR,dtCAR,txidCAR,amtCAR, nickname);
             if (isExist==0) {
            	 //check timeout first
            	 fileTimeStamp = dtCAR;
            	 string currentTime = currentDateTime2();
            	 short int timeout = bizrules.atr_tmt;
            	 int timeoutInSec = timeout * 60;
            	 struct tm tmNow, tmRequest;
            	 strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
            	 strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
            	 double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
            	 int dayDiff=0;
            	 int timeDiff=0;
            	 //cout << "Time different : " << secDiff;
            	 if (secDiff>timeoutInSec) {
            		 //request already expired, delete pending file
            		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Pending request already timeout, delete pending file, then proceed]");
            		 deletePendingCAR(a_phone, b_phone,mySQLConn.myData,logger);
            	 } else {
            		 //request not expired yet, so have pending request
            		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Still Have Pending Request]");
	             	 txLogger.logData(b_phone+ "|"+a_phone+"||"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|70|"+currentDateTime3()+"|"+tx_id);
	             	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(70, langA);
	             	 return MTMsgOut;
            	 }
             } else if (isExist==2) {
            	 //cannot open directory
            	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Error Query database in chk pending CAR]");
            	 txLogger.logData(b_phone+ "|"+a_phone+"||"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
            	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
            	 return MTMsgOut;
             } else if (isExist==1) {
            	 //no pending request, proceed to process request
            	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [No Pending Request, proceed]");
             } else {
            	 //error opening directory
            	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:while query database in chk pending CAR]");
            	 txLogger.logData(b_phone+ "|"+a_phone+"||"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
            	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
            	 return MTMsgOut;
             }

             logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Check Pending:No Pending Request,check initial business rules]");

             int yr;
             int mth;
             int dt;
             long dt1,dt2;
             short int blacklistStat=0;
             short int whiteListStatB;
             string resultDB;

        	 //Subscriber profileA;
         	 Subscriber profileB;
         	 //profileA.setPhone(a_phone);//requestor
         	 profileB.setPhone(b_phone);//donor

         	 //get profile A & B from DB
         	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Get Profile Donor from DB]");
         	 getProfileDB(profileB,mySQLConn.myData,logger);
		logger.logDebug("[TX ID:"+tx_id+"] profile B type:"+intToString(profileB.getType())); //TODO izwan: remove back

		//****** TODO izwan: this block to check whether b_no is post or pre.
                if(profileB.getType()==-1){ //*** Profile B does not exist in profile DB, we must query MP and PG to ensure post or pre!!
                        logger.logDebug("[TX ID:"+tx_id+"] [B no not exist in profile DB. Proceed to check from MP.]");
                        struct IN_UserProfile profilepostorpre;
                        IN_Conn inconnpostpre;
                        profilepostorpre = inconnpostpre.QueryProfile(profileB.getPhone(),inPort,tx_id);
                        if(profilepostorpre.queryStatus==0) { //*** confirmed prepaid
                                logger.logDebug("[TX ID:"+tx_id+"] [MP ACK: B no confirmed prepaid]");
                                profileB.setType(0);
                        }
                        else if(profilepostorpre.queryStatus==7){ //*** MP confirmed not in prepaid, lets try if this is postpaid
                                logger.logDebug("[TX ID:"+tx_id+"] [MP ACK: B no confirmed NOT in prepaid. Proceed to check from PG]");
                                struct Post_UserProfile postprofile;
                                Post_Conn postconnpostpre;
                                postprofile = postconnpostpre.QueryProfile(profileB.getPhone(),inPort,tx_id);
                                if (postprofile.queryStatus==0) { //*** confirm postpaid
                                        logger.logDebug("[TX ID:"+tx_id+"] [PG ACK: B no confirmed postpaid");
                                        profileB.setType(1);
                                        profileB.setLang(postprofile.userLang);
                                        profileB.setAppSatus(0);
                                        preRegisterUserProfilePostpaid(profileB, tx_id, mySQLConn.myData, logger);
					logger.logDebug("[TX ID:"+tx_id+"] [Postpaid profile inserted into db");
                                }
                                else{ //*** PG might give erroneous status.
                                        logger.logDebug("[TX ID:"+tx_id+"] [PG ERROR: cannot confirm B no as postpaid]");
                       			txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|18|"+currentDateTime3()+"|"+tx_id);
                       			MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(18, profileA.getLang());
                       			return MTMsgOut;
                                }
                        }
                        else{ //*** MP might give erroneous status.
                                logger.logDebug("[TX ID:"+tx_id+"] [MP ERROR: cannot confirm B prepaid/postpaid]");
				txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|18|"+currentDateTime3()+"|"+tx_id);
                                MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(18, profileA.getLang());
                                return MTMsgOut;
                        }

                }
                //*** End of TODO izwan
         	 //changes on 23/10/2008: get profile DB at core process
         	 //getProfileDB(profileA,mySQLConn.myData,logger);

         	 if (profileA.getBlacklistStat()==6  || profileA.getBlacklistStat()==7) {
         		 //requestor is blacklisted all or blacklisted receive only
         		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Requestor blacklist status:"+intToString(profileA.getBlacklistStat())+"]");
         	 	 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|178|"+currentDateTime3()+"|"+tx_id);
         	 	 smsOut1 = mtLibrary.getMsg(178, profileA.getLang(),  profileA.getBrand());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<bno>",a_phone);
		 		 return MTMsgOut;
         	 } else if (profileB.getBlacklistStat()==6 || profileB.getBlacklistStat()==8) {
         		 //donor is blacklisted all or blacklisted send only
         		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Receiver blacklist status:"+intToString(profileB.getBlacklistStat())+"]");
         	 	 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|177|"+currentDateTime3()+"|"+tx_id);
         	 	 smsOut1 = mtLibrary.getMsg(177, profileA.getLang(),profileA.getBrand());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<ano>",b_phone);
		 		 return MTMsgOut;
         	 } else if (profileA.getPromoStat()==6  || profileA.getPromoStat()==7) {
				 //check if receiver is under promo : (cannot send & receive) or (cannot receive)
				 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Requestor promo status:"+intToString(profileA.getPromoStat())+"]");
				 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|182|"+currentDateTime3()+"|"+tx_id);
				 smsOut1 = mtLibrary.getMsg(182, profileA.getLang(),  profileA.getBrand());
				 smsOut1 = find_replace(smsOut1,"<bno>",a_phone);
				 smsOut1 = find_replace(smsOut1,"<ped>",profileA.getPromoEndDate());
				 MTMsgOut.smsMT[0].msg = smsOut1;
				 return MTMsgOut;
         	 } else if (profileB.getPromoStat()==6 || profileB.getPromoStat()==8) {
				 //check if donor is under promo : (cannot send & receive) or (cannot send)
				 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Receiver promo status:"+intToString(profileB.getPromoStat())+"]");
				 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|181|"+currentDateTime3()+"|"+tx_id);
				 smsOut1 = mtLibrary.getMsg(181, profileA.getLang(),profileA.getBrand());
				 smsOut1 = find_replace(smsOut1,"<ano>",b_phone);
				 smsOut1 = find_replace(smsOut1,"<ped>",profileB.getPromoEndDate());
				 MTMsgOut.smsMT[0].msg = smsOut1;
		 		 return MTMsgOut;
             } else if ((amt < bizrules.atr_min_req) && (profileB.getType()==0)) {
         		 //minimum request amount
            	 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Amount < min amount to request]");
            	 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|72|"+currentDateTime3()+"|"+tx_id);
         	 	 smsOut1 = mtLibrary.getMsg(72, langA);
         	 	 smsOut1 = find_replace(smsOut1,"<amt>",intToString(int(bizrules.atr_min_req)));
         	 	 MTMsgOut.smsMT[0].msg = smsOut1;
         		 return MTMsgOut;
         	 } else if ((amt > bizrules.atr_max_req) && (profileB.getType()==0)) {
         		 //maximum request amount
         		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Amount > max amount to request]");
         		 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|73|"+currentDateTime3()+"|"+tx_id);
         	 	 smsOut1 = mtLibrary.getMsg(73, langA);
         	 	 smsOut1 = find_replace(smsOut1,"<amt>",intToString(int(bizrules.atr_max_req)));
         	 	 MTMsgOut.smsMT[0].msg = smsOut1;
         	 	 return MTMsgOut;
         	 /*} else if (profileB.getWhiteListStat()==0 && profileB.getType()==0) {
         		 //donor is not whitelisted
         		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Donor is not whitelisted]");
          		 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|87|"+currentDateTime3()+"|"+tx_id);
          		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(87, langA, profileA.getBrand());
          		 return MTMsgOut;*/
         	 }  else if ((amt < bizrules.atr_min_req_ptd) && (profileB.getType()==1)) {
                         //minimum request amount
                 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error (Donor is postpaid):Amount < min amount to request]");
                 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|240|"+currentDateTime3()+"|"+tx_id);
                         smsOut1 = mtLibrary.getMsg(240, langA);
                         smsOut1 = find_replace(smsOut1,"<amt>",intToString(int(bizrules.atr_min_req)));
                         MTMsgOut.smsMT[0].msg = smsOut1;
                         return MTMsgOut;
                 }  else if ((amt > bizrules.atr_max_req_ptd) && (profileB.getType()==1)) {
                         //maximum request amount
                         logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error (Donor is postpaid):Amount > max amount to request]");
                         txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|241|"+currentDateTime3()+"|"+tx_id);
                         smsOut1 = mtLibrary.getMsg(241, langA);
                         smsOut1 = find_replace(smsOut1,"<amt>",intToString(int(bizrules.atr_max_req)));
                         MTMsgOut.smsMT[0].msg = smsOut1;
                         return MTMsgOut;
                 } 


		

         	 //check sum of requestor failed tx for today
             int sumAll=0;
             int sumDonor=0;

         	 //check donor transfer limit
             if (profileB.getIsRegistered()==1) {
 	        	 max_trf_amt = bizrules.max_trf_dly_reg;
 	         } else {
 	        	 max_trf_amt = bizrules.max_trf_dly_unr;
 	         }

        	 //if ((profileB.getDailyLimit() + amt) > max_trf_amt && profileA.getType()==0) { //*** commented by izwan 12/9/2011
		    if ((profileB.getDailyLimit() + amt) > max_trf_amt && profileB.getType()==0) { //*** a correction by izwan 12/9/2011 side effect expected
        		 //check max daily limit prepaid
        		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Prepaid donor has reach daily limit "+floatToString(max_trf_amt)+" of transfer " + floatToString(profileB.getDailyLimit())+"]");
        		 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|74|"+currentDateTime3()+"|"+tx_id);
        		 smsOut1 = mtLibrary.getMsg(74, profileA.getLang());
        		 smsOut1 = find_replace(smsOut1,"<max>",intToString(int(max_trf_amt)));
        		 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
        		 float trfAllowed = max_trf_amt - profileB.getDailyLimit();
        		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(trfAllowed)));
        		 return MTMsgOut;
        	 }

        	 if (profileA.getType()==1) {
          		 //requestor is postpaid
          		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Requestor is postpaid]");
          	 	 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|71|"+currentDateTime3()+"|"+tx_id);
          	 	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(71, langA);
          		 return MTMsgOut;
        	 }

        	 if (profileA.getBalance()<=0 || profileA.getGracePeriod()==true) {
        		 //check if balance==0 or A is grace period
        		 if (profileA.getCounterDay()>1) {
        			 //check day counter
        			 deletePendingCAR(a_phone, b_phone,mySQLConn.myData,logger);
        			 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Counter for zero balance:"+intToString(profileA.getCounterDay())+"]");
        			 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|66|"+currentDateTime3()+"|"+tx_id);
        			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(66, profileA.getLang());
        			 return MTMsgOut;
        		 } else {
        			 //increment day counter for A
        			 if (profileA.getIsRegistered()==1)
    		   	   	 	updateDayCounter(profileA.getPhone(), profileA.getCounterDay()+1, mySQLConn.myData, logger);
        			 else
        				updateDayCounterUnreg(profileA, tx_id, mySQLConn.myData, logger);
        		 }
        	 }

         	 //if no error, insert into car pending table
             short int createStat = createPendingCAR(a_phone, b_phone, profileA.getLang(), currentDateTime2(), tx_id, amt,mySQLConn.myData,logger, smsc_id, requestor_name);
          	 if (createStat!=0) {
          		 //cannot create pending file
          		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Error:Cannot create pending log file]");
          		 txLogger.logData(b_phone+ "|"+a_phone+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getBrand())+"|"+intToString(profileA.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|8|"+currentDateTime3()+"|"+tx_id);
          		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(8, langA);
          		 return MTMsgOut;
          	 } else {
				 //charge abuse user
				 /*
				 if (profileA.getBalance()>=0.3) {
					 int abuse_counter = checkAbuseCounter(a_phone,mySQLConn.myData,logger, 0);
					 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Abuse counter="+intToString(abuse_counter)+"]");
					 if (abuse_counter>=2) {
						//send charged MT
						float newBal = profileA.getBalance()-0.30;
						MTMsgOut.smsMT[1].oadc = bizrules.atr_pfx + a_phone.substr(1);
            	 		MTMsgOut.smsMT[1].receiver = a_phone;
            	 		//MTMsgOut.smsMT[1].msg = "Your RQ with message has been sent to "+b_phone+". Your current balance is RM"+floatToString2d(newBal)+". Svc charge of 10sen has been deducted from your acc.";
            	 		smsOut1 = mtLibrary.getMsg(96, profileA.getLang());
						smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
						smsOut1 = find_replace(smsOut1,"<bal>",floatToString2d(newBal));
            	 		MTMsgOut.smsMT[1].msg = smsOut1;
            	 		MTMsgOut.smsMT[1].charging = 30;
					 }
				 }*/

          		 //pending file created successfully
          		 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Successfully create pending log file]");
          		 string mtOut;
            	 //Ask confirmation from donor
          		 short int bStat = profileB.getAppStatus();
          		 langB = profileB.getLang();
          		 if (bStat==1 || bStat==0) {
          			 //pre-registered user
          			 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Pre-Registered user, ask confirmation from donor]");
          			 mtOut = mtLibrary.getMsg(91, langB);
          		 } else if (bStat==2) {
          			 //registered user
				 //*** Exemption for POSTPAID, sendconfirm MT without password
				 if(profileB.getType()==1){
					logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [postpaid user, ask confirmation from donor]");
                                         mtOut = mtLibrary.getMsg(91, langB);
				 }
				 else{
	          			 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [Registered user, ask confirmation from donor]");
        	  		  	 mtOut = mtLibrary.getMsg(90, langB);
				 }
          		 } else {
          			 //unregistered user
          			 //get language from IN
          			 logger.logDebug("[TX ID:"+tx_id+"][Get user language from IN]");
          			 IN_Conn profileINB;
          			 struct IN_UserProfile ProfileResultB;
          			 ProfileResultB = profileINB.QueryProfile(profileB.getPhone(),inPort,tx_id);
          			 if (ProfileResultB.queryStatus==0) {
          				 langB = ProfileResultB.userLang;
          				 logger.logDebug("[TX ID:"+tx_id+"][Query from IN success, language from IN:"+intToString(langB)+"][Register user in DB]");
          				 //register profile in DB
          				 //store only phone, in_status, user type, language
          				 profileB.setINStatus(ProfileResultB.userStatus);
          				 profileB.setType(ProfileResultB.userType);
          				 profileB.setLang(ProfileResultB.userLang);
          				 profileB.setAppSatus(0);
          				 preRegisterUserProfile(profileB, tx_id, mySQLConn.myData, logger);
          			 } else {
					 //query postpaid number
					 logger.logDebug("[TX ID:"+tx_id+"][Query from IN fail, Query Postpaid number]");
					 struct Post_UserProfile PostProfileResultB;
			                 Post_Conn postConn;
			                 PostProfileResultB = postConn.QueryProfile(profileB.getPhone(),inPort,tx_id);
					 if (PostProfileResultB.queryStatus==0) {
						profileB.setINStatus(PostProfileResultB.userStatus);
                	                        profileB.setType(1);
        	                                profileB.setLang(1);
	                                        profileB.setAppSatus(0);
						preRegisterUserProfilePostpaid(profileB, tx_id, mySQLConn.myData, logger);
					 } else {
	          				logger.logDebug("[TX ID:"+tx_id+"][Query from IN failed! Use default language]");
        	  				//use default language
          					langB = 1;
					 }
          			 }
          			 logger.logDebug("[TX ID:"+tx_id+"] [Request.makeRequest] [UnRegistered user, ask confirmation from donor]");
          			 mtOut = mtLibrary.getMsg(91, langB);
          		 }
          		 MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1);
            	 MTMsgOut.smsMT[0].receiver = b_phone;
            	 mtOut = find_replace(mtOut,"<ano>",a_phone);
            	 if (requestor_name!="")
            		 smsOut1 = find_replace_celcom(mtOut,"<msg>",requestor_name);
            	 else
            	 	 smsOut1 = find_replace_celcom(mtOut,"<msg>","");
          		 smsOut1 = find_replace(smsOut1,"<amt>",intToString(amt));
          		 smsOut1 = find_replace(smsOut1,"<min>",intToString(bizrules.atr_tmt));
          		 MTMsgOut.smsMT[0].msg = smsOut1;
          		 return MTMsgOut;
          	 }
          }

          //approve request
          MTCollection Request::confirmRequest() {
        	   float prevBalanceA=0;
        	   float prevBalanceB=0;
        	   float newBalanceA=0;
        	   float newBalanceB=0;
        	   string pendingFile;
        	   string foundFile;
        	   string fileTimeStamp;
        	   string smsOut1;
        	   string smsOut2;
        	   float max_trf_amt=0;
        	   int max_suc_txn;
        	   float svc_chg_a;
        	   float svc_chg_b;
        	   float balanceAfterTransfer=0;
               float newUsage=0;
               int yr,mth,dt;
               long dt1,dt2;
  	           string expB;
  	           string expA;
  	           string expiryDt;
  	           string activeDt;
  	           string dtCAR, txidCAR;
  	           int langCAR, amtCAR;
		   string nickname;

        	   logger.logDebug("[TX ID:"+tx_id+"] [Start Confirm Request]");
        	   MTMsgOut.smsMT[0].receiver = a_phone;
        	   MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];
        	   MTMsgOut.smsMT[1].receiver = b_phone;
        	   MTMsgOut.smsMT[1].oadc = bizrules.ats_pfx + b_phone.substr(1,3) + bizrules.ATSoadc[0];

        	   //log for transaction logs
           	   string logtx = mtLibrary.dir_atr_tx+"/"+currentDate()+".tx";
           	   FileLogger txLogger (logtx);

	   	   string logUssd = mtLibrary.dir_ussd_tx+"/car.cdr";
                   FileLogger ussdCdr (logUssd);

               //check pending ShareTopup request from A to B from table car_pending
	           short int isExist = searchPendingCAR(b_phone, a_phone,mySQLConn.myData,logger,langCAR,dtCAR,txidCAR,amtCAR, nickname);

	           if (isExist==0) {

	        	   logger.logDebug("[TX ID:"+tx_id+"][Found file:"+foundFile+"]");
	        	   amt = amtCAR;

	        	   if (amt<1) {
           			      logger.logError("[TX ID:"+tx_id+"][Amount is invalid:"+foundFile.substr(56)+"]");
           			  	  //deleteFile(mtLibrary.dir_atr_pending+"/"+foundFile);
                 		  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|4|"+currentDateTime3()+"|"+tx_id);
                 		  smsOut1 = mtLibrary.getMsg(4, langA);
                 		  MTMsgOut.smsMT[0].msg = smsOut1;
                 		  return MTMsgOut;
           		    }

	        	   	fileTimeStamp = dtCAR;
	          	 	logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Have Pending Request, proceed process]");
	           } else if (isExist==2) {
	          	 	logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Error Opening Directory]");
	          	 	txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	          	 	MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	          	 	return MTMsgOut;
	           } else if (isExist==1) {
	          	 	logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:No Pending Request]");
	          	 	txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|79|"+currentDateTime3()+"|"+tx_id);
	          	 	MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(79, langA);
	          	 	return MTMsgOut;
	           } else {
	          	 	logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error while opening directory]");
	          	 	txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	          	 	MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	          	 	return MTMsgOut;
	           }

	           string resultDB = getUserLangDB(b_phone, mySQLConn.myData, logger);
	           langB = langCAR;
			   string currentTime = currentDateTime2();
			   //check timeout
			   short int timeout = bizrules.atr_tmt;
			   int timeoutInSec = timeout * 60;
			   struct tm tmNow, tmRequest;
			   strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
			   strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
			   double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
			   int dayDiff=0;
			   int timeDiff=0;
			   if (secDiff>timeoutInSec) {
				   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [CurrentTime:"+currentTime+" FileTimeStamp:"+fileTimeStamp+" Error Pending request already timeout]");
				   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|80|"+currentDateTime3()+"|"+tx_id);
				   smsOut1 = mtLibrary.getMsg(80, langA);
				   smsOut1 = find_replace(smsOut1,"<min>",intToString(bizrules.atr_tmt));
				   MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<bno>",b_phone);
				   smsOut2 = mtLibrary.getMsg(81, langB);
				   MTMsgOut.smsMT[1].msg = find_replace(smsOut2,"<ano>",a_phone);
				   return MTMsgOut;
			   }

               //confirm transfer
	           //Subscriber profileA;
	           Subscriber profileB;
	           //profileA.setPhone(a_phone);//donor
	           profileB.setPhone(b_phone);//requestor

	    	   //check profile A & B from DB
	    	   logger.logDebug("[TX ID:"+tx_id+"] [Check profile donor and receiver from DB]");
	    	   //changes on 23/10/2008:get profile from db at core process
	    	   //getProfileDB(profileA,mySQLConn.myData,mtLibrary.dir_error);
	    	   getProfileDB(profileB,mySQLConn.myData,mtLibrary.dir_error);

	    	   if (profileA.getAppStatus()==2 && profileA.getType()==0) {
	    		   //if user registered, must include password FOR PREPAID profile A only!
           		   if (pin=="") {
           			   //no password inserted
				   		deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           			   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Password is blank by registered user]");
           			   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|88|"+currentDateTime3()+"|"+tx_id);
           			   smsOut1 = mtLibrary.getMsg(88, profileA.getLang());
           			   smsOut1 = find_replace(smsOut1,"<min>",intToString(timeout));
				   		//MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + b_phone.substr(1);
           			   MTMsgOut.smsMT[0].msg = smsOut1;
           			   return MTMsgOut;
           		   } else if (pin.compare(profileA.getPwd())!=0) {
           			   //Invalid password by registered user
				   		deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           			   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Invalid password by registered user. pwd inserted:"+pin+" pwd db:"+profileA.getPwd()+"]");
           			   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|88|"+currentDateTime3()+"|"+tx_id);
           			   smsOut1 = mtLibrary.getMsg(88, profileA.getLang());
           			   smsOut1 = find_replace(smsOut1,"<min>",intToString(timeout));
				   		//MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + b_phone.substr(1);
           				MTMsgOut.smsMT[0].msg = smsOut1;
           			   return MTMsgOut;
           		   } else {
           			   //Password is valid, proceed
           			   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Valid password by registered user,proceed]");
           		   }
	    	   }


               //maximum requestor received amount for today (ATS)
               //int requestorReceivedATS  = sumAmountReceived(mtLibrary.dir_ats_success, b_phone);
	           int requestorReceivedATS = int(profileB.getDailyReceived());
               if ((requestorReceivedATS+amt)>bizrules.max_rcv_amt) {
             	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
             	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Requestor has reach max limit amt received for today from ATS only]");
             	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|84|"+currentDateTime3()+"|"+tx_id);
             	   smsOut1 = mtLibrary.getMsg(84, profileA.getLang());
             	   smsOut1 = find_replace(smsOut1,"<max>",intToString(int(bizrules.max_rcv_amt)));
             	   smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
             	   float amtAllowed = bizrules.max_rcv_amt - requestorReceivedATS;
             	   MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(amtAllowed)));
             	   smsOut2 = mtLibrary.getMsg(85, langB);
             	   smsOut2 = find_replace(smsOut2,"<lim>",intToString(int(bizrules.max_rcv_amt)));
             	   MTMsgOut.smsMT[1].msg = find_replace(smsOut2,"<amt>",intToString(int(amtAllowed)));
             	   return MTMsgOut;
               }

               //maximum requestor received amount for today (ATR)
               //int requestorReceivedATR  = sumAmountReceived(mtLibrary.dir_atr_success, b_phone);
	       	   int requestorReceivedATR = int(profileB.getDailyReceived());
               if ((requestorReceivedATR+amt)>bizrules.max_rcv_amt) {
             	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
             	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Requestor has reach max limit amt received for today from ATS+ATR]");
             	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|84|"+currentDateTime3()+"|"+tx_id);
             	   smsOut1 = mtLibrary.getMsg(84, profileA.getLang());
             	   smsOut1 = find_replace(smsOut1,"<max>",intToString(int(bizrules.max_rcv_amt)));
             	   smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
             	   float amtAllowed = bizrules.max_rcv_amt - requestorReceivedATS - requestorReceivedATR;
             	   MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(amtAllowed)));
             	   smsOut2 = mtLibrary.getMsg(85, langB);
             	   smsOut2 = find_replace(smsOut2,"<lim>",intToString(int(bizrules.max_rcv_amt)));
             	   MTMsgOut.smsMT[1].msg = find_replace(smsOut2,"<amt>",intToString(int(amtAllowed)));
             	   return MTMsgOut;
               }

               //check donor daily limit
    	       if (profileA.getAppStatus()==2) {
    	    	   max_trf_amt = bizrules.max_trf_dly_reg;
    	    	   max_suc_txn = bizrules.max_suc_txn_reg;
    	       } else {
    	    	   max_trf_amt = bizrules.max_trf_dly_unr;
    	    	   max_suc_txn = bizrules.max_suc_txn_unr;
    	       }

  	           if ((profileA.getDailyLimit() + amt) > max_trf_amt && profileA.getType()==0) {
  	        	   //check max daily limit for prepaid
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Reach Max Daily Limit. Prepaid user has reach daily limit "+floatToString(max_trf_amt)+" of transfer " + floatToString(profileA.getDailyLimit())+"]");
           		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|86|"+currentDateTime3()+"|"+tx_id);
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   smsOut1 = mtLibrary.getMsg(86, profileA.getLang());
           		   smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(max_trf_amt)));
           		   float trfAllowed = max_trf_amt - profileA.getDailyLimit();
           		   MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(trfAllowed)));
           		   smsOut2 = mtLibrary.getMsg(74, langB);
           		   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
           		   MTMsgOut.smsMT[1].msg = smsOut2;
           		   return MTMsgOut;
           	   } else if (profileA.getSuccessCount()>=max_suc_txn && profileA.getType()==0) {
           		   //check daily max successfull transaction for Prepaid
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Reach daily successfull transaction] [Prepaid user has reach daily limit "+intToString(max_suc_txn)+" of transfer " + intToString(profileA.getSuccessCount())+"]");
           		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|175|"+currentDateTime3()+"|"+tx_id);
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   smsOut1 = mtLibrary.getMsg(176, profileA.getLang());
           		   smsOut1 = find_replace(smsOut1,"<cnt>",intToString(int(max_suc_txn)));
           		   MTMsgOut.smsMT[0].msg = smsOut1;
           		   smsOut2 = mtLibrary.getMsg(175, langB);
           		   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
           		   smsOut2 = find_replace(smsOut2,"<cnt>",intToString(max_suc_txn));
           		   MTMsgOut.smsMT[1].msg = smsOut2;
           		   return MTMsgOut;
           	   } 

		       //check business rules for Donor
		       svc_chg_a = bizrules.atr_sms_chg_prd_ano;
           	   svc_chg_b = bizrules.atr_sms_chg_prd_bno;

           	   logger.logDebug("[TX ID:"+tx_id+"] [Get service charge] [A="+floatToString(svc_chg_a)+"] [B="+floatToString(svc_chg_b)+"]");

           	   //check A business rules
                   if (profileA.getWhiteListStat()==0 && profileA.getType()==0) {
                         //donor is not whitelisted
                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Donor is not whitelisted]");
                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|87|"+currentDateTime3()+"|"+tx_id);
                         MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(87, langA, profileA.getBrand());
			 MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(87, langB, profileB.getBrand());
                         return MTMsgOut;
           	   } else if (profileA.getINStatus()!=2) {
           		   //A not active
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:A no is not active, return user status from IN="+intToString(profileA.getINStatus())+"]");
           		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|55|"+currentDateTime3()+"|"+tx_id);
           		   smsOut1 = mtLibrary.getMsg(55, profileA.getLang());
           		   MTMsgOut.smsMT[0].msg = smsOut1;
           		   smsOut2 = mtLibrary.getMsg(56, langB);
           		   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
           		   MTMsgOut.smsMT[1].msg = smsOut2;
           		   return MTMsgOut;
           	   } else if (profileA.getBrand()==12 || profileA.getBrand()==13) {
           		   //check if A brand is celcom staff (12 or 13)
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:A no is celcom staff, return user status from IN="+intToString(profileA.getBrand())+"]");
           		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|60|"+currentDateTime3()+"|"+tx_id);
           		   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(60, profileA.getLang(), profileA.getBrand());
           		   smsOut2 = mtLibrary.getMsg(61, langB);
				   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
				   MTMsgOut.smsMT[1].msg = smsOut2;
           		   return MTMsgOut;
           	   /*} else if (profileA.getType()!=0) {
           		   //check if A prepaid
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:A no is not prepaid, return user status from IN="+intToString(profileA.getType())+"]");
           		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|62|"+currentDateTime3()+"|"+tx_id);
           		   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(62, profileA.getLang());
           		   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(63,langB);
           		   return MTMsgOut;
           		   */
           	   } else if (profileA.getBrand()==1 && profileA.getGracePeriod()==true) {
           		   //check if A brand is MAX & if grace period
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:A no brand is MAX, return user status from IN="+intToString(profileA.getBrand())+"]");
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|64|"+currentDateTime3()+"|"+tx_id);
				   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(64, profileA.getLang());
				   smsOut2 = mtLibrary.getMsg(65, langB);
				   smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
				   MTMsgOut.smsMT[1].msg = smsOut2;
				   return MTMsgOut;
           	    } else if (profileA.getExpiry() < currentDateLimit() && profileA.getType()==0) {
				   //A is expired
           	       deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           	       logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:A is expired. Expiry Data="+profileA.getExpiry()+"]");
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|68|"+currentDateTime3()+"|"+tx_id);
				   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(68, profileA.getLang());
				   smsOut2 = mtLibrary.getMsg(69, langB);
				   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
				   MTMsgOut.smsMT[1].msg = smsOut2;
   				   return MTMsgOut;
           	   } else if (profileA.getBrand()==30) {
			   	   //donor is XOX
			   	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
			   	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Donor is XOX]");
			   	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|183|"+currentDateTime3()+"|"+tx_id);
				   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(183, profileA.getLang());
				   smsOut2 = mtLibrary.getMsg(184, langB);
				   smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
				   MTMsgOut.smsMT[1].msg = smsOut2;
   				   return MTMsgOut;
		 	   } else if (profileA.getBrand()==17) {
				   //donor is 17
				   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
				   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Donor is 17]");
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|187|"+currentDateTime3()+"|"+tx_id);
				   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(187, profileA.getLang());
				   smsOut2 = mtLibrary.getMsg(188, langB);
				   smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
				   MTMsgOut.smsMT[1].msg = smsOut2;
				   return MTMsgOut;
                             } else if (profileA.get_has_1p5()==true && profileA.getType()==1) {
                                         //postpaid is 1+5
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user has 1+5 plan]");
                                         smsOut1 = mtLibrary.getMsg(252, profileA.getLang());
					 smsOut2 = mtLibrary.getMsg(253, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|252|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } 	else if (profileA.get_has_tcl()==true && profileA.getType()==1) {
                                         //postpaid is active TCL
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user has active TCL]");
                                         smsOut1 = mtLibrary.getMsg(254, profileA.getLang());
					 smsOut2 = mtLibrary.getMsg(255, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|254|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             }	else if (profileA.get_has_LA()==true && profileA.getType()==1) {
                                         //postpaid is LA
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user has LA]");
                                         smsOut1 = mtLibrary.getMsg(256, profileA.getLang());
					 smsOut2 = mtLibrary.getMsg(257, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|256|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } 	else if (checkVIP(profileA.getVIP())==false && profileA.getType()==1) {
                                         //postpaid is VIP
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid is VIP not allowed]");
                                         smsOut1 = mtLibrary.getMsg(258, profileA.getLang());
					 smsOut2 = mtLibrary.getMsg(259, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|258|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } else if ((profileA.getCurrentUsage() + profileA.getCurrentDue()  + amt > profileA.getCreditLimit()) && profileA.getType()==1) {
                                         //postpaid user reach credit limit
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user Reach credit limit]");
                                         smsOut1 = mtLibrary.getMsg(248, profileA.getLang());
					 smsOut2 = mtLibrary.getMsg(249, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|248|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } else if ((profileA.getMonthlyLimit() + amt > bizrules.mth_lim_ptd) && profileA.getType()==1)  {
                                         //postpaid user reach monthly limit
                                         //todo - new error code
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user Reach monthly limit "+floatToString(bizrules.mth_lim_ptd)+"]");
                                         smsOut1 = mtLibrary.getMsg(244, profileA.getLang());
                                         smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.mth_lim_ptd)));
					 smsOut2 = mtLibrary.getMsg(245, profileB.getLang());
					 smsOut2 = find_replace(smsOut2,"<lim>",intToString(int(bizrules.mth_lim_ptd)));
					 smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|244|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } else if ((profileA.getTotalDue()>0) && profileA.getType()==1) {
                                        //has overdue invoice
					deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user has overdue invoice]");
                                         smsOut1 = mtLibrary.getMsg(250, profileA.getLang());
                                         smsOut2 = mtLibrary.getMsg(251, profileB.getLang());
                                         smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|250|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } else if (profileA.getRoaming()==1 && profileA.getType()==1) {
                                        //is roaming
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid user is roaming]");
                                         smsOut1 = mtLibrary.getMsg(246, profileA.getLang());
                                         smsOut2 = mtLibrary.getMsg(247, profileB.getLang());
                                         smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|246|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
                             } else if ((profileA.getLOS()<bizrules.los_ptd) && profileA.getType()==1 ) {
                                        //lenght of stay not sufficient
					 deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                                         logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Postpaid LOS not sufficient: "+intToString(profileA.getLOS())+"]");
                                         smsOut1 = mtLibrary.getMsg(242, profileA.getLang());
                                         smsOut2 = mtLibrary.getMsg(243, profileB.getLang());
                                         smsOut2 = find_replace(smsOut2,"<ano>",a_phone);
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|242|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
					 MTMsgOut.smsMT[1].msg = smsOut2;
                                         return MTMsgOut;
			   }

           	   balanceAfterTransfer = profileA.getBalance() - amt - svc_chg_a;

           	   if (profileA.getType()==0 && balanceAfterTransfer < bizrules.min_bal_aft_trf) {
               	   //check min balance after transfer
           		   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
           		   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Bal After Transfer<Min Bal Allowed. Prepaid User want transfer " + floatToString(amt) + ".Minimum balance after transfer is " + floatToString(bizrules.min_bal_aft_trf) + "]");
               	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|82|"+currentDateTime3()+"|"+tx_id);
               	   smsOut1 = mtLibrary.getMsg(82, profileA.getLang());
               	   smsOut2 = mtLibrary.getMsg(83, langB);
               	   smsOut2 = find_replace(smsOut2,"<bno>",a_phone);
               	   MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(bizrules.min_bal_aft_trf)));
               	   MTMsgOut.smsMT[1].msg = find_replace(smsOut2,"<amt>",intToString(int(bizrules.min_bal_aft_trf)));
               	   return MTMsgOut;
               }

		       //query profile from IN for B
		       logger.logDebug("[TX ID:"+tx_id+"] [Attempt to query profile requestor]");
		       IN_Conn profileINB;
               struct IN_UserProfile ProfileResultB;

               //query profile from IN for B
           	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Query Profile B no from IN]");
           	   ProfileResultB = profileINB.QueryProfile(profileB.getPhone(),inPort,tx_id);

               if (ProfileResultB.queryStatus==0) {
            	   	profileB.setBalance(ProfileResultB.userBalance);
               	 	activeDt = ProfileResultB.activationDate.substr(0,4) + ProfileResultB.activationDate.substr(5,2) + ProfileResultB.activationDate.substr(8,2);
               	 	profileB.setActivationDate(activeDt);
               	 	profileB.setType(ProfileResultB.userType);
               	 	profileB.setINType(ProfileResultB.userPlatform);
               	 	profileB.setINStatus(ProfileResultB.userStatus);
               	 	expiryDt = ProfileResultB.expiryDate.substr(0,4) + ProfileResultB.expiryDate.substr(5,2) + ProfileResultB.expiryDate.substr(8,2);
               	 	profileB.setExpiry(expiryDt);
               	 	profileB.setBrand(ProfileResultB.brand);
               	 	profileB.setLang(ProfileResultB.userLang);
               	 	logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Check profile B from IN Success:Result=0][bal:"+floatToString(ProfileResultB.userBalance)+" activation:"+ProfileResultB.activationDate+" status:"+intToString(ProfileResultB.userStatus)+" expiry:"+ProfileResultB.expiryDate+" brand:"+intToString(ProfileResultB.brand)+"]");
			   } else if (ProfileResultB.queryStatus==7) {
					//profile not found, query from postpaid
					logger.logDebug("[TX ID:"+tx_id+"][Query from IN failed, response : User  Not Found]");
					struct Post_UserProfile PostProfileResultB;
					Post_Conn postConn;
					PostProfileResultB = postConn.QueryProfile(profileB.getPhone(),inPort,tx_id);
					if (PostProfileResultB.queryStatus==0) {
						profileB.setType(PostProfileResultB.userType);
                        			profileB.setAccountNo(PostProfileResultB.act_no);
				                profileB.setINStatus(PostProfileResultB.userStatus);
			                        profileB.setCreditLimit(PostProfileResultB.credit_limit/100);
			                        profileB.setCurrentUsage(PostProfileResultB.unbill/100);
						profileB.setTotalDue(PostProfileResultB.total_due/100);
						profileB.setCurrentDue(PostProfileResultB.current_due/100);
						profileB.setLOS(PostProfileResultB.los);
						profileB.set_has_1p5(PostProfileResultB.has_1p5);
						profileB.set_has_tcl(PostProfileResultB.has_act_tcl);
						profileB.set_has_LA(PostProfileResultB.has_LA);
						profileB.setLang(PostProfileResultB.userLang);
						profileB.setRoaming(PostProfileResultB.roaming);
					} else {
						logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:while Check profile B from PG:Result="+intToString(PostProfileResultB.queryStatus)+"]");
						txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
						MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(7, profileA.getLang());
						return MTMsgOut;
					}
               } else if (ProfileResultB.queryStatus==5) {
            	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while Check profile B Failed:Result=5]");
	               txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|16|"+currentDateTime3()+"|"+tx_id);
	               MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(16, profileA.getLang());
	               return MTMsgOut;
               } else {
            	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while Check profile B:Result="+intToString(ProfileResultB.queryStatus)+"]");
            	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
            	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(7, profileA.getLang());
            	   return MTMsgOut;
               }

               prevBalanceA = profileA.getBalance();
               prevBalanceB = profileB.getBalance();
               logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [a brand:"+intToString(profileA.getBrand())+"][b brand:"+intToString(profileB.getBrand())+"]");

               if (profileB.getINStatus()!=2) {
	        	   //B is terminated
	        	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
	        	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:B is terminated]");
	        	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|57|"+currentDateTime3()+"|"+tx_id);
	        	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(57, profileA.getLang());
	        	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(58, profileB.getLang());
	        	   return MTMsgOut;
	           } else if (profileB.getType()==1) {
	        	   //cannot transfer to postpaid number
	        	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
	        	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Cannot transfer to postpaid number]");
	    		   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|71|"+currentDateTime3()+"|"+tx_id);
	    		   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(71, profileA.getLang());
	    		   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(170, profileB.getLang());
	    		   return MTMsgOut;
	           /*} else if (profileA.getBrand()==1 && (profileB.getBrand()!=1 && profileB.getBrand()!=9)) {
	        	   //Donor is MAX and receiver is not MAX or 3G MAX
	        	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Donor is MAX and receiver is not MAX or 3G MAX]");
	        	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|171|"+currentDateTime3()+"|"+tx_id);
	        	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(171, profileA.getLang());
	        	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(172, profileB.getLang());
	        	   return MTMsgOut;*/
	           }  else if ((profileA.getBrand()==14 && profileB.getBrand()!=14) || (profileA.getBrand()!=14 && profileB.getBrand()==14)) {
	        	   //(Donor is MTRADE and receiver is not MTRADE) or (Donor is not mtrade and receiver is mtrade)
	        	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
	        	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Donor is MTRADE and receiver is not MTRADE]");
   	        	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|173|"+currentDateTime3()+"|"+tx_id);
   	        	   smsOut1 = mtLibrary.getMsg(173, profileA.getLang());
   	        	   smsOut1 = find_replace(smsOut1,"<bno>",profileB.getPhone());
   	        	   MTMsgOut.smsMT[0].msg = smsOut1;
   	        	   smsOut2 = mtLibrary.getMsg(174, profileB.getLang());
   	        	   smsOut2 = find_replace(smsOut2,"<ano>",profileA.getPhone());
   	        	   MTMsgOut.smsMT[1].msg = smsOut2;
   	        	   return MTMsgOut;
	           } else if ((profileA.getBrand()==70 && profileB.getBrand()!=70) || (profileA.getBrand()!=70 && profileB.getBrand()==70)) {
                           //(Donor is RUGBY and receiver is not RUGBY) or (Donor is not RUGBY and receiver is RUGBY)
                           deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
                           logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Donor is MTRADE and receiver is not MTRADE]");
                           txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|200|"+currentDateTime3()+"|"+tx_id);
                           smsOut1 = mtLibrary.getMsg(200, profileA.getLang());
                           smsOut1 = find_replace(smsOut1,"<bno>",profileB.getPhone());
                           MTMsgOut.smsMT[0].msg = smsOut1;
                           smsOut2 = mtLibrary.getMsg(201, profileB.getLang());
                           smsOut2 = find_replace(smsOut2,"<ano>",profileA.getPhone());
                           MTMsgOut.smsMT[1].msg = smsOut2;
                           return MTMsgOut;
                   } else if (profileA.getBrand()==1 && profileB.getExpiry() < currentDateLimit()) {
	        	   //Donor is MAX and B is expired
	        	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
	        	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error:Donor is MAX and receiver is expired]");
  	        	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|179|"+currentDateTime3()+"|"+tx_id);
  	        	   smsOut1 = mtLibrary.getMsg(179, profileA.getLang());
  	        	   smsOut1 = find_replace(smsOut1,"<bno>",profileB.getPhone());
  	        	   MTMsgOut.smsMT[0].msg = smsOut1;
  	        	   smsOut2 = mtLibrary.getMsg(180, profileB.getLang());
  	        	   smsOut2 = find_replace(smsOut2,"<ano>",profileA.getPhone());
  	        	   MTMsgOut.smsMT[1].msg = smsOut2;
  	        	   return MTMsgOut;
	           } else if (profileB.getBrand()==30) {
			   	   //receiver is XOX
			   	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
				   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Receiver is XOX]");
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|185|"+currentDateTime3()+"|"+tx_id);
				   smsOut1 = mtLibrary.getMsg(185, profileA.getLang());
				   smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
				   MTMsgOut.smsMT[0].msg = smsOut1;
				   smsOut2 = mtLibrary.getMsg(186, langB);
				   MTMsgOut.smsMT[1].msg = smsOut2;
   				   return MTMsgOut;
		  	   } else if (profileB.getBrand()==17) {
					//receiver is 17 (Data Only)
					deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
					logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Receiver is 17]");
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|189|"+currentDateTime3()+"|"+tx_id);
					smsOut1 = mtLibrary.getMsg(189, profileA.getLang());
					smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
					MTMsgOut.smsMT[0].msg = smsOut1;
					smsOut2 = mtLibrary.getMsg(199, langB);
					MTMsgOut.smsMT[1].msg = smsOut2;
					return MTMsgOut;
			   }


               int validity;
               int yrExp, mthExp, dtExp;
           	   long dt3,dt4;
           	   if (profileA.getBrand()==1) {
           		   //if donor MAX, dont transfer validity
           		   validity=0;
        	   } else if (profileB.getBrand()==1) {
           		   //if receiver MAX, dont add validity
           		   validity=0;
           	   } else {
           		   if (amt==1 || amt==2)
           			   validity=1;
           		   else
           			   validity=3;
           		   time_t myTime = time(NULL);
           		   tm *myUsableTime = localtime(&myTime);
           		   myUsableTime->tm_mday = myUsableTime->tm_mday+validity;
           		   mktime ( myUsableTime );
           		   date validityDt(myUsableTime->tm_mday,myUsableTime->tm_mon+1,myUsableTime->tm_year+1900);
           		   dt3 = long_date(validityDt);
           		   yrExp = stringToInt(profileB.getExpiry().substr(0,4));
           		   mthExp = stringToInt(profileB.getExpiry().substr(4,2));
           		   dtExp = stringToInt(profileB.getExpiry().substr(6,2));
           		   date expiryDt(dtExp,mthExp,yrExp);
           		   dt4 = long_date(expiryDt);
           		   if (dt3 < dt4)
           			   validity=0;
           	   }

			   short int transferStatus=99;//default value for transfer status
			 string aExpiryDate;
			 string bExpiryDate;
			   if (profileA.getType()==0) {
				   //send transfer request to IN (Prepaid)
				   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Attempt to send instruction for IN to Transfer][a_no="+profileA.getPhone()+" b_no="+profileB.getPhone()+"amt="+intToString(amt)+",validity="+intToString(validity)+",a_chg="+floatToString(svc_chg_a)+",b_chg="+floatToString(svc_chg_b)+"]");
				   IN_Conn transferProcess;
				   struct IN_TransferResult TransferResult;
				   TransferResult = transferProcess.Transfer(profileA.getPhone(),profileB.getPhone(),amt,validity,svc_chg_a,svc_chg_b,tx_id,profileA.getINType(),profileB.getINType(),inPort,"CAR");
				   transferStatus = TransferResult.transferStatus;
				   newBalanceA = TransferResult.aBalance;
	                           newBalanceB = TransferResult.bBalance;
        		           aExpiryDate = TransferResult.aExpiryDate;
		       		   bExpiryDate = TransferResult.bExpiryDate;
			   } else {
			   	   //send transfer request to PG (Postpaid)
			   	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Attempt to send instruction for IN to Transfer][a_no="+profileA.getPhone()+" b_no="+profileB.getPhone()+"amt="+intToString(amt)+",validity="+intToString(validity)+",a_chg="+floatToString(svc_chg_a)+",b_chg="+floatToString(svc_chg_b)+"]");
				   Post_Conn transferProcess;
				   struct Post_TransferResult TransferResult;
				   TransferResult = transferProcess.Transfer(profileA.getPhone(),profileB.getPhone(),amt,validity,svc_chg_a,svc_chg_b,tx_id,profileA.getINType(),profileB.getINType(),inPort,"CAR",profileA.getAccountNo());
				   transferStatus = TransferResult.transferStatus;
				   newBalanceA = TransferResult.aBalance;
	                           newBalanceB = TransferResult.bBalance;
        			   aExpiryDate = TransferResult.aExpiryDate;
		        	   bExpiryDate = TransferResult.bExpiryDate;
			   }

               if (transferStatus==0) {

            	   //transaction being processed
            	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(152, profileA.getLang());
   	 			   MTMsgOut.smsMT[0].receiver = profileA.getPhone();
   	 			   MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

            	   deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);

            	   logger.logDebug("[TX ID:"+tx_id+"] [Debit & Credit Success!]");
		    	   logger.logDebug("[TX ID:"+tx_id+"] [Debit & Credit Success!] [New Balance A:"+floatToString2d(newBalanceA)+" New Balance B:"+floatToString2d(newBalanceB)+" Exp A:"+aExpiryDate+" Exp B:"+bExpiryDate+"]");

			   /*  ORIGINAL BEFORE BUGFIXING ON July 18 2011	
		    	   //update limit in profile DB
		    	   float newDailyLimit = profileA.getDailyLimit()+amt;
        		   float newWeeklyLimit = profileA.getWeeklyLimit()+amt;
        		   float newMonthlyLimit = profileA.getMonthlyLimit()+amt;
        		   short int newDailySuccessCount = profileA.getSuccessCount() + 1;
        		   updateUserLimit(a_phone, newDailyLimit, newDailySuccessCount, newMonthlyLimit, profileA.getStatementDate(), mySQLConn.myData,logger);

			   		float newDailyReceived = profileB.getDailyReceived() + amt;
			   		updateReceivedLimit(profileB, newDailyReceived, mySQLConn.myData, logger, tx_id);

			   */

			if(profileA.getType()==0){ //** for prepaid	
				logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmTransfer] [update balance]");
				float newDailyLimit = profileA.getDailyLimit()+amt;
			        short int newDailySuccessCount = profileA.getSuccessCount() + 1;
        			float newMthlyLimit = profileA.getMonthlyLimit(); //** bug fixing prepaid no need update mthly amt
        			updateUserLimit(a_phone, newDailyLimit, newDailySuccessCount, newMthlyLimit, profileA.getStatementDate(), mySQLConn.myData, logger);
				float newDailyReceived = profileB.getDailyReceived() + amt;
                                updateReceivedLimit(profileB, newDailyReceived, mySQLConn.myData, logger, tx_id);
			}else{ //*** POSPTPAID
				float newDailyLimit = profileA.getDailyLimit(); //*** bug fixing, postpaid no need update
       				 short int newDailySuccessCount = profileA.getSuccessCount(); //*** bug fixing postpaid no need update
        			float newMthlyLimit = profileA.getMonthlyLimit()+amt; // **bugfixing, mthly need to update
				logger.logDebug("mthly:"+floatToString2d(profileA.getMonthlyLimit())+": amt:"+floatToString2d(amt)+": phone:"+a_phone+":");
        			logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmTransfer] [POSTPAID - update balance]");
        			updateUserLimit(a_phone, newDailyLimit, newDailySuccessCount, newMthlyLimit, profileA.getStatementDate(), mySQLConn.myData, logger);
				float newDailyReceived = profileB.getDailyReceived() + amt;
                                updateReceivedLimit(profileB, newDailyReceived, mySQLConn.myData, logger, tx_id);
			}	

                   //send msg to donor
		   if (profileA.getType()==0)
        		   smsOut1 = find_replace(mtLibrary.getMsg(92, profileA.getLang()),"<bno>",profileB.getPhone());
		   else
			smsOut1 = find_replace(mtLibrary.getMsg(230, profileA.getLang()),"<bno>",profileB.getPhone());
                   smsOut1 = find_replace(smsOut1,"<amt>",intToString(amt));
                   smsOut1 = find_replace(smsOut1,"<bal>",floatToString2d(newBalanceA));
                   smsOut1 = find_replace(smsOut1,"<dat>",currentUserDate());
                   smsOut1 = find_replace(smsOut1,"<caj>",intToString(int(svc_chg_a * 100)));
                   smsOut1 = find_replace(smsOut1,"<pnt>",intToString(profileA.getLoyaltyPoint()+1));
                   if (aExpiryDate.length()>6) {
                	   expA = aExpiryDate.substr(8,2)+"/"+aExpiryDate.substr(5,2)+"/"+aExpiryDate.substr(0,4);
                   }
                   smsOut1 = find_replace(smsOut1,"<exp>",expA);
                   MTMsgOut.smsMT[1].msg = smsOut1;
                   MTMsgOut.smsMT[1].receiver = profileA.getPhone();
                   MTMsgOut.smsMT[1].oadc = bizrules.atr_pfx + profileA.getPhone().substr(1,3) + bizrules.ATSoadc[0];

				  string smsOut4 = mtLibrary.getMsg(154, profileA.getLang());
				  smsOut4 = find_replace(smsOut4,"<pnt>",intToString(profileA.getLoyaltyPoint()+1));
				  MTMsgOut.smsMT[5].msg = smsOut4;
				  MTMsgOut.smsMT[5].receiver = profileA.getPhone();
	    	  		MTMsgOut.smsMT[5].oadc = bizrules.ats_pfx + profileA.getPhone().substr(1,3) + bizrules.ATSoadc[0];

                   //send msg to requestor (prepaid-prepaid)
		   if (profileB.getType()==0 && profileA.getType()==0){
				
			logger.logDebug("TX type: prepaid-prepaid");
                	   smsOut2 = find_replace(mtLibrary.getMsg(93, profileB.getLang()),"<ano>",profileA.getPhone());
	                   smsOut2 = find_replace(smsOut2,"<amt>",intToString(amt));
        	           smsOut2 = find_replace(smsOut2,"<bal>",floatToString2d(newBalanceB));
                   	smsOut2 = find_replace(smsOut2,"<dat>",currentUserDate());
                  	 if (bExpiryDate.length()>6) {
               		 	   expB = bExpiryDate.substr(8,2)+"/"+bExpiryDate.substr(5,2)+"/"+bExpiryDate.substr(0,4);
                  	 }
                   	smsOut2 = find_replace(smsOut2,"<exp>",expB);
                   	smsOut2 = find_replace(smsOut2,"<caj>",intToString(int(svc_chg_b * 100)));
                   	MTMsgOut.smsMT[2].msg = smsOut2;
                  	 MTMsgOut.smsMT[2].receiver = profileB.getPhone();
                   	MTMsgOut.smsMT[2].oadc = bizrules.atr_pfx + profileB.getPhone().substr(1,3) + bizrules.ATSoadc[0];
		   } else if (profileB.getType()==0 && profileA.getType()==1){  //send msg to requestor (postpaid-prepaid)
			logger.logDebug("TX type: postpaid-prepaid");
			smsOut2 = find_replace(mtLibrary.getMsg(99, profileB.getLang()),"<ano>",profileA.getPhone());
                           smsOut2 = find_replace(smsOut2,"<amt>",intToString(amt));
                           smsOut2 = find_replace(smsOut2,"<bal>",floatToString2d(newBalanceB));
                        smsOut2 = find_replace(smsOut2,"<dat>",currentUserDate());
                         if (bExpiryDate.length()>6) {
                                   expB = bExpiryDate.substr(8,2)+"/"+bExpiryDate.substr(5,2)+"/"+bExpiryDate.substr(0,4);
                         }
                        smsOut2 = find_replace(smsOut2,"<exp>",expB);
                        //smsOut2 = find_replace(smsOut2,"<caj>",intToString(int(svc_chg_b * 100)));
                        MTMsgOut.smsMT[2].msg = smsOut2;
                         MTMsgOut.smsMT[2].receiver = profileB.getPhone();
                        MTMsgOut.smsMT[2].oadc = bizrules.atr_pfx + profileB.getPhone().substr(1,3) + bizrules.ATSoadc[0];
		   }
                   
                   //log success tx into transaction log file
                   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|0|"+currentDateTime3()+"|"+tx_id);

                   //write successfull transfer to ats_success dir
       			   string logSuccessTx = mtLibrary.dir_atr_success+"/"+tx_id+"."+profileA.getPhone()+"."+profileB.getPhone()+"."+intToString(amt);
       			   FileLogger successTx (logSuccessTx);
       			   successTx.createLog();
				  //write success tx into ussd_cdr
					ussdCdr.logData(a_phone.substr(1)+ "|"+b_phone.substr(1)+"|"+intToString(amt)+"|"+currentDateLimit2()+"|"+tx_id);

       			   if ((profileA.getAppStatus()!=2) && (profileA.getType()!=1) ) {
       				   //send general msg for unregistered user A number
       				   MTMsgOut.smsMT[3].msg = mtLibrary.getMsg(153, profileA.getLang());
       				   MTMsgOut.smsMT[3].receiver = profileA.getPhone();
       				   MTMsgOut.smsMT[3].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];
       			   }

       			   if (profileB.getAppStatus()!=2) {
       				   //send general msg for unregistered user B number
       				   MTMsgOut.smsMT[4].msg = mtLibrary.getMsg(153, profileB.getLang());
       				   MTMsgOut.smsMT[4].receiver = profileB.getPhone();
       				   MTMsgOut.smsMT[4].oadc = bizrules.ats_pfx + b_phone.substr(1,3) + bizrules.ATSoadc[0];
       			   }

                   return MTMsgOut;
		       } else if (transferStatus==4) {
		    	   //error while doing debit, need to refund manually
		    	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while doing debit & credit error code from IN:4]");
		    	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|9|"+currentDateTime3()+"|"+tx_id);
		    	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(9, profileA.getLang());
		    	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(10, profileB.getLang());
		    	   return MTMsgOut;
		       } else if (transferStatus==3) {
		    	   //successfully refunded
		    	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while doing debit & credit error code from IN:3]");
		    	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|11|"+currentDateTime3()+"|"+tx_id);
		    	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(11, profileA.getLang());
		    	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(12, profileB.getLang());
		    	   return MTMsgOut;
		       } else if (transferStatus==5) {
		    	   //error 99,100
	    	   	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while doing debit & credit error code from IN:"+intToString(transferStatus)+"]");
  		    	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|15|"+currentDateTime3()+"|"+tx_id);
  		    	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(15, profileA.getLang());
  		    	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(17, profileB.getLang());
  		    	   return MTMsgOut;
		   	   } else if (transferStatus==6) {
				   //error while doing credit, need to refund manually
				   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while doing debit & credit error code from IN:6]");
				   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|19|"+currentDateTime3()+"|"+tx_id);
				   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(19, profileA.getLang());
				   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(10, profileB.getLang());
		    	   return MTMsgOut;
		       } else {
		    	   //error
		    	   logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Error from IN while doing debit & credit error code from IN:"+intToString(transferStatus)+"]");
   		    	   txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|13|"+currentDateTime3()+"|"+tx_id);
   		    	   MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(13, profileA.getLang());
   		    	   MTMsgOut.smsMT[1].msg = mtLibrary.getMsg(14, profileB.getLang());
   		    	   return MTMsgOut;
		       }
          }

          //cancel request
          MTCollection Request::cancelRequest() {
        	  string pendingFile;
	      	  string foundFile;
	      	  string fileTimeStamp;
	      	  string smsOut1;
	      	  string smsOut2;
	      	  string dtCAR, txidCAR;
	      	  int langCAR, amtCAR;
		  	  string nickname="";

	      	  logger.logDebug("[TX ID:"+tx_id+"] [Start Cancel Request]");

	      	  MTMsgOut.smsMT[0].receiver = a_phone;
	      	  MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

	      	  //log for transaction logs
	          string logtx = mtLibrary.dir_atr_tx+"/"+currentDate()+".tx";
	          FileLogger txLogger (logtx);

        	  //check pending ShareTopup request from A to B in pending file list
	          string fileToSearch = b_phone+"."+a_phone;
	          short int isExist = searchPendingCAR(b_phone, a_phone,mySQLConn.myData,logger,langCAR,dtCAR,txidCAR,amtCAR,nickname);
		  	  logger.logDebug("[TX ID:"+tx_id+"] [isExist:"+intToString(isExist)+"]");

	          if (isExist==0) {
					amt = amtCAR;
        	   		logger.logDebug("[TX ID:"+tx_id+"] [Request.cancelRequest] [Have Pending Request, cancel transfer]");
        	   		//delete from pending file
        	   		deletePendingCAR(b_phone, a_phone,mySQLConn.myData,logger);
        	   		//log tx into transaction log file
        	   		txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|94|"+currentDateTime3()+"|"+tx_id);
        	   		smsOut1 = find_replace(mtLibrary.getMsg(94, langA),"<bno>",b_phone);
        	   		MTMsgOut.smsMT[0].msg = smsOut1;
        	   		//send reject msg to requestor
        	   		string resultDB = getUserLangDB(b_phone,mySQLConn.myData,logger);
	   				langB = stringToInt(resultDB.substr(0,1));
	   				smsOut2 = find_replace(mtLibrary.getMsg(95, langB),"<ano>",a_phone);
        	   		MTMsgOut.smsMT[1].msg = smsOut2;
        	   		MTMsgOut.smsMT[1].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];
        	   		MTMsgOut.smsMT[1].receiver = b_phone;

			if (nickname!="") {
				//abuse counter
				int abuse_counter = checkAbuseCounter(b_phone,a_phone,mySQLConn.myData,logger, 1);
				abuse_counter=abuse_counter+1;
				string smsOut3;
				logger.logDebug("[TX ID:"+tx_id+"] [Request.cancelRequest] [Abuse counter="+intToString(abuse_counter)+"]");
				if (abuse_counter>1 && nickname!="") {
					//send warning MT
					MTMsgOut.smsMT[3].oadc = bizrules.atr_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];
					MTMsgOut.smsMT[3].receiver = b_phone;
					smsOut3 = mtLibrary.getMsg(97, langB);
					smsOut3 = find_replace(smsOut3,"<cnt>",intToString(abuse_counter));
					MTMsgOut.smsMT[3].msg = smsOut3;
				 }
				if (abuse_counter>2 && nickname!="") {
							//send charged MT
			                //query profile from IN for B
							Subscriber profileB;
                   			profileB.setPhone(b_phone);//requestor
			                logger.logDebug("[TX ID:"+tx_id+"] [Request.confirmRequest] [Attempt Query Profile B no from IN]");
							IN_Conn profileINB;
							struct IN_UserProfile ProfileResultB;
          			 		ProfileResultB = profileINB.QueryProfile(profileB.getPhone(),inPort,tx_id);
		                   	if (ProfileResultB.queryStatus==0) {
							   //query success
								logger.logDebug("[TX ID:"+tx_id+"][Query from IN success, B balance:"+floatToString(profileB.getBalance())+"]");
								if (ProfileResultB.userBalance>=0.30) {
									float newBal = ProfileResultB.userBalance-0.30;
									MTMsgOut.smsMT[2].oadc = bizrules.atr_pfx + b_phone.substr(1);
									MTMsgOut.smsMT[2].receiver = b_phone;
									//MTMsgOut.smsMT[1].msg = "Your RQ with message has been sent to "+b_phone+". Your current balance is RM"+floatToString2d(newBal)+". Svc charge of 10sen has been deducted from your acc.";
									smsOut1 = mtLibrary.getMsg(96, profileB.getLang());
									smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
									smsOut1 = find_replace(smsOut1,"<bal>",floatToString2d(newBal));
									MTMsgOut.smsMT[2].msg = smsOut1;
									MTMsgOut.smsMT[2].charging = 30;
								}
							} else if (ProfileResultB.queryStatus==7) {
								logger.logDebug("[TX ID:"+tx_id+"][Query from IN failed, response : User  Not Found]");
								struct Post_UserProfile PostProfileResultB;
								Post_Conn postConn;
								PostProfileResultB = postConn.QueryProfile(profileB.getPhone(),inPort,tx_id);
								if (PostProfileResultB.queryStatus==0) {
									logger.logDebug("[TX ID:"+tx_id+"][Query from PG Success!]");
									MTMsgOut.smsMT[2].oadc = bizrules.atr_pfx + b_phone.substr(1);
									MTMsgOut.smsMT[2].receiver = b_phone;
									//MTMsgOut.smsMT[1].msg = "Your RQ with message has been sent to "+b_phone+". Your current balance is RM"+floatToString2d(newBal)+". Svc charge of 10sen has been deducted from your acc.";
									smsOut1 = mtLibrary.getMsg(96, profileB.getLang());
									smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
								//	smsOut1 = find_replace(smsOut1,"<bal>",floatToString2d(newBal));
									MTMsgOut.smsMT[2].msg = smsOut1;
									MTMsgOut.smsMT[2].charging = 30;
								} else {
									//cannot query from PG, stop process
									logger.logDebug("[TX ID:"+tx_id+"][Query from PG failed!]");
								}
			                } else if (ProfileResultB.queryStatus==5) {
								//cannot query from IN, stop process
								logger.logDebug("[TX ID:"+tx_id+"][Query from IN failed!, error 99,100]");
                   			}
				 }
			    }
	          } else if (isExist==2) {
	        	  	logger.logDebug("[TX ID:"+tx_id+"] [Request.cancelRequest] [Error:Error Opening Directory]");
	        	  	txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	        	  	MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	          } else if (isExist==1) {
          	 		logger.logDebug("[TX ID:"+tx_id+"] [Request.cancelRequest] [Error:No Pending Request to cancel]");
          	 		txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|79|"+currentDateTime3()+"|"+tx_id);
          	 		MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(79, langA);
	          } else {
          	 		logger.logDebug("[TX ID:"+tx_id+"] [Request.cancelRequest] [Error: while opening directory]");
          	 		txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
          	 		MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	          }

              return MTMsgOut;
          }

}

