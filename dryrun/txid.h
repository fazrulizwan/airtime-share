#include <iostream>

class TXID{

	public:
		//ip,port,dbname,dbuname,dbpwd
		TXID(std::string,int,std::string,std::string,std::string);
		std::string getTXID(void);

	private:	
		char *stringToChar(std::string);
		std::string currentTimestamp(void);
		std::string trailZero(int);
		std::string IntToString(int);
		std::string m_db_ip_addrs;
		int m_db_port;
		std::string m_db_name;
		std::string m_db_usr_name;
		std::string m_db_pwd;
		std::string getHexStr(int no);
		std::string intToHexStr(int n);
		std::string buffToStr(char *buff);

};
