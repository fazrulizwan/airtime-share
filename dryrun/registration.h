#include <iostream>

#include "FileLogger.h"
#include "MTMessage.h"
#include "sharedmem.h"

using namespace std;

namespace ats {
    class registration {
          public:
                  registration(string, string , string, const FileLogger&, PortReg, MTMessage);
                  registration(string, string , string, string,  const FileLogger&, MTMessage);
                  short int registerUser();
                  short int changePassword();
                  short int disablePassword();
                  short int enablePassword();
          private:
        	  	  string txid;
                  string phone;
                  string pin;
                  string newPin;
                  FileLogger logger;
                  PortReg inPort;
                  MTMessage mtLibrary;
    };
}
