#ifndef MP_CONFIG_H_
#define MP_CONFIG_H_


using namespace std;

typedef struct MPConfigStruct{

	int mp_conn_prof_count;
	string mp_ip_addrs[200];
	int mp_port[200];
	int max_retry_count;
	long retry_delay;	
	int load_status;
	int in_resp_timeout;
	string log_path;

}MPConfig_t;

	class MPConfig {
    	public:
    		MPConfig_t ReadConfig(string);

	private:	
		void trim(string&);
	
		
    	};
	
#endif /*BUSINESSRULES_H_*/
