#include <iostream>

using namespace std;

typedef struct PostPaidDataStruct{

	int qry_status;
	string acct_status;
	float credit_limit;
	float current_bill;
	string acct_id;

}PostPaidData;

typedef struct CDRDataStruct{

	string cust_ref;
	string payer_no;
	string payee_no;
	float amount;
	string txid;
	string datetime;
	string comment;	

}CDRData;

class PostPaid{

	public:
	int Send(string msisdn,string txid);
	PostPaidDataStruct Recv(string msisdn);
	int WriteCDR(struct CDRDataStruct);

	private:
	string getTagValue(string,string);

};
