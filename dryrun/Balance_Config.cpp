#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "Balance_Config.h"

using namespace std;

	BalanceConfig_t BalanceConfig::ReadConfig(string conf_file) {
		string line;
		string name;
		string value;
		int posEqual;
		BalanceConfig_t inconf;
		ifstream fileConfig (conf_file.c_str());

		//**** Init of conf struct
		inconf.balance_conn_prof_count=-1;		
		for(int i=0;i<200;i++){
		        inconf.balance_ip_addrs[i]="";
		        inconf.balance_port[i]=-1;
        }		
		inconf.max_retry_count=-1;
		inconf.retry_delay=-1;
				
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trim(name);
				value = line.substr(posEqual+1);
				trim(value);
				//cout << "[Name:" << name << "=" << value << "]";
				
				if (name=="balance_conn_prof_count") {
					inconf.balance_conn_prof_count=atoi(value.c_str());
					continue;
				}
                else if(name=="ip_addrs1"){
					inconf.balance_ip_addrs[0]=value;
					continue;
				}
                else if(name=="ip_addrs2"){
					inconf.balance_ip_addrs[1]=value;
					continue;
				}
                else if(name=="ip_addrs3"){
					inconf.balance_ip_addrs[2]=value;
					continue;
				}
		else if(name=="ip_addrs4"){
                                        inconf.balance_ip_addrs[3]=value;
                                        continue;
                                }
		else if(name=="ip_addrs5"){
                                        inconf.balance_ip_addrs[4]=value;
                                        continue;
                                }
		else if(name=="ip_addrs6"){
                                        inconf.balance_ip_addrs[5]=value;
                                        continue;
                                }
		else if(name=="ip_addrs7"){
                                        inconf.balance_ip_addrs[6]=value;
                                        continue;
                                }
		else if(name=="ip_addrs8"){
                                        inconf.balance_ip_addrs[7]=value;
                                        continue;
                                }



                else if(name=="balance_port1"){
					inconf.balance_port[0]=atoi(value.c_str());
					continue;
				}	
                else if(name=="balance_port2"){
					inconf.balance_port[1]=atoi(value.c_str());
					continue;
				}
                else if(name=="balance_port3"){
					inconf.balance_port[2]=atoi(value.c_str());
					continue;
				}		
		else if(name=="balance_port4"){
                                        inconf.balance_port[3]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="balance_port5"){
                                        inconf.balance_port[4]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="balance_port6"){
                                        inconf.balance_port[5]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="balance_port7"){
                                        inconf.balance_port[6]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="balance_port8"){
                                        inconf.balance_port[7]=atoi(value.c_str());
                                        continue;
                                }



				else if(name=="max_retry_count"){
					inconf.max_retry_count=atoi(value.c_str());
					continue;
				}
				else if(name=="retry_delay"){
					inconf.retry_delay=atol(value.c_str());
					continue;
				}
				else if(name=="in_resp_timeout"){
					inconf.in_resp_timeout=atoi(value.c_str());
				}
				else if(name=="log_path"){
					inconf.log_path=value;
				}

				
			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			inconf.load_status=1;
			return inconf;
		} else {
			//cout << "Cannot open config file!";
			return inconf;
		}
	}


void BalanceConfig::trim(string& str)
{
        string::size_type pos1 = str.find_first_not_of(' ');
        string::size_type pos2 = str.find_last_not_of(' ');
        str = str.substr(pos1 == string::npos ? 0 : pos1,
        pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

