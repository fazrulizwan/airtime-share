#ifndef BALANCE_CONFIG_H_
#define BALANCE_CONFIG_H_


using namespace std;

typedef struct BalanceConfigStruct{

	int balance_conn_prof_count;
	string balance_ip_addrs[200];
	int balance_port[200];
	int max_retry_count;
	long retry_delay;	
	int load_status;
	int in_resp_timeout;
	string log_path;

}BalanceConfig_t;

	class BalanceConfig {
    	public:
    		BalanceConfig_t ReadConfig(string);

	private:	
		void trim(string&);
	
		
    	};
	
#endif /*BUSINESSRULES_H_*/
