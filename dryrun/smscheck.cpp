#include <string>
#include <vector>
#include "smscheck.h"
#include "generalfunction.h"

using namespace std;

namespace ats {

	bool checkSyntax(string sms) {
		string keyword;
	    vector<string> smsVector;
	    string pwd="";
	    string newpwd="";
	    int amt;
	    string amtString="";
	    string requestorName;
	    string receiverPhone="";
		short int operation_type=0;
		string newPwd;
		string currentPwd;
		string motherName;
			
		sms = StringToUpper(sms);
		
		//convert new lines with space
		size_t found;
		int startAt;
		bool notFound=false;
		while (notFound==false) {
			found = sms.find("\n");
			//cout << "new line found";	
			if (found!=string::npos) {
				startAt= int(found);
				sms.replace(startAt,1," ");
			} else {
				notFound=true;
			}
		}
		
		smsVector = Tokenize(sms," ");

		if (sms=="") {
			return false;
		}
		
		
		
		for( int i = 0; i < smsVector.size(); i++ ) {
			if (i==0) {
				if (smsVector.at(i).compare("HELP")==0) {
					//first word is HELP
					operation_type=3;
				} else if (smsVector.at(i).compare("CPWD")==0) {
					//first word is CPWD (change password)
					operation_type=7;
				} else if (smsVector.at(i).compare("FPWD")==0) {
					//first word is FPWD (forgot password)
					operation_type=8;
				} else if (smsVector.at(i).compare("CAT")==0) {
					//first word is CAT
					operation_type=4;
				} else if (smsVector.at(i).compare("RM")==0) {
					//First Word is RM
					operation_type=1;
				} else if (smsVector.at(i).compare("RQ")==0) {
					//First Word is RQ
					operation_type=2;
				} else if (smsVector.at(i).compare("YA")==0 || smsVector.at(i).compare("YES")==0 || smsVector.at(i).compare("Y")==0 ) {
					//ATR approval First Word is YA / YES / Y
					operation_type=5;
				} else if (smsVector.at(i).compare("TIDAK")==0 || smsVector.at(i).compare("NO")==0 || smsVector.at(i).compare("N")==0 ) {
					//ATR approval First Word is TIDAK / NO / N
					operation_type=6;
				} else if (smsVector.at(i).compare(0,2,"RM")==0) {
					//Substring First Word is RM
					operation_type=1;
					amtString = smsVector.at(i).substr(2);
				} else if (smsVector.at(i).compare(0,2,"RQ")==0) {
					//Substring First Word is RQ
					operation_type=2;
					amtString = smsVector.at(i).substr(2);
				} else if (smsVector.at(i).compare("OK")==0) {
					//ATS approval
					operation_type=9;
				} else if (smsVector.at(i).compare("X")==0) {
					//ATS approval
					operation_type=10;
				} else {
					//First Word is amount
					operation_type=1;
					amtString = smsVector.at(i);
					//do some filtering to reduce use of resources
					if (chkAmtFormat(amtString)==false) {
						return false;
					}
				}
			} else if (i==1) {
				if (smsVector.at(i).compare("RM")==0) {
					//second word is RM, do nothing
				} else if (smsVector.at(i).compare(0,2,"RM")==0) {
					//substring second Word is RM
					amtString = smsVector.at(i).substr(2);
				} else if (operation_type==4) {
					//second word is password
					pwd = smsVector.at(i);
				} else if (operation_type==5) {
					//second Word is pwd
					pwd = smsVector.at(i);
				} else if (operation_type==7) {
					//second word is current pwd
					currentPwd = smsVector.at(i);
				} else if (operation_type==8) {
					//second word is mother's name
					motherName = smsVector.at(i);
					} else if (operation_type==2) {
						if (amtString=="") {
							//second word is amount
							amtString = smsVector.at(i);
						} else {
							//second word is name
							requestorName = smsVector.at(i);
						}
				} else if (amtString=="") {
					//second Word is amount
					amtString = smsVector.at(i);
				} else {
					//second Word is pwd
					pwd = smsVector.at(i);
				}
			} else if (i==2) {
				if (operation_type==4) {
					//thirs word is mother's name
					motherName = smsVector.at(i);
				} else if (operation_type==7) {
					//third word is new password
					newPwd = smsVector.at(i);
				} else if (operation_type==2) {
					if (amtString=="") {
							//second word is amount
							amtString = smsVector.at(i);
						} else {
							//second word is name
							requestorName = smsVector.at(i);
						}
				} else if (amtString=="") {
					//third word is amt
					amtString = smsVector.at(i);
				} else if (pwd=="") {
					//third Word is pwd
					pwd = smsVector.at(i);
				}
			}
		}


		if (operation_type==2 || operation_type==1) {
			amt = stringToInt(amtString);
			if (!chkAmtFormat(amtString) || amt==0) {
				return false;
			}
		}

		return true;
	}
}

