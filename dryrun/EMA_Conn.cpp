#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <iostream>
#include <string>
#include <arpa/inet.h>

#include "FileLogger.h"
#include "EMA_Conn.h"

using namespace std;
using namespace ats;


   
 string EMA_Conn::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}

int  EMA_Conn::getBytes (char *byteArray, string plainText){

       int i;
        string token;
        int dataLen=0;
	char tok;

        //*byteArray='\2';
        //byteArray++;

        for(i=0;i<plainText.length();i++){
                dataLen++;
                //token=plainText.substr(i,2);
		tok=plainText.at(i);
                //*byteArray=(unsigned char)HexStrToInt(token);
		*byteArray=((int)tok);
                byteArray++;
                //cout << "Token:" << ((int)tok) << endl;
        }
        //*byteArray=(unsigned char)13;
        //byteArray++;
        //*byteArray=(unsigned char)3;
        *byteArray='\n';
	byteArray++;
	//*byteArray='\r';
	//byteArray++;
	 *byteArray='\0';
        return dataLen+1;


        //cout << "Token:" << byteArray << endl;
}


 int EMA_Conn::getroaming(string phone_no,string tx_id, PortReg port_reg)
 {

	//string ip = "10.10.64.35";
	string ip=port_reg.ema_ip;
        //int ema_port = 3301;
	int ema_port=port_reg.ema_port;
	int ret_pos = 0;
        int com_pos = 0;
        string ret_status;
	//string fname =  "EMA_Conn.log";
	string fname=port_reg.postconn_log_path;
	FileLogger log(fname);
	string resPacket;
	string reqPacket;
	char *mSvrAddrs;
	char buf[1024];
	char sendBuf[1024];
	int returnStatus;
	int sockfd;	
	struct sockaddr_in svrAddr;
	int numbytes;
	char buf2[1024];
        char sendBuf2[1024];
	int byteLen;

	mSvrAddrs=(char *) ip.c_str();

	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
    		log.logError("Error creating a socket txid:"+tx_id+"");
    		exit(1);
	}

	bzero(&svrAddr,sizeof(svrAddr)); 
	svrAddr.sin_family = AF_INET;
	svrAddr.sin_port = htons(ema_port);
	svrAddr.sin_addr.s_addr = inet_addr(mSvrAddrs);
 
	if(connect(sockfd, (struct sockaddr *)&svrAddr, sizeof(struct sockaddr)) == -1)
	{
    		log.logError("Error connectiong to EMA. txid:"+tx_id+"");
		close(sockfd);
		return -1;
	}
 
	log.logNotice("Connected to EMA. txid:"+tx_id+" IP:"+ip+"");

	
	//*** RECV Terminal Welcome message
	if((numbytes = recv(sockfd, buf, sizeof(buf), 0)) == -1)
	{
    		log.logError("Error receiving data from EMA. txid:"+tx_id+"");
		close(sockfd);
		return -1;
	}
 
	buf[numbytes] = '\0';
	resPacket=buffToStr(buf);

	log.logNotice("Received packet from EMA: "+resPacket);

	//**** RECV prompt 
	if((numbytes = recv(sockfd, buf, sizeof(buf), 0)) == -1)
	{
    		log.logError("Error receiving data from EMA. txid:"+tx_id+"");
		close(sockfd);
		return -1;
	}
 
	buf[numbytes] = '\0';
	resPacket=buffToStr(buf);

	log.logNotice("Received packet from EMA: "+resPacket);

	reqPacket="LOGIN:idottv:idottv;";
	byteLen=getBytes(&sendBuf[0],reqPacket);
	//strcpy(sendBuf , reqPacket.c_str());

	returnStatus = send(sockfd,sendBuf,byteLen, 0); 
	if(returnStatus<0){
		log.logError("Error sending LOGIN to EMA. txid:"+tx_id+"");
		close(sockfd);
		return -1;
	}

	log.logNotice("Packet sent to ema ("+reqPacket+")  txid:"+tx_id+"");

	// Get response enter command
	if((numbytes = recv(sockfd, buf, sizeof(buf), 0)) == -1){
		log.logError("Error receiving Login response.  txid:"+tx_id+"");
		close(sockfd);
	    	return -1;
	}
	buf[numbytes] = '\0';
        resPacket=buffToStr(buf);

	log.logNotice("Received packet from EMA: "+resPacket);

	if(resPacket.substr(0,14)   == "Enter command:")
	{ // Test login again

		 reqPacket="LOGIN:idottv:idottv;";
        byteLen=getBytes(&sendBuf[0],reqPacket);
        //strcpy(sendBuf , reqPacket.c_str());

        returnStatus = send(sockfd,sendBuf,byteLen, 0);
        if(returnStatus<0){
                log.logError("Error sending LOGIN to EMA. txid:"+tx_id+"");
                close(sockfd);
                return -1;
        }

        log.logNotice("Packet sent to ema ("+reqPacket+")  txid:"+tx_id+"");

        // Get response enter command
        if((numbytes = recv(sockfd, buf, sizeof(buf), 0)) == -1){
                log.logError("Error receiving Login response.  txid:"+tx_id+"");
                close(sockfd);
                return -1;
        }
        buf[numbytes] = '\0';
        resPacket=buffToStr(buf);

        log.logNotice("Received packet from EMA: "+resPacket);




	}




	//** RESP:0;
	if(resPacket.substr(0,7)!="RESP:0;"){
		log.logError("EMA Login Fail  txid:"+tx_id+"");
		close(sockfd);
                return -1;
	}
	log.logNotice("Login OK. ("+resPacket+") txid:"+tx_id+"");

	//**** RECV prompt
        if((numbytes = recv(sockfd, buf, sizeof(buf), 0)) == -1)
        {
                log.logError("Error receiving data from EMA. txid:"+tx_id+"");
                close(sockfd);
                return -1;
        }

        buf[numbytes] = '\0';
        resPacket=buffToStr(buf);

        log.logNotice("Received packet from EMA: "+resPacket);

	

	//*** Send GET HLR SUB COMMAND
	reqPacket="GET:HLRSUB:MSISDN,"+phone_no+";";
	byteLen=getBytes(&sendBuf2[0],reqPacket);
	//strcpy(sendBuf2 , reqPacket.c_str());

	returnStatus = send(sockfd,sendBuf2,byteLen, 0); 
	if(returnStatus<0){
		log.logError("Error sending GETHLRSUB to EMA. txid:"+tx_id+"");
		close(sockfd);
		return -1;
	}

	log.logNotice("Packet sent to ema ("+reqPacket+")  txid:"+tx_id+"");


	// Get response enter command
	if((numbytes = recv(sockfd, buf2, sizeof(buf), 0)) == -1){
		log.logError("Error receiving Login response.  txid:"+tx_id+"");
		close(sockfd);
	    	return -1;
	}
	buf2[numbytes] = '\0';
        resPacket=buffToStr(buf2);


	log.logNotice("Received packet from EMA: "+resPacket);

	//** RESP:0;
	if(resPacket.substr(0,7)!="RESP:0:"){
		log.logError("EMA GETHLRSUB Fail  txid:"+tx_id+"");
		close(sockfd);
                return -1;
	}
	log.logNotice("GETHLRSUB OK. ("+resPacket+") txid:"+tx_id+"");

	close(sockfd);

	//*** Decide whether roaming or local
        ret_pos=resPacket.find("LOC,4-",0);
        com_pos=resPacket.find(",",ret_pos);
        //ret_status=resPacket.substr(ret_pos+6,com_pos-ret_pos+5);
	ret_status=resPacket.substr(ret_pos+6,com_pos-ret_pos+8);

	if(ret_status.substr(0,2) == "")
        {
                //return 0;
                log.logError("Unable to determine roaming  txid:"+tx_id+"");
		return -1;
		
        }
	else if(ret_status.substr(0,2) == "60")
	{
		//return 0;
		log.logNotice("This no is local  txid:"+tx_id+"");
		 return 0;
	}
	else
	{
		//return 1;
		log.logNotice("This no is roaming txid:"+tx_id+"");
		return 1;
	}

//close(sockfd);

 }
/*
 void StringExplode(string str, string separator, vector<string>* results){
    int found;
    found = str.find_first_of(separator);
    while(found != string::npos){
        if(found > 0){
            results->push_back(str.substr(0,found));
        }
        str = str.substr(found+1);
        found = str.find_first_of(separator);
    }
    if(str.length() > 0){
        results->push_back(str);
    };
}
*/

 



