#ifndef UCPCODER_H_
#define UCPCODER_H_

#include <iostream>

#include "UCPConn.h"

using namespace std;

//***** Genereal Constants *******
const string STX="\x2";
const string ETX="\x3";
const string OTON="6";
const string ONPI="5";
const string STYPE="1";
const string VERSION="0100";

//***** Response Packet Constants ******
const int OP60_RESP_OK=0; 
const int OP60_RESP_FAIL=-60;
const int INVALID_RESP_FORMAT=-3;
const int OP52_OP_PACKET=52;
const int OP57_OP_PACKET=57;
const int OP51_RESP_OK=51;
const int OP51_RESP_FAIL=-51;

typedef struct MTStruct{
	int seq_no;
	string oadc;
	string adc;
	string msg; 
}MT_t;

class UCPCoder{

	public:
	string getOp60(string oadc,string pwd);
	string getOp52Resp(string resp_packet,string resp_type);
	int  getBytes (char *byteArray, string plainText);
	int decodeResp (string resp_packet);
	string getKeepAlive(void);
	string getOp57Resp(void);
	string getOp51(string seq_no,string oadc,string adc,string msg);
	struct MO getMODetail(string packet);
	int getHeaderLen(string pck);
	bool isCSok(string pck);
	string getOp51ACKSeq(string pck);
	string getOp51ACK_ADC(string pck);

	private:
	string setPacketLen(string ucp_packet);
	string getCheckSum(string ucp_packet);
	string intToHexStr(int n);
	string IntToBinaryString(int no);
	int BinaryStringToInt(string in);
	string IntToString (int number);
	string AsciiToHexDump (string asciiStr, bool c_string);
	string buffToStr(char *buff);
	long int HexStrToInt(string hexStr);
	int StrToInt(string inval);
	string HexDumpToAscii(string hexDump);

};

#endif
