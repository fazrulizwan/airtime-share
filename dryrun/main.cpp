#include <iostream>
#include <stdio.h>

#include "UCPConn.h"

string buffToStringMain(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}

bool isFileExist(string filename){

	FILE *file;

	file=fopen(filename.c_str(),"r");

	if(file==NULL){
		//fclose(file);
		return false;
	}
	
	fclose(file);
	return true;

}


int main(int argc,char* argv[]){

	if(argc!=6){
		cout << "CATCAR Usage:" << endl;
		cout << "CATCAR [smsc config] [mp config] [balance config] [prepaid config] [postconn config]" << endl;
		cout << endl << "Example:" << endl;
		cout << "ShareTopup  smpp.conf mp.conf balance.conf prepaid.conf postconn.config" << endl;
		return 1;
	}

	if(! isFileExist(buffToStringMain(argv[1]))){
		cout << argv[1] <<" does not exist!" << endl;
		cout << "ShareTopup Usage:" << endl;
                cout << "ShareTopup [smsc config] [in config]" << endl;
                cout << endl << "Example:" << endl;
                cout << "ShareTopup  smpp.conf in.conf" << endl;
		return 1;
	}
	else if(! isFileExist(buffToStringMain(argv[2]))){
                cout << argv[2] <<" does not exist!" << endl;
		cout << "ShareTopup Usage:" << endl;
                cout << "ShareTopup [smsc config] [in config]" << endl;
                cout << endl << "Example:" << endl;
                cout << "ShareTopup  smpp.conf in.conf" << endl;
		return 1;
        }
	else if(! isFileExist(buffToStringMain(argv[3]))){
                cout << argv[3] <<" does not exist!" << endl;
                cout << "ShareTopup Usage:" << endl;
                cout << "ShareTopup [smsc config] [in config]" << endl;
                cout << endl << "Example:" << endl;
                cout << "ShareTopup  smpp.conf in.conf" << endl;
                return 1;
        }
	else if(! isFileExist(buffToStringMain(argv[4]))){
                cout << argv[4] <<" does not exist!" << endl;
                cout << "ShareTopup Usage:" << endl;
                cout << "ShareTopup [smsc config] [in config]" << endl;
                cout << endl << "Example:" << endl;
                cout << "ShareTopup  smpp.conf in.conf" << endl;
                return 1;
        }
	else if(! isFileExist(buffToStringMain(argv[5]))){
                cout << argv[5] <<" does not exist!" << endl;
                cout << "ShareTopup Usage:" << endl;
                cout << "ShareTopup [smsc config] [in config]" << endl;
                cout << endl << "Example:" << endl;
                cout << "ShareTopup  smpp.conf in.conf" << endl;
                return 1;
        }


	UCPConn conn(buffToStringMain(argv[1]),buffToStringMain(argv[2]),buffToStringMain(argv[3]),buffToStringMain(argv[4]),buffToStringMain(argv[5]));

	conn.Conn();

}

