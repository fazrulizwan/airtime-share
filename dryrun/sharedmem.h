/***************************************************
Multi-Process Round Robin Structures
ver1.0 13 May 2009
By Fazrul Izwan Hassan
For idot tv sdn bhd
****************************************************/

#ifndef SHAREDMEM_H_
#define SHAREDMEM_H_


#include <iostream>

using namespace std;

#define MY_SHM_ID 999
#define MY_SEM_ID 888

typedef struct ConnProfileStruct{

	string ip_addrs;
	int port_no;

}ConnProfile;

typedef struct PortRegStruct{

		int mp_conn_prof_count;
		int balance_conn_prof_count;
		int prepaid_conn_prof_count;
		ConnProfile mp_conn_prof[200];
		ConnProfile balance_conn_prof[100];	
		ConnProfile prepaid_conn_prof[100];		
        int shm_id;
		int mp_max_retry_count;
		int prepaid_max_retry_count;
		int balance_max_retry_count;
		long mp_retry_delay;
		long balance_retry_delay;
		long prepaid_retry_delay;
		int mp_resp_timeout;
		int balance_resp_timeout;
		int prepaid_resp_timeout;
		std::string mp_log_path;
		std::string prepaid_log_path;
		std::string balance_log_path;
		//*** Payment Gateway
		std::string pg_endpoint;
		int pg_port;	
		std::string SSL_cert;
		std::string pg_login;
		std::string pg_pwd;
		//*** EMA 
		std::string ema_ip;
		int ema_port;
		std::string ema_login;
		std::string ema_pwd;
		std::string postconn_log_path;

} PortReg;

typedef struct RRInfoStruct{
	int mp_next_profile;
	int balance_next_profile;
	int prepaid_next_profile;
}RRInfo;

#endif

