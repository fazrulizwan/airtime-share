#ifndef PREPAID_CONFIG_H_
#define PREPAID_CONFIG_H_


using namespace std;

typedef struct PrepaidConfigStruct{

	int prepaid_conn_prof_count;
	string prepaid_ip_addrs[200];
	int prepaid_port[200];
	int max_retry_count;
	long retry_delay;	
	int load_status;
	int in_resp_timeout;
	string log_path;

}PrepaidConfig_t;

	class PrepaidConfig {
    	public:
    		PrepaidConfig_t ReadConfig(string);

	private:	
		void trim(string&);
	
		
    	};
	
#endif /*BUSINESSRULES_H_*/
