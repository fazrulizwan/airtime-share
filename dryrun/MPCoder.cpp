#include <iostream>
#include <sstream>

#include "MPCoder.h"

using namespace std;

string MPCoder::getQueryPacket(string msisdn){

	return "4,"+msisdn+",CAT";

}

int MPCoder::getReturnStatus(string packet){

	if(packet.substr(0,2)=="01")
		return ACK;
	else if(packet.substr(0,6)=="99 100")
		return EXT_FAIL;
	else
		return NACK;

}

string MPCoder::getDebitPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string tx_type){

	return "18 "+msisdn+" -"+floatToString2d(amount)+" "+IntToString(validity)+" "+tx_type+" CAT-TFR "+IntToString(pform)+" "+b_no+" "+txid;

}

string MPCoder::getCreditPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string tx_type){

	return "18 "+msisdn+" "+floatToString2d(amount)+" "+IntToString(validity)+" "+tx_type+" CAT-RCV "+IntToString(pform)+" "+b_no+" "+txid;

}

string MPCoder::getVerifyPacket(string phone_no,string txid,int variation){

	/***********
	(1)  10,0193620660,CAT,188779951,5,1     99,1403,0193620660
	*********/
	if(variation==1)	
		return "10,"+phone_no+",CAT,"+txid+",5,1";
	else if(variation==2)
		return "10,"+phone_no+",CAT,"+txid+",6,0";

}

int MPCoder::getVerifyReturnStatus(string packet){

	if(packet.substr(0,2)=="01")
		return 0;
	else			
		return -1;
}

int MPCoder::getRefundReturnStatus(string packet){
	
	if(packet.substr(0,2)=="01")
                return 0;
        else
                return -1;


}

string MPCoder::getRefundPacket(string a_no,string txid,float amt,string tx_type,string b_no){

	return "18 "+a_no+" "+floatToStr(amt)+" 0 "+tx_type+" CAT-RFND 1 "+b_no+" "+txid;

}

MP_Transfer_Status MPCoder::LoadTransferStatus(string packet){

	/***********************************************
	Receives raw transfer reply packet and loads	
	details into structure
	***********************************************/

	MP_Transfer_Status mp_tran_status;
	string new_balance;
	string new_expiry_date;
	int start_pos=0;
	int end_pos=0;
	int i;

	packet=trim(packet);

	//**** Extracting new balance from packet
        start_pos=packet.find(" ",0);
        //**** If the first space not found, then must be some error
        if(start_pos==string::npos){
              mp_tran_status.transfer_status=-1;
              return mp_tran_status;
        }
        for(i=start_pos+1;i<packet.length();i++){
                if(packet.at(i)!=' ')
                        break;
        }
        start_pos=i;
        end_pos=packet.find(" ",start_pos);
        if(end_pos==string::npos){
              mp_tran_status.transfer_status=-1;
              return mp_tran_status;
        }
        new_balance=packet.substr(start_pos,end_pos-start_pos);
	mp_tran_status.new_balance=atof(new_balance.c_str());

        //**** Extracting new expiry from packet
        for(i=end_pos+1;i<packet.length();i++){
                if(packet.at(i)!=' ')
                        break;
        }
        start_pos=i;
        //**** If the  space not found, then must be some error
        if(start_pos==string::npos){
                mp_tran_status.transfer_status=-1;
                return mp_tran_status;
        }
        new_expiry_date=packet.substr(start_pos,packet.length()-start_pos);
	mp_tran_status.new_expiry_date=new_expiry_date.substr(4,4)+"-"+new_expiry_date.substr(2,2)+"-"+new_expiry_date.substr(0,2);
	


        //**** Test whether it is success reply or otherwise
        if(packet.substr(0,2)=="01"){
                mp_tran_status.transfer_status=1;
                //mp_tran_status.new_balance=StrToFloat(new_balance);
                //mp_tran_status.new_expiry_date=new_expiry_date;
                //cout<<"pck ok"<<endl;
        }
        //cout<<"bal:"<<new_balance<<":"<<endl;
        //cout<<"exp:"<<new_expiry_date<<":"<<endl;
        else{
              mp_tran_status.transfer_status=-1;
        }

        return mp_tran_status;	
	
}


string MPCoder::trim(string& s)
{
	string drop=" ";
 	string r=s.erase(s.find_last_not_of(drop)+1);
 	return r.erase(0,r.find_first_not_of(drop));
}


string MPCoder::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string MPCoder::floatToStr(float val){

        char buff[20];

        sprintf(buff,"%f", val);
        return buffToStr(buff);
}

string MPCoder::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}

MP_Usr_Profile MPCoder::LoadUsrProf(string packet){

	int start_pos=0;
	int end_pos=0;
	MP_Usr_Profile usr_prof;

	//*** Looking for reply status end separator
	end_pos=packet.find(",",start_pos+1);

	//********* MSISDN ****************************************
	//*** setting msisdn start separator
	start_pos=end_pos;
	//*** Looking for msisdn end separator
	end_pos=packet.find(",",start_pos+1);
	usr_prof.msisdn=packet.substr(start_pos+1,end_pos-start_pos-1);

	//********* AIRTIME ****************************************
	//*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);   
        usr_prof.balance=StrToFloat(packet.substr(start_pos+1,end_pos-start_pos));
			
	//********* EXPIRY DATE ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.expiry_date=packet.substr(start_pos+1,end_pos-start_pos-1);
	usr_prof.expiry_date=usr_prof.expiry_date.substr(4,4)+"-"+usr_prof.expiry_date.substr(2,2)+"-"+usr_prof.expiry_date.substr(0,2);

	//********* STATUS ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.status=StrToInt(packet.substr(start_pos+1,end_pos-start_pos));

	//********* PLATFORM ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.platform=StrToInt(packet.substr(start_pos+1,end_pos-start_pos));

	//********* LANGUAGE  ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.language=StrToInt(packet.substr(start_pos+1,end_pos-start_pos));

	//********* BRAND ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.brand=StrToInt(packet.substr(start_pos+1,end_pos-start_pos));

	//********* ACTIVATION DATE ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.activation_date=packet.substr(start_pos+1,end_pos-start_pos-1);
	usr_prof.activation_date=usr_prof.activation_date.substr(4,4)+"-"+usr_prof.activation_date.substr(2,2)+"-"+usr_prof.activation_date.substr(0,2);

	//********* INP NUMBER ****************************************
        //*** setting start separator
        start_pos=end_pos;
        //*** Looking for end separator
        end_pos=packet.find(",",start_pos+1);
        usr_prof.inp=StrToInt(packet.substr(start_pos+1,end_pos-start_pos));


	return usr_prof;

}


int MPCoder::StrToInt(string inval){

        istringstream buffer(inval);
        int retval;

        buffer>>retval;

        return retval;
}

float MPCoder::StrToFloat(string inval){

	std::stringstream ss;
	float retval;

	ss<<inval;
	ss>>retval;

	return retval;
}
                 

//convert float to 2 decimal point string
string MPCoder::floatToString2d(float floatVal) {
char charVal[100];
sprintf(charVal,"%.2f",floatVal); 
string strVal(charVal);
return strVal;
} 
