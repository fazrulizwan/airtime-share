#include "date.h"

///////////////////////////// construction //////////
date::date(const int& d, const int& m, const int& y) {
	day_ = d;
	month_ = m;
	if (y<100) {
	if (y<25){ // the simplest possible x for Y2K, choose 2K if year < 20 (unusual
	// that dates before 1930 is referred to
	year_ = 2000+y;
	}
	else {
	year_ = 1900+y;
	};
	} // assume XX is shorthand for 19XX
	else { year_ = y;};
};


date::date(const long& date){
	year_ = date/10000;
	if (year_ < 100) { year_ = 1900+year_ ;}; /* assume XX is shorthand for 19XX 30
	(if >2000 presumably uses all four) */
	month_ = (date%10000)/100;
	day_ = date%100;
}

bool date::valid() const {
	// This function will check the given date is valid or not.
	// If the date is not valid then it will return the value false.
	// Need some more checks on the year, though
	if (year_ <0) return false;
	if (month_ >12 || month_ <1) return false;
	if (day_ >31 || day_ <1) return false;
	if ((day_ ==31 &&
	( month_ ==2 || month_ ==4 || month_ ==6 || month_ ==9 || month_ ==11) ) )
	return false;
	if ( day_ ==30 && month_ ==2) return false;
	if ( year_ <2000){
	if ((day_ ==29 && month_ ==2) && !((year_ -1900)%4==0)) return false;
	};
	if ( year_ >2000){
	if ((day_ ==29 && month_ ==2) && !((year_ -2000)%4==0)) return false;
	};
	return true;
};

date date::operator ++(int){ // postx operator
	date d = *this;
	*this = next_date(d);
	return d;
}
date date::operator ++(){ // prex operator
	*this = next_date(*this);
	return *this;
}
date date::operator --(int){ // postx operator, return current value
	date d = *this;
	*this = previous_date(*this);
	return d;
}
date date::operator --(){ // prex operator, return new value
	*this = previous_date(*this);
	return *this;
};

date next_date(const date& d){
	date ndat;
	if (!d.valid()) { return ndat; };
	ndat=date((d.day()+1),d.month(),d.year()); if (ndat.valid()) return ndat;
	ndat=date(1,(d.month()+1),d.year()); if (ndat.valid()) return ndat;
	ndat = date(1,1,(d.year()+1)); return ndat;
}

date previous_date(const date& d){
	date ndat;
	if (!d.valid()) { return ndat; }; // return zero 90
	ndat = date((d.day()-1),d.month(),d.year()); if (ndat.valid()) return ndat;
	ndat = date(31,(d.month()-1),d.year()); if (ndat.valid()) return ndat;
	ndat = date(30,(d.month()-1),d.year()); if (ndat.valid()) return ndat;
	ndat = date(29,(d.month()-1),d.year()); if (ndat.valid()) return ndat;
	ndat = date(28,(d.month()-1),d.year()); if (ndat.valid()) return ndat;
	ndat = date(31,12,(d.year()-1)); return ndat;
};

long long_date(const date& d) {
	if (d.valid()){ return d.year() * 10000 + d.month() * 100 + d.day(); };
	return -1;
};

ostream & operator << (ostream& os, const date& d){
	if (d.valid()) { os << " " << long_date(d) << " " ; }
	else { os << " invalid date "; };
	return os;
}

date operator + (const date& d, const int& days){ // IMPROVE!
	if (!d.valid()){return date();};
	if (days<0) {return (d-(-days));};
	date d1=d;
	for (int day=1;day<=days;++day){ d1=next_date(d1); };
	return d1;
}

date operator += (date& d, const int& days){
	d = (d+days);
	return d;
};

date operator - (const date& d, const int& days) {
	if (!d.valid()) { return date();};
	if (days<0) { return (d+(-days));};
	date d1=d;
	for (int day=0; day<days; ++day){ d1=previous_date(d1); }; // IMPROVE!
	return d1;
};


date operator -= (date& d, const int& days){
	return (d-days);
}

date add_months(const date& indate, const int& no_months)
// nd date no months months earlier/later
// this is brute force, haven't gotten round to some cleverer implementation.
{
	date d=indate;
	// cout << d << endl;
	if (!d.valid()) return indate;
	if (no_months==0) return indate;
	int no_years = no_months/12;
	if (no_years!=0){ d.set_year(indate.year()+no_years); };
	int nmonths=no_months%12;
	if (nmonths==0) return d;
	date ndat;
	if (no_months>0) {
	if (d.month()+nmonths<=12) {
	ndat=date((d.day()),d.month()+nmonths,d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-1,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-2,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-3,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	}
	else {
	ndat=date((d.day()),d.month()+nmonths-12,d.year()+1);
	if (ndat.valid()) return ndat;
	ndat=date((d.day()-1),d.month()+nmonths-12,d.year()+1);
	if (ndat.valid()) return ndat;
	ndat=date((d.day()-2),d.month()+nmonths-12,d.year()+1);
	if (ndat.valid()) return ndat;
	ndat=date((d.day()-3),d.month()+nmonths-12,d.year()+1);
	if (ndat.valid()) return ndat;
	};
	}
	else {
	if (d.month()+nmonths>=1) {
	ndat=date((d.day()),d.month()+nmonths,d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-1,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-2,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	ndat=date(d.day()-3,(d.month()+nmonths),d.year());
	if (ndat.valid()) return ndat;
	}
	else {
	ndat=date((d.day()),d.month()+nmonths+12,d.year()-1);

	if (ndat.valid()) return ndat;
	ndat=date((d.day()-1),d.month()+nmonths+12,d.year()-1);
	if (ndat.valid()) return ndat;
	ndat=date((d.day()-2),d.month()+nmonths+12,d.year()-1);
	if (ndat.valid()) return ndat;
	ndat=date((d.day()-3),d.month()+nmonths+12,d.year()-1);
	if (ndat.valid()) return ndat;
	};
	};
	cerr << " Error in Add month " << endl;
	return indate; // something wroong if arrive here
};

date next_month(const date& d) {
	return add_months(d,1);
};

