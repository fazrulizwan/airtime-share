/**************************************************************

	UCPConn ver 1.00 (Dev): SMSC (UCP) Gateway Connector
	----------------------------------------------------

	Handles all MO/MT to/from UCP SMSC.
	Spawn a CoreProcess object for business rules process.

	Developed by Fazrul Izwan Hassan (fazrulizwan@gmail.com)
	For Idot TV Sdn Bhd
	24 March 2008

	"Maybe I deserve someone better who deserves a better me"
	- me

***************************************************************/

#include <iostream>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/shm.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include <cstdlib>
#include <ctime>

#include "UCPCoder.h"
#include "UCPConn.h"
#include "CoreProcess.h"
#include "FileLogger.h"
#include "myfunction.h"
#include "sharedmem.h"
#include "BusinessRules.h"
#include "MTMessage.h"
#include "txid.h"
#include "SMSC_Config.h"
#include "MP_Config.h"
#include "Prepaid_Config.h"
#include "Balance_Config.h"
#include "Post_Config.h"
#include "CDR_SMSGW.h"
#include "smscheck.h"

using namespace std;
using namespace ats;

//**** Proto fn
void* thread_keepalive(void *arg); //***thread, handling keep alive separately n concurrently

//**** Modular global vars
static int childCount; //**** to keep track of child process count
bool sh_ok_reset_alarm;	//*** flag, shared between threads, if true then reset alarm
static bool sh_is_time_keep_alive; //**** flag, if true then its time to send SMPP enquire link, shared resource
int sh_keep_alive_period; //**** keep alive period
string sh_log_path; //**** Log_path

// ***** Error reporting mechanism
static void bail(const char *on_what, bool resume) {

	/***********************************
	bail
	to report detailed reason of error
	on_what: the operation
	resume: set to false to halt program
	************************************/

	string errMsg;
	FileLogger log(sh_log_path);

	errMsg=buffToStr(strerror(errno))+": "+buffToStr((char*) on_what);
	log.logError(errMsg);

	//TODO: comment back
	//fputs(strerror(errno),stderr);
      	//fputs(": ",stderr);
      	//fputs(on_what,stderr);
      	//fputc('\n',stderr);

	if(!resume){
		log.logWarn("SMPP Connector terminated normally, with error");
      		exit(1);
	}
}

// ***** Child process signal handler
void sigchld_handler(int signo)
{

 	while (waitpid(-1, NULL, WNOHANG) > 0);
	childCount--;

	/* Re-instate handler */
        signal(SIGCHLD,sigchld_handler);
}

// ***** SMPPConn constructor
UCPConn::UCPConn(string ucp_conf_file,string mp_conf_file,string bal_conf_file,string prepaid_conf_file,string postconn_conf_file){

       UCPConn::m_ucp_conf_file=ucp_conf_file;
       UCPConn::m_mp_conf_file=mp_conf_file;
       UCPConn::m_bal_conf_file=bal_conf_file;
       UCPConn::m_prepaid_conf_file=prepaid_conf_file;
       UCPConn::m_postconn_conf_file=postconn_conf_file;

}

UCPConn::UCPConn(void){

}


//*** Executor
void UCPConn::Conn(){

	int returnStatus;
	struct sockaddr_in svrAddr;
	char writeBuff[1028];
	char readBuff[1028];
	int sockID;
	string smsc_ip;
	int smsc_port;
	int flag=1;
	UCPCoder ucpcoder;
	string strPDU;
	int byteLen;
	char *mSvrAddr;
	int mSvrPort;
	string resp_packet;
	string out_packet;
	int thr_result;
	pthread_t thread_id;
	MT_t mymt;
	struct MO mo;
	pid_t pid;
	string iid;
	string tx_id;
	MTCollection mtcol;
	int packet_len;
	string peek_buff;
	CDR_SMSGW cdr_writer;
	FileLogger logParam("params.txt");
	short int smsc_id; //**1-nextgen 2-trinode

	logParam.createLog();

	//**** Config loading vars
	SMSCConfig smsc_conf;
	SMSCConfig_t smsc_conf_data;
	MPConfig MP_conf; 	//*** MP Config object loading mechanism
	MPConfig_t MP_conf_data;
    	PrepaidConfig prepaid_conf; 	//*** Prepaid Config object loading mechanism
	PrepaidConfig_t prepaid_conf_data;
	BalanceConfig balance_conf; 	//*** balance Config object loading mechanism
	BalanceConfig_t balance_conf_data;
	PostConfig post_conf;
	PostConfig_t post_conf_data;

	//**** Sharedmem vars
	PortReg port_reg;

	//**** BUILD VERSIONING
	string VER="20110722.001 [NGIN UAT Candidate]";

	//**** Load SMSC Config
	smsc_conf_data=smsc_conf.ReadConfig(UCPConn::m_ucp_conf_file);
	sh_log_path=smsc_conf_data.log_path;
    	FileLogger log(smsc_conf_data.log_path);
	//FileLogger log("SMPPConn.log");
    	iid=smsc_conf_data.instance_id;
	log.logNotice("CATCAR (Build:"+VER+") <IID:"+iid+"> started");
	mSvrPort=smsc_conf_data.smsc_port;
	mSvrAddr=(char *)smsc_conf_data.smsc_ip_addrs.c_str();
	m_max_process=smsc_conf_data.max_concurrency;
	log.logNotice("<IID:"+iid+"> SMSC IP Addrs:"+buffToStr(mSvrAddr));
	log.logNotice("<IID:"+iid+"> SMSC shortcode:"+smsc_conf_data.smsc_shortcode);
	log.logNotice("<IID:"+iid+"> SMSC pwd:"+smsc_conf_data.smsc_pwd);
	log.logNotice("<IID:"+iid+"> SMSC Port:"+IntToString(mSvrPort));
	log.logNotice("<IID:"+iid+"> Max concurrency:"+IntToString(m_max_process));
	log.logNotice("<IID:"+iid+"> Flood prune:"+IntToString(smsc_conf_data.flood_prune));
	log.logNotice("<IID:"+iid+"> db IP addrs:"+smsc_conf_data.db_ip_addrs);
	log.logNotice("<IID:"+iid+"> db Port:"+IntToString(smsc_conf_data.db_port));
	log.logNotice("<IID:"+iid+"> db name:"+smsc_conf_data.db_name);
	log.logNotice("<IID:"+iid+"> db user name:"+smsc_conf_data.db_usr_name);
	log.logNotice("<IID:"+iid+"> db password:"+smsc_conf_data.db_pwd);
	log.logNotice("<IID:"+iid+"> log path:"+smsc_conf_data.log_path);

	//**** Creating txid object by constructor and pass some db params
	TXID txid(smsc_conf_data.db_ip_addrs,smsc_conf_data.db_port,smsc_conf_data.db_name,smsc_conf_data.db_usr_name,smsc_conf_data.db_pwd);

    //**** Load MP Config
	log.logNotice("Loading MP config");
	MP_conf_data = MP_conf.ReadConfig(UCPConn::m_mp_conf_file);
	port_reg.mp_conn_prof_count=MP_conf_data.mp_conn_prof_count;
	log.logNotice("MP Conf: MP Profile Count:"+IntToString(port_reg.mp_conn_prof_count));
	for(int i=0;i<port_reg.mp_conn_prof_count;i++){
            port_reg.mp_conn_prof[i].ip_addrs=MP_conf_data.mp_ip_addrs[i];
            port_reg.mp_conn_prof[i].port_no=MP_conf_data.mp_port[i];
	   cout<<port_reg.mp_conn_prof[i].ip_addrs<<endl;
		cout<<port_reg.mp_conn_prof[i].port_no<<endl;
    	}
	port_reg.mp_log_path=MP_conf_data.log_path;
        port_reg.mp_resp_timeout=MP_conf_data.in_resp_timeout;
        port_reg.mp_retry_delay=MP_conf_data.retry_delay;
        port_reg.mp_max_retry_count=MP_conf_data.max_retry_count;

    //**** Load balance Config
	log.logNotice("Loading balance config");
	balance_conf_data = balance_conf.ReadConfig(UCPConn::m_bal_conf_file);
	port_reg.balance_conn_prof_count=balance_conf_data.balance_conn_prof_count;
	log.logNotice("Balance Conf: Balance Profile Count:"+IntToString(port_reg.balance_conn_prof_count));
	for(int i=0;i<port_reg.balance_conn_prof_count;i++){
            port_reg.balance_conn_prof[i].ip_addrs=balance_conf_data.balance_ip_addrs[i];
            port_reg.balance_conn_prof[i].port_no=balance_conf_data.balance_port[i];
    	}
	port_reg.balance_log_path=balance_conf_data.log_path;
	port_reg.balance_resp_timeout=balance_conf_data.in_resp_timeout;
	port_reg.balance_retry_delay=balance_conf_data.retry_delay;
	port_reg.balance_max_retry_count=balance_conf_data.max_retry_count;

    //**** Load prepaid Config
	log.logNotice("Loading prepaid config");
	prepaid_conf_data = prepaid_conf.ReadConfig(UCPConn::m_prepaid_conf_file);
	port_reg.prepaid_conn_prof_count=prepaid_conf_data.prepaid_conn_prof_count;
	log.logNotice("Prepaid Conf: Prepaid Profile Count:"+IntToString(port_reg.prepaid_conn_prof_count));
	for(int i=0;i<port_reg.prepaid_conn_prof_count;i++){
            port_reg.prepaid_conn_prof[i].ip_addrs=prepaid_conf_data.prepaid_ip_addrs[i];
            port_reg.prepaid_conn_prof[i].port_no=prepaid_conf_data.prepaid_port[i];
    	}
	port_reg.prepaid_log_path=prepaid_conf_data.log_path;
        port_reg.prepaid_resp_timeout=prepaid_conf_data.in_resp_timeout;
	//cout<<"aaa:"<<port_reg.prepaid_resp_timeout<<endl;
        port_reg.prepaid_retry_delay=prepaid_conf_data.retry_delay;
        port_reg.prepaid_max_retry_count=prepaid_conf_data.max_retry_count;

	logParam.logNotice("Balance timeout:"+IntToString(port_reg.balance_resp_timeout));
	//logParam.logNotice("MP timeout:"+IntToString(mp_resp_timeout));

	//*** PostConn related msg
	log.logNotice("Loading postconn config");
	post_conf_data=post_conf.ReadConfig(UCPConn::m_postconn_conf_file);
	port_reg.pg_endpoint=post_conf_data.pg_endpoint;
	log.logNotice("PG Endpoint:"+port_reg.pg_endpoint);
	port_reg.pg_port=post_conf_data.pg_port;
	log.logNotice("PG port:"+IntToString(port_reg.pg_port));
	port_reg.SSL_cert=post_conf_data.SSL_cert;
	log.logNotice("SSL Cert:"+port_reg.SSL_cert);
	port_reg.pg_login=post_conf_data.pg_login;
	log.logNotice("PG login:"+port_reg.pg_login);
	port_reg.pg_pwd=post_conf_data.pg_pwd;
	log.logNotice("PG password:"+port_reg.pg_pwd);
	port_reg.ema_ip=post_conf_data.ema_ip;
	log.logNotice("EMA ip:"+port_reg.ema_ip);
	port_reg.ema_port=post_conf_data.ema_port;
	log.logNotice("EMA port:"+IntToString(port_reg.ema_port));	
	port_reg.ema_login=post_conf_data.ema_login;
	log.logNotice("EMA login:"+port_reg.ema_login);
	port_reg.ema_pwd=post_conf_data.ema_pwd;
	log.logNotice("EMA pwd:"+port_reg.ema_pwd);
	port_reg.postconn_log_path=post_conf_data.postconn_log_path;
	log.logNotice("PostConn log path:"+port_reg.postconn_log_path);

	//**** Coreprocess conf loading
	BusinessRules business_rules;
	MTMessage mt_msg;

	if(!business_rules.ReadConfig()){
		log.logError("Error loading business rules config");
		exit(0);
	}

	if(!mt_msg.ReadMessage()){
			log.logError("Error loading business rules config");
			exit(0);
	}

	//**** Console welcome print
	cout << endl << endl << "CATCAR (build: " << VER << ") started" << endl;
    cout<< "Using " << UCPConn::m_ucp_conf_file << " for SMSC config." << endl;
	cout  << "Copyrights Idottv Sdn Bhd (c) 2007-2008" << endl;
	cout << "Please see appropriate log to see progress." << endl<<endl;

	childCount=0;

	//***** get a socket FD
	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
		log.logError("Error getting a socket FD");
		exit(1);
        }

	//***** set destination address struct
	bzero(&svrAddr,sizeof(svrAddr));
        svrAddr.sin_family=AF_INET;
        svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
        svrAddr.sin_port=htons(mSvrPort);

	//***** Let's connect to SMSC
	returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));
        // ** Fail to connect to SMSC
        if(returnStatus<0){
		log.logError("Error: cannot connect to SMSC <IID:"+iid+">");
		close(sockID);
		exit(1);
        }

	log.logNotice("Connected to SMSC <IID:"+iid+">");

	//*** Turn off Nagle's algorithm
       	setsockopt(sockID, IPPROTO_TCP, TCP_NODELAY, (char *) &flag,sizeof(int));

	//***** Establishing UCP binding
	strPDU=ucpcoder.getOp60(smsc_conf_data.smsc_shortcode,smsc_conf_data.smsc_pwd);
	returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
	log.logNotice("Sent Op60 packet:"+strPDU);
	//*** Cannot send TxRx PDU
        if(returnStatus<0){
                log.logError("Error: cannot establish bind handshake.<IID:"+iid+">");
		close(sockID);
		exit(1);
        }

	//*** Wait for TxRx response from SMSC
	returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
	readBuff[returnStatus]='\0';
	resp_packet=buffToStr(readBuff);
	//*** Error in reading PDU
	if(returnStatus<0){
		log.logError("error: cannot receive bind handshake response <IID:"+iid+">");
		close(sockID);
		exit(1);
	}
	else{
		log.logNotice("Received:: "+IntToString(returnStatus)+ " bytes");
		log.logNotice("UCP packet received::"+resp_packet+"::");

		//*** Binding OK
		if(ucpcoder.decodeResp(resp_packet)==OP60_RESP_OK){
			log.logNotice("SMSC Binding OK <IID:"+iid+">");
		}
		else{ //*** Binding fail
			log.logError("SMSC Binding fail! <IID:"+iid+">");
			exit(1);
		}

	}

	//**** Creating a thread to handle keep alive
	thr_result = pthread_create(&thread_id, NULL, thread_keepalive, (void *) sockID);
    	if (thr_result != 0) {
      		log.logError("Could not create thread for enquire link.");
		close(sockID);
		exit(1);
    	}
	pthread_detach(thread_id);
        sched_yield();

	//***** Signal handler for child process
	signal(SIGCHLD, sigchld_handler);

	//***** Looping for MO/MT
	while(1){
		strcpy(readBuff,"");

		//*** Waiting for incoming packet
		log.logNotice("Waiting for incoming packet..<IID:"+iid+">");
		//*** We peek at the packet first
		//returnStatus=recv(sockID,readBuff,10,MSG_PEEK);
		//readBuff[returnStatus]='\0';
		//if(returnStatus<0){
		//	log.logError("Error: cannot peek packet.");
                //        close(sockID);
                //        exit(1);
		//}
		//else{
		//	peek_buff=buffToStr(readBuff);
		//	log.logDebug("Peek buff:"+peek_buff);
		//	packet_len=ucpcoder.getHeaderLen(peek_buff);
		//	log.logNotice("Peeked packet len"+IntToString(packet_len));
		//}

		returnStatus=recv(sockID,readBuff,sizeof(readBuff)-1,0);
		//returnStatus=recv(sockID,readBuff,packet_len,0);
		readBuff[returnStatus]='\0';
		//***** Error in receiving
		if(returnStatus<0){
			log.logError("Error: cannot receive packet.<IID:"+iid+">");
			close(sockID);
			exit(1);
		}
		else{ //*** We've received a packet from SMSC
			//*** Reset keep alive alarm since we have activity here
			sh_ok_reset_alarm=true;
			log.logDebug("Alarm reset flag is raised");

			resp_packet=buffToStr(readBuff);
			//if(!ucpcoder.isCSok(resp_packet)){
			//	log.logError("Checksum for packet :"+resp_packet+": is invalid");
			//	exit(1);
			//}

			//*** Incoming MO from SMSC
			if(ucpcoder.decodeResp(resp_packet)==OP52_OP_PACKET){
				log.logNotice("Received MO:"+resp_packet+":"+IntToString(returnStatus)+" bytes <IID:"+iid+">");
				//*** Get the ACK resp packet
				out_packet=ucpcoder.getOp52Resp(resp_packet,"A");
				log.logNotice("Sending OP 52 RESP ACK...<IID:"+iid+">");
				returnStatus=send(sockID,out_packet.c_str(),out_packet.length()+1,0);
				if(returnStatus<0){
					log.logError("Error: cannot sent DELIVER_SM_RESP ACK <IID:"+iid+">");
					close(sockID);
					exit(1);
				}
				else{
					log.logNotice("Sent Op52 Resp:"+out_packet+": "+IntToString(returnStatus)+" bytes");


					//**** Getting MO details
					mo=ucpcoder.getMODetail(resp_packet);
					log.logNotice("MO details: seq["+IntToString(mo.seq_no)+"] oadc["+mo.oadc+"] adc["+mo.adc+"] msg["+mo.msg+"]");


                    //**** If we exceed max concurency AND make sure it's enabled
					if(childCount>=m_max_process && m_max_process!=0){
						log.logWarn("Maximum concurency reached!");

						//*** insert in cdr anyway
						cdr_writer.insertInATS("0",currentTimestamp(),mo.oadc,mo.adc,mo.msg,"0");

						//***send msg when busy
						initMT(mymt,1);
						mymt.oadc="10191000000";
						mymt.adc=mo.oadc;
						mymt.msg="Perkhidmatan airtime share sedang sibuk.Sila cuba sebentar lagi.";

						if(sendMT(sockID,mymt)>0){
	        						cdr_writer.insertOutATS("0",currentTimestamp(),mymt.oadc,mymt.adc,mymt.msg,"0","0");

							   log.logNotice("<System Busy> MT sent oadc["+mymt.oadc+"] adc["+mymt.adc+"] msg["+mymt.msg+"]");
						} //**if sendMT
						else{
						           log.logWarn("SendMT for busy fail.");
						}

					}
					else { //*** if below max, then ok
						//log.logNotice("Spawning a child process... (Currently have "+IntToString(childCount)+" child count)<IID:"+iid+">");
						//*** inc child proc counter
						//childCount++;


						//*** VAlidating MO
						//Arahan tidak sah. Untuk maklumat lanjut, sila taip HELP & hantar ke 1019
						//*** Mo msg is invalid
						if(checkSyntax(mo.msg)==false){
							log.logWarn("MO msg is invalid oadc["+mo.oadc+"] adc["+mo.adc+"] msg["+mo.msg+"]");
							//*** insert in cdr anyway
        	                                        cdr_writer.insertInATS("0",currentTimestamp(),mo.oadc,mo.adc,mo.msg,"0");

							//***send msg when busy
                                                	initMT(mymt,1);
                                                	mymt.oadc="10191000000";
                                                	mymt.adc=mo.oadc;
                                                	mymt.msg="Arahan tidak sah. Untuk maklumat lanjut, sila taip HELP & hantar ke 1019";

                                                	if(sendMT(sockID,mymt)>0){
								   cdr_writer.insertOutATS("0",currentTimestamp(),mymt.oadc,mymt.adc,mymt.msg,"0","0");
                                                        	   log.logNotice("<Invalid syntax> MT sent oadc["+mymt.oadc+"] adc["+mymt.adc+"] msg["+mymt.msg+"]");
                                                	} //**if sendMT
                                                	else{
                                                           	log.logWarn("SendMT for invalid msg fail.");
                                                	}

							continue;
							log.logError("inform izwan immediatly!");
						}

						 log.logNotice("Spawning a child process... (Currently have "+IntToString(childCount)+" child count)<IID:"+iid+">");
                                                //*** inc child proc counter
                                                childCount++;


						//tx_id=txid.getTXID();
                                        	//log.logNotice("Tx ID generated:"+tx_id);

    					//***** Let's spawn a child process!
    					if((pid=fork())==0){
    //******************************************** BEGIN CHILD PROCESS AREA *****************************************

                              //log.logNotice("child process spawn (Count:"+IntToString(childCount)+")");
				//*** Commenting note: MO validation should not be done after txid generated.
                              //***** Validate MO
                              //mo=ucpcoder.getMODetail(resp_packet);
				//              log.logNotice("MO details: seq["+IntToString(mo.seq_no)+"] oadc["+mo.oadc+"] adc["+mo.adc+"] msg["+mo.msg+"]");

				              //****TODO: Anti flooding mechanism (optional)
				              //*******************************************


                             //**** This body block we assume, MO has been fully validated

                             //**** Get a txid
							 tx_id=txid.getTXID();
							 log.logNotice("Tx ID generated:"+tx_id);

							 //*** TODO
							 //**** Write CDR
				             cdr_writer.insertInATS(tx_id,currentTimestamp(),mo.oadc,mo.adc,mo.msg,"0");

						//** check smsc platform

							smsc_id=smsc_conf_data.smsc_id;

				             // *** Spawn a core process object
				             log.logDebug("Spawning ATS coreprocess (txid:"+tx_id+")");
                             		     CoreProcess cp=CoreProcess("6"+mo.oadc,mo.msg,mo.adc,tx_id,port_reg,business_rules,mt_msg,smsc_id);
                             		     mtcol=cp.ProcessMessage();

                             for(int i=0;i<=5;i++){
					//sleep(1);
					usleep(50000);

									//translate taufik MT to my MT
									log.logNotice("MT details> msg("+IntToString(i)+"): ["+mtcol.smsMT[i].msg+"] dest_addrs["+mtcol.smsMT[i].receiver+"] src_addrs["+mtcol.smsMT[i].oadc+"] (txid:"+tx_id+") smscid:"+IntToString(smsc_id));
									if(mtcol.smsMT[i].msg!=""){
                                        mymt=translateMT(mtcol.smsMT[i],i);
										if(mtcol.smsMT[i].charging==15){
											if(SendMTCharge(smsc_id,tx_id,mtcol.smsMT[i].receiver,mtcol.smsMT[i].oadc,mtcol.smsMT[i].msg,15)==0){
												log.logNotice("SendMT15 OK (txid:"+tx_id+")");
												cdr_writer.insertOutATS(tx_id,currentTimestamp(),mtcol.smsMT[i].oadc,mtcol.smsMT[i].receiver,mtcol.smsMT[i].msg,"0","15");
											}
										}
										else if(mtcol.smsMT[i].charging==30){
											if(SendMTCharge(smsc_id,tx_id,mtcol.smsMT[i].receiver,mtcol.smsMT[i].oadc,mtcol.smsMT[i].msg,30)==0){
												log.logNotice("SendMT30 OK (txid:"+tx_id+")");
												cdr_writer.insertOutATS(tx_id,currentTimestamp(),mtcol.smsMT[i].oadc,mtcol.smsMT[i].receiver,mtcol.smsMT[i].msg,"0","30");
											}
										}
										else{
											log.logNotice("Sending notification MT (txid:"+tx_id+")");
											if(sendMT(sockID,mymt)>0){
												cdr_writer.insertOutATS(tx_id,currentTimestamp(),mtcol.smsMT[i].oadc,mtcol.smsMT[i].receiver,mtcol.smsMT[i].msg,"0","0");

											} //**if sendMT
											else{
												log.logWarn("SendMT fail.  (txid:"+tx_id+") ");
											}
										}
									} //**if mtcol
								} //** for

                              close(sockID);
					      	  log.logNotice("Closing down a child process  (txid:"+tx_id+") ");
           		              exit(0);

    //******************************************** END CHILD PROCESS AREA *******************************************
    					}
    					else if(pid==-1){
    							log.logWarn("Fail to spawn a child process");
    							childCount--;	//*** decr child process count
    					}
					} //** else max child

				} //*** Else return status


			} //***  if incoming MO
			else if(ucpcoder.decodeResp(resp_packet)==OP51_RESP_OK){
				log.logNotice("Received Op51 ACK (MT Ack): "+resp_packet+"<IID:"+iid+">");
			}
			//***Op 57
			else if(ucpcoder.decodeResp(resp_packet)==OP57_OP_PACKET){
                                log.logNotice("Received Op 57:"+resp_packet+"<IID:"+iid+">");
                                out_packet=ucpcoder.getOp57Resp();
                                log.logNotice("Sending OP 57 RESP ACK:"+out_packet);
                                returnStatus=send(sockID,out_packet.c_str(),out_packet.length()+1,0);
                                if(returnStatus<0){
                                        log.logError("Error: cannot sent DELIVER_SM_RESP ACK");
                                        close(sockID);
                                        exit(1);
                                }
                                else{
                                        log.logNotice("Sent Op57 Resp:"+out_packet+":"+IntToString(returnStatus)+" bytes");
                                }

                        }
			else{
				log.logWarn("Received other packet:"+resp_packet+"<IID:"+iid+">");
			}
		}

	} //** while(1)

}

void UCPConn::initMT(MT_t& mt,int index){

	mt.oadc="";
	mt.adc="";
	srand((unsigned)time(0)+(unsigned)index);
	mt.seq_no=(rand()%99)+1;
	mt.msg="";

}

MT_t UCPConn::translateMT (MT_tau mt,int index){

	MT_t mymt;

	initMT(mymt,index);
	mymt.oadc=mt.oadc;
	mymt.adc=mt.receiver;
	mymt.msg=mt.msg;

	return mymt;

}

int UCPConn::sendMT(int sockID, MT_t mt){

	/***********************************
	sendMT
	sends submit_sm PDU to smsc
	receives: sockID (the existing FD from main)
		  MT structure
	returns : non-negative value if success
	***********************************/

	string strPDU;
	UCPCoder ucpcoder;
	int returnStatus;
	FileLogger log(sh_log_path);

	strPDU=ucpcoder.getOp51(trim2zero(IntToStr(mt.seq_no)),mt.oadc,mt.adc,mt.msg);
	returnStatus=send(sockID,strPDU.c_str(),strPDU.length()+1,0);
	if(returnStatus<0){
		//log.logError("Unable to send MT");
		return -1;
	}
	else{
		log.logNotice("MT sent ("+IntToString(returnStatus)+" bytes)"+strPDU);
		return 1;
	}
}

string UCPConn::trim2zero(string num){

	if(num.length()==1)
		return "0"+num;
	else
		return num;

}

string UCPConn::IntToStr( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string UCPConn::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


void* thread_keepalive(void *arg){
	/********************
	thread_keepalive
	Receives sock fd from parent thread
	To handle keep-alive.
	********************/

	long int t_sockID;
	int t_returnStatus;
	int t_keep_alive_period;
	char t_writeBuff[128];
	int t_byteLen;
	string t_strPDU;
	time_t start,finish;
	UCPCoder ucpcoder;
	UCPConn ucpconn;
	FileLogger t_log(sh_log_path);

	//**** Copying a sock FD from parent thread
	t_sockID=dup((long int) arg);

	start=time(NULL);

	//**** Loop indefinately
	while(1){
		//**** Check for flag,whether its time to reset alarm
		if(sh_ok_reset_alarm){
			//*** Resetting alarm
			sh_ok_reset_alarm=false;
			start=time(NULL);
			t_log.logDebug("Alarm has been reset");
		}

		//**** Check flag,whether its time to send keep alive
		finish=time(NULL);
		if(finish-start>=150){
			t_strPDU=ucpcoder.getKeepAlive();
			t_returnStatus=send(t_sockID,t_strPDU.c_str(),t_strPDU.length()+1,0);
			if(t_returnStatus<0){
				t_log.logError("Error sending enquire link");
			}
			else{
				t_log.logNotice("KEEP ALIVE sent ("+ucpconn.IntToString(t_returnStatus)+" bytes):"+t_strPDU);
			}
			start=time(NULL);
		}

		sleep(1);
	}

}

string UCPConn::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


int UCPConn::SendMTCharge(int platform,string txid,string adc,string oadc,string msg, int price){

	//**** Platform 1-nextgen 2-trinode
	int sockID;
	string ip_addrs;
	char *svrAddrs;
	int svrPort;
	struct sockaddr_in svrAddr;
	int returnStatus;
	FileLogger log(sh_log_path);
	string packet;
	char writeBuff[1028];
	char readBuff[1028];
	struct timeval tv;

	if (price==15) {
		//15 send
		if(platform==1)
			svrPort=20020;
		else if(platform==2)
			svrPort=20020;
		else if(platform==3)
			svrPort=20020;
		else if(platform==4)
			svrPort=20020;
		else if(platform==5)
			svrPort=20020;
		else if(platform==6)
			svrPort=20020;
		else
			svrPort=20020;
	} else {
		//30 send
                if(platform==1)
                        svrPort=20030;
                else if(platform==2)
                        svrPort=20030;
                else if(platform==3)
                        svrPort=20030;
                else if(platform==4)
                        svrPort=20030;
                else if(platform==5)
                        svrPort=20030;
                else if(platform==6)
                        svrPort=20030;
		else
			svrPort=20030;
	}

	ip_addrs="10.122.17.102";
	svrAddrs=(char *)ip_addrs.c_str();

	//***** get a socket FD
	sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
		log.logError("Error getting a socket FD");
                bail("socket(2)",false);
        }

	//***** set destination address struct
	bzero(&svrAddr,sizeof(svrAddr));
        svrAddr.sin_family=AF_INET;
        svrAddr.sin_addr.s_addr=inet_addr(svrAddrs);
        svrAddr.sin_port=htons(svrPort);

	//***** Let's connect
	returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));
        // ** Fail to connect
        if(returnStatus<0){
		log.logError("Error: cannot connect to SMSOUT"+IntToString(price));
		close(sockID);
                bail("connect(2)",true);
        }

	log.logNotice("Connected to SMSOUTCharge on port "+IntToString(svrPort));

	for(int i=0;i<msg.length();i++){
		if(msg.at(i)=='|')
			msg.replace(i,1,"*");
	}
	packet="STX|023|QRYRES=QRY|OPCODE=01|TXID="+txid+"|ADC="+adc+"|OADC="+oadc+"|MSG="+msg+"|ETX";

	strcpy(writeBuff,packet.c_str());

	returnStatus=send(sockID,writeBuff,packet.length()+1,0);
	if(returnStatus<0){
		log.logError("Failed to send query packet");
		close(sockID);
                return -1;
	}

	log.logNotice("Sent to SMSOUTCharge("+IntToString(price)+"):"+packet+" (txid:"+txid+")");

	tv.tv_usec=0;
        tv.tv_sec = 15;
        setsockopt(sockID, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));

	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
	readBuff[returnStatus]='\0';
	if(returnStatus<0){
		//**** If timeout occurs
		if (returnStatus==-1){
			log.logWarn("Timeout, while waiting for query reply SMSOUT15! (txid:"+txid+")");
                        close(sockID);
                        return -2;
		}
		else{
			log.logError("Error receiving query resp from SMSOUTCharge (txid:"+txid+")");
			close(sockID);
                	return -1;
		}
	}
	else{
		packet=buffToStr(readBuff);
		log.logNotice("Received from SMSOUTCharge:"+packet+": (txid:"+txid+")");
	}

	close(sockID);
	return 0;

}


string UCPConn::currentTimestamp(void) {
                char timestamp[25];
                string currentdatetime;
                time_t mytime;
                struct tm *mytm;
                mytime=time(NULL);
                mytm=localtime(&mytime);

                strftime(timestamp,sizeof timestamp,"%Y-%m-%d %H:%M:%S",mytm);

                currentdatetime = (string)timestamp;
                return currentdatetime;
}


int UCPConn::getChildCount(string iid){

	string command="ps -ef | grep 101"+iid+" | grep -v grep | wc -l > /home/cat/src/"+iid+".watch";
	string fname="/home/cat/src/"+iid+".watch";
	string line;

        ifstream fileConfig (fname.c_str());

        if (fileConfig.is_open()) {
                        getline (fileConfig,line);
        }

        fileConfig.close();

        return atoi(line.c_str());


}
