#include <iostream>
#include <vector>

#include "generalfunction.h"
#include "registration.h"
#include "IN_Conn.h"
#include "FileLogger.h"
#include "Subscriber.h"
#include "MysqlConnDB.h"


using namespace std;

namespace ats {

    registration::registration(string tx_id, string userPhone, string userPin, const FileLogger& dlogger, PortReg portIN, MTMessage mtLib) {
         phone = userPhone;
         pin = userPin;
         txid = tx_id;
         logger = dlogger;
         inPort = portIN;
         mtLibrary = mtLib;
         logger.logDebug("[TX ID:"+txid+"] [Constructor Registration] [PHONE:"+phone+"] [PIN:"+pin+"]");
    }
    
    registration::registration(string tx_id, string userPhone, string oldPwd, string newPwd,  const FileLogger& dlogger, MTMessage mtLib) {
         phone = userPhone;
         pin = oldPwd;
         txid = tx_id;
         newPin = newPwd;
         logger = dlogger;
         mtLibrary = mtLib;
         logger.logDebug("[TX ID:"+txid+"] [Constructor Registration] [PHONE:"+phone+"] [PIN:"+pin+"] [NEW PIN:"+newPin+"]");
    }
    
    //user registration process
    short int registration :: registerUser() 
    { 
         vector<string> responseVector;
         short int PrepaidQueryStatus;
         short int userLanguage;
         short int userPlatform;
         short int  userPackage;
         short int  userStatus;
         string userExpiryDate;
         string acitvationDate;
         string PrepaidResponse;
         MysqlConnDB mysqlObject;
         
         logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-START]");
 
         //check phone number format
         if (chkPhoneFormat(phone)==false) {
        	cout << "Invalid phone number";
        	logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-Invalid Phone Number]");
        	return 1;
         //check pin number format
         } else if (chkPinFormat(pin)==false) {
        	cout << "Invalid pin number";
        	logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-Invalid Pin Number]");
        	return 2;
         //check user in profile
         } else {
        	if (mysqlObject.OpenConnDB()) {
        		short int profileDB = chkProfile(phone,mysqlObject.myData,logger);
        		if (profileDB==1) {
        			logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-Phone Already Registered]");
        			mysql_close(mysqlObject.myData);
        			return 3;
        		} else {
        			logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-Error in sql]");
	        		return 10;
        		}
        	} else {
        		logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-Cannot connect to database]");
        		return 10;
        	}
          } 
         
         //no syntax error, proceed to query profile in Prepaid
         logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()-No errror, make request to IN]");
                    		
         //query Prepaid profile 
         IN_Conn prepaid;
         struct IN_UserProfile ProfileResult = prepaid.QueryProfile(phone,inPort,txid);
         
         PrepaidQueryStatus = ProfileResult.queryStatus;
         userLanguage = ProfileResult.userLang;
         userPlatform = ProfileResult.userPlatform;
         userPackage = ProfileResult.userType;
         userStatus = ProfileResult.userStatus;
         userExpiryDate = ProfileResult.expiryDate;
         
         logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Reply from IN:"+PrepaidResponse+"]");
                     
         if (PrepaidQueryStatus==1) { 
           //error cannot connect to IN
           cout << "Cannot connect to IN";
           logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Cannot connect to IN]");
           return 4;
         } else if (PrepaidQueryStatus==2) {  
           //if Prepaid Timeout
           cout << "Invalid phone number";
           logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Invalid phone no]");
           return 5;
         } else if (PrepaidQueryStatus==3) {
           //if Prepaid BAD PACKET
           cout << "Phone no not exist";
           logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Phone no not exist]");
           return 6;
         }
         
         //no Prepaid Server error, proceed to registration
         logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- No Error on IN Reply]");
                        
         //insert into profile database
         Subscriber regUser;
         regUser.setPhone(phone);
         regUser.setPwd(pin);
         regUser.setType(userPackage);
         regUser.setExpiry(userExpiryDate);
         regUser.setINStatus(userStatus);
         regUser.setINType(ProfileResult.userPlatform);
         short int regStatus = registerUserProfile(regUser,txid,mysqlObject.myData,logger);
         mysql_close(mysqlObject.myData);
         if (regStatus==2) {
        	 logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Cannot connect to database!]");
        	 return 10;
         } else if (regStatus==1) {
        	 logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Error in sql insert! Pls chk error log]");
        	 return 7;
         } else if (regStatus==0) {
        	 logger.logDebug("[TX ID:"+txid+"] [registration.registerUser()- Registration Successfull]");
             return 0;
         }
    }
    
    //change password user
    short int registration :: changePassword() 
    {
    	//cout << "Enter Change Password function";
    	MysqlConnDB mysqlObject;
    	logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-START]");
        //check current password format
    	if (chkPinFormat(pin)==false) {
    		logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Invalid format current password]");
    		return 4;
    	} else if (chkPinFormat(newPin)==false) {
    		logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Invalid format new password]");
    		return 5;
    	}
    		
    	if (mysqlObject.OpenConnDB()) {
	    	short int chkResult = chkUserPwd(phone,pin,mysqlObject.myData,mtLibrary.dir_error);
    		if (chkResult==0) {
	        	//change password
	        	logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Check Current Password successfull]");
	        	short int chgResult = changeUserPwd(phone,newPin,mysqlObject.myData,mtLibrary.dir_error);
	        	mysql_close(mysqlObject.myData);
	        	if (chgResult==0) {
	        		logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Change Password successfull]");
	        		return 0;
	        	} else if (chgResult==1) {
	        		logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Error in sql]");
	        		return 1;
	        	} 
	        } else if (chkResult==1) {
	        	mysql_close(mysqlObject.myData);
	        	logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Phone no not exist]");
	        	return 2;
	        } else if (chkResult==2) {
	        	mysql_close(mysqlObject.myData);
	        	logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Error in sql]");
	        	return 1;
	        } else if (chkResult==3) {
	        	mysql_close(mysqlObject.myData);
	        	logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Current pwd is not correct]");
	        	return 6;
	        }
    	} else {
    		mysql_close(mysqlObject.myData);
    		logger.logDebug("[TX ID:"+txid+"] [registration.changePassword()-Cannot connect to database]");
    		return 3;
    	}
    }
    
}
