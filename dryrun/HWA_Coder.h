#ifndef HUAWEI_CODER_H_
#define HUAWEI_CODER_H_


#include <iostream>

typedef struct HWAUSRProfileStruct{

	std::string msisdn;
	float balance;
	int status;
	std::string activation_date;
	std::string expiry_date;

}HWAUSRProfile;

const int HWA_RET_STATUS_OK=0;

const std::string START_FLAG="60534360";

class HWACoder{

	public:
		std::string getLoginPacket(unsigned long int,unsigned long int,std::string,std::string);
		std::string getCheckSum(std::string,std::string,std::string,std::string);
		std::string getLogoutPacket(unsigned long int,unsigned long int);
		int  getBytes (char *, std::string string );
		std::string getProfilePacket(unsigned long int,unsigned long int,std::string);
		std::string getDeductPacket(unsigned long int,unsigned long int,std::string,float);
		std::string getAddPacket(unsigned long int,unsigned long int,std::string,float);
		int getReturnStatus(std::string);
		HWAUSRProfile loadProfile(std::string);

	private:
		std::string trimOperative(std::string);
		std::string AsciiToHexDump(std::string,bool);	
		std::string intToHexStr(int,int);
		std::string ULongIntToHexStr(unsigned long int,int);
		long int HexStrToInt(std::string);
		std::string buffToStr(char *);
		std::string HexIntToHwaInt(std::string,int);	
		std::string IntToString(int);
		std::string floatToIntStr(float);
		float fround(float,unsigned);

};

#endif
