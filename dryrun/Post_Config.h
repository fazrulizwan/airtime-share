#ifndef POST_CONFIG_H_
#define POST_CONFIG_H_

using namespace std;

typedef struct PostConfigStruct{
	std::string pg_endpoint;
        int pg_port;
        std::string SSL_cert;
        std::string pg_login;
        std::string pg_pwd;
        //*** EMA
        std::string ema_ip;
        int ema_port;
        std::string postconn_log_path;	
	std::string ema_login;
	std::string ema_pwd;
	int load_status;
}PostConfig_t;

	class PostConfig {
    	public:
    		PostConfig_t ReadConfig(string);

	private:	
		void trim(string&);
	
		
    	};
	
#endif /*BUSINESSRULES_H_*/
