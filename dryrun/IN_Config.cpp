#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "IN_Config.h"

using namespace std;

	INConfig_t INConfig::ReadConfig(string conf_file) {
		string line;
		string name;
		string value;
		int posEqual;
		INConfig_t inconf;
		ifstream fileConfig (conf_file.c_str());

		//**** Init of conf struct
		inconf.huawei_port_count=-1;
		inconf.huawei_port_list[0]=-1;
		inconf.huawei_port_list[1]=-1;
		inconf.huawei_port_list[2]=-1;
		inconf.huawei_port_list[3]=-1;
		inconf.huawei_port_list[4]=-1;
		inconf.huawei_port_list[5]=-1;
		inconf.huawei_port_list[6]=-1;
		inconf.huawei_port_list[7]=-1;
		inconf.huawei_port_list[8]=-1;
		inconf.huawei_port_list[9]=-1;
		inconf.max_retry_count=-1;
		inconf.retry_delay=-1;
		inconf.huawei_in_ip_addrs="";
		inconf.load_status=-1;
		inconf.ericson_in_ip_addrs="";
		inconf.ericson_port_count=-1;
                inconf.ericson_port_list[0]=-1;
                inconf.ericson_port_list[1]=-1;
                inconf.ericson_port_list[2]=-1;
                inconf.ericson_port_list[3]=-1;
                inconf.ericson_port_list[4]=-1;
                inconf.ericson_port_list[5]=-1;
                inconf.ericson_port_list[6]=-1;
                inconf.ericson_port_list[7]=-1;
                inconf.ericson_port_list[8]=-1;
                inconf.ericson_port_list[9]=-1;

		
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trim(name);
				value = line.substr(posEqual+1);
				trim(value);
				//cout << "[Name:" << name << "=" << value << "]";
				
				if (name=="huawei_port_count") {
					inconf.huawei_port_count=atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port_count") {
					inconf.ericson_port_count=atoi(value.c_str());
					continue;
				} else if (name=="huawei_port1") {
					inconf.huawei_port_list[0] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port2") {
					inconf.huawei_port_list[1] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port3") {
					inconf.huawei_port_list[2] = atoi(value.c_str());
					continue;
				}	
				else if (name=="huawei_port4") {
					inconf.huawei_port_list[3] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port5") {
					inconf.huawei_port_list[4] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port6") {
					inconf.huawei_port_list[5] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port7") {
					inconf.huawei_port_list[6] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port8") {
					inconf.huawei_port_list[7] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port9") {
					inconf.huawei_port_list[8] = atoi(value.c_str());
					continue;
				}
				else if (name=="huawei_port10") {
					inconf.huawei_port_list[9] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port1") {
					inconf.ericson_port_list[0] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port2") {
					inconf.ericson_port_list[1] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port3") {
					inconf.ericson_port_list[2] = atoi(value.c_str());
					continue;
				}	
				else if (name=="ericson_port4") {
					inconf.ericson_port_list[3] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port5") {
					inconf.ericson_port_list[4] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port6") {
					inconf.ericson_port_list[5] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port7") {
					inconf.ericson_port_list[6] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port8") {
					inconf.ericson_port_list[7] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port9") {
					inconf.ericson_port_list[8] = atoi(value.c_str());
					continue;
				}
				else if (name=="ericson_port10") {
					inconf.ericson_port_list[9] = atoi(value.c_str());
					continue;
				}

				else if(name=="max_retry_count"){
					inconf.max_retry_count=atoi(value.c_str());
					continue;
				}
				else if(name=="retry_delay"){
					inconf.retry_delay=atol(value.c_str());
					continue;
				}
				else if(name=="huawei_in_ip_addrs"){
					inconf.huawei_in_ip_addrs=value;
				}
				else if(name=="huawei_in_usr_name"){
                                        inconf.huawei_in_usr_name=value;
                                }
				else if(name=="huawei_in_pwd"){
                                        inconf.huawei_in_pwd=value;
                                }
				else if(name=="ericson_in_ip_addrs"){
                                        inconf.ericson_in_ip_addrs=value;
                                }
				else if(name=="ericson_in_usr_name"){
                                        inconf.ericson_in_usr_name=value;
                                }
				else if(name=="ericson_in_pwd"){
                                        inconf.ericson_in_pwd=value;
                                }
				else if(name=="in_resp_timeout"){
					inconf.in_resp_timeout=atoi(value.c_str());
				}
				else if(name=="log_path"){
					inconf.log_path=value;
				}

				
			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			inconf.load_status=1;
			return inconf;
		} else {
			//cout << "Cannot open config file!";
			return inconf;
		}
	}


void INConfig::trim(string& str)
{
        string::size_type pos1 = str.find_first_not_of(' ');
        string::size_type pos2 = str.find_last_not_of(' ');
        str = str.substr(pos1 == string::npos ? 0 : pos1,
        pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

