#include <stdio.h>
#include "curl/curl.h"
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "Post_Conn.h"
#include "pugixml.hpp"
#include "IN_Conn.h"
#include "FileLogger.h"
#include "EMA_Conn.h"
#include "NginCoder.h"

using namespace std;
using namespace ats;

size_t write_to_string(void *ptr, size_t size, size_t count, void *stream) {
  ((string*)stream)->append((char*)ptr, 0, size*count);
  return size*count;
}

string floatToString2d(float floatVal) {
	char charVal[100];
	sprintf(charVal,"%.2f",floatVal);
	string strVal(charVal);
	return strVal;
}

string Post_Conn::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


string Post_Conn::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                        retval=retval+buff[i];
        }

        return retval;
}





long int Post_Conn::HexStrToInt(string hexStr){

        string tempHexStr="";
        int i;

        for(i=0;i<hexStr.length();i++){
                if(hexStr.substr(i,1)!=" ")
                        tempHexStr=tempHexStr+hexStr.substr(i,1);
        }

        return strtol(tempHexStr.c_str(),NULL,16);

}


string Post_Conn::getAccNum(string svcnum,string txid,PortReg port_reg){

	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist = NULL;
	//static const char *pCertFile = "/home/cat/libcurl/certdata.pem";
	static const char *pCertFile = port_reg.SSL_cert.c_str();
	string wspacket;
	string sqpacket;
	string response;
	int found;
	struct Post_UserProfile usrprof;
	pugi::xml_document doc;
	//FileLogger log("postconn.log");
	FileLogger log(port_reg.postconn_log_path);	
	//***** response struct vars
	int respcode;
	string acctnum;

  	curl = curl_easy_init();
  	if(curl==NULL) {
		log.logError("Error initializing CURL <txid:"+txid+">");
		exit(0);
	}
	else{
    		//curl_easy_setopt(curl, CURLOPT_URL, "https://10.10.202.25/axis2/services/TestPG");
		curl_easy_setopt(curl, CURLOPT_URL,port_reg.pg_endpoint.c_str());
		curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 0); 
 		curl_easy_setopt(curl,CURLOPT_CAINFO,pCertFile);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    		/* Now specify the POST data */
    		curl_slist_free_all(headerlist);
    		headerlist = curl_slist_append(headerlist, "Content-Type: text/xml");
		//curl_easy_setopt(curl, CURLOPT_CAPATH, "/home/cat/libcurl");
   		curl_easy_setopt(curl, CURLOPT_POST, 1);
   		curl_easy_setopt(curl, CURLOPT_HEADER, 1);
   		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		//wspacket="<?xml version='1.0' encoding='utf-8'?>";
		//wspacket+="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		//wspacket+="<soapenv:Body><ns1:IdotAccountQuery xmlns:ns1=\"http://paymentGateway.celcom.com\">";
		//wspacket+="<ns1:login>idotTv</ns1:login><ns1:pass>idotTv</ns1:pass>";
		//wspacket+="<ns1:acctNum>0192435317</ns1:acctNum><ns1:svcNum></ns1:svcNum>";
		//wspacket+="<ns1:acctNum>44084597</ns1:acctNum>";
		//wspacket+="</ns1:AccountQuery></soapenv:Body></soapenv:Envelope>";
		//cout<<"wspacket:"<<wspacket<<endl;
		//curl_easy_setopt(curl, CURLOPT_POSTFIELDS,wspacket.c_str());

		sqpacket="<?xml version='1.0' encoding='utf-8'?>";
        	sqpacket+="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
        	sqpacket+="<soapenv:Body><ns1:SimpleAccountQuery xmlns:ns1=\"http://paymentGateway.celcom.com\">";
	        sqpacket+="<ns1:login>"+port_reg.pg_login+"</ns1:login><ns1:pass>"+port_reg.pg_pwd+"</ns1:pass>";
	        sqpacket+="<ns1:acctNum></ns1:acctNum><ns1:svcNum>"+svcnum+"</ns1:svcNum>";
        	sqpacket+="</ns1:SimpleAccountQuery></soapenv:Body></soapenv:Envelope>";
		log.logNotice("Get Accnum packet(simple account query):"+sqpacket+":  <txid:"+txid+">  ");
        	curl_easy_setopt(curl, CURLOPT_POSTFIELDS,sqpacket.c_str());

    		/* Perform the request, res will get the return code */
		//cout<<"about to send rq"<<endl;
    		res = curl_easy_perform(curl);

		found=response.find("<?xml");
                if (found==string::npos){
			log.logError("Error in response XML. XML tag not found. Response packet:"+response+":  <txid:"+txid+">  ");
			exit(1);
                }
                else{
                        //cout<<"found at:"<<found<<endl;
                        response=response.substr(found,response.length()-found);
                }

		log.logNotice("Response getAccnum (SimpleAccountQuery):"+response+":  <txid:"+txid+"> ");

		pugi::xml_parse_result result = doc.load_buffer(response.c_str(), response.length());
		//std::cout << "Load result: " << result.description() << endl;

		pugi::xml_node tools = doc.child("soapenv:Envelope").child("soapenv:Body").child("ns1:SimpleAccountResponse");

	        for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
    		{
        		//std::cout << "Tool:" << tool.name()<<":  value::"<<tool.child_value()<<endl;
			if(strcmp(tool.name(),"ns1:retCode")==0){
				respcode=atoi(tool.child_value());
				log.logNotice("Return code (SimpleAccountQuery):"+IntToStr(respcode)+":  <txid:"+txid+">");
				if(respcode==103){
						
				}
				else if(respcode!=0){
					log.logError("Error Return code (SimpleAccountQuery):"+IntToStr(respcode)+": this operation fail <txid:"+txid+">");
                                	return "NULL";
				}
			}

			if(strcmp(tool.name(),"ns1:account")==0){
				if(strcmp(tool.child("ns1:active").child_value(),"true")==0){	
					//cout<<"acct:"<<tool.child("ns1:acctNum").child_value()<<endl;
			                //cout<<"svcnum:"<<tool.child("ns1:svcNum").child_value()<<endl;
			                //cout<<"name:"<<tool.child("ns1:name").child_value()<<endl;	
					acctnum=buffToStr((char *)tool.child("ns1:acctNum").child_value());
					log.logNotice("Acctnum:"+acctnum+": <txid:"+txid+">");
					break;
				}
			}
				
		}			

		curl_easy_cleanup(curl);

		return acctnum;

	}
}


string Post_Conn::getStatementDate(string phone_no,PortReg port_reg,string txid,string accnum){

	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist = NULL;
	//static const char *pCertFile = "/home/cat/libcurl/certdata.pem";
	static const char *pCertFile = port_reg.SSL_cert.c_str();
	string wspacket;
	string sqpacket;
	string response;
	int found;
	struct Post_UserProfile usrprof;
	pugi::xml_document doc;
	string retval;
	//FileLogger log("postconn.log");
	FileLogger log(port_reg.postconn_log_path);

	//*** struct
	int respcode;
	

	curl = curl_easy_init();
  	if(curl==NULL) {
		log.logError("Error initializing CURL <txid:"+txid+">");
                exit(0);	
	}
	else{
    		//curl_easy_setopt(curl, CURLOPT_URL, "https://10.10.202.25/axis2/services/TestPG");
		curl_easy_setopt(curl, CURLOPT_URL,port_reg.pg_endpoint.c_str());
		curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 0); 
 		curl_easy_setopt(curl,CURLOPT_CAINFO,pCertFile);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    		/* Now specify the POST data */
    		curl_slist_free_all(headerlist);
    		headerlist = curl_slist_append(headerlist, "Content-Type: text/xml");
		//curl_easy_setopt(curl, CURLOPT_CAPATH, "/home/cat/libcurl");
   		curl_easy_setopt(curl, CURLOPT_POST, 1);
   		curl_easy_setopt(curl, CURLOPT_HEADER, 1);
   		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		
		wspacket="<?xml version='1.0' encoding='utf-8'?>";
		wspacket+="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		wspacket+="<soapenv:Body><ns1:AccountQuery xmlns:ns1=\"http://paymentGateway.celcom.com\">";
		wspacket+="<ns1:login>"+port_reg.pg_login+"</ns1:login><ns1:pass>"+port_reg.pg_pwd+"</ns1:pass>";
		wspacket+="<ns1:acctNum>"+accnum+"</ns1:acctNum>";
		wspacket+="<ns1:svcNum></ns1:svcNum>";
		wspacket+="</ns1:AccountQuery></soapenv:Body></soapenv:Envelope>";
		log.logNotice("Request Packet (AccountQuery):"+wspacket+": <txid:"+txid+"> ");	
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS,wspacket.c_str());

		
    		/* Perform the request, res will get the return code */
		//cout<<"about to send rq"<<endl;
    		res = curl_easy_perform(curl);

		found=response.find("<?xml");
                if (found==string::npos){
			log.logError("Error in response XML. XML tag not found. Response packet:"+response+":  <txid:"+txid+">  ");
			exit(1);
                }
                else{
                        //cout<<"found at:"<<found<<endl;
                        response=response.substr(found,response.length()-found);
                }
		
		log.logNotice("Response Packet (Account Query):"+response+": <txid:"+txid+"> ");

		pugi::xml_parse_result result = doc.load_buffer(response.c_str(), response.length());
                //std::cout << "Load result: " << result.description() << endl;

                pugi::xml_node tools = doc.child("soapenv:Envelope").child("soapenv:Body").child("ns1:AccountResponse");

                for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
                {
                        //std::cout << "Tool:" << tool.name()<<":  value::"<<tool.child_value()<<endl;
                        if(strcmp(tool.name(),"ns1:retCode")==0){
                                respcode=atoi(tool.child_value());
				log.logNotice("Response code (account query):"+IntToStr(respcode)+": <txid:"+txid+">");
				if(respcode!=0){
					log.logError("Error Return code (AccountQuery):"+IntToStr(respcode)+": this operation fail <txid:"+txid+">");
                                	return "NULL";
				}
                        }

                        if(strcmp(tool.name(),"ns1:account")==0){
                                if(strcmp(tool.child("ns1:active").child_value(),"true")==0){
					retval=buffToStr((char *)tool.child("ns1:statementDate").child_value());
					retval=retval.substr(0,10);
					log.logNotice("Statement Date:"+retval+": <txid:"+txid+">");
					return retval;
                                }
                        }

                }
		
		
	    /* always cleanup */
	    	curl_easy_cleanup(curl);
  	}

}

string Post_Conn::boolToString(bool val){

	if(val==true) return "yes";
	else return "no";

}

bool Post_Conn::strToBool(string val){

	if(val=="true")
		return true;
	else if(val=="false")
		return false;
	

}

struct Post_UserProfile Post_Conn::QueryProfile(string phone_no,PortReg port_reg,string txid){

	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist = NULL;
	//static const char *pCertFile = "/home/cat/libcurl/certdata.pem";
	static const char *pCertFile = port_reg.SSL_cert.c_str();
	string wspacket;
	string sqpacket;
	string response;
	int found;
	struct Post_UserProfile usrprof;
	pugi::xml_document doc;
	//FileLogger log("postconn.log");
	FileLogger log(port_reg.postconn_log_path);
	string last_invoice_date;
	int roaming;
	EMA_Conn ema;

	//**** Normalizing phone no, eliminating 6; country code
	phone_no=phone_no.substr(1,phone_no.length()-1);

	string accnum=getAccNum(phone_no,txid,port_reg);

	if(accnum=="NULL" || accnum==""){
		usrprof.queryStatus=QRY_STATUS_FAIL;
		return usrprof;	
	}
	
	//*** struct
	int respcode;

	/**** Simulation overriden vars
        usrprof.roaming=0;
        usrprof.has_1p5=false;
        usrprof.has_act_tcl=false;
	usrprof.has_LA=false;
        usrprof.userLang=2;
        usrprof.credit_limit=5000;
        usrprof.total_due=0;

        usrprof.last_invoice_date="2011-07-01";
        usrprof.current_due=1000;
        usrprof.unbill=1000;
        usrprof.vip_code=3;
        //usrprof.userStatus=USR_STATUS_ACTIVE;
	usrprof.userStatus=4;
        usrprof.los=3;
	usrprof.act_no="105098198";
	usrprof.userType=1;
	usrprof.activationDate="2011-01-01";
	usrprof.queryStatus=0;
	
        return  usrprof;

        //**** END OF - Simulation overriden vars */


	

	curl = curl_easy_init();
  	if(curl==NULL) {
		log.logError("Error INIT CURL  <txid:"+txid+">  ");
		exit(1);
	}
	else{
    		//curl_easy_setopt(curl, CURLOPT_URL, "https://10.10.202.25/axis2/services/TestPG");
		curl_easy_setopt(curl, CURLOPT_URL,port_reg.pg_endpoint.c_str());
		curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 0); 
 		curl_easy_setopt(curl,CURLOPT_CAINFO,pCertFile);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    		/* Now specify the POST data */
    		curl_slist_free_all(headerlist);
    		headerlist = curl_slist_append(headerlist, "Content-Type: text/xml");
		//curl_easy_setopt(curl, CURLOPT_CAPATH, "/home/cat/libcurl");
   		curl_easy_setopt(curl, CURLOPT_POST, 1);
   		curl_easy_setopt(curl, CURLOPT_HEADER, 1);
   		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		
		wspacket="<?xml version='1.0' encoding='utf-8'?>";
		wspacket+="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		wspacket+="<soapenv:Body><ns1:IdotAccountQuery xmlns:ns1=\"http://paymentGateway.celcom.com\">";
		wspacket+="<ns1:login>"+port_reg.pg_login+"</ns1:login><ns1:pass>"+port_reg.pg_pwd+"</ns1:pass>";
		wspacket+="<ns1:acctNum>"+accnum+"</ns1:acctNum>";
		//wspacket+="<ns1:svcNum></ns1:svcNum>";
		wspacket+="</ns1:IdotAccountQuery></soapenv:Body></soapenv:Envelope>";
		log.logNotice("Request packet (IdotAccountQuery):"+wspacket+": <txid:"+txid+">");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS,wspacket.c_str());

		
    		/* Perform the request, res will get the return code */
		//cout<<"about to send rq"<<endl;
    		res = curl_easy_perform(curl);

		found=response.find("<?xml");
                if (found==string::npos){
			log.logError("Error in response XML. XML tag not found. Response packet:"+response+":  <txid:"+txid+">  ");
			exit(1);
                }
                else{
                        //cout<<"found at:"<<found<<endl;
                        response=response.substr(found,response.length()-found);
                }
		
		log.logNotice("Response packet (IdotAccountQuery):"+response+": <txid:"+txid+">");

		pugi::xml_parse_result result = doc.load_buffer(response.c_str(), response.length());
                //std::cout << "Load result: " << result.description() << endl;

                pugi::xml_node tools = doc.child("soapenv:Envelope").child("soapenv:Body").child("ns1:IdotAccountResponse");

                for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
                {
                        //std::cout << "Tool:" << tool.name()<<":  value::"<<tool.child_value()<<endl;

                        if(strcmp(tool.name(),"ns1:retCode")==0){ // ORI
                                respcode=atoi(tool.child_value());
				log.logNotice("Return code (IdotAccountQuery):"+IntToStr(respcode)+": <txid:"+txid+">");
				if(respcode!=0){
					usrprof.queryStatus=QRY_STATUS_FAIL;
					log.logError("Error Return code (IdotAccountQuery):"+IntToStr(respcode)+": this operation fail <txid:"+txid+">");
					usrprof.queryStatus=QRY_STATUS_FAIL;
		                        return usrprof;
				}
			}

                        if(strcmp(tool.name(),"ns1:account")==0){ //ori
                                if(strcmp(tool.child("ns1:active").child_value(),"true")==0){ // ori
                                        //cout<<"creditlimit:"<<tool.child("ns1:creditLimit").child_value()<<endl;
					//cout<<"overdue:"<<tool.child("ns1:overdue").child_value()<<endl;
					//cout<<"unbill:"<<tool.child("ns1:unbilled").child_value()<<endl;
					//cout<<"acct num:"<<tool.child("ns1:acctNum").child_value()<<endl;
					//cout<<"status:"<<tool.child("ns1:status").child_value()<<endl;
					//cout<<"LOS:"<<tool.child("ns1:lengthOfStay").child_value()<<endl;
					usrprof.credit_limit=atof(tool.child("ns1:creditLimit").child_value());		
					usrprof.total_due=atof(tool.child("ns1:overdue").child_value());
					usrprof.current_due=atof(tool.child("ns1:totalDue").child_value());
					usrprof.unbill=atof(tool.child("ns1:unbilled").child_value());
					usrprof.vip_code=atoi(tool.child("ns1:vipCode").child_value());
					usrprof.has_1p5=strToBool(tool.child("ns1:hasOnePlusFive").child_value());
					usrprof.has_act_tcl=strToBool(tool.child("ns1:hasActiveTcl").child_value());
					usrprof.has_LA=strToBool(tool.child("ns1:hasLargeAccount").child_value());
					usrprof.userLang=atoi(tool.child("ns1:billLanguage").child_value());

					//*** toggle lang val
					if(usrprof.userLang==1) usrprof.userLang=2;
					else if(usrprof.userLang==2) usrprof.userLang=1;

					last_invoice_date=getStatementDate(phone_no,port_reg,txid,accnum);
					if(last_invoice_date=="NULL"){
						usrprof.queryStatus=QRY_STATUS_FAIL;
						return usrprof;
					}
					else if(last_invoice_date==""){
						last_invoice_date="0000-00-00";
					}
					usrprof.last_invoice_date=last_invoice_date;
					usrprof.act_no=buffToStr((char *) tool.child("ns1:acctNum").child_value());
					usrprof.los=atoi(tool.child("ns1:lengthOfStay").child_value());
					usrprof.queryStatus=0;
					usrprof.userType=USR_TYPE_POSTPAID;
					log.logNotice("Credit limit:"+floatToStr(usrprof.credit_limit)+": <txid:"+txid+">");
					log.logNotice("Overdue:"+floatToStr(usrprof.total_due)+": <txid:"+txid+">");
					log.logNotice("Unbill:"+floatToStr(usrprof.unbill)+": <txid:"+txid+">");
					log.logNotice("Acct num:"+usrprof.act_no+": <txid:"+txid+">");
					log.logNotice("LOS:"+IntToStr(usrprof.los)+": <txid:"+txid+">");
					log.logNotice("Last invoice date:"+usrprof.last_invoice_date+": <txid:"+txid+">");
					log.logNotice("Totaldue:"+floatToStr(usrprof.current_due)+": <txid:"+txid+">");
					log.logNotice("VIP Code:"+IntToStr(usrprof.vip_code)+": <txid:"+txid+">");				
					log.logNotice("1+5:"+boolToString(usrprof.has_1p5)+": <txid:"+txid+">");
					log.logNotice("Active TCL:"+boolToString(usrprof.has_act_tcl)+": <txid:"+txid+">");	
					log.logNotice("Large Account:"+boolToString(usrprof.has_LA)+": <txid:"+txid+">");
					log.logNotice("Language:"+IntToStr(usrprof.userLang)+": <txid:"+txid+">");
	
					if(strcmp(tool.child("ns1:status").child_value(),"1")==0)
						usrprof.userStatus=USR_STATUS_ACTIVE;
					else
						usrprof.userStatus=USR_STATUS_TERMINATED;

					log.logNotice("Status:"+IntToStr(usrprof.userStatus)+": <txid:"+txid+">");
					
											
						
					roaming=ema.getroaming("6"+phone_no,txid,port_reg);
					log.logNotice("Roaming status:"+IntToStr(roaming)+": <txid:"+txid+">");
        				if(roaming==-1){
      						usrprof.queryStatus=QRY_STATUS_FAIL;
	        				return usrprof;
        				}
					usrprof.roaming=roaming;
					


                                        break;
                                }
                        }

                }
		
		
	    /* always cleanup */
	    	curl_easy_cleanup(curl);
  	}	

	/**** Simulation overriden vars
	
	log.logNotice("*****************************************");
	log.logNotice("Simulation status : TRUE");		
	usrprof.roaming=1;
	usrprof.has_1p5=false;
	usrprof.has_act_tcl=false;
	usrprof.has_LA=false;
	usrprof.userLang=2;
	usrprof.credit_limit=15000;
        usrprof.total_due=0;

	usrprof.last_invoice_date="2011-07-01";
        usrprof.current_due=1000;
        usrprof.unbill=1000;
        usrprof.vip_code=6;
	usrprof.userStatus=USR_STATUS_ACTIVE;
	usrprof.los=3;


	 				log.logNotice("Credit limit:"+floatToStr(usrprof.credit_limit)+": <txid:"+txid+">");
                                        log.logNotice("Overdue:"+floatToStr(usrprof.total_due)+": <txid:"+txid+">");
                                        log.logNotice("Unbill:"+floatToStr(usrprof.unbill)+": <txid:"+txid+">");
                                        log.logNotice("Acct num:"+usrprof.act_no+": <txid:"+txid+">");
                                        log.logNotice("LOS:"+IntToStr(usrprof.los)+": <txid:"+txid+">");
                                        log.logNotice("Last invoice date:"+usrprof.last_invoice_date+": <txid:"+txid+">");
                                        log.logNotice("Totaldue:"+floatToStr(usrprof.current_due)+": <txid:"+txid+">");
                                        log.logNotice("VIP Code:"+IntToStr(usrprof.vip_code)+": <txid:"+txid+">");
                                        log.logNotice("1+5:"+boolToString(usrprof.has_1p5)+": <txid:"+txid+">");
                                        log.logNotice("Active TCL:"+boolToString(usrprof.has_act_tcl)+": <txid:"+txid+">");
                                        log.logNotice("Large Account:"+boolToString(usrprof.has_LA)+": <txid:"+txid+">");
                                        log.logNotice("Language:"+IntToStr(usrprof.userLang)+": <txid:"+txid+">");

	log.logNotice("*****************************************");	
         				
	//**** END OF - Simulation overriden vars */
	//usrprof.roaming=0; //*** simulation
	
	log.logDebug("Roaming Re-Status:"+IntToStr(usrprof.roaming)+": <txid:"+txid+">");	

	return	usrprof;
}

struct Credit_Op Post_Conn::DoCredit(string a_no,string b_no,string txid,float amt,int validity,int b_pform,PortReg port_reg,string tx_type,float b_charging){

	int sockID;
	int mSvrPort=8990;
	string ip_addrs="10.99.3.15";
	char *mSvrAddr;
	int returnStatus;
	struct sockaddr_in svrAddr;
	char writeBuff[4112];
	char readBuff[4112];
	string reqPacket;
	string resPacket;
	NginCoder mpcoder;
	MP_Transfer_Status tran_status;
	struct Credit_Op credit_status;
	//FileLogger log("postconn.log");
	FileLogger log(port_reg.mp_log_path);
	int next_prof;
	int retry_count=0;
	bool isMaxRetry=false;
	bool isConnected=false;

	//**** seeding random engine
        srand((unsigned)time(0)+(unsigned)HexStrToInt(txid.substr(8,8).c_str()));
        //**** Get random profile
        next_prof=(rand()%port_reg.mp_conn_prof_count)+1;
        //*** Adjust for array subscript
        next_prof--;
        log.logDebug("prof:"+IntToString(next_prof));

        sockID=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(sockID<0){
                        log.logError("Error getting a socket FD");
			credit_status.op_status=-1;
                        return credit_status;
        }

        //**** Get server port based on port index
        mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
        //**** Assign IP IN addrs
        mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
        log.logDebug("NGIN IP:"+buffToStr(mSvrAddr));

	//**** Loop until connected or reaching max retry count
        while(!isConnected && !isMaxRetry){

                //***** set destination address struct
                bzero(&svrAddr,sizeof(svrAddr));
                svrAddr.sin_family=AF_INET;
                svrAddr.sin_addr.s_addr=inet_addr(mSvrAddr);
                svrAddr.sin_port=htons(mSvrPort);

                //***** Let's connect to IN
                log.logNotice("Connecting to "+ port_reg.mp_conn_prof[next_prof].ip_addrs +", port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+" [txid:"+txid+"]");
                returnStatus=connect(sockID,(struct sockaddr *) &svrAddr, sizeof(svrAddr));

                // ** Fail to connect to IN
                if(returnStatus<0){
                        log.logError("Error: cannot connect to NGIN [txid:"+txid+"]");
                        log.logWarn("Cannot connect to "+ port_reg.mp_conn_prof[next_prof].ip_addrs + " port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

                                        //**** Make some delay if need be
                                        usleep(port_reg.mp_retry_delay);

                                        //**** Inc retry count
                                        retry_count++;

                                        //**** Fail Over: get another conn profile to retry
                                        if(next_prof==port_reg.mp_conn_prof_count){
                                                next_prof=0;
                                                mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
                                                mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
                                        }
                                        else{
                                                next_prof++;
                                                mSvrPort=port_reg.mp_conn_prof[next_prof].port_no;
                                                mSvrAddr=(char *) port_reg.mp_conn_prof[next_prof].ip_addrs.c_str();
                                        }

					//**** Check if reach max retry count
                                        if(retry_count>=port_reg.mp_max_retry_count)
                                                isMaxRetry=true;        //**then raise the flag

                                        if(!isMaxRetry)
                                                log.logNotice("Retrying to "+port_reg.mp_conn_prof[next_prof].ip_addrs+" via port "+IntToString(port_reg.mp_conn_prof[next_prof].port_no)+"[txid:"+txid+"]");

                                         }
                        else{
                                //**** Raise the flag once we get connected
                                isConnected=true;
                        }

        } //*** while


	//**** Max Retry when connecting has reached
        if(isMaxRetry){
                log.logWarn("Max retry count reached, retry aborted [txid:"+txid+"]");
                close(sockID);
		credit_status.op_status=-1;
                return credit_status;
        }

        log.logNotice("Connected to NGIN [txid:"+txid+"]");

	//*** Temporary waive or CAR charging on requestor
        if(tx_type=="CAR")
                b_charging=0;
	
	
	//************** CONNECTION FOR CREDIT OPERATION *************************************
	reqPacket=mpcoder.getCreditPacketPostpaid(b_no,amt-b_charging,validity,txid,b_pform,a_no,tx_type);
	strcpy(writeBuff,reqPacket.c_str());

	log.logNotice("Request packet to MP:"+reqPacket+": <txid:"+txid+">");
	
	returnStatus=send(sockID,writeBuff,reqPacket.length()+1,0);
	if(returnStatus<0){
		log.logError("Fail to send to MP <txid:"+txid+">");
		close(sockID);
		credit_status.op_status=-1;
                return credit_status;
	}	

	returnStatus=recv(sockID,readBuff,sizeof(readBuff),0);
	readBuff[returnStatus]='\0';
	resPacket=buffToStr(readBuff);	

	if(returnStatus<0){
		cout<<"fail to send to MP"<<endl;
		close(sockID);
		credit_status.op_status=-1;
                return credit_status;
	}
	log.logNotice("Response packet from MP:"+resPacket+": <txid:"+txid+">");	

	tran_status=mpcoder.LoadTransferStatus(resPacket);	

	if(tran_status.transfer_status==1){
		credit_status.op_status=0;
		credit_status.new_bal=tran_status.new_balance;
		credit_status.new_expdate=tran_status.new_expiry_date;
		log.logNotice("New bal:"+floatToStr(credit_status.new_bal)+": <txid:"+txid+">");
		log.logNotice("New exp date:"+credit_status.new_expdate+": <txid:"+txid+">");
	}
	else{
		close(sockID);
		credit_status.op_status=-1;
                return credit_status;
	}

	close(sockID);

	return credit_status;

}



struct Post_TransferResult Post_Conn::Transfer(string a_no,string b_no,float amt,int validity,float a_charging,float b_charging,string txid,int a_pform,int b_pform,PortReg port_reg,string tx_type,string act_no){

	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist = NULL;
	//static const char *pCertFile = "/home/cat/libcurl/certdata.pem";
	static const char *pCertFile = port_reg.SSL_cert.c_str();
	string wspacket;
	string sqpacket;
	string response;
	int found;
	struct Post_TransferResult trans_result;
	pugi::xml_document doc;
	struct Credit_Op credit_status;
	//FileLogger log("postconn.log");
	FileLogger log(port_reg.postconn_log_path);
	float amt_debit;
	string isCAR;

	amt_debit=amt; //** correction to minus fee

	//*** is prepaid initiate
        if(tx_type=="CAR")
                isCAR="true";
        else if(tx_type=="CAT")
                isCAR="false";

	//**** Normalizing phone no, eliminating 6; country code
	a_no=a_no.substr(1,a_no.length()-1);
	b_no=b_no.substr(1,b_no.length()-1);
	
	//*** struct
	int respcode;

	credit_status=DoCredit(a_no,b_no,txid,amt,validity,b_pform,port_reg,tx_type, b_charging);
	if(credit_status.op_status==-1){
		trans_result.transferStatus=TRANSFER_STATUS_CREDIT_FAIL;
		return trans_result;	
	}

	curl = curl_easy_init();
  	if(curl==NULL) {
		log.logError("Error init CURL <txid:"+txid+">");
		exit(0);
	}
	else{
    		//curl_easy_setopt(curl, CURLOPT_URL, "https://10.10.202.25/axis2/services/TestPG");
		curl_easy_setopt(curl, CURLOPT_URL,port_reg.pg_endpoint.c_str());
		curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 0); 
 		curl_easy_setopt(curl,CURLOPT_CAINFO,pCertFile);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    		/* Now specify the POST data */
    		curl_slist_free_all(headerlist);
    		headerlist = curl_slist_append(headerlist, "Content-Type: text/xml");
		//curl_easy_setopt(curl, CURLOPT_CAPATH, "/home/cat/libcurl");
   		curl_easy_setopt(curl, CURLOPT_POST, 1);
   		curl_easy_setopt(curl, CURLOPT_HEADER, 1);
   		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		
		wspacket="<?xml version='1.0' encoding='utf-8'?>";
		wspacket+="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		wspacket+="<soapenv:Body><ns1:IdotPostpaid2PrepaidFundTransfer xmlns:ns1=\"http://paymentGateway.celcom.com\">";
		wspacket+="<ns1:login>"+port_reg.pg_login+"</ns1:login><ns1:pass>"+port_reg.pg_pwd+"</ns1:pass>";
		wspacket+="<ns1:clientRefId>"+txid+"</ns1:clientRefId>";
		wspacket+="<ns1:acctNum>"+act_no+"</ns1:acctNum>";
		wspacket+="<ns1:prepaidSvcNum>"+b_no+"</ns1:prepaidSvcNum>";
		wspacket+="<ns1:amount>"+IntToStr((int)(amt_debit*100))+"</ns1:amount>";
		wspacket+="<ns1:transDate>"+currentTimestamp()+"</ns1:transDate>";
		wspacket+="<ns1:prepaidInitiate>"+isCAR+"</ns1:prepaidInitiate>";
		//wspacket+="<ns1:transDate>2009-10-19T14:10:59.719+08:00</ns1:transDate>";
		wspacket+="</ns1:IdotPostpaid2PrepaidFundTransfer></soapenv:Body></soapenv:Envelope>";
		log.logNotice("Request Packet (Postpretransfer):"+wspacket+": <txid:"+txid+">");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS,wspacket.c_str());

		
    		/* Perform the request, res will get the return code */
		//cout<<"about to send rq"<<endl;
    		res = curl_easy_perform(curl);

		found=response.find("<?xml");
                if (found==string::npos){
			log.logError("Error in response XML. XML tag not found. Response packet:"+response+":  <txid:"+txid+">  ");
			exit(1);
                }
                else{
                        //cout<<"found at:"<<found<<endl;
                        response=response.substr(found,response.length()-found);
                }
		
		log.logNotice("Response packet (PostPre transfer):"+response+": <txid:"+txid+">");

		pugi::xml_parse_result result = doc.load_buffer(response.c_str(), response.length());
                //std::cout << "Load result: " << result.description() << endl;

                pugi::xml_node tools = doc.child("soapenv:Envelope").child("soapenv:Body").child("ns1:PaymentResponse");

                for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
                {
                        //std::cout << "Tool:" << tool.name()<<":  value::"<<tool.child_value()<<endl;
                        if(strcmp(tool.name(),"ns1:retCode")==0){
                                respcode=atoi(tool.child_value());
				log.logNotice("Return code:"+IntToStr(respcode)+": <txid:"+txid+">");
				if(respcode==106 || respcode==0){
					trans_result.transferStatus=0;
					trans_result.bExpiryDate=credit_status.new_expdate;
					trans_result.bBalance=credit_status.new_bal;
				}
				else{
					trans_result.transferStatus=TRANSFER_STATUS_DEBIT_FAIL;
					return trans_result;
				}
                        }

                        //cout<<"creditlimit:"<<tool.child("ns1:creditLimit").child_value()<<endl;

                }
		
		
	    /* always cleanup */
	    	curl_easy_cleanup(curl);
  	}	

	return trans_result;
	//return	usrprof;


}




string Post_Conn::floatToStr(float val){

	char buff[20];

	sprintf(buff,"%f", val);
	return buffToStr(buff);

}

string Post_Conn::currentTimestamp(void){

	char timestamp[25];
	string currentdatetime;
	time_t mytime;
	struct tm *mytm;
	mytime=time(NULL);
	mytm=localtime(&mytime);
	//2009-10-19T14:10:59.719+08:00
	strftime(timestamp,sizeof timestamp,"%Y-%m-%dT%H:%M:%S",mytm);

	long milliseconds;
  	char stringMillisecond[100];
  	struct timeval tv;
  	gettimeofday (&tv, NULL);
  	milliseconds = tv.tv_usec / 1000;
  	strcat(timestamp,".");
  	sprintf(stringMillisecond,"%d",milliseconds);
	strcat(timestamp,stringMillisecond);
	strcat(timestamp,"+08:00");
	currentdatetime = (string)timestamp;
	return currentdatetime;


}


string Post_Conn::IntToStr ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


