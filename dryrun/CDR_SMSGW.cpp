#include <iostream>
#include <fstream>

#include "CDR_SMSGW.h"

using namespace std;

int CDR_SMSGW::insertInATS(string txid, string gw_ts, string oadc, string adc, string msg, string flag){

	ofstream file;
	const char *cdr_file="/home/cat/src/smpp_tx/InATS.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "\n";
	file.close();
	return 0;	

}

int CDR_SMSGW::insertOutATS(string txid, string gw_ts, string oadc, string adc, string msg, string flag,string charging){

	ofstream file;
	const char *cdr_file="/home/cat/src/smpp_tx/OutATS.tx";
	file.open(cdr_file,ios::out | ios::app | ios::binary);		
	file << txid << "|" << gw_ts << "|" << oadc << "|" << adc << "|" << msg << "|" << flag << "|" << charging << "\n";
	file.close();
	return 0;	

}


