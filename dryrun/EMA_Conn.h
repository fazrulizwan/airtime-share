#ifndef EMA_CONN_H
#define EMA_CONN_H

#include <iostream>

#include "sharedmem.h"

using namespace std;

class EMA_Conn{

	public:
	int getroaming(string,string,PortReg);
	string buffToStr(char *buff);
	int getBytes (char *byteArray, string plainText);

};


#endif


