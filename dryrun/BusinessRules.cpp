#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "BusinessRules.h"
#include "generalfunction.h"

using std::string;

namespace ats {
	bool BusinessRules::ReadConfig() {
		string line;
		string name;
		string value;
		int posEqual;
		int msg_index;
		ifstream fileConfig ("businessrules.config");
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;

				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trimString(name);
				value = line.substr(posEqual+1);
				trimString(value);

				if (line.substr(0,3)=="ATS") {
					posEqual=line.find('=');
					name = line.substr(8,1);
					trimString(name);
					msg_index = stringToInt(name);
					value = line.substr(posEqual+1);
					trimString(value);
					if (msg_index<=10)
						ATSoadc[msg_index]=value;
					else
						ATSoadc[msg_index]=value;
					continue;
				} else if (name=="shr_len") {
					shr_len = stringToInt(value);
					continue;
				} else if (name=="ats_pfx") {
					ats_pfx = value;
					continue;
				} else if (name=="hlp_ptx") {
					hlp_ptx = value;
					continue;
                                } else if (name=="hlp_ptx_8") {
                                        hlp_ptx_8 = value;
                                        continue;
				} else if (name=="ats_ptx") {
					ats_ptx = value;
					continue;
				} else if (name=="atr_pfx") {
					atr_pfx = value;
					continue;
				} else if (name=="atr_min_req") {
					atr_min_req = stringToInt(value);
					continue;
				} else if (name=="atr_max_req") {
					atr_max_req = stringToInt(value);
					continue;
				}else if (name=="atr_max_req_ptd") {
                                        atr_max_req_ptd = stringToInt(value);
                                        continue;
				}else if (name=="atr_min_req_ptd") {
                                        atr_min_req_ptd = stringToInt(value);
                                        continue;
                                }else if (name=="atr_tmt") {
					atr_tmt = stringToInt(value);
					continue;
				} else if (name=="ats_tmt") {
					ats_tmt = stringToInt(value);
					continue;
				} else if (name=="max_rcv_amt") {
					max_rcv_amt = stringToInt(value);
					continue;
				} else if (name=="max_trf_dly_reg") {
					max_trf_dly_reg = stringToInt(value);
					continue;
				} else if (name=="max_trf_dly_unr") {
					max_trf_dly_unr = stringToInt(value);
					continue;
				} else if (name=="max_trf_amt_uni") {
					max_trf_amt_uni = stringToInt(value);
					continue;
				} else if (name=="min_trf_prd") {
					min_trf_prd = stringToInt(value);
					continue;
				} else if (name=="max_trf_prd") {
					max_trf_prd = stringToInt(value);
					continue;
				} else if (name=="min_trf_ptd") {
					min_trf_ptd = stringToInt(value);
					continue;
				} else if (name=="max_trf_ptd") {
					max_trf_ptd = stringToInt(value);
					continue;
				} else if (name=="max_suc_txn_reg") {
					max_suc_txn_reg = stringToInt(value);
					continue;
				} else if (name=="max_suc_txn_unr") {
					max_suc_txn_unr = stringToInt(value);
					continue;
				} else if (name=="min_bal_aft_trf") {
					min_bal_aft_trf = stringToFloat(value);
					continue;
				} else if (name=="atr_sms_chg_prd_ano") {
					atr_sms_chg_prd_ano = stringToFloat(value);
					continue;
				} else if (name=="atr_sms_chg_prd_bno") {
					atr_sms_chg_prd_bno = stringToFloat(value);
					continue;
				} else if (name=="ats_sms_chg_prd_ano") {
					ats_sms_chg_prd_ano = stringToFloat(value);
					continue;
				} else if (name=="ats_sms_chg_prd_bno") {
					ats_sms_chg_prd_bno = stringToFloat(value);
					continue;
				} else if (name=="grc_prd") {
					grc_prd = stringToInt(value);
					continue;
				} else if (name=="chg_pwd_chg") {
					chg_pwd_chg = stringToInt(value);
					continue;
				} else if (name=="mth_lim_ptd") {
					mth_lim_ptd = stringToInt(value);
					continue;
                                } else if (name=="los_ptd") {
                                        los_ptd = stringToInt(value);
                                        continue;		
				}

			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			return true;
		} else {
			//cout << "Cannot open config file!";
			return false;
		}
	}
}

