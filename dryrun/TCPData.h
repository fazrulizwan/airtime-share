#include <iostream>

using namespace std;

class TCPData{
        public:
        TCPData();
        int getRetryCount(void);
        void setRetryCount(int);
        string getOADC(void);
        void setOADC(string);
	string getADC(void);
        void setADC(string);
	string getMSG(void);
        void setMSG(string);
	string getTXID(void);
        void setTXID(string);
	int getMTIndex(void);
        void setMTIndex(int);

        private:
        int m_retry_count;
	string m_oadc;
	string m_adc;
	string m_msg;
	string m_txid;
	int m_mt_index;
};


