/*********************************************************************
	HWAWEI IN MML (v1.00) Encoder/Decoder Library
	By Fazrul Izwan Hassan
	Idottv Sdn Bhd (Sedania Media Group)

	HWA Coder ver1.0
	(c) 2007-2008 Idot TV Sdn Bhd

	"Truth is not to be told, but to be discovered" - me

*********************************************************************/

#include <iostream>
#include <sstream>
#include <math.h>

#include "HWA_Coder.h"

using namespace std;

string HWACoder::getLoginPacket(unsigned long int session_id,unsigned long int tx_id,string usr_name,string pwd){

	string hex_retval;	//in hexdump
	string hex_operative;
	string operative; //in plain text
	int msg_len;
	string hex_msg_len;
	string hex_ver_no=AsciiToHexDump("1.00",false);
	string hex_term_id="696E7465726E616C";	//**** =internal
	string hex_serv_name="5352564d20202020"; //**** =SRVM____
	string hex_session_id=AsciiToHexDump(ULongIntToHexStr(session_id,8),false); //"3030303030303032"; //**** =00000002
	string hex_session_ctrl=AsciiToHexDump("DLGLGN",false);
	string hex_session_rsv="30303030"; //**** =0000
	string hex_tx_id=AsciiToHexDump(ULongIntToHexStr(tx_id,8),false); //"3030303030303031"; //**** =00000001
	string hex_tx_ctrl="545842454720"; //**** =TXBEG_
	string hex_tx_rsv="30303030";	//**** =0000
	string hex_checksum;

	operative="LOGIN:PSWD="+pwd+",USER="+usr_name;
	operative=trimOperative(operative);

	msg_len=56+operative.length();
	//cout << "Msg len:" << msg_len << endl;
	//cout << "Op len:" << operative.length() << endl;
	hex_msg_len=HexIntToHwaInt(intToHexStr(msg_len,2),4);
	//cout << "Hex Msg Len:" << hex_msg_len << endl;
	hex_operative=AsciiToHexDump(operative,false);

	hex_checksum=getCheckSum(hex_ver_no+hex_term_id+hex_serv_name,hex_session_id+hex_session_ctrl+hex_session_rsv,hex_tx_id+hex_tx_ctrl+hex_tx_rsv,hex_operative);

	hex_retval=START_FLAG+hex_msg_len+hex_ver_no+hex_term_id+hex_serv_name+hex_session_id+hex_session_ctrl+hex_session_rsv+hex_tx_id+hex_tx_ctrl+hex_tx_rsv+hex_operative+hex_checksum;
	//cout << "checksun:" << hex_checksum << endl;
	//cout << "retval:" << hex_retval << endl;
	return hex_retval;	

}

string HWACoder::getLogoutPacket(unsigned long int session_id,unsigned long int tx_id){

	string hex_retval;	//in hexdump
	string hex_operative;
	string operative; //in plain text
	int msg_len;
	string hex_msg_len;
	string hex_ver_no=AsciiToHexDump("1.00",false);
	string hex_term_id="696E7465726E616C";	//**** =internal
	string hex_serv_name="5352564d20202020"; //**** =SRVM____
	string hex_session_id=AsciiToHexDump(ULongIntToHexStr(session_id,8),false); //"3030303030303032"; //**** =00000002
	string hex_session_ctrl=AsciiToHexDump("DLGCON",false);
	string hex_session_rsv="30303030"; //**** =0000
	string hex_tx_id=AsciiToHexDump(ULongIntToHexStr(tx_id,8),false); //"3030303030303031"; //**** =00000001
	string hex_tx_ctrl="545842454720"; //**** =TXBEG_
	string hex_tx_rsv="30303030";	//**** =0000
	string hex_checksum;

	operative="LOGOUT:";
	operative=trimOperative(operative);

	msg_len=56+operative.length();
	//cout << "Msg len:" << msg_len << endl;
	//cout << "Op len:" << operative.length() << endl;
	hex_msg_len=HexIntToHwaInt(intToHexStr(msg_len,2),4);
	//cout << "Hex Msg Len:" << hex_msg_len << endl;
	hex_operative=AsciiToHexDump(operative,false);

	hex_checksum=getCheckSum(hex_ver_no+hex_term_id+hex_serv_name,hex_session_id+hex_session_ctrl+hex_session_rsv,hex_tx_id+hex_tx_ctrl+hex_tx_rsv,hex_operative);

	hex_retval=START_FLAG+hex_msg_len+hex_ver_no+hex_term_id+hex_serv_name+hex_session_id+hex_session_ctrl+hex_session_rsv+hex_tx_id+hex_tx_ctrl+hex_tx_rsv+hex_operative+hex_checksum;
	//cout << "checksun:" << hex_checksum << endl;
	//cout << "retval:" << hex_retval << endl;
	return hex_retval;

}


string HWACoder::getProfilePacket(unsigned long int session_id,unsigned long int tx_id,string msisdn){

	string hex_retval;	//in hexdump
	string hex_operative;
	string operative; //in plain text
	int msg_len;
	string hex_msg_len;
	string hex_ver_no=AsciiToHexDump("1.00",false);
	string hex_term_id="696E7465726E616C";	//**** =internal
	string hex_serv_name=AsciiToHexDump("PPS",false)+"2020202020"; //**** =PPS_____
	string hex_session_id=AsciiToHexDump(ULongIntToHexStr(session_id,8),false); //"3030303030303032"; //**** =00000002
	string hex_session_ctrl=AsciiToHexDump("DLGCON",false);
	string hex_session_rsv="30303030"; //**** =0000
	string hex_tx_id=AsciiToHexDump(ULongIntToHexStr(tx_id,8),false); //"3030303030303031"; //**** =00000001
	string hex_tx_ctrl="545842454720"; //**** =TXBEG_
	string hex_tx_rsv="30303030";	//**** =0000
	string hex_checksum;

	operative="DISP PPS ACNTINFO: MSISDN="+msisdn+",ATTR=BALANCE&ACCOUNTSTATE&SERVICESTART&SERVICESTOP";
	operative=trimOperative(operative);

	msg_len=56+operative.length();
	//cout << "Msg len:" << msg_len << endl;
	//cout << "Op len:" << operative.length() << endl;
	hex_msg_len=HexIntToHwaInt(intToHexStr(msg_len,2),4);
	//cout << "Hex Msg Len:" << hex_msg_len << endl;
	hex_operative=AsciiToHexDump(operative,false);

	hex_checksum=getCheckSum(hex_ver_no+hex_term_id+hex_serv_name,hex_session_id+hex_session_ctrl+hex_session_rsv,hex_tx_id+hex_tx_ctrl+hex_tx_rsv,hex_operative);

	hex_retval=START_FLAG+hex_msg_len+hex_ver_no+hex_term_id+hex_serv_name+hex_session_id+hex_session_ctrl+hex_session_rsv+hex_tx_id+hex_tx_ctrl+hex_tx_rsv+hex_operative+hex_checksum;
	//cout << "checksun:" << hex_checksum << endl;
	//cout << "retval:" << hex_retval << endl;
	return hex_retval;



}


string HWACoder::getAddPacket (unsigned long int session_id,unsigned long int tx_id,string msisdn, float amt){

	float multi_amt;
	string hex_retval;	//in hexdump
	string hex_operative;
	string operative; //in plain text
	int msg_len;
	string hex_msg_len;
	string hex_ver_no=AsciiToHexDump("1.00",false);
	string hex_term_id="696E7465726E616C";	//**** =internal
	string hex_serv_name=AsciiToHexDump("PPS",false)+"2020202020"; //**** =PPS_____
	string hex_session_id=AsciiToHexDump(ULongIntToHexStr(session_id,8),false); //"3030303030303032"; //**** =00000002
	string hex_session_ctrl=AsciiToHexDump("DLGCON",false);
	string hex_session_rsv="30303030"; //**** =0000
	string hex_tx_id=AsciiToHexDump(ULongIntToHexStr(tx_id,8),false); //"3030303030303031"; //**** =00000001
	string hex_tx_ctrl="545842454720"; //**** =TXBEG_
	string hex_tx_rsv="30303030";	//**** =0000
	string hex_checksum;

	multi_amt=amt*10000;

	cout << "amt:" << floatToIntStr(multi_amt) << endl;
	printf("amt2:%f\n",multi_amt);

	operative="MODI PPS BALANCE: MSISDN="+msisdn+",INCRMENT="+floatToIntStr(multi_amt)+",COMMENT=QuickShare Testing";
	cout << "op:" << operative << endl;
	operative=trimOperative(operative);

	msg_len=56+operative.length();
	//cout << "Msg len:" << msg_len << endl;
	//cout << "Op len:" << operative.length() << endl;
	hex_msg_len=HexIntToHwaInt(intToHexStr(msg_len,2),4);
	//cout << "Hex Msg Len:" << hex_msg_len << endl;
	hex_operative=AsciiToHexDump(operative,false);

	hex_checksum=getCheckSum(hex_ver_no+hex_term_id+hex_serv_name,hex_session_id+hex_session_ctrl+hex_session_rsv,hex_tx_id+hex_tx_ctrl+hex_tx_rsv,hex_operative);

	hex_retval=START_FLAG+hex_msg_len+hex_ver_no+hex_term_id+hex_serv_name+hex_session_id+hex_session_ctrl+hex_session_rsv+hex_tx_id+hex_tx_ctrl+hex_tx_rsv+hex_operative+hex_checksum;
	//cout << "checksun:" << hex_checksum << endl;
	//cout << "retval:" << hex_retval << endl;
	return hex_retval;



}




string HWACoder::getDeductPacket (unsigned long int session_id,unsigned long int tx_id,string msisdn, float amt){

	float multi_amt;
	string hex_retval;	//in hexdump
	string hex_operative;
	string operative; //in plain text
	int msg_len;
	string hex_msg_len;
	string hex_ver_no=AsciiToHexDump("1.00",false);
	string hex_term_id="696E7465726E616C";	//**** =internal
	string hex_serv_name=AsciiToHexDump("PPS",false)+"2020202020"; //**** =PPS_____
	string hex_session_id=AsciiToHexDump(ULongIntToHexStr(session_id,8),false); //"3030303030303032"; //**** =00000002
	string hex_session_ctrl=AsciiToHexDump("DLGCON",false);
	string hex_session_rsv="30303030"; //**** =0000
	string hex_tx_id=AsciiToHexDump(ULongIntToHexStr(tx_id,8),false); //"3030303030303031"; //**** =00000001
	string hex_tx_ctrl="545842454720"; //**** =TXBEG_
	string hex_tx_rsv="30303030";	//**** =0000
	string hex_checksum;

	multi_amt=amt*10000;

	cout << "amt:" << floatToIntStr(multi_amt) << endl;
	printf("amt2:%f\n",multi_amt);

	operative="MODI PPS BALANCE: MSISDN="+msisdn+",INCRMENT=-"+floatToIntStr(multi_amt)+",COMMENT=QuickShare Testing";
	cout << "op:" << operative << endl;
	operative=trimOperative(operative);

	msg_len=56+operative.length();
	//cout << "Msg len:" << msg_len << endl;
	//cout << "Op len:" << operative.length() << endl;
	hex_msg_len=HexIntToHwaInt(intToHexStr(msg_len,2),4);
	//cout << "Hex Msg Len:" << hex_msg_len << endl;
	hex_operative=AsciiToHexDump(operative,false);

	hex_checksum=getCheckSum(hex_ver_no+hex_term_id+hex_serv_name,hex_session_id+hex_session_ctrl+hex_session_rsv,hex_tx_id+hex_tx_ctrl+hex_tx_rsv,hex_operative);

	hex_retval=START_FLAG+hex_msg_len+hex_ver_no+hex_term_id+hex_serv_name+hex_session_id+hex_session_ctrl+hex_session_rsv+hex_tx_id+hex_tx_ctrl+hex_tx_rsv+hex_operative+hex_checksum;
	//cout << "checksun:" << hex_checksum << endl;
	//cout << "retval:" << hex_retval << endl;
	return hex_retval;



}

string HWACoder::trimOperative(string in_str){

	int len=in_str.length();

	if(len % 4 ==0)
		return in_str;

	while(len % 4 !=0){
		in_str=in_str+" ";
		len=in_str.length();
	}

	return in_str;

}


string HWACoder::intToHexStr(int n, int trim){

        char buff[9];
        sprintf(buff,"%X",n);
        string retval;

        if(strlen(buff)==1){
                buff[8]=buff[1];
                buff[7]=buff[0];
                buff[6]='0';
                buff[5]='0';
                buff[4]='0';
                buff[3]='0';
                buff[2]='0';
                buff[1]='0';
                buff[0]='0';
        }

        retval=buffToStr(buff);
        //log.logDebug("input:"+IntToString(n)+" retval:"+retval);
	//cout<< "Input:" << n << " retval:" <<  << endl;
        return retval.substr(retval.length()-trim,trim);

}

string HWACoder::ULongIntToHexStr(unsigned long int n, int trim){

        char buff[9];
        sprintf(buff,"%X",n);
        string retval;
	string temp_retval;

        if(strlen(buff)==1){
                buff[8]=buff[1];
                buff[7]=buff[0];
                buff[6]='0';
                buff[5]='0';
                buff[4]='0';
                buff[3]='0';
                buff[2]='0';
                buff[1]='0';
                buff[0]='0';
        }

        retval=buffToStr(buff);
	if(retval.length()<trim){
                for(int i=0;i<trim-retval.length();i++){
                        temp_retval=temp_retval+"0";
                }
                retval=temp_retval+retval;
        }
        //log.logDebug("input:"+IntToString(n)+" retval:"+retval);
	//cout<< "Input:" << n << " retval:" << retval.substr(retval.length()-trim,trim) << endl;
        //return retval.substr(retval.length()-trim,trim);

	return retval.substr(0,retval.length());

}


string HWACoder::AsciiToHexDump (string asciiStr, bool c_string){

        /*************************************
        AsciiToHexDump
        given ascii readable string, converts into hex string.
        if c_string is true, '00' (null) is appended to end of string
        *************************************/

        int i;
        string retval;


        for(i=0;i<asciiStr.length();i++){
                retval=retval+intToHexStr((int)asciiStr.at(i),2);
        }

        if(c_string)
                retval=retval+"00";

        return retval;

}

string HWACoder::getCheckSum(string msg_header,string session_header,string tx_header,string operative){

	string hex_dump=msg_header+session_header+tx_header+operative;
	string hex_token;
	long int lng_token;
	long int lng_result;

	//cout << "cs trest: "<< session_header << endl;

	lng_result=HexStrToInt(hex_dump.substr(0,8)); 

	for(int i=8;i<=hex_dump.length();i+=8){
		hex_token=hex_dump.substr(i,8);
		//cout << "token:" << hex_token << endl;
		lng_token=HexStrToInt(hex_token);
		//cout << "Debug:hex_token:" << hex_token << " lng_token:" << lng_token << endl;
		lng_result=lng_result ^ lng_token;	
		//cout << "after xor:" << lng_result << endl;

	}

	//cout << "Final xor:" << ~lng_result << endl;

	//cout << "cs here:" << intToHexStr(~lng_result,8) << endl;
	return AsciiToHexDump(intToHexStr(~lng_result,8),false);

}


long int HWACoder::HexStrToInt(string hexStr){

        string tempHexStr="";
        int i;

        for(i=0;i<hexStr.length();i++){
                if(hexStr.substr(i,1)!=" ")
                        tempHexStr=tempHexStr+hexStr.substr(i,1);
        }

        return strtol(tempHexStr.c_str(),NULL,16);

}

string HWACoder::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string HWACoder::HexIntToHwaInt(string hex,int bytesize){

	string hex_temp;
	string hwa_temp;
	string hwa_token;

	if(hex.length()<bytesize){
		for(int i=1;i<=bytesize-hex.length();i++){
			hex_temp=hex_temp+"0";	
		}
		hex_temp=hex_temp+hex;
	}

	for(int i=0;i<hex_temp.length();i++){
		hwa_token=hex_temp.at(i);
		hwa_temp=hwa_temp+AsciiToHexDump(hwa_token,false);
	}

	return hwa_temp;

}


int  HWACoder::getBytes (char *byteArray, string plainText){

	int i;
	string token;
	int dataLen=0;

	//*byteArray='\2';
	//byteArray++;

	for(i=0;i<plainText.length();i+=2){
		dataLen++;
		token=plainText.substr(i,2);
		*byteArray=(unsigned char)HexStrToInt(token);
		byteArray++;
		//cout << "Token:" << HexStrToInt(token) << endl;
	}
	//*byteArray=(unsigned char)13;
	//byteArray++;
	//*byteArray=(unsigned char)3;
	*byteArray='\0';
	return dataLen;
	

	//cout << "Token:" << byteArray << endl;

}


string HWACoder::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}


string HWACoder::floatToIntStr(float val){

        char buff[50];
	string str_float;
	int dec_pos;

        sprintf(buff,"%f", val);
        str_float=buffToStr(buff);
	dec_pos=str_float.find(".",0);
	cout << "dec pos" << dec_pos << endl;
	return str_float.substr(0,dec_pos);

}

int HWACoder::getReturnStatus(string return_packet){

	int ret_pos;
	int com_pos;
	string ret_status;

	ret_pos=return_packet.find("RETN=",0);
	com_pos=return_packet.find(",",ret_pos);
	ret_status=return_packet.substr(ret_pos+5,com_pos-ret_pos+5);

	return atoi(ret_status.c_str());

}



HWAUSRProfile HWACoder::loadProfile(string packet){

	string cdr_result;
	int cdr_start_pos;
	int cdr_end_pos;	
	HWAUSRProfile profile;
	float balance;

	cdr_start_pos=packet.find("RESULT=",0);
	cdr_start_pos=cdr_start_pos+8;
	cdr_end_pos=packet.find(";",cdr_start_pos);
	cdr_result=packet.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos-1);
	//cout << "Cdr res:" << cdr_result << endl;

	cdr_start_pos=0;
	cdr_end_pos=cdr_result.find("|",cdr_start_pos);
	profile.msisdn=cdr_result.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos);

	cdr_start_pos=cdr_end_pos+1;
	cdr_end_pos=cdr_result.find("|",cdr_start_pos);
	balance=atof(cdr_result.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos).c_str());
	profile.balance=fround(balance/10000,2);

	cdr_start_pos=cdr_end_pos+1;
        cdr_end_pos=cdr_result.find("|",cdr_start_pos);
        profile.status=atoi(cdr_result.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos).c_str());

	cdr_start_pos=cdr_end_pos+1;
        cdr_end_pos=cdr_result.find("|",cdr_start_pos);
        profile.activation_date=cdr_result.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos);

	cdr_start_pos=cdr_end_pos+1;
        cdr_end_pos=cdr_result.find("|",cdr_start_pos);
        profile.expiry_date=cdr_result.substr(cdr_start_pos,cdr_end_pos-cdr_start_pos);

	return profile;

}


float HWACoder::fround(float n, unsigned d)
{
	return floor(n * pow(10., d) + .5) / pow(10., d);
}

