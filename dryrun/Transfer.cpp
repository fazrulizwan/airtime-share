#include <iostream>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>

#include "generalfunction.h"
#include "Transfer.h"
#include "Subscriber.h"
#include "FileLogger.h"
#include "MysqlConnDB.h"
#include "BusinessRules.h"
#include "date.h"
#include "IN_Conn.h"
#include "Post_Conn.h"

using namespace std;

namespace ats {
          Transfer::Transfer(string txid, Subscriber senderProfile, int transferAmt, string userPin, string receiverPhone,  const BusinessRules& rules, const FileLogger& dlogger, const MTMessage& mtMsg, PortReg portIN, short int channelType, MysqlConnDB& myData, short int lang, short int op_type,short int smsc_type) {
        	 profileA = senderProfile;
        	 a_phone = profileA.getPhone();
             amt = transferAmt;
             pin = userPin;
             b_phone = receiverPhone;
             tx_id = txid;
             bizrules = rules;
             mtLibrary = mtMsg;
             logger = dlogger;
             inPort = portIN;
             channel = channelType;
             langA = profileA.getLang();
             mySQLConn = myData;
             operation_type = op_type;
	     	 smsc_id = smsc_type;
             logger.logDebug("[TX ID:"+tx_id+"] [Constructor Transfer] [A PHONE:"+a_phone+"] [PWD:"+pin+"] [mthliimit:"+floatToString(profileA.getMonthlyLimit())+"] [B PHONE:"+b_phone+"] [AMT:"+intToString(amt)+"]");
          }


          //approval by donor
          MTCollection Transfer::approveTransfer() {
        	  string foundFile;
        	  string fileTimeStamp;
        	  string smsOut1;
        	  string smsOut2;
           	  string smsOut3;
		      string dtCAT, txidCAT;
  	          int langCAT, amtCAT;

           	  //log for transaction logs
        	  string logtx = mtLibrary.dir_ats_tx+"/"+currentDate()+".tx";
        	  FileLogger txLogger (logtx);

        	  MTMsgOut.smsMT[0].receiver = a_phone;
        	  MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

        	  string fileToSearch = a_phone+"."+b_phone;
        	  //short int isExist = searchPendingFile(mtLibrary.dir_ats_pending,fileToSearch,foundFile);
        	  //logger.logDebug("[TX ID:"+tx_id+"][Transfer.approveTransfer] [Search Pending File:"+fileToSearch+" in "+mtLibrary.dir_ats_pending+" return result:"+intToString(isExist)+"]");
        	  short int isExist = searchPendingCAT(a_phone, b_phone,mySQLConn.myData,logger,langCAT,dtCAT,txidCAT,amtCAT);
	          if (isExist==0) {
        	    //60199688739.60199433244.1.20081026014218.20081026064940.1
        		  logger.logDebug("[TX ID:"+tx_id+"][Pending transaction exist]");
        		  amt = amtCAT;
			  fileTimeStamp = dtCAT;

        		  if (amt<1) {
        			  logger.logError("[TX ID:"+tx_id+"][Amount is invalid:"+intToString(amtCAT)+"]");
        			  //deleteFile(mtLibrary.dir_ats_pending+"/"+foundFile);
              		  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|4|"+currentDateTime3()+"|"+tx_id);
              		  smsOut1 = mtLibrary.getMsg(4, langA);
              		  MTMsgOut.smsMT[0].msg = smsOut1;
              		  return MTMsgOut;
        		  }

        	   	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Have Pending Request, proceed process]");

        	   	  //check timeout
 				  string currentTime = currentDateTime2();
	           	  short int timeout = bizrules.atr_tmt;
            	  int timeoutInSec = timeout * 60;
            	  struct tm tmNow, tmRequest;
            	  strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
            	  strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
            	  double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
            	  int dayDiff=0;
            	  int timeDiff=0;
            	  //cout << "Time different : " << secDiff;
            	  if (secDiff>timeoutInSec) {
            		 //request already expired, delete pending file
            		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Pending transfer already timeout, delete pending file, then proceed]");
            		 //deleteFile(mtLibrary.dir_ats_pending+"/"+foundFile);
			 		 deletePendingCAT(a_phone, b_phone,mySQLConn.myData,logger);
            		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|26|"+currentDateTime3()+"|"+tx_id);
            		 smsOut1 = find_replace(mtLibrary.getMsg(26, langA),"<min>",intToString(timeout));
            		 MTMsgOut.smsMT[0].msg = smsOut1;
	        	  	 return MTMsgOut;
			      }

        	   	  //check profile from database for B
        	   	  //Subscriber profileA;
        	   	  Subscriber profileB;
        	   	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Profile A frm DB][pwd:"+profileA.getPwd()+" type:"+intToString(profileA.getType())+" app_status:"+intToString(profileA.getAppStatus())+" whitelist:"+intToString(profileA.getWhiteListStat())+"]");

        	   	  //get profile B from DB
        	   	  profileB.setPhone(b_phone);
        	   	  getProfileDB(profileB,mySQLConn.myData,mtLibrary.dir_error);
        	   	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Get Profile B frm DB][pwd:"+profileB.getPwd()+" type:"+intToString(profileB.getType())+" app_status:"+intToString(profileB.getAppStatus())+" whitelist:"+intToString(profileB.getWhiteListStat())+"]");
        	   	  //delete pending file
			  	  deletePendingCAT(a_phone, b_phone,mySQLConn.myData,logger);
        	   	  //deleteFile(mtLibrary.dir_ats_pending+"/"+foundFile);
        	   	  MTMsgOut = confirmTransfer(profileA, profileB);
        	   	  return MTMsgOut;
	          } else if (isExist==2) {
	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error:Error Opening Directory]");
	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	        	  return MTMsgOut;
	          } else if (isExist==1) {
	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error:No Pending Request]");
	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|49|"+currentDateTime3()+"|"+tx_id);
	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(49, langA);
	        	  return MTMsgOut;
	          } else {
	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error while opening directory]");
	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	        	  return MTMsgOut;
	          }
          }

          //cancellation by donor
          MTCollection Transfer::cancelTransfer() {
        	  string foundFile;
         	  string fileTimeStamp;
         	  string smsOut1;
         	  string smsOut2;
         	  string smsOut3;
		  	  string dtCAT, txidCAT;
	      	  int langCAT, amtCAT;

         	  //log for transaction logs
         	  string logtx = mtLibrary.dir_ats_tx+"/"+currentDate()+".tx";
         	  FileLogger txLogger (logtx);

         	  MTMsgOut.smsMT[0].receiver = a_phone;
         	  MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

         	  string fileToSearch = a_phone+"."+b_phone;
 	          //short int isExist = searchPendingFile(mtLibrary.dir_ats_pending,fileToSearch,foundFile);
 		  	  short int isExist = searchPendingCAT(a_phone, b_phone,mySQLConn.myData,logger,langCAT,dtCAT,txidCAT,amtCAT);
 	          if (isExist==0) {
 	        	  //amt = stringToInt(foundFile.substr(56));
         	   	  //fileTimeStamp = foundFile.substr(26,14);
			  	  amt = amtCAT;
         	   	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Have Pending Request, proceed process]");
         	   	  //delete from pending file
         	   	  //deleteFile(mtLibrary.dir_ats_pending+"/"+foundFile);
			  	  deletePendingCAT(a_phone, b_phone,mySQLConn.myData,logger);
         	   	  //log tx into transaction log file
         	   	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|54|"+currentDateTime3()+"|"+tx_id);
         	   	  smsOut1 = find_replace(mtLibrary.getMsg(54, langA),"<bno>",b_phone);
         	   	  MTMsgOut.smsMT[0].msg = smsOut1;
         	   	  return MTMsgOut;
 	          } else if (isExist==2) {
 	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error:Error Opening Directory]");
 	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
 	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
 	        	  return MTMsgOut;
 	          } else if (isExist==1) {
 	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error:No Pending Request]");
 	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|49|"+currentDateTime3()+"|"+tx_id);
 	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(49, langA);
 	        	  return MTMsgOut;
 	          } else {
 	        	  logger.logDebug("[TX ID:"+tx_id+"] [Transfer.approveTransfer] [Error while opening directory]");
 	        	  txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"|||"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
 	        	  MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
 	        	  return MTMsgOut;
 	          }
          }

          //transfer request
          MTCollection Transfer::makeTransfer() {
        	 string smsOut1;
         	 string smsOut2;
         	 string smsOut3;
        	 string foundFile;
        	 string fileTimeStamp;
		 	 string dtCAT, txidCAT;
        	 int langCAT, amtCAT;

        	 //log for transaction logs
           	 string logtx = mtLibrary.dir_ats_tx+"/"+currentDate()+".tx";
           	 FileLogger txLogger (logtx);

        	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer - START] ");

             MTMsgOut.smsMT[0].receiver = a_phone;
             MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];

             //check profile from database for A & B
             Subscriber profileB;
        	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Profile A frm DB][pwd:"+profileA.getPwd()+" type:"+intToString(profileA.getType())+" app_status:"+intToString(profileA.getAppStatus())+" whitelist:"+intToString(profileA.getWhiteListStat())+" mthlylimitdt:"+profileA.getStatementDate()+" limit:"+floatToString(profileA.getMonthlyLimit())+"]");

             //get profile B from DB
             profileB.setPhone(b_phone);
             getProfileDB(profileB,mySQLConn.myData,mtLibrary.dir_error);
             logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Get Profile B frm DB][pwd:"+profileB.getPwd()+" type:"+intToString(profileB.getType())+" app_status:"+intToString(profileB.getAppStatus())+" whitelist:"+intToString(profileB.getWhiteListStat())+" mthlylimitdt:"+profileB.getStatementDate()+" limit:"+floatToString(profileB.getMonthlyLimit())+"]");
             logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Try to check syntax error]");

          	if (profileA.getBlacklistStat()==6 || profileA.getBlacklistStat()==8) {
          		 //check if donor is blacklisted all or blacklisted send only
          		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:donor blacklist status:"+intToString(profileA.getBlacklistStat())+"]");
		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|190|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(190, profileA.getLang(),profileA.getBrand());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<ano>",a_phone);
		 		 return MTMsgOut;
          	} else if (profileB.getBlacklistStat()==6 || profileB.getBlacklistStat()==7) {
          		 //check if receiver is blacklisted all or blacklisted receive only
          		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Receiver blacklist status:"+intToString(profileB.getBlacklistStat())+"]");
		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|191|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(191, profileA.getLang(),profileA.getBrand());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<bno>",b_phone);
		 		 return MTMsgOut;
          	} else if (profileA.getPromoStat()==6 || profileA.getPromoStat()==8) {
				 //check if donor is under promo : (cannot send & receive) or (cannot send)
				 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:donor promo status:"+intToString(profileA.getPromoStat())+"]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|193|"+currentDateTime3()+"|"+tx_id);
				 smsOut1 = mtLibrary.getMsg(193, profileA.getLang(),profileA.getBrand());
				 smsOut1 = find_replace(smsOut1,"<ano>",a_phone);
				 smsOut1 = find_replace(smsOut1,"<ped>",profileA.getPromoEndDate());
				 MTMsgOut.smsMT[0].msg = smsOut1;
		 		 return MTMsgOut;
			} else if (profileB.getPromoStat()==6 || profileB.getPromoStat()==7) {
				 //check if receiver is under promo : (cannot send & receive) or (cannot receive)
				 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Receiver promo status:"+intToString(profileB.getPromoStat())+"]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|194|"+currentDateTime3()+"|"+tx_id);
				 smsOut1 = mtLibrary.getMsg(194, profileA.getLang(),profileA.getBrand());
				 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
				 smsOut1 = find_replace(smsOut1,"<ped>",profileB.getPromoEndDate());
				 MTMsgOut.smsMT[0].msg = smsOut1;
		 		 return MTMsgOut;
          	} else if (amt < bizrules.min_trf_prd && profileA.getType()==0) {
		 		 //check min amount to transfer prepaid
		 		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Amount<Min Amount allowed] [Prepaid User want transfer " + floatToString(amt) + ".Minimum amount to transfer for prepaid user is " + floatToString(bizrules.min_trf_prd) + "]");
		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|35|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(35, profileA.getLang());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(bizrules.min_trf_prd)));
		 		 return MTMsgOut;
		 	 } else if (amt > bizrules.max_trf_prd && profileA.getType()==0) {
		 		 //check maximum amount to transfer prepaid
		 		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Amount>Max Amount allowed] [Prepaid User want transfer " + floatToString(amt) + ".Maximum amount to transfer for prepaid user is " + floatToString(bizrules.max_trf_prd) + "]");
 		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|36|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(36, profileA.getLang());
		  		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(bizrules.min_trf_prd)));
		  		 return MTMsgOut;
          	} else if (amt < bizrules.min_trf_ptd && profileA.getType()==1) {
		 		 //check min amount to transfer postpaid
		 		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Amount<Min Amount allowed] [Postpaid User want transfer " + floatToString(amt) + ".Minimum amount to transfer for postpaid user is " + floatToString(bizrules.min_trf_ptd) + "]");
		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|210|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(210, profileA.getLang());
		 		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(bizrules.min_trf_prd)));
		 		 return MTMsgOut;
		 	 } else if (amt > bizrules.max_trf_ptd && profileA.getType()==1) {
		 		 //check maximum amount to transfer postpaid
		 		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.makeTransfer] [Error:Amount>Max Amount allowed] [Postpaid User want transfer " + floatToString(amt) + ".Maximum amount to transfer for postpaid user is " + floatToString(bizrules.max_trf_prd) + "]");
 		 		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|211|"+currentDateTime3()+"|"+tx_id);
		 		 smsOut1 = mtLibrary.getMsg(211, profileA.getLang());
		  		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(bizrules.max_trf_ptd)));
		  		 return MTMsgOut;
		 	 } else if (profileA.getWhiteListStat()==0 && profileA.getType()==0) {
         		 //A No not whitelisted (prepaid only)
         		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:A not whitelisted to using AirtimeShare]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|31|"+currentDateTime3()+"|"+tx_id);
      			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(31, profileA.getLang(), profileA.getBrand());
      			 return MTMsgOut;
         	 }

		if (profileA.getAppStatus()==2 || profileA.getType()==1) {
        		 //If user registered, check password OR user is postpaid no need to give confirmation

			//*** For Postpaid, no need to chekc for password
			if(profileA.getType()==1){
				logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [POSTPAID user bypass password checking,proceed]");
				MTMsgOut = confirmTransfer(profileA, profileB);
                         	return MTMsgOut;
			}

        		 if (pin=="") {
        			 //no password inserted
        			 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Password is blank by registered user]");
        			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|29|"+currentDateTime3()+"|"+tx_id);
        			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(29, profileA.getLang());
         			 return MTMsgOut;
        	 	 } else if (pin.compare(profileA.getPwd())!=0) {
        			 //Invalid Password by registered user
        			 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Invalid password by registered user]");
        			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|30|"+currentDateTime3()+"|"+tx_id);
        			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(30, profileA.getLang());
        			 return MTMsgOut;
        		 } else {
        			 //Password is valid, proceed
        			 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Valid password by registered user,proceed]");
        		 }

        		 MTMsgOut = confirmTransfer(profileA, profileB);
        		 return MTMsgOut;
        	 } else {
        		 //for unregistered user, ask confirmation

        		 //check donor daily limit
 	     		 int max_trf_amt = bizrules.max_trf_dly_unr;
 	     		 int max_suc_txn = bizrules.max_suc_txn_unr;



	 	    	 if (((profileA.getDailyLimit() + amt) > max_trf_amt) && profileA.getType()==0) {
	 	    		 //donor reach max daily transfer amount limit prepaid
	 	    		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Reach daily limit] [Prepaid user has reach daily limit "+floatToString(max_trf_amt)+" of transfer " + floatToString(profileA.getDailyLimit())+"]");
	 	    		 if (profileA.getAppStatus()==2) {
	 	    			 smsOut1 = mtLibrary.getMsg(39, profileA.getLang());
	 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|39|"+currentDateTime3()+"|"+tx_id);
	 	    		 } else {
	 	    			 smsOut1 = mtLibrary.getMsg(45, profileA.getLang());
	 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|45|"+currentDateTime3()+"|"+tx_id);
	 	    		 }
	 	    		 smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(max_trf_amt)));
	 	    		 float trfAllowed = max_trf_amt - profileA.getDailyLimit();
	 	    		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(trfAllowed)));
	 	    		 return MTMsgOut;
	 	    	 } else if ((profileA.getSuccessCount()>=max_suc_txn) && profileA.getType()==0) {
	 	    		 //check daily max successfull transaction
	 	    		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Reach daily successfull transaction] [Prepaid user has reach daily limit "+intToString(max_suc_txn)+" of transfer " + intToString(profileA.getSuccessCount())+"]");
	 	    		 if (profileA.getAppStatus()==2) {
	 	    			 smsOut1 = mtLibrary.getMsg(46, profileA.getLang());
	 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|46|"+currentDateTime3()+"|"+tx_id);
	 	    		 } else {
	 	    			 smsOut1 = mtLibrary.getMsg(47, profileA.getLang());
	 	    			txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|47|"+currentDateTime3()+"|"+tx_id);
	 	    		 }
	 	    		 smsOut1 = find_replace(smsOut1,"<cnt>",intToString(max_suc_txn));
	 	    		 MTMsgOut.smsMT[0].msg = smsOut1;
	 	    		 return MTMsgOut;

	
	 	    	     } 	else if (profileA.get_has_1p5()==true && profileA.getType()==1){
					//***postpaid is a 1+5 package
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:1+5 not allowed to transfer] ");
					smsOut1 = mtLibrary.getMsg(217, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|217|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
					
			     }	else if (profileA.get_has_tcl()==true && profileA.getType()==1){
					//***postpaid has active TCL
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:active TCL  not allowed to transfer] ");
					smsOut1 = mtLibrary.getMsg(218, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|218|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

			    }	else if (profileA.get_has_LA()==true && profileA.getType()==1){
					//***postpaid has LA
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:account has LA] ");
					smsOut1 = mtLibrary.getMsg(219, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|219|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

			    }  else if (checkVIP(profileA.getVIP())==false && profileA.getType()==1){
					//*** VIP Code
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:postpaid VIP code not allowed] ");
					smsOut1 = mtLibrary.getMsg(220, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|220|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

			    }	else if ((profileA.getCurrentUsage() + amt > profileA.getCreditLimit()) && profileA.getType()==1) {
					 //postpaid user reach credit limit
					 logger.logDebug("[TX ID:"+tx_id+"] TODO [(profileA.getCurrentUsage():"+floatToString(profileA.getCurrentUsage())+":]");
					 logger.logDebug("[TX ID:"+tx_id+"] TODO [amt:"+floatToString(amt)+":]");
					 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user Reach credit limit]");
					 smsOut1 = mtLibrary.getMsg(215, profileA.getLang());
					 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|215|"+currentDateTime3()+"|"+tx_id);
					 MTMsgOut.smsMT[0].msg = smsOut1;
					 return MTMsgOut;
			     } else if ((profileA.getMonthlyLimit() + amt > bizrules.mth_lim_ptd) && profileA.getType()==1)  {
					 //postpaid user reach monthly limit
					 //todo - new error code
					 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user Reach monthly limit "+floatToString(bizrules.mth_lim_ptd)+"]");
					 smsOut1 = mtLibrary.getMsg(213, profileA.getLang());
					 smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.mth_lim_ptd)));
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|213|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
			     } else if ((profileA.getTotalDue()>0) && profileA.getType()==1) {
					//has overdue invoice
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user has overdue invoice]");
                                         smsOut1 = mtLibrary.getMsg(216, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|216|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
			     } else if (profileA.getRoaming()==1 && profileA.getType()==1) {
					//is roaming
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user is roaming]");
                                         smsOut1 = mtLibrary.getMsg(214, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|214|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                             } else if ((profileA.getLOS()<bizrules.los_ptd) && profileA.getType()==1) {
                                        //lenght of stay not sufficient
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid LOS not sufficient: "+intToString(profileA.getLOS())+"]");
                                         smsOut1 = mtLibrary.getMsg(212, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|212|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
			     } 				

        		 //check pending transaction
	             string fileToSearch = a_phone+"."+b_phone;
	             //short int isExist = searchPendingFile(mtLibrary.dir_ats_pending,fileToSearch,foundFile);
	             short int isExist = searchPendingCAT(a_phone, b_phone,mySQLConn.myData,logger,langCAT,dtCAT,txidCAT,amtCAT);
	             if (isExist==0) {
	            	 //check timeout first
			 		fileTimeStamp = dtCAT;
	            	 //fileTimeStamp = foundFile.substr(26,14);
	            	 string currentTime = currentDateTime2();
	            	 short int timeout = bizrules.ats_tmt;
	            	 int timeoutInSec = timeout * 60;
	            	 struct tm tmNow, tmRequest;
	            	 strptime(stringToChar(currentTime), "%Y%m%d%H%M%S",&tmNow);
	            	 strptime(stringToChar(fileTimeStamp.substr(0,14)), "%Y%m%d%H%M%S",&tmRequest);
	            	 double secDiff = difftime(mktime(&tmNow),mktime(&tmRequest));
	            	 int dayDiff=0;
	            	 int timeDiff=0;
	            	 if (secDiff>timeoutInSec) {
	            		 //request already expired, delete pending file
	            		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Pending request already timeout, delete pending file, then proceed]");
	            		 deleteFile(mtLibrary.dir_ats_pending+"/"+foundFile);
	            	 } else {
	            		 //request not expired yet, so have pending request
	            		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Still Have Pending Request]");
	            		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|48|"+currentDateTime3()+"|"+tx_id);
		             	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(48, langA);
		             	 return MTMsgOut;
	            	 }
	             } else if (isExist==2) {
	            	 //cannot open directory
	            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Error Opening Directory]");
	            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	            	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	            	 return MTMsgOut;
	             } else if (isExist==1) {
	            	 //no pending request, proceed to process request
	            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [No Pending Request, proceed]");
	             } else {
	            	 //error opening directory
	            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:while opening directory]");
	            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|6|"+currentDateTime3()+"|"+tx_id);
	            	 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(6, langA);
	            	 return MTMsgOut;
	             }

	             //if no error, write into pending file
                // string pendingLog = mtLibrary.dir_ats_pending+"/"+a_phone+"."+b_phone+"."+intToString(langA)+"."+currentDateTime2()+"."+tx_id+"."+intToString((int)amt);
               	 //FileLogger pendingLogger (pendingLog);
               	 //pendingLogger.createLog();
               	 //if (isFileExist(pendingLog)!=0) {
		 		short int createStat = createPendingCAT(a_phone, b_phone, profileA.getLang(), currentDateTime2(), tx_id, amt,mySQLConn.myData,logger, smsc_id);
          	 	if (createStat!=0) {
               		 //cannot create pending file
               		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Error:Cannot create pending log file]");
               		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|8|"+currentDateTime3()+"|"+tx_id);
               		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(8, langA);
               		 return MTMsgOut;
               	 } else {
               		 //pending file created successfully
               		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Successfully create pending log file]");
               		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.makeTransfer] [Unregistered user, ask confirmation first]");
               		 smsOut1 = mtLibrary.getMsg(53, profileA.getLang());
               		 smsOut1 = find_replace(smsOut1,"<bno>",profileB.getPhone());
               		 smsOut1 = find_replace(smsOut1,"<amt>",intToString(amt));
               		 MTMsgOut.smsMT[0].msg = smsOut1;
               		 MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + profileB.getPhone().substr(1);
               		 //general msg to unregistered user
               		 smsOut2 = mtLibrary.getMsg(151, profileA.getLang());
               		 MTMsgOut.smsMT[1].msg = smsOut2;
               		 MTMsgOut.smsMT[1].receiver = a_phone;
               		 MTMsgOut.smsMT[1].oadc = bizrules.ats_pfx + profileA.getPhone().substr(1,3) + bizrules.ATSoadc[0];
               		 return MTMsgOut;
               	 }
        	 }
          }


          //confirm transfer
          MTCollection Transfer::confirmTransfer(Subscriber& profileA, Subscriber& profileB) {
        	 float prevBalanceA=0;
          	 float prevBalanceB=0;
          	 float newBalanceA=0;
          	 float newBalanceB=0;
          	 string expB;
        	 string expA;
        	 float svc_chg_a;
        	 float svc_chg_b;
        	 float balanceAfterTransfer=0;
             float max_trf_amt_allowed=0;
             float max_trf_amt=0;
             float newUsage=0;
             int yr,mth,dt;
             long dt1,dt2;
             int max_suc_txn;
             string smsOut1;
         	 string smsOut2;
         	 string smsOut3;
         	 string expiryDt;
         	 string activeDt;

         	 //log for transaction logs
         	 string logtx = mtLibrary.dir_ats_tx+"/"+currentDate()+".tx";
         	 FileLogger txLogger (logtx);
			 //log for ussd sync cdr
			 string logUssd = mtLibrary.dir_ussd_tx+"/cat.cdr";
			 FileLogger ussdCdr (logUssd);

             logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Try to check business rules]");
		cout << "monthly limit:" << profileA.getMonthlyLimit() << endl;
		cout << "a no:" << profileA.getPhone() << endl;

             if (profileA.getWhiteListStat()==0 && profileA.getType()==0) {
         		 //A No not whitelisted
         		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A not whitelisted to using AirtimeShare]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|31|"+currentDateTime3()+"|"+tx_id);
      			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(31, profileA.getLang(), profileA.getBrand());
      			 return MTMsgOut;
         	 }

             //check donor daily limit
 	     	 if (profileA.getAppStatus()==2) {
 	     		 max_trf_amt = bizrules.max_trf_dly_reg;
 	     		 max_suc_txn = bizrules.max_suc_txn_reg;
 	     	 } else {
 	     		 max_trf_amt = bizrules.max_trf_dly_unr;
 	     		 max_suc_txn = bizrules.max_suc_txn_unr;
 	     	 }

 	    	 if ((profileA.getDailyLimit() + amt) > max_trf_amt && profileA.getType()==0) {
 	    		 //donor reach max daily transfer amount limit prepaid
 	    		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Reach daily limit] [Prepaid user has reach daily limit "+floatToString(max_trf_amt)+" of transfer " + floatToString(profileA.getDailyLimit())+"]");
 	    		 if (profileA.getAppStatus()==2) {
 	    			 smsOut1 = mtLibrary.getMsg(39, profileA.getLang());
 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|39|"+currentDateTime3()+"|"+tx_id);
 	    		 } else {
 	    			 smsOut1 = mtLibrary.getMsg(45, profileA.getLang());
 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"||"+intToString(profileA.getBrand())+"||"+intToString(amt)+"|"+intToString(operation_type)+"|45|"+currentDateTime3()+"|"+tx_id);
 	    		 }
 	    		 smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(max_trf_amt)));
 	    		 float trfAllowed = max_trf_amt - profileA.getDailyLimit();
 	    		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(trfAllowed)));
 	    		 return MTMsgOut;
 	    	 } else if (profileA.getSuccessCount()>=max_suc_txn && profileA.getType()==0) {
 	    		 //check daily max successfull transaction for prepaid
 	    		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Reach daily successfull transaction] [Prepaid user has reach daily limit "+intToString(max_suc_txn)+" of transfer " + intToString(profileA.getSuccessCount())+"]");
 	    		 if (profileA.getAppStatus()==2) {
 	    			 smsOut1 = mtLibrary.getMsg(46, profileA.getLang());
 	    			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|46|"+currentDateTime3()+"|"+tx_id);
 	    		 } else {
 	    			 smsOut1 = mtLibrary.getMsg(47, profileA.getLang());
 	    			txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|47|"+currentDateTime3()+"|"+tx_id);
 	    		 }
 	    		 smsOut1 = find_replace(smsOut1,"<cnt>",intToString(max_suc_txn));
 	    		 MTMsgOut.smsMT[0].msg = smsOut1;
 	    		 return MTMsgOut;
                 }  else if (profileA.get_has_1p5()==true && profileA.getType()==1){
					//***postpaid is a 1+5 package
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:1+5 not allowed to transfer] ");
					smsOut1 = mtLibrary.getMsg(217, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|217|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

		} else if (profileA.get_has_tcl()==true && profileA.getType()==1){
					//***postpaid has active TCL
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:active TCL  not allowed to transfer] ");
					smsOut1 = mtLibrary.getMsg(218, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|218|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

		}  else if (profileA.get_has_LA()==true && profileA.getType()==1){
					//***postpaid has LA
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:account has LA] ");
					smsOut1 = mtLibrary.getMsg(219, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|219|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

		} else if (checkVIP(profileA.getVIP())==false && profileA.getType()==1){
					//***postpaid is VIP code
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:account VIP code not allowed] ");
					smsOut1 = mtLibrary.getMsg(220, profileA.getLang());
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|220|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;

		} else if ((profileA.getCurrentUsage() + profileA.getCurrentDue()  + amt > profileA.getCreditLimit()) && profileA.getType()==1) {
                                         //postpaid user reach credit limit
					logger.logDebug("[TX ID:"+tx_id+"] TODO [(profileA.getCurrentUsage():"+floatToString(profileA.getCurrentUsage())+":]");
                                         logger.logDebug("[TX ID:"+tx_id+"] TODO [amt:"+floatToString(amt)+":]");
					logger.logDebug("[TX ID:"+tx_id+"] TODO [currentdue:"+floatToString(profileA.getCurrentDue())+":]");
					logger.logDebug("[TX ID:"+tx_id+"] TODO [crditlimit:"+floatToString(profileA.getCreditLimit())+":]");
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user Reach credit limit]");
                                         smsOut1 = mtLibrary.getMsg(215, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|215|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                 } else if ((profileA.getMonthlyLimit() + amt > bizrules.mth_lim_ptd) && profileA.getType()==1)  {
                                         //postpaid user reach monthly limit
                                         //todo - new error code
					 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user Reach monthly limit "+floatToString(bizrules.mth_lim_ptd)+"]");
                                         smsOut1 = mtLibrary.getMsg(213, profileA.getLang());
					 smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.mth_lim_ptd)));
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|213|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                             } else if ((profileA.getTotalDue()>0) && profileA.getType()==1) {
                                        //has overdue invoice
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user has overdue invoice]");
                                         smsOut1 = mtLibrary.getMsg(216, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|216|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                             } else if (profileA.getRoaming()==1 && profileA.getType()==1) {
                                        //is roaming
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid user is roaming]");
                                         smsOut1 = mtLibrary.getMsg(214, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|214|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                             } else if ((profileA.getLOS()<bizrules.los_ptd) && profileA.getType()==1) {
                                        //lenght of stay not sufficient
                                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Postpaid LOS not sufficient: "+intToString(profileA.getLOS())+"]");
                                         smsOut1 = mtLibrary.getMsg(212, profileA.getLang());
                                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|212|"+currentDateTime3()+"|"+tx_id);
                                         MTMsgOut.smsMT[0].msg = smsOut1;
                                         return MTMsgOut;
                             }


 	    	 //check maximum daily transfered for current receiver

 	    	 //int amtTransfered = sumAmountTransfered(mtLibrary.dir_ats_success, a_phone, b_phone);
 	    	 int amtTransfered = 0;
 	    	 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Total Transfered for current receiver:"+intToString(amtTransfered)+"]");
 	    	 if ((amtTransfered+amt)>bizrules.max_trf_amt_uni) {
 	    		logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Error:Donor reach Max transfered amount allowed for current receiver] [Amt:"+floatToString(amt)+" + amt received:"+intToString(amtTransfered)+" has reach maximum transfered amount for current receiver:"+floatToString(bizrules.max_trf_amt_uni)+"]");
 	    		txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|50|"+currentDateTime3()+"|"+tx_id);
         		smsOut1 = mtLibrary.getMsg(50, profileA.getLang());
         		smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.max_trf_amt_uni)));
         		MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<bno>",b_phone);
         		return MTMsgOut;
 	    	 }

 	    	 //check maximum received amount for today (ATS)
		 	 int amtReceivedATS = int(profileB.getDailyReceived());
        	 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Total Received ATS Today:"+intToString(amtReceivedATS)+"]");

        	 if ((amtReceivedATS+amt)>bizrules.max_rcv_amt) {
        		 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Error:Receiver reach Max received amount allowed] [Amt:"+floatToString(amt)+" + amt received:"+intToString(amtReceivedATS)+" has reach maximum received amount:"+floatToString(bizrules.max_rcv_amt)+"]");
        		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|38|"+currentDateTime3()+"|"+tx_id);
         		 smsOut1 = mtLibrary.getMsg(38, profileA.getLang());
         		 smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.max_rcv_amt)));
         		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(amtReceivedATS));
         		 return MTMsgOut;
        	 }

        	 //maximum received amount for today (ATR)
		     int totalreceived = int(profileB.getDailyReceived());
         	 logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Total Received Today:"+intToString(totalreceived)+"]");

         	 if ((totalreceived+amt)>bizrules.max_rcv_amt) {
         		logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Error:Receiver reach Max received amount allowed] [Amt:"+floatToString(amt)+" + amt received:"+intToString(amtReceivedATS)+" has reach maximum received amount:"+floatToString(bizrules.max_rcv_amt)+"]");
         		txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|38|"+currentDateTime3()+"|"+tx_id);
         		smsOut1 = mtLibrary.getMsg(38, profileA.getLang());
         		smsOut1 = find_replace(smsOut1,"<lim>",intToString(int(bizrules.max_rcv_amt)));
         		MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(totalreceived));
         		return MTMsgOut;
         	 }


             svc_chg_a = bizrules.ats_sms_chg_prd_ano;
             svc_chg_b = bizrules.ats_sms_chg_prd_bno;
             logger.logDebug("[TX ID:"+tx_id+"] [Get service charge] [A="+floatToString(svc_chg_a)+"] [B="+floatToString(svc_chg_b)+"]");

             //check A business rules
             if (profileA.getINStatus()!=2) {
         		 //A not active
         		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A no is not active, return user status from IN="+intToString(profileA.getINStatus())+"]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|32|"+currentDateTime3()+"|"+tx_id);
  		 		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(32, profileA.getLang(), profileA.getBrand());
  		 		 return MTMsgOut;
             } else if (profileA.getBrand()==12 || profileA.getBrand()==13) {
            	 //check if A brand is celcom staff (12 or 13)
            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A no is celcom staff, return user status from IN="+intToString(profileA.getBrand())+"]");
            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|40|"+currentDateTime3()+"|"+tx_id);
		 		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(40, profileA.getLang());
		 		 return MTMsgOut;
             /*} else if (profileA.getType()!=0) {
            	 //check if A prepaid
            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A no is not prepaid, return user status from IN="+intToString(profileA.getType())+"]");
            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|41|"+currentDateTime3()+"|"+tx_id);
   		 		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(41, profileA.getLang());
   		 		 return MTMsgOut;*/
             } else if (profileA.getBrand()==1 && profileA.getGracePeriod()==true) {
            	 //check if A brand is MAX & check if grace period
				 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A no brand is MAX, return user status from IN="+intToString(profileA.getBrand())+"]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|42|"+currentDateTime3()+"|"+tx_id);
				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(42, profileA.getLang());
				 return MTMsgOut;
             } else if (profileA.getExpiry() < currentDateLimit() && profileA.getType()==0) {
				 //A is expired for prepaid
				 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is expired. Expiry Data="+profileA.getExpiry()+"]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|44|"+currentDateTime3()+"|"+tx_id);
				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(44, profileA.getLang());
				 return MTMsgOut;
             } else if (profileA.getBrand()==30) {
		 	 	 //A is XOX
		 	 	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is XOX]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|195|"+currentDateTime3()+"|"+tx_id);
				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(195, profileA.getLang());
				 return MTMsgOut;
	    	} else if (profileA.getBrand()==17) {
				 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is 17]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|197|"+currentDateTime3()+"|"+tx_id);
				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(197, profileA.getLang());
				 return MTMsgOut;
		 	 }


             balanceAfterTransfer = profileA.getBalance() - amt - svc_chg_a;
             float min_bal_allow = bizrules.min_bal_aft_trf;
             if (balanceAfterTransfer < min_bal_allow  && profileA.getType()==0 ) {
         		 //check min balance after transfer for prepaid
         		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:Bal After Transfer<Min Bal Allowed. Prepaid User want transfer " + floatToString(amt) + ".Minimum balance after transfer is " + floatToString(min_bal_allow) + "]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|37|"+currentDateTime3()+"|"+tx_id);
          		 smsOut1 = mtLibrary.getMsg(37, profileA.getLang());
          		 MTMsgOut.smsMT[0].msg = find_replace(smsOut1,"<amt>",intToString(int(min_bal_allow)));
          		 return MTMsgOut;
         	 }

             //query profile from IN for B
             logger.logDebug("[TX ID:"+tx_id+"]  [Transfer.confirmTransfer] [Attempt Query Profile B no from IN]");
             IN_Conn profileINB;
             struct IN_UserProfile ProfileResultB;

             //Query Profile B from IN
         	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Attempt Query Profile B no from IN, IN type from DB = "+intToString(profileB.getINType())+"]");
        	 ProfileResultB = profileINB.QueryProfile(profileB.getPhone(),inPort,tx_id);

             if (ProfileResultB.queryStatus==0) {
            	 profileB.setBalance(ProfileResultB.userBalance);
            	 activeDt = ProfileResultB.activationDate.substr(0,4) + ProfileResultB.activationDate.substr(5,2) + ProfileResultB.activationDate.substr(8,2);
            	 profileB.setActivationDate(activeDt);
            	 profileB.setType(ProfileResultB.userType);
            	 profileB.setINType(ProfileResultB.userPlatform);
            	 profileB.setINStatus(ProfileResultB.userStatus);
            	 expiryDt = ProfileResultB.expiryDate.substr(0,4) + ProfileResultB.expiryDate.substr(5,2) + ProfileResultB.expiryDate.substr(8,2);
            	 profileB.setExpiry(expiryDt);
            	 profileB.setBrand(ProfileResultB.brand);
            	 profileB.setLang(ProfileResultB.userLang);
            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Check profile B from IN Success:Result=0][bal:"+floatToString(ProfileResultB.userBalance)+" activation:"+ProfileResultB.activationDate+" status:"+intToString(ProfileResultB.userStatus)+" expiry:"+ProfileResultB.expiryDate+" brand:"+intToString(ProfileResultB.brand)+"]");
             } else if (ProfileResultB.queryStatus==5) {
            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:while Check profile B from IN:Result=5]");
            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|16|"+currentDateTime3()+"|"+tx_id);
       			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(16, profileA.getLang());
       			 return MTMsgOut;
           	 } else if (ProfileResultB.queryStatus==7) {
				//profile not found, query from postpaid
				logger.logDebug("[TX ID:"+tx_id+"][Query from IN failed, response : User  Not Found]");
				struct Post_UserProfile PostProfileResultB;
				Post_Conn postConn;
				PostProfileResultB = postConn.QueryProfile(profileB.getPhone(),inPort,tx_id);
				if (PostProfileResultB.queryStatus==0) {
					profileB.setType(PostProfileResultB.userType);
                		        profileB.setAccountNo(PostProfileResultB.act_no);
		                        profileB.setINStatus(PostProfileResultB.userStatus);
                		        profileB.setCreditLimit(PostProfileResultB.credit_limit/100);
		                        profileB.setCurrentUsage(PostProfileResultB.unbill/100);
					profileB.setTotalDue(PostProfileResultB.total_due/100);
					profileB.setCurrentDue(PostProfileResultB.current_due/100);
					profileB.set_has_1p5(PostProfileResultB.has_1p5);
					profileB.set_has_tcl(PostProfileResultB.has_act_tcl);
					profileB.set_has_LA(PostProfileResultB.has_LA);
					profileB.setLang(PostProfileResultB.userLang);
					profileB.setLOS(PostProfileResultB.los);	
					profileB.setRoaming(PostProfileResultB.roaming);
					profileB.setVIP(PostProfileResultB.vip_code);
					logger.logDebug("[TX ID:"+tx_id+"][B is postpaid]");
				} else {
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:while Check profile B from PG:Result="+intToString(PostProfileResultB.queryStatus)+"]");
					txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
					MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(7, profileA.getLang());
       			 	return MTMsgOut;
				}
		 	 } else {
            	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:while Check profile B from IN:Result="+intToString(ProfileResultB.queryStatus)+"]");
            	 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|7|"+currentDateTime3()+"|"+tx_id);
       			 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(7, profileA.getLang());
       			 return MTMsgOut;
             }
             logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Finish Query Profile B]");
             logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [a brand:"+intToString(profileA.getBrand())+"][b brand:"+intToString(profileB.getBrand())+"]");

        	 //check B business rules
        	 if (profileB.getINStatus()!=2) {
        		 //B is terminated
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:B is terminated]");
        		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|33|"+currentDateTime3()+"|"+tx_id);
  		 		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(33, profileA.getLang());
  		 		 return MTMsgOut;
        	 } else if (profileB.getType()==1) {
        		 //B is postpaid
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:B is postpaid]");
        		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|34|"+currentDateTime3()+"|"+tx_id);
		 		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(34, profileA.getLang());
		 		 return MTMsgOut;
        	 /*} else if (profileA.getBrand()==1 && (profileB.getBrand()!=1 && profileB.getBrand()!=9)) {
        		 //A is brand MAX and B is not brand MAX or 3G MAX
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is brand MAX and B is not brand MAX or 3G MAX]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|27|"+currentDateTime3()+"|"+tx_id);
         		 smsOut1 = mtLibrary.getMsg(27, profileA.getLang());
         		 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
         		 MTMsgOut.smsMT[0].msg = smsOut1;
 		 		 return MTMsgOut;*/
        	 } else if ((profileA.getBrand()==14 && profileB.getBrand()!=14) || (profileA.getBrand()!=14 && profileB.getBrand()==14)) {
        		 //(A is not MTRADE and B is not MTRADE) or (A is not mtrade and b is mtrade)
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is brand MAX and B is not brand MAX or 3G MAX]");
          		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|28|"+currentDateTime3()+"|"+tx_id);
          		 smsOut1 = mtLibrary.getMsg(28, profileA.getLang());
          		 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
          		 MTMsgOut.smsMT[0].msg = smsOut1;
  		 		 return MTMsgOut;
		
        	 } else if ((profileA.getBrand()==70 && profileB.getBrand()!=70) || (profileA.getBrand()!=70 && profileB.getBrand()==70)) {
                         //(A is not RUGBY and B is not RUGBY) or (A is not RUGBY and b is RUGBY)
                         logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error: not an internal MVNO Rugby transferor/receiver]");
                         txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|98|"+currentDateTime3()+"|"+tx_id);
                         smsOut1 = mtLibrary.getMsg(98, profileA.getLang());
                         smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
                         MTMsgOut.smsMT[0].msg = smsOut1;
                                 return MTMsgOut;

                 } else if (profileA.getBrand()==1 && profileB.getExpiry() < currentDateLimit()) {
        		 //A is MAX and B is expired
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:A is brand MAX and B is expired]");
           		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|192|"+currentDateTime3()+"|"+tx_id);
           		 smsOut1 = mtLibrary.getMsg(192, profileA.getLang());
           		 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
           		 MTMsgOut.smsMT[0].msg = smsOut1;
   		 		 return MTMsgOut;
        	 } else if (profileB.getBrand()==30) {
			 	 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:B is XOX]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|196|"+currentDateTime3()+"|"+tx_id);
				 smsOut1 = mtLibrary.getMsg(196, profileA.getLang());
				 smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
				 MTMsgOut.smsMT[0].msg = smsOut1;
   		 		 return MTMsgOut;
			} else if (profileB.getBrand()==17) {
				logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error:B is 17]");
				txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|198|"+currentDateTime3()+"|"+tx_id);
				smsOut1 = mtLibrary.getMsg(198, profileA.getLang());
				smsOut1 = find_replace(smsOut1,"<bno>",b_phone);
				MTMsgOut.smsMT[0].msg = smsOut1;
				return MTMsgOut;
			 }

             prevBalanceA = profileA.getBalance();
             prevBalanceB = profileB.getBalance();

             int validity;
        	 int yrExp, mthExp, dtExp;
        	 long dt3,dt4;
        	 if (profileA.getBrand()==1) {
        		//if donor MAX, dont transfer validity
        		validity=0;
        	 } else if (profileB.getBrand()==1) {
        		//if receiver MAX, dont add validity
        		validity=0;
        	 } else {
        		if (amt==1 || amt==2)
        			validity=1;
        		else
        			validity=3;
        		time_t myTime = time(NULL);
        		tm *myUsableTime = localtime(&myTime);
        		myUsableTime->tm_mday = myUsableTime->tm_mday+validity;
        		mktime ( myUsableTime );
        		date validityDt(myUsableTime->tm_mday,myUsableTime->tm_mon+1,myUsableTime->tm_year+1900);
        		dt3 = long_date(validityDt);
        		yrExp = stringToInt(profileB.getExpiry().substr(0,4));
        		mthExp = stringToInt(profileB.getExpiry().substr(4,2));
        		dtExp = stringToInt(profileB.getExpiry().substr(6,2));
        		date expiryDt(dtExp,mthExp,yrExp);
        		dt4 = long_date(expiryDt);
        		if (dt3 < dt4)
        			validity=0;
        	 }

			 short int transferStatus=99;//default value for transfer status
			 string aExpiryDate;
			 string bExpiryDate;
			 if (profileA.getType()==0) {
        		 //send transfer request to transfer prepaid
		         	    logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Attempt to send instruction for IN to Transfer][a_no="+profileA.getPhone()+" b_no="+profileB.getPhone()+" amt="+intToString(amt)+",validity="+intToString(validity)+",a_chg="+floatToString(svc_chg_a)+",b_chg="+floatToString(svc_chg_b)+"]");
         			IN_Conn transferProcess;
        	 		struct IN_TransferResult TransferResult;
        			TransferResult = transferProcess.Transfer(profileA.getPhone(),profileB.getPhone(),amt,validity,svc_chg_a,svc_chg_b,tx_id,profileA.getINType(),profileB.getINType(),inPort,"CAT");
				transferStatus=TransferResult.transferStatus;
				 newBalanceA = TransferResult.aBalance;
                                 newBalanceB = TransferResult.bBalance;
        			aExpiryDate = TransferResult.aExpiryDate;
	        		bExpiryDate = TransferResult.bExpiryDate;
		 	 } else {
				//send transfer request to transfer postpaid
				logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Attempt to send instruction for IN to Transfer][a_no="+profileA.getPhone()+" b_no="+profileB.getPhone()+" amt="+intToString(amt)+",validity="+intToString(validity)+",a_chg="+floatToString(svc_chg_a)+",b_chg="+floatToString(svc_chg_b)+"]");
				Post_Conn transferProcess;
				struct Post_TransferResult TransferResult;
				TransferResult = transferProcess.Transfer(profileA.getPhone(),profileB.getPhone(),amt,validity,svc_chg_a,svc_chg_b,tx_id,profileA.getINType(),profileB.getINType(),inPort,"CAT",profileA.getAccountNo());
				cout << "Transfer status:" << TransferResult.transferStatus << endl;
				transferStatus=TransferResult.transferStatus;
				newBalanceA = TransferResult.aBalance;
	                         newBalanceB = TransferResult.bBalance;
        			aExpiryDate = TransferResult.aExpiryDate;
	        		bExpiryDate = TransferResult.bExpiryDate;
			 }
        	 if (transferStatus==0) {

        		 //transaction being processed
  				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(152, profileA.getLang());
	 			 MTMsgOut.smsMT[0].receiver = profileA.getPhone();
	 			 MTMsgOut.smsMT[0].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];


        		 logger.logDebug("[TX ID:"+tx_id+"] [Debit & Credit Success!] [New Balance A:"+floatToString2d(newBalanceA)+" New Balance B:"+floatToString2d(newBalanceB)+" Exp A:"+aExpiryDate+" Exp B:"+bExpiryDate+"]");
				
				//*** Profile A not yet registered and it's prepaid case 
 				 if (profileA.getIsRegistered()==0 && profileA.getType()==0) {
					//for unregistered user, insert into db
					preRegisterUserProfile(profileA, tx_id, mySQLConn.myData, logger);
				 }
				else if(profileA.getIsRegistered()==0 && profileA.getType()==1){ //** for postpaid case
					preRegisterUserProfilePostpaid(profileA, tx_id, mySQLConn.myData, logger);
				}
				
				 //for registered user, update limit in profile DB 
				 if(profileA.getType()==0){ //** for prepaid
				 	logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [registered user - update balance]");
	    			 	float newDailyLimit = profileA.getDailyLimit()+amt;
	    			 	short int newDailySuccessCount = profileA.getSuccessCount() + 1;
			 	 	//float newMthlyLimit = profileA.getMonthlyLimit() + amt; //** bug fixing
					float newMthlyLimit = profileA.getMonthlyLimit(); //** bug fixing prepaid no need update mthly amt
				 //cout << " new mth limit: " << profileA.getMonthlyLimit() << " + " << amt << "=" << newMthlyLimit << endl;
	    		 	 	updateUserLimit(profileA.getPhone(), newDailyLimit, newDailySuccessCount, newMthlyLimit, profileA.getStatementDate(), mySQLConn.myData, logger);
				}
				else{//*** POSPTPAID
					float newDailyLimit = profileA.getDailyLimit(); //*** bug fixing, postpaid no need update
                                        short int newDailySuccessCount = profileA.getSuccessCount(); //*** bug fixing postpaid no need update
					float newMthlyLimit = profileA.getMonthlyLimit()+amt; // **bugfixing, mthly need to update
					logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [POSTPAID registered auto - update balance]");
					updateUserLimit(profileA.getPhone(), newDailyLimit, newDailySuccessCount, newMthlyLimit, profileA.getStatementDate(), mySQLConn.myData, logger);
				}
			 	 //updateUserLimit(profileA.getPhone(), 0, 0, mySQLConn.myData, logger); //for stress test only

			     float newDailyReceived = profileB.getDailyReceived() + amt;
			     updateReceivedLimit(profileB, newDailyReceived, mySQLConn.myData, logger, tx_id);

        		 //send sms to a number
			 if (profileA.getType()==0)
		    		 smsOut1 = find_replace(mtLibrary.getMsg(51, profileA.getLang()),"<bno>",profileB.getPhone());
			 else
				 smsOut1 = find_replace(mtLibrary.getMsg(300, profileA.getLang()),"<bno>",profileB.getPhone());
        		 smsOut1 = find_replace(smsOut1,"<amt>",intToString(amt));
     			 smsOut1 = find_replace(smsOut1,"<bal>",floatToString2d(newBalanceA));
     			 smsOut1 = find_replace(smsOut1,"<dat>",currentUserDate());
     			 smsOut1 = find_replace(smsOut1,"<caj>",intToString(int(svc_chg_a * 100)));
     			 if (aExpiryDate.length()>6) {
     				 expA = aExpiryDate.substr(8,2)+"/"+aExpiryDate.substr(5,2)+"/"+aExpiryDate.substr(0,4);
     			 }
     			 smsOut1 = find_replace(smsOut1,"<exp>",expA);
     			 MTMsgOut.smsMT[1].msg = smsOut1;
     			 MTMsgOut.smsMT[1].receiver = profileA.getPhone();
     			 MTMsgOut.smsMT[1].oadc = bizrules.ats_pfx + profileA.getPhone().substr(1,3) + bizrules.ATSoadc[0];

				 string smsOut4 = mtLibrary.getMsg(154, profileA.getLang());
				 smsOut4 = find_replace(smsOut4,"<pnt>",intToString(profileA.getLoyaltyPoint()+1));
				 MTMsgOut.smsMT[5].msg = smsOut4;
				 MTMsgOut.smsMT[5].receiver = profileA.getPhone();
				 MTMsgOut.smsMT[5].oadc = bizrules.ats_pfx + profileA.getPhone().substr(1,3) + bizrules.ATSoadc[0];

     			 //send sms to b number
     			 smsOut2 = find_replace(mtLibrary.getMsg(52, profileB.getLang()),"<ano>",profileA.getPhone());
     			 smsOut2 = find_replace(smsOut2,"<amt>",intToString(amt));
     			 smsOut2 = find_replace(smsOut2,"<bal>",floatToString2d(newBalanceB));
     			 smsOut2 = find_replace(smsOut2,"<dat>",currentUserDate());
     			 smsOut2 = find_replace(smsOut2,"<caj>",intToString(int(svc_chg_b * 100)));
     			 if (bExpiryDate.length()>6) {
     				 expB = bExpiryDate.substr(8,2)+"/"+bExpiryDate.substr(5,2)+"/"+bExpiryDate.substr(0,4);
     			 }
     		     smsOut2 = find_replace(smsOut2,"<exp>",expB);
     			 MTMsgOut.smsMT[2].msg = smsOut2;
     			 MTMsgOut.smsMT[2].receiver = profileB.getPhone();
     			 MTMsgOut.smsMT[2].oadc = bizrules.ats_pfx + profileB.getPhone().substr(1,3) + bizrules.ATSoadc[0];

     			 if (profileA.getAppStatus()!=2) {
     				 //send general msg for unregistered user A number
				if(profileA.getType()!=1){ //**postpaid wont get general msg
	     				 MTMsgOut.smsMT[3].msg = mtLibrary.getMsg(153, profileA.getLang());
		 			 MTMsgOut.smsMT[3].receiver = profileA.getPhone();
		 			 MTMsgOut.smsMT[3].oadc = bizrules.ats_pfx + a_phone.substr(1,3) + bizrules.ATSoadc[0];
				}
     			 }

     			 if (profileB.getAppStatus()!=2) {
     				 //send general msg for unregistered user B number
				if(profileB.getType()!=1){ //**postpaid wont get general msg
     				 	MTMsgOut.smsMT[4].msg = mtLibrary.getMsg(153, profileB.getLang());
		 			 MTMsgOut.smsMT[4].receiver = profileB.getPhone();
		 			 MTMsgOut.smsMT[4].oadc = bizrules.ats_pfx + b_phone.substr(1,3) + bizrules.ATSoadc[0];
				}
     			 }

     			 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|0|"+currentDateTime3()+"|"+tx_id);

     			 //write successfull transfer to ats_success dir
     			 string logSuccessTx = mtLibrary.dir_ats_success+"/"+tx_id+"."+profileA.getPhone()+"."+profileB.getPhone()+"."+intToString(amt);
     			 FileLogger successTx (logSuccessTx);
     			 successTx.createLog();

			     //write success cat into ussd cdr
			     ussdCdr.logData(a_phone.substr(1)+ "|"+b_phone.substr(1)+"|"+intToString(amt)+"|"+currentDateLimit2()+"|"+tx_id);
     			 return MTMsgOut;
        	 } else if (transferStatus==4) {
        		 //error while doing debit, need to refund manually
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error from IN while doing debit & credit error code from IN:4]");
        		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|9|"+currentDateTime3()+"|"+tx_id);
         		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(9, profileA.getLang());
         		 return MTMsgOut;
        	 } else if (transferStatus==3) {
        		 //refund successfull
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error from IN while doing debit & credit error code from IN:"+intToString(transferStatus)+"]");
        		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|11|"+currentDateTime3()+"|"+tx_id);
        		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(11, profileA.getLang());
        		 return MTMsgOut;
        	 } else if (transferStatus==5) {
        		 //error 99,100
        		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error from IN while doing debit & credit error code from IN:"+intToString(transferStatus)+"]");
          		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|15|"+currentDateTime3()+"|"+tx_id);
          		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(15, profileA.getLang());
          		 return MTMsgOut;
        	 } else if (transferStatus==6) {
				 //error while doing credit, need to refund manually
				 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error from IN while doing debit & credit error code from IN:6]");
				 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|19|"+currentDateTime3()+"|"+tx_id);
				 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(19, profileA.getLang());
         		 return MTMsgOut;
        	 } else {
        		 //error
         		 logger.logDebug("[TX ID:"+tx_id+"] [Transfer.confirmTransfer] [Error from IN while doing debit & credit error code from IN:"+intToString(transferStatus)+"]");
         		 txLogger.logData(a_phone+ "|"+b_phone+"|"+intToString(profileA.getType())+"|"+intToString(profileB.getType())+"|"+intToString(profileA.getBrand())+"|"+intToString(profileB.getBrand())+"|"+intToString(amt)+"|"+intToString(operation_type)+"|13|"+currentDateTime3()+"|"+tx_id);
         		 MTMsgOut.smsMT[0].msg = mtLibrary.getMsg(13, profileA.getLang());
         		 return MTMsgOut;
        	 }

          }
 }

