#ifndef MTMESSAGE_H_
#define MTMESSAGE_H_

#include <string>

using namespace std;

namespace ats {
	class MTMessage {
    	public:
    		string msg_e[500];
    		string msg_m[500];
    		string dir_log;
    		string dir_error;
    		string dir_ats_tx;
    		string dir_atr_tx;
    		string dir_ats_success;
    		string dir_atr_success;
    		string dir_atr_fail;
    		string dir_atr_pending;
    		string dir_ats_pending;
    		string dir_ussd_tx;
    		string dir_hlp_tx;
    		bool ReadMessage();
    		string getMsg(int, short int);
    		string getMsg(int, short int, short int);
	};
}

#endif /*MTMESSAGE_H_*/

