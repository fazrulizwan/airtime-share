/************************************************************************
	TXID Class Implementation
	By Fazrul Izwan Hassan

************************************************************************/

#include <iostream>
#include <mysql.h>
#include <time.h>
#include <sstream>

#include "txid.h"
#include "myfunction.h"

using namespace std;

//convert string to constant char
        char * TXID::stringToChar(string strValue) {
                char *charVal;
                charVal = new char[strValue.length()+1];
                strcpy(charVal,strValue.c_str());
                return charVal;
        }

string TXID::IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

string TXID::getHexStr(int no){

        string hexstr=intToHexStr(no);
        int iter=6-hexstr.length();
        string retval;

        for(int i=0;i<iter;i++)
                retval=retval+"0";

        return retval+hexstr;
}



string TXID::intToHexStr(int n){

        char buff[10];
        sprintf(buff,"%X",n);
        string retval;
        //FileLogger log("SMPPConn.log");

        if(strlen(buff)==1){
                buff[9]='\0';
                buff[8]=buff[8];
                buff[7]=buff[7];
                buff[6]=buff[6];
                buff[5]=buff[5];
                buff[4]=buff[4];
                buff[3]=buff[3];
                buff[2]=buff[2];
                buff[1]=buff[1];
                buff[0]=buff[0];
        }

        //cout << "Buffer:::"<<buff<<endl;
        retval=buffToStr(buff);
        //log.logDebug("input:"+IntToString(n)+" retval:"+retval);
        //log.logDebug("Debughere:"+retval.substr(retval.length()-2,2));
        return retval.substr(0,9);

}


string TXID::buffToStr(char *buff){

        int len,i;
        string retval;

        len=strlen(buff)+1;

        for(i=0;i<len-1;i++){
                retval=retval+buff[i];
        }

        return retval;
}


string TXID::currentTimestamp(void) {
                char timestamp[20];
                string currentdatetime;
                time_t mytime;
                struct tm *mytm;
                mytime=time(NULL);
                mytm=localtime(&mytime);

                strftime(timestamp,sizeof timestamp,"%Y%m%d",mytm);

                currentdatetime = (string)timestamp;
                return currentdatetime;
}

string TXID::trailZero(int val){

	string retval="";

	if(val<10)
		return "00000"+IntToString(val);
	else if(val<100)
		return "0000"+IntToString(val);
	else if(val<1000)
		return "000"+IntToString(val);
	else if(val<10000)
		return "00"+IntToString(val);
	else if(val<100000)
		return "0"+IntToString(val);
	else
		return IntToString(val);
	

}

TXID::TXID(string db_ip_addrs,int db_port,string db_name,string db_usr_name,string db_pwd){

	m_db_ip_addrs=db_ip_addrs;
	m_db_port=db_port;
	m_db_name=db_name;
	m_db_usr_name=db_usr_name;
	m_db_pwd=db_pwd;

}


string TXID::getTXID(void){

	MYSQL *conn;
        MYSQL_RES *res_set;
        MYSQL_ROW row;
	char *txid;
	string sql;
	string cmd="/home/cat/txidreset/resettxid "+m_db_ip_addrs+" 3306 ats root admin32 1";

	conn = mysql_init(NULL);
        mysql_real_connect(conn,m_db_ip_addrs.c_str(),m_db_usr_name.c_str(),m_db_pwd.c_str(),m_db_name.c_str(),0,NULL,0);

	//mysql_autocommit(conn, 0);

	//sql="call getNextTXID(@txid)";
        //char *csql = stringToChar(sql);
	//mysql_query(conn,csql);

	//sql="select @txid";
	sql="select getNextTXID(0) as txid ";
	char *csql2 = stringToChar(sql);
	mysql_query(conn,csql2);

        res_set = mysql_store_result(conn);
	
        unsigned int numrows = mysql_num_rows(res_set);
        //cout << "numrow:" << numrows << endl;

        while ((row = mysql_fetch_row(res_set)) != NULL){
      		  txid=row[0];
      	 	  //cout << "txid:" << atoi(txid) << endl;
		  //cout << "stamp:" << currentTimestamp() << endl;
        }

	if(atol(txid)==15000000){
		system(cmd.c_str());	
		sleep(1);
	}

	mysql_close(conn);
	return currentTimestamp()+getHexStr(atoi(txid));

}



