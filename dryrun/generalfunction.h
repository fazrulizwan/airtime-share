#include <iostream>
#include <vector>
#include <mysql.h>

#include "Subscriber.h"
#include "MTMessage.h"
#include "FileLogger.h"

using namespace std;

namespace ats {

	//---------check phone no format---------------
	bool chkPhoneFormat(string);

	//---------check password format--------------
	bool chkPinFormat(string);

	//---------check password is numeric
	bool chkPinFormatIsNumeric(string);

	//---------check transfer amount--------------
	bool chkAmtFormat(string);

	//---------to tokinize string--------------
	vector<string> Tokenize(const string, const string );

	//---------check if user profile exist in database--------------
	//(phone,mysql,error_dir)
	short int chkProfile(string, MYSQL *,const FileLogger&);

	//get user status from DB
	short int getUserStatus(string, MYSQL *,const FileLogger&);

	//--------to stamp current timestamp (Y-m-d H:i:s.ms)-------------------
	string currentTimestamp();

	//--------to stamp current date (Y-m-d)-------------------
	string currentDate();

	//--------to stamp current date (d/m/Y)-------------------
	string currentUserDate();

	//to stamp current month to check limit (Ymd)
	string currentMonthLimit();

	//to stamp current week to check limit (Ymw)
	string currentWeekLimit();

	//to stamp current date to check limit (Ymd)
	string currentDateLimit();

        //to stamp current date to check limit (ymd)
        string currentDateLimit2();

	//to stamp current date time (YmdHis)
	string currentDateTime2();

	//to stamp current date time (Y-m-d H:i:s)
	string currentDateTime3();

	//convert string to upper case
	string StringToUpper(string);

	//convert string to lower case
	string StringToLower(string);

	//pre-register user into db userprofile
	short int preRegisterUserProfile(Subscriber, string, MYSQL *,const FileLogger&);

	//pre-register user into db userprofile POSTPAID
        short int preRegisterUserProfilePostpaid(Subscriber, string, MYSQL *,const FileLogger&);

	//register user into db userprofile
	short int registerUserProfile(Subscriber, string, MYSQL *,const FileLogger&);

	//update user in userprofile
	short int updateUserProfile(Subscriber, string, MYSQL *,const FileLogger&);

	//update user limit
	short int updateUserLimit(string,float,short int,float,string,MYSQL*,const FileLogger&);

	//convert string to int
	int stringToInt(string);

	//convert string to const char
	char const stringToConstChar(string);

	//convert int to string
	string intToString(int);

	//convert float to string
	string floatToString(float);

	//convert float to string in 2 floating point
	string floatToString2d(float);

	//convert char to string
	string charToString(char*);

	//search file in directory
	short int searchFile(string, string);

	//search pending file
	short int searchPendingFile(string,string,string&);

	//search successfull received amount
	int sumAmountReceived(string, string);

	//search successfull transfered amount for current receiver
	int sumAmountTransfered(string, string, string);

	//check if file is exist
	short int isFileExist(string);

	//get user profile from DB
	short int getProfileDB(Subscriber&,MYSQL *,const FileLogger&);

	//convert string to float
	float stringToFloat(string);

	//convert string to char
	char *stringToChar(string);

	//trim string
	void trimString(string&);

	//find and replace string
	string find_replace(string,string,string);

	//find and replace string 'Celcom'
	string find_replace_celcom(string,string,string);

	//calculate time different in second within same day
	int TimeDiffSec(string,string);

	//calculate time different in second different day
	int TimeDiffSecDifDay(string,string);

	//convert time to int second
	int tmToInt(string);

	//check current password (phone,pin,mysql,error_dir)
	short int chkUserPwd(string,string,MYSQL *, const FileLogger&);

	//change user password
	short int changeUserPwd(string,string,MYSQL *, const FileLogger&);

	//get user password (phone, pwd, Mysql object,error_dir)
	short int getUserPwd(string, string&, MYSQL*, const FileLogger&);

	//delete pending file
	short int deleteFile(string);

	//rename pending tx file
	short int renameFile(string, string);

	//find user network based on range (phone)
	short int findUserNetwork(string);

	//create failed tx
	short int createFailedLog(string);

	//check total failed transaction (string directoryName, string b_no, string a_no, int& totalAll, int& totalDonor)
	short int checkFailedATRTx(string, string, string, int&, int&);

	//check user language & whitelist status from profile in DB
	string getUserLangDB(string, MYSQL *, const FileLogger&);

	//update counter day
	short int updateDayCounter(string, short int, MYSQL *, const FileLogger&);

	//get mother name
	short int getUserMotherName(string, string, MYSQL *,const FileLogger&);

	//check sms syntax
	bool checkSyntax(string sms);
	
	//update counter for zero balance for unregistered user
	short int updateDayCounterUnreg(Subscriber, string, MYSQL*, const FileLogger&);
	
	//search pending request from pending CAR pending table
	short int searchPendingCAR(string, string, MYSQL*, const FileLogger&, int&, string&, string&, int&, string&);
	
	//delete pending request from CAR pending table
	short int deletePendingCAR(string,string, MYSQL *, const FileLogger&);
	
	//create pending request 
	short int createPendingCAR(string, string, short int, string, string, int, MYSQL *, const FileLogger&, short int, string);

	//search pending request from pending CAT pending table
	short int searchPendingCAT(string, string, MYSQL*, const FileLogger&, int&, string&, string&, int&);

	//delete pending request from CAT pending table
	short int deletePendingCAT(string,string, MYSQL *, const FileLogger&);

	//create pending request
	short int createPendingCAT(string, string, short int, string, string, int, MYSQL *, const FileLogger&, short int);
	
	//update receiver limit
	short int updateReceivedLimit(Subscriber, float, MYSQL *, const FileLogger&, string);
	
	//check abuse counter
	short int checkAbuseCounter(string phone, string donor, MYSQL *myData, const FileLogger&, short int option);

	//check whether vip given is allowed. return true if allowed, false no allowed
	bool checkVIP(int vipcode);
}

