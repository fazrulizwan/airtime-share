#ifndef UCPCONN_H_
#define UCPCONN_H_


#include <iostream>

#include "UCPCoder.h"
#include "CoreProcess.h"

using namespace std;
using namespace ats;

typedef struct MO{
	int seq_no;
	string oadc;
	string adc;
	string msg;
};

class UCPConn{

	public:
	UCPConn(string,string,string,string,string);
	UCPConn(void);
	void Conn(void);
	int sendMT(int,struct MTStruct);
	std::string IntToString(int);

	private:
	struct MTStruct translateMT(MT_tau,int);
	int m_max_process;
	std::string m_mp_conf_file;
	std::string m_bal_conf_file;
	std::string m_prepaid_conf_file;
	std::string m_ucp_conf_file;
	std::string m_postconn_conf_file;
	string buffToStr(char *buff);
	string IntToStr(int no);
	string trim2zero(string num);
	void initMT(struct MTStruct &,int);
	//int SendMT15(int platform,string txid,string adc,string oadc,string msg);
	int SendMTCharge(int platform,string txid,string adc,string oadc,string msg, int charge);
        
	string currentTimestamp(void);
	int getChildCount(string iid);
};

#endif
