#ifndef MPCODER_H_
#define MPCODER_H_

#include <iostream>

using namespace std;

//**** Constants
const short int ACK=0;
const short int NACK=-1;
const short int EXT_FAIL=-2;

typedef struct MP_Usr_Profile_Struct{
	string msisdn;
	float balance;
	string expiry_date; //*** ddmmyyyy
	int status; //*** 1-Pre-registered 2-active 4-terminated
	int platform; //*** 0:NEC 1:Logica -9:postpaid
	int language; //*** 1:BM 2:English 3:Mandarin
	int brand; //*** 1 – Max 2 – Xceed 3 – Xplore 4 – InTm 5 – TA 6 – Max 7 – Mid 8 - Lite
	string activation_date;
	int inp; //*** -9 – Postpaid, 0 – NEC, 1 – 019 1st Quad, 2 – TA 1st Quad, 3 – TA 2nd Quad ( N Class ), 4 – 019 2nd Quad ( K Class).
}MP_Usr_Profile;

typedef struct MP_Transfer_Status_Struct{
	int transfer_status;	//*** 1:success -1:failure (reason unknown)
	float new_balance;
	string new_expiry_date;
}MP_Transfer_Status;

class MPCoder{

	public:
	string getQueryPacket(string msisdn);
	string getDebitPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string);
	string getCreditPacket(string msisdn,float amount,int validity,string txid,int pform,string b_no,string);
	MP_Transfer_Status LoadTransferStatus(string packet);
	int getReturnStatus(string packet);
	MP_Usr_Profile LoadUsrProf(string packet);
	string getVerifyPacket(string,string,int);
	string getRefundPacket(string a_no,string txid,float amt,string tx_type,string b_no);
	int getVerifyReturnStatus(string);
	int getRefundReturnStatus(string);

	private:
	int StrToInt(string inval);
	float StrToFloat(string inval);
	string floatToStr(float);
	string IntToString(int);
	string buffToStr(char *buff);
	string trim(string& s);
	string floatToString2d(float floatVal);
};

#endif
