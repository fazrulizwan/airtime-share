#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "MP_Config.h"

using namespace std;

	MPConfig_t MPConfig::ReadConfig(string conf_file) {
		string line;
		string name;
		string value;
		int posEqual;
		MPConfig_t inconf;
		ifstream fileConfig (conf_file.c_str());

		//**** Init of conf struct
		inconf.mp_conn_prof_count=-1;		
		for(int i=0;i<200;i++){
		        inconf.mp_ip_addrs[i]="";
		        inconf.mp_port[i]=-1;
        }		
		inconf.max_retry_count=-1;
		inconf.retry_delay=-1;
				
		if (fileConfig.is_open()) {
			while (!fileConfig.eof()) {
				getline (fileConfig,line);
				if (!line.length()) continue;
				if (line[0]=='#') continue;
				if (line[0]==';') continue;
				
				posEqual=line.find('=');
				name = line.substr(0,posEqual);
				trim(name);
				value = line.substr(posEqual+1);
				trim(value);
				//cout << "[Name:" << name << "=" << value << "]";
				
				if (name=="mp_conn_prof_count") {
					inconf.mp_conn_prof_count=atoi(value.c_str());
					continue;
				}
                else if(name=="ip_addrs1"){
					inconf.mp_ip_addrs[0]=value;
					continue;
				}
                else if(name=="ip_addrs2"){
					inconf.mp_ip_addrs[1]=value;
					continue;
				}
                else if(name=="ip_addrs3"){
					inconf.mp_ip_addrs[2]=value;
					continue;
				}
		else if(name=="ip_addrs4"){
                                        inconf.mp_ip_addrs[3]=value;
                                        continue;
                                }
		else if(name=="ip_addrs5"){
                                        inconf.mp_ip_addrs[4]=value;
                                        continue;
                                }
		else if(name=="ip_addrs6"){
                                        inconf.mp_ip_addrs[5]=value;
                                        continue;
                                }
		else if(name=="ip_addrs7"){
                                        inconf.mp_ip_addrs[6]=value;
                                        continue;
                                }
		else if(name=="ip_addrs8"){
                                        inconf.mp_ip_addrs[7]=value;
                                        continue;
                                }
		 else if(name=="ip_addrs9"){
                                        inconf.mp_ip_addrs[8]=value;
                                        continue;
                                }
		 else if(name=="ip_addrs10"){
                                        inconf.mp_ip_addrs[9]=value;
                                        continue;
                                }
		 else if(name=="ip_addrs11"){
                                        inconf.mp_ip_addrs[10]=value;
                                        continue;
                                }


                else if(name=="mp_port1"){
					inconf.mp_port[0]=atoi(value.c_str());
					continue;
				}	
                else if(name=="mp_port2"){
					inconf.mp_port[1]=atoi(value.c_str());
					continue;
				}
                else if(name=="mp_port3"){
					inconf.mp_port[2]=atoi(value.c_str());
					continue;
				}		
		else if(name=="mp_port4"){
                                        inconf.mp_port[3]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port5"){
                                        inconf.mp_port[4]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port6"){
                                        inconf.mp_port[5]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port7"){
                                        inconf.mp_port[6]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port8"){
                                        inconf.mp_port[7]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port9"){
                                        inconf.mp_port[8]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port10"){
                                        inconf.mp_port[9]=atoi(value.c_str());
                                        continue;
                                }
		else if(name=="mp_port11"){
                                        inconf.mp_port[10]=atoi(value.c_str());
                                        continue;
                                }


			

				else if(name=="max_retry_count"){
					inconf.max_retry_count=atoi(value.c_str());
					continue;
				}
				else if(name=="retry_delay"){
					inconf.retry_delay=atol(value.c_str());
					continue;
				}
				else if(name=="in_resp_timeout"){
					inconf.in_resp_timeout=atoi(value.c_str());
				}
				else if(name=="log_path"){
					inconf.log_path=value;
				}

				
			}
			//cout << "Finish reading config file!";
			fileConfig.close();
			inconf.load_status=1;
			return inconf;
		} else {
			//cout << "Cannot open config file!";
			return inconf;
		}
	}


void MPConfig::trim(string& str)
{
        string::size_type pos1 = str.find_first_not_of(' ');
        string::size_type pos2 = str.find_last_not_of(' ');
        str = str.substr(pos1 == string::npos ? 0 : pos1,
        pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

